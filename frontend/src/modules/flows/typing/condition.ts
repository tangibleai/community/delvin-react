import { Node } from "reactflow";

export const ConditionTypes = {
  TAG_DETECTED: "tag_detected",
  ELSE: "else",
} as const;

type ConditionType = (typeof ConditionTypes)[keyof typeof ConditionTypes];

export interface Condition {
  type: ConditionType;
  properties: {
    tag_name: string;
    tag_id: string;
  };
  attachedNodeId: string;
}
