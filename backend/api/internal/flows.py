from rest_framework import generics, serializers
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import Project, Flow

from flows.flow_graph_builder import build_flow_graph


class FlowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flow
        fields = "__all__"


class FlowListSerializer(serializers.Serializer):
    project_id = serializers.UUIDField(required=True)


class FlowListView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = FlowSerializer

    def get_queryset(self):
        serializer = FlowListSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        project_id = validated_data.get("project_id")
        project = Project.objects.get(id=project_id)

        flows = Flow.objects.filter(project=project)

        return flows


flows_list_view = FlowListView.as_view()


class CreateFlowSerializer(serializers.Serializer):
    project_id = serializers.UUIDField(required=True)
    name = serializers.CharField(required=True)
    description = serializers.CharField(required=False, allow_blank=True)
    assistant_role = serializers.CharField(required=False, allow_blank=True)


class FlowGraphSerializer(serializers.Serializer):
    nodes = serializers.ListField
    edges = serializers.ListField


class CreateFlowView(generics.CreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = FlowSerializer

    def post(self, request):
        serializer = CreateFlowSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        project_id = validated_data.get("project_id")
        project = Project.objects.get(id=project_id)
        name = validated_data.get("name")
        description = validated_data.get("description")
        assistant_role = validated_data.get("assistant_role")

        flow = Flow.objects.create(
            project=project,
            name=name,
            description=description,
            assistant_role=assistant_role,
        )

        response_serializer = self.serializer_class(flow)
        serialized_flow = response_serializer.data

        return Response(serialized_flow, status=status.HTTP_201_CREATED)


create_flow_view = CreateFlowView.as_view()


class DeleteFlowView(generics.DestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Flow.objects.all()
    serializer_class = FlowSerializer


delete_flow_view = DeleteFlowView.as_view()



class SaveFlowGraphView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializers = FlowGraphSerializer

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        id = self.kwargs.get("id")
        flow = Flow.objects.filter(id=id).first()
        nodes_raw = data.get("nodes", [])
        edges_raw = data.get("edges", [])

        flow_graph = build_flow_graph(nodes_raw, edges_raw)

        flow.ui = {"nodes": nodes_raw, "edges": edges_raw}

        flow.flow_graph = flow_graph.dict()
        flow.save()

        serializer = FlowSerializer(flow)

        return Response(serializer.data, status=status.HTTP_200_OK)


save_flow_graph_view = SaveFlowGraphView.as_view()


class FlowDetailsView(generics.RetrieveAPIView):
    """Returns the detail of the Flow with a given ID"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = FlowSerializer
    queryset = Flow.objects.all()
    lookup_field = "id"


flow_details_view = FlowDetailsView.as_view()
