import axiosInstance from "@/api/axios";
import { processError } from "./utils";

const updateUser = async (
  userId: string,
  options: { name?: string; newCurrentWorkspaceId?: string }
) => {
  const body: Record<string, any> = {};

  if (options.newCurrentWorkspaceId) {
    body.current_workspace = options.newCurrentWorkspaceId;
  }

  if (options.name) {
    body.name = options.name;
  }
  try {
    const response = await axiosInstance.put(
      `/api/users/${userId}/update/`,
      body
    );
    const updatedUser = response.data;

    return updatedUser;
  } catch (error: any) {
    processError(`Failed to update user. Details: ${error}`);
  }
};

const deleteUser = async (userId: string) => {
  try {
    const response = await axiosInstance.delete(`/api/user/${userId}/delete/`);
    return response;
  } catch (error: any) {
    const errorDetail = error.response.data.detail;
    console.log("err", errorDetail);
    processError(`Failed to delete user. Details: ${errorDetail}`);
    throw error;
  }
};

export { updateUser, deleteUser };
