import emptyApi from "../apiSlice";
import { Provider } from "@/typings/provider";

interface CreateProviderRequestBody {
  label: string;
  type: string;
  api_key: string;
  workspace: string;
}

const providersApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    fetchProviders: builder.query<Provider[], string>({
      query: (workspaceId: string) => ({
        url: "providers/",
        params: { workspace_id: workspaceId },
        method: "GET",
      }),
      providesTags: (result = [], error, arg) => [
        "Provider",
        ...result.map(({ id }) => ({ type: "Provider", id }) as const),
      ],
    }),
    createProvider: builder.mutation<Provider, CreateProviderRequestBody>({
      query: (providerData) => ({
        url: "providers/create/",
        method: "POST",
        data: {
          label: providerData.label,
          type: providerData.type,
          api_key: providerData.api_key,
          workspace: providerData.workspace,
        },
      }),
      invalidatesTags: ["Provider"],
    }),
    deleteProvider: builder.mutation<void, string>({
      query: (providerId: string) => ({
        url: `providers/${providerId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: (result, error, arg) => [{ type: "Provider", id: arg }],
    }),
  }),
});

export const {
  useFetchProvidersQuery,
  useCreateProviderMutation,
  useDeleteProviderMutation,
} = providersApiSlice;

export default providersApiSlice;
