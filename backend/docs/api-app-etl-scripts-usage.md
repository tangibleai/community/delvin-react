"""Explanation of how to use `json_to_csv.py` and `csv_to_json.py` scripts inside `backend/api/scripts/etl` folder"""

1. Run `python manage.py dumpdata` command on desired model to create json. For e.g.:
   python manage.py dumpdata api.Message > message.json
2. Move json file to `backend/api/scripts/etl` folder
3. Call `json_to_csv.py` script putting message.json as first argument, and arbitrary name ending with .csv for second one (for e.g. message.csv)
4. Having generated csv you can modify it (don't touch `model` and `id` fields)
5. After modifications, call `csv_to_json.py` to generate json file back (name it for .e.g `updated_message.json`). This is accepted by django json
6. Run `python manage.py loaddata updated_message.json` to create/update records in db
