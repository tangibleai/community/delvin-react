import { MessageDirection } from "./message";

export interface ChatMessageType {
  channel_type: string;
  chat: string;
  contact: string;
  direction: MessageDirection;
  id: string;
  project: string;
  sender_type: string;
  text: string;
  timestamp: string;
  display_timestamp_before_message: boolean;
}
