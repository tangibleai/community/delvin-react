import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";

import { IoReload } from "react-icons/io5";
import { useToast } from "@/components/ui/use-toast";
import { ChatbotConfig } from "@/typings/chatbot_config";
import {
  useDeleteChatbotMutation,
  useListChatbotsQuery,
} from "@/slices/api/chatbotsApiSlice";
import { useDeleteFlowMutation } from "@/slices/api/flowApiSlice";
import { useParams } from "react-router-dom";

type Props = {
  selectedChatbot: ChatbotConfig;
};

function DeleteChatbotModal({ selectedChatbot }: Props) {
  const { toast } = useToast();
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const { isLoading: retrievingChatbots } = useListChatbotsQuery({
    project_id: projectId,
  });
  const [deleteChatbot] = useDeleteChatbotMutation();
  const [deleteFlow] = useDeleteFlowMutation();

  useEffect(() => {
    // Handles resetting the form after submit or x
    // Can't directly access x in Dialog
    if (!isModalOpen) {
      form.reset();
    }
    setError("");
    setLoading(false);
  }, [isModalOpen]);

  async function onAssistantDelete() {
    setLoading(true);
    setError("");

    const deletedFlowId = deleteFlowIfGuidedChatbotOrSetError();
    if (!deletedFlowId) {
      setLoading(false);
      return;
    }

    const deletedChatbotId = deleteChatbotOrSetError();
    if (!deletedChatbotId) {
      setLoading(false);
      return;
    }

    setIsModalOpen(false);
    setLoading(false);
  }

  const deleteFlowIfGuidedChatbotOrSetError = async () => {
    try {
      const flowId = selectedChatbot.config.flow;
      await deleteFlow(flowId).unwrap();
      return flowId;
    } catch (error) {
      setError(
        "An error occurred while deleting the chatbot. Please try again."
      );
    }

    return "";
  };

  const deleteChatbotOrSetError = async () => {
    try {
      await deleteChatbot(selectedChatbot.id).unwrap();
      toast({
        title: "Success!",
        description: "Assistant was deleted successfully.",
      });
      return selectedChatbot.id;
    } catch (err) {
      setError(
        "An error occurred while deleting the chatbot. Please try again."
      );
    }

    return "";
  };

  const confirmationString = `delete ${selectedChatbot?.origin}`;

  const formSchema = z.object({
    userDeleteConfirmation: z
      .string()
      .min(8, {
        message: "Please type the confirmation phrase",
      })
      .refine((val) => val.includes(`${confirmationString}`), {
        message: `The confirmation phrase must be "${confirmationString}"`,
      })
      .refine((val) => !/\s+$/.test(val), {
        message: "The confirmation phrase should not end with spaces.",
      }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      userDeleteConfirmation: "",
    },
  });

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button
          variant="destructive"
          className="w-fit"
          disabled={retrievingChatbots}
        >
          Delete assistant
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-md w-fit">
        <DialogHeader>
          <DialogTitle>Delete assistant</DialogTitle>
        </DialogHeader>
        {error && (
          <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
            Error: {error}
          </p>
        )}
        <p>This action cannot be undone.</p>

        <p>
          All data associated with this chatbot will be permanently deleted and
          unrecoverable.
        </p>

        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onAssistantDelete)}
            className="space-y-8"
          >
            <FormField
              control={form.control}
              name="userDeleteConfirmation"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>
                    Type <pre className="inline">{confirmationString}</pre> to
                    confirm.
                  </FormLabel>
                  <FormControl>
                    <Input placeholder={confirmationString} {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <DialogFooter>
              <Button
                disabled={loading}
                type="submit"
                variant="destructive"
                className="order-2 sm:order-1"
              >
                {loading && (
                  <IoReload className={"mr-2 h-4 w-4 animate-spin"} />
                )}
                {loading ? "Deleting..." : "Delete assistant"}
              </Button>
              <Button
                type="button"
                variant="outline"
                className="order-1 sm:order-2"
                onClick={() => setIsModalOpen(false)}
              >
                Cancel
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}

export default DeleteChatbotModal;
