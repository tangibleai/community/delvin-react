import { useEffect, useState } from "react";
import { Tab, TabList, TabPanel, TabPanels, TabGroup } from "@tremor/react";
import ChatbotsPanel from "../components/chatbots_panel/ChatbotsPanel";
import ChatbotProfile from "../components/chatbot_profile/ChatbotProfile";
import { AppDispatch } from "@/store";
import { Link, useParams } from "react-router-dom";
import AISettings from "../components/AISettings";
import KnowledgeSettings from "../components/KnowledgeSettings";
import KnowledgeSources from "../components/KnowledgeSources";
import { ChatbotPreview } from "../components/ChatbotPreview";
import {
  ChatbotConfig as Chatbot,
  chatbotTypes,
} from "@/typings/chatbot_config";
import { buttonVariants } from "@/components/ui/button";
import { setFlowName } from "@/slices/flowSlice";
import { useDispatch } from "react-redux";
import { useListChatbotsQuery } from "@/slices/api/chatbotsApiSlice";

const ChatbotConfig = () => {
  const dispatch: AppDispatch = useDispatch();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const { data: chatbots = [], isLoading: retrievingChatbots } =
    useListChatbotsQuery({ project_id: projectId });
  const [selectedChatbot, setSelectedChatbot] = useState(
    chatbots?.[0] ?? ({} as Chatbot)
  );
  const [enabledKnowledge, setEnabledKnowledge] = useState<boolean>(false);

  useEffect(() => {
    const chatbotConfig = async () => {
      if (!chatbots.length) return;

      const firstChatbot = chatbots[0];
      if (!Object.keys(selectedChatbot).length)
        setSelectedChatbot(firstChatbot);
      setEnabledKnowledge(firstChatbot.config.knowledge.enabled);
    };

    chatbotConfig();
  }, [chatbots]);

  const handleUpdateEnable = (enabled_setting: boolean) => {
    // Expand this to deal with other config elements
    setEnabledKnowledge(enabled_setting);
  };

  const updateChatbot = (updatedChatbot: Chatbot) => {
    setSelectedChatbot((chatbot) => ({
      ...chatbot,
      ...updatedChatbot,
    }));
  };

  return (
    <div className="flex h-full -ml-10 overflow-y-hidden">
      <ChatbotsPanel
        selectedChatbot={selectedChatbot}
        setSelectedChatbot={setSelectedChatbot}
      />
      {chatbots.length ? (
        <div className="flex flex-col h-full overflow-hidden w-full">
          {selectedChatbot && Object.keys(selectedChatbot).length && (
            <div className="p-4 h-full overflow-y-hidden">
              <div className="head-section flex justify-between">
                <h1 className="header">{selectedChatbot.origin}</h1>
                <div>
                  <div className="flex gap-4">
                    {selectedChatbot.config.bot_type ===
                      chatbotTypes.GUIDED_CONVERSATION && (
                      <Link
                        to={`/project/${selectedChatbot.project}/flow/${selectedChatbot.config.flow}`}
                        className={buttonVariants({ variant: "default" })}
                        onClick={() =>
                          dispatch(setFlowName(selectedChatbot.origin))
                        }
                      >
                        Open in Flow Editor
                      </Link>
                    )}

                    <ChatbotPreview
                      projectId={projectId!}
                      selectedChatbot={selectedChatbot}
                    />
                  </div>
                </div>
              </div>

              <TabGroup className="flex flex-col h-full overflow-hidden">
                <TabList className="mt-8">
                  <Tab>General</Tab>
                  {!retrievingChatbots ? <Tab>AI settings</Tab> : <></>}
                  {!retrievingChatbots && enabledKnowledge ? (
                    <>
                      <Tab>Knowledge settings</Tab>
                      <Tab>Knowledge sources</Tab>
                    </>
                  ) : (
                    <></>
                  )}
                </TabList>
                <TabPanels className="flex-1 overflow-hidden">
                  <TabPanel className="text-left mt-5">
                    <ChatbotProfile selectedChatbot={selectedChatbot} />
                  </TabPanel>
                  <TabPanel className="text-left h-full overflow-y-hidden">
                    {selectedChatbot && Object.keys(selectedChatbot).length && (
                      <AISettings
                        knowledgeEnabled={enabledKnowledge}
                        selectedChatbot={selectedChatbot}
                        handleUpdateEnable={handleUpdateEnable}
                        handleUpdateConfig={updateChatbot}
                      />
                    )}
                  </TabPanel>
                  <TabPanel>
                    <KnowledgeSettings
                      selectedChatbot={selectedChatbot}
                      updateChatbot={updateChatbot}
                    />
                  </TabPanel>
                  <TabPanel>
                    <KnowledgeSources selectedChatbot={selectedChatbot} />
                  </TabPanel>
                </TabPanels>
              </TabGroup>
            </div>
          )}
        </div>
      ) : (
        <div className="flex flex-row items-center gap-2 justify-center py-2.5 rounded-2xl bg-gray-200 p-4 h-full w-full">
          <p className="text-xl">Create first chatbot</p>
        </div>
      )}
    </div>
  );
};

export default ChatbotConfig;
