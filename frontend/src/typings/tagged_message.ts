import { Message } from "./message";
import { MessageTagDefinition } from "./message_tag_definition";

export interface TaggedMessage {
  id: string;
  message: Message;
  tag: MessageTagDefinition;
  message_tags: Array<MessageTagDefinition>;
}
