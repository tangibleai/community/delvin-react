1. Change `DJANGO_DATABASE_URL` variable in .env file to match url of production db
2. Run `python manage.py dumpdata` command against each models of the application to generate json fixtures
3. Put saved fixture to fixtures folder
