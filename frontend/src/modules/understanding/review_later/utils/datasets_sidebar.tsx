import axiosInstance from "@/api/axios";

const createNewDatasetInDb = async (
  projectId: string,
  newDatasetName: string,
  // newDatasetDescription: string
) => {
  const body = JSON.stringify({
    project: projectId,
    name: newDatasetName,
    // description: newDatasetDescription,
  });
  const response = await axiosInstance.post(
    `/api/project/dataset/copy_from_default/`,
    body,
  );

  const newDataset = response.data;
  return newDataset;
};

export { createNewDatasetInDb };
