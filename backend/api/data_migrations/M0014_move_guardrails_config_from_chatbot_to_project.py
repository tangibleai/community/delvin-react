import copy

from api.models import (
    Project,
    Chatbot,
)

from core.constant import CONSTANTS

from api.utils import moderation


# forward migration function
def save_guardrails_config_in_project():
    for project in Project.objects.exclude(owner__email=CONSTANTS.DEFAULT_USER_EMAIL):
        chatbot = Chatbot.objects.filter(project=project).first()
        config = chatbot.config
        if not config.get("guardrails"):
            config["guardrails"] = {
                "builtin": {
                    "enabled": False,
                    "categories": moderation.BUILTIN_DEFAULT_CATEGORIES,
                }
            }
        guardrails_config = config["guardrails"]
        guardrails_config_copy = copy.deepcopy(guardrails_config)

        project.config = {"guardrails": guardrails_config_copy}
        project.save()


def test_forward_migration():
    """tests `save_guardrails_config_in_project` function"""
    for p in Project.objects.all():
        chatbots = Chatbot.objects.filter(project=p)
        for c in chatbots:
            assert c.config["guardrails"] == p.config["guardrails"]


# reverse migration function
def save_guardrails_config_in_chatbot():
    for project in Project.objects.all():
        chatbot = Chatbot.objects.filter(project=project).first()
        config = project.config
        guardrails_config = config["guardrails"]
        guardrails_config_copy = copy.deepcopy(guardrails_config)

        chatbot.config = {"guardrails": guardrails_config_copy}
        chatbot.save()


if __name__ == "__main__":
    save_guardrails_config_in_project()
