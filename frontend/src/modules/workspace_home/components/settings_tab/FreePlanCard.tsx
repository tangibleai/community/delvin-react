import { Button } from "@/components/ui/button";
import { Callout } from "@/components/ui/callout";
import { useToast } from "@/components/ui/use-toast";
import CONSTANTS from "@/constants";
import { useUpdatePlanMutation } from "@/slices/api/planApiSlice";
import { useFetchProvidersQuery } from "@/slices/api/providerApiSlice";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { Plan, PlanType } from "@/typings/plan";
import { Workspace } from "@/typings/workspace";
import { Card } from "@tremor/react";
import { FaLockOpen } from "react-icons/fa";
import { IoReload } from "react-icons/io5";
import { useParams } from "react-router-dom";

type Props = {
  plan: Plan;
};

export default function FreePlanCard({ plan }: Props) {
  const { toast } = useToast();

  const planProperties = plan.properties;
  var freeMessageCount = parseInt(planProperties.free_message);
  const quotaUsed = Math.floor(
    (freeMessageCount / CONSTANTS.MAX_FREE_MESSAGES) * 100
  );
  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;
  const { data: providers = [], isFetching: fetchingProviders } =
    useFetchProvidersQuery(workspaceId, {
      skip: !workspaceId,
    });
  const { data: activeWorkspace = {} as Workspace } =
    useGetWorkspaceQuery(workspaceId);

  const [updatePlan, { isLoading: updatingPlan }] = useUpdatePlanMutation();

  const handleEmailClick = () => {
    window.location.href = `mailto:${CONSTANTS.CONTACT_EMAIL}?subject=upgrade plan`;
  };

  const switchToBringYourOwnKeyPlan = async () => {
    const updatedPlan = { ...plan, type: PlanType.OWN_KEY };
    try {
      await updatePlan({
        plan: updatedPlan,
        workspace: activeWorkspace,
      }).unwrap();
      return;
    } catch (err) {
      toast({
        variant: "destructive",
        title: "Error",
        description:
          "Failed to switch workspace to Bring-Your-Own-Key plan. Please try again.",
      });
    }
  };

  return (
    <Card title="" className="max-w-lg">
      <div className="flex flex-col gap-4">
        <div className="flex items-center justify-between">
          <span className="text-2xl font-bold">Current Plan</span>
          <span className="bg-green-500 text-black font-semibold text-sm px-3 py-1 rounded-full">
            Free
          </span>
        </div>

        <div className="text-sm text-gray-600">
          Message credits used : {freeMessageCount} /{" "}
          {CONSTANTS.MAX_FREE_MESSAGES}
        </div>

        <div className="w-full bg-gray-200 rounded-full h-4">
          <div
            className="bg-blue-500 h-4 rounded-full"
            style={{ width: `${quotaUsed}%` }} // Adjust this width dynamically based on your progress
          ></div>
        </div>

        <Callout
          variant="default"
          title="You are on a free plan"
          className="w-full"
          icon={FaLockOpen}
        >
          <>
            Upgrade your plan to remove message restrictions on all projects and
            access more options!
            <br />
            <br />
            You can also add your own provider and API key in the "LLM
            Providers" tab and switch to Bring-Your-Own-Key (BYOK) plan.
          </>
        </Callout>

        <div className="flex justify-end gap-5">
          {providers.length > 1 && (
            <Button
              onClick={switchToBringYourOwnKeyPlan}
              disabled={updatingPlan || fetchingProviders}
            >
              {(updatingPlan || fetchingProviders) && (
                <IoReload className="mr-2 h-4 w-4 animate-spin" />
              )}
              Switch to BYOK
            </Button>
          )}
          <Button onClick={handleEmailClick}>Upgrade plan</Button>
        </div>
      </div>
    </Card>
  );
}
