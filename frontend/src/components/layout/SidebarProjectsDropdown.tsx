import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown";
import { cn, focusInput } from "@/lib/utils";
import { RiExpandUpDownLine } from "react-icons/ri";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/store";
import { ScrollArea } from "@/components/ui/scroll-area";
import { Link, useNavigate, useParams } from "react-router-dom";
import { PROJECT_PAGE_URLS } from "@/routes/pageUrls";
import { FaPlusCircle } from "react-icons/fa";
import { composeProjectInitials } from "./utils/util";
import { Project } from "@/typings/project";
import { useListProjectsQuery } from "@/slices/api/projectsApiSlice";

type Props = {
  isOpen: boolean;
  currentProject: Project;
};

export const ProjectsDropdownDesktop = ({ isOpen, currentProject }: Props) => {
  const routeParams = useParams();
  const dispatch = useDispatch();

  const [dropdownOpen, setDropdownOpen] = React.useState(false);
  const [hasOpenDialog, setHasOpenDialog] = React.useState(false);
  const dropdownTriggerRef = React.useRef<null | HTMLButtonElement>(null);
  const focusRef = React.useRef<null | HTMLButtonElement>(null);

  const currentProjectId = routeParams.projectId;
  const { data: projects, isLoading } = useListProjectsQuery(
    currentProject.workspace
  );

  const handleDialogItemSelect = () => {
    focusRef.current = dropdownTriggerRef.current;
  };

  const handleDialogItemOpenChange = (open: boolean) => {
    setHasOpenDialog(open);
    if (open === false) {
      setDropdownOpen(false);
    }
  };

  return (
    <>
      {/* sidebar (lg+) */}
      <DropdownMenu
        open={dropdownOpen}
        onOpenChange={setDropdownOpen}
        modal={false}
      >
        <DropdownMenuTrigger asChild>
          <button
            className={cn(
              "flex w-full items-center  rounded-md  bg-white p-2 text-sm shadow-sm transition-all hover:bg-gray-50 dark:border-gray-800 dark:bg-gray-950 hover:dark:bg-gray-900",
              isOpen ? "border border-gray-300 gap-x-2.5" : "",
              focusInput
            )}
          >
            {isOpen ? (
              <>
                <span
                  className="flex aspect-square size-8 items-center justify-center rounded bg-primary p-2 text-xs font-medium text-white dark:bg-indigo-500"
                  aria-hidden="true"
                >
                  {currentProject.name!.slice(0, 2)}
                </span>
                <div className="flex w-full items-center justify-between gap-x-4 truncate">
                  <div className="truncate">
                    <p className="truncate whitespace-nowrap text-sm font-medium text-gray-900 dark:text-gray-50">
                      {currentProject.name}
                    </p>
                  </div>
                  <RiExpandUpDownLine
                    className="size-5 shrink-0 text-gray-500"
                    aria-hidden="true"
                  />
                </div>
              </>
            ) : (
              <span
                className="flex aspect-square size-8 items-center justify-center rounded bg-primary p-2 text-xs font-medium text-white dark:bg-indigo-500"
                aria-hidden="true"
              >
                {currentProject.name!.slice(0, 2)}
              </span>
            )}
          </button>
        </DropdownMenuTrigger>
        <DropdownMenuContent
          hidden={hasOpenDialog}
          onCloseAutoFocus={(event: any) => {
            if (focusRef.current) {
              focusRef.current.focus();
              focusRef.current = null;
              event.preventDefault();
            }
          }}
        >
          <DropdownMenuGroup>
            <DropdownMenuLabel>Projects ({projects?.length})</DropdownMenuLabel>
            <ScrollArea
              key={"projects scroll area"}
              className="flex flex-col max-h-48"
            >
              {projects?.map((project) =>
                project.id === currentProjectId ? (
                  <></>
                ) : (
                  <DropdownMenuItem key={project.id}>
                    <Link
                      to={`/project/${project.id}/${PROJECT_PAGE_URLS.project.children.home}`}
                      className="flex w-full items-center gap-x-2.5"
                    >
                      <span
                        className={cn(
                          "bg-indigo-600 dark:bg-indigo-500",
                          "flex aspect-square w-[2rem] h-[2rem] items-center justify-center rounded p-2 text-xs font-medium text-white"
                        )}
                        aria-hidden="true"
                      >
                        {composeProjectInitials(project.name)}
                      </span>
                      <div>
                        <p
                          key={project.id}
                          className="text-sm font-medium text-gray-900 dark:text-gray-50"
                        >
                          {project.name}
                        </p>
                      </div>
                    </Link>
                  </DropdownMenuItem>
                )
              )}
            </ScrollArea>
            {/* <div className="w-full flex flex-col items-center text-center mt-2">
              <Link
                to={`${INIT_PAGE_URLS.assistant.path}${INIT_PAGE_URLS.assistant.children.onboarding}`}
                className="w-[90%] flex justify-center bg-slate-200 rounded py-2 hover:bg-slate-300"
              >
                <div className="flex flex-row items-center justify-center gap-2">
                  <FaPlusCircle size={"1rem"} />
                  <span>New project</span>
                </div>
              </Link>
            </div> */}
          </DropdownMenuGroup>
          <DropdownMenuSeparator />
          {/* <ModalAddWorkspace
            onSelect={handleDialogItemSelect}
            onOpenChange={handleDialogItemOpenChange}
            itemName="Add workspace"
          /> */}
        </DropdownMenuContent>
      </DropdownMenu>
    </>
  );
};
