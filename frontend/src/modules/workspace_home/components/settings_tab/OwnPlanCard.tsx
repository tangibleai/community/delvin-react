import { Button } from "@/components/ui/button";
import { Callout } from "@/components/ui/callout";
import { useToast } from "@/components/ui/use-toast";
import CONSTANTS from "@/constants";
import { useUpdatePlanMutation } from "@/slices/api/planApiSlice";
import { useFetchProvidersQuery } from "@/slices/api/providerApiSlice";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { Plan, PlanType } from "@/typings/plan";
import { Workspace } from "@/typings/workspace";
import { Card } from "@tremor/react";
import { IoReload } from "react-icons/io5";
import { useParams } from "react-router-dom";

type Props = {
  plan: Plan;
};

export default function OwnPlanCard({ plan }: Props) {
  const { toast } = useToast();

  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;
  const { data: providers = [], isFetching: fetchingProviders } =
    useFetchProvidersQuery(workspaceId, {
      skip: !workspaceId,
    });
  const { data: activeWorkspace = {} as Workspace } =
    useGetWorkspaceQuery(workspaceId);

  const [updatePlan, { isLoading: updatingPlan }] = useUpdatePlanMutation();

  const handleEmailClick = () => {
    window.location.href = `mailto:${CONSTANTS.CONTACT_EMAIL}?subject=upgrade plan`;
  };

  const switchToBringYourOwnKeyPlan = async () => {
    const updatedPlan = { ...plan, type: PlanType.OWN_KEY };
    try {
      await updatePlan({
        plan: updatedPlan,
        workspace: activeWorkspace,
      }).unwrap();
      return;
    } catch (err) {
      toast({
        variant: "destructive",
        title: "Error",
        description:
          "Failed to switch workspace to Bring-Your-Own-Key plan. Please try again.",
      });
    }
  };

  return (
    <Card title="" className="max-w-lg">
      <div className="flex flex-col gap-4">
        <div className="flex items-center justify-between">
          <span className="text-2xl font-bold">Current Plan</span>
          <span className="bg-green-500 text-black font-semibold text-sm px-3 py-1 rounded-full">
            BYOK
          </span>
        </div>

        <Callout
          variant="default"
          title='You are on "Bring-Your-Key-Plan" plan'
          className="w-full"
        ></Callout>

        <div className="flex justify-end gap-5">
          <Button onClick={handleEmailClick}>Upgrade plan</Button>
        </div>
      </div>
    </Card>
  );
}
