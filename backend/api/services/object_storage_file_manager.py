import boto3
import io
import os

from datetime import datetime as dt
from llama_index.core.readers import SimpleDirectoryReader
from llama_index.core.schema import Document
from typing import List


class ObjectStorageDocumentManager:
    def __init__(
        self,
        project_id,
        document_data={},
    ):
        self.credentials_json = {
            "endpoint_url": os.environ.get("OBJECT_STORAGE_ENDPOINT", ""),
            "region_name": os.environ.get("OBJECT_STORAGE_REGION", ""),
            "aws_access_key_id": os.environ.get("OBJECT_STORAGE_ACCESS_KEY", ""),
            "aws_secret_access_key": os.environ.get("OBJECT_STORAGE_SECRET", ""),
            "bucket_name": os.environ.get("OBJECT_STORAGE_BUCKET", ""),
            "bucket_subdir": os.environ.get("OBJECT_STORAGE_SUBDIR", ""),
        }
        self.document_id = document_data["id"]
        self.document_file_name = document_data["title"]
        self.normalized_file_name = self.document_file_name.replace(" ", "-").replace(
            "_", "-"
        )
        self.full_file_name = f"{self.document_id}-{self.normalized_file_name}"

        self.key = f"project/{project_id}/documents/{self.full_file_name}"

        self.client = self.set_client_session(self.credentials_json)

    def set_client_session(self, creds_json):
        session = boto3.session.Session()

        # Create a connection to the Space
        client = session.client(
            "s3",
            endpoint_url=creds_json["endpoint_url"],
            aws_access_key_id=creds_json["aws_access_key_id"],
            aws_secret_access_key=creds_json["aws_secret_access_key"],
        )
        return client

    def load_s3_files_as_docs(self, temp_dir=None) -> List[Document]:
        """Load file(s) from S3.

        Derived from llama_index's S3Reader class
        """
        from s3fs import S3FileSystem

        s3fs = S3FileSystem(
            key=self.credentials_json["aws_access_key_id"],
            secret=self.credentials_json["aws_secret_access_key"],
            client_kwargs={"endpoint_url": self.credentials_json["endpoint_url"]},
        )

        input_files = None
        input_files = [f"{self.credentials_json['bucket_subdir']}/{self.key}"]

        loader = SimpleDirectoryReader(
            input_files=input_files,
            # file_extractor=self.file_extractor,
            # required_exts=self.required_exts,
            filename_as_id=True,
            # num_files_limit=self.num_files_limit,
            recursive=True,
            fs=s3fs,
        )

        return loader.load_data()

    def upload_file_to_object_storage(self, file):
        """Formats and uploads a Document file to object storage"""
        file_content = file.read()
        converted_file = io.BytesIO(file_content)

        self.client.upload_fileobj(
            converted_file,
            self.credentials_json["bucket_subdir"],
            self.key,
            ExtraArgs={"ACL": "public-read"},
        )
        return self.full_file_name

    def delete_object_from_object_storage(self):
        """Deletes a file from the object storage"""
        self.client.delete_object(
            Bucket=self.credentials_json["bucket_subdir"],
            Key=self.key,
        )
