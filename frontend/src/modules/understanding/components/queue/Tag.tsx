import { Button } from "@/components/ui/button";
import { UnderstandingQueueTag } from "@/typings/understanding_queue_tag";
import { FaCheck } from "react-icons/fa6";

type Props = {
  tag: UnderstandingQueueTag;
  handleTagConfirmationToggled: (tag: UnderstandingQueueTag) => void;
};

const Tag = ({ tag, handleTagConfirmationToggled }: Props) => {
  return (
    <div className="flex items-center mb-2 justify-between">
      <div
        className={`mr-2 ${
          tag.isConfirmed && "bg-muted"
        } py-1 rounded w-44 text-center ${
          tag.isConfirmed &&
          "borger-primary border-2 border-tremor-brand rounded-xl"
        }`}
      >
        {tag.tag.name}
      </div>
      <div className="flex justify-between ml-10 items-center gap-5">
        {tag.score ? <span className="">{tag.score} %</span> : <></>}

        <Button
          onClick={() => handleTagConfirmationToggled(tag)}
          className={`ml-2 px-4 py-1 rounded w-36 ${
            tag.isConfirmed
              ? "bg-green-600 hover:bg-green-700"
              : "border-2 border-black"
          }`}
          variant={tag.isConfirmed ? "default" : "outline"}
        >
          {tag.isConfirmed ? (
            <div className="flex gap-1">
              <FaCheck size={20} />
              Confirmed
            </div>
          ) : (
            "Confirm"
          )}
        </Button>
      </div>
    </div>
  );
};

export { Tag };
