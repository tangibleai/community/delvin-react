import { ChatbotConfig } from "@/typings/chatbot_config";
import emptyApi from "../apiSlice";
import { MessageTagDefinition } from "@/typings/message_tag_definition";
import { UnderstandingQueueTag } from "@/typings/understanding_queue_tag";

interface CreateMessageTagDefinitionRequestBody {
  project_id: string;
  name: string;
  description: string;
}

const messageTagDefinitionApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    listMessageTagDefinitions: builder.query<MessageTagDefinition[], string>({
      query: (projectId: string) => ({
        url: "message-tag-definitions/",
        method: "GET",
        params: { project_id: projectId },
      }),
      providesTags: (result = [], error, arg) => [
        "MessageTagDefinition",
        ...result.map(
          ({ id }) => ({ type: "MessageTagDefinition", id }) as const
        ),
      ],
    }),
    createMessageTagDefinition: builder.mutation<
      ChatbotConfig,
      CreateMessageTagDefinitionRequestBody
    >({
      query: (requestBody) => ({
        url: "message-tag-definitions/",
        method: "POST",
        data: requestBody,
      }),
      invalidatesTags: ["MessageTagDefinition", "MLModel"],
    }),
    deleteMessageTagDefinition: builder.mutation<null, string>({
      query: (messageTagDefinitionId: string) => ({
        url: `message-tag-definition/${messageTagDefinitionId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: ["MessageTagDefinition", "MLModel", "MessageTag"],
    }),
    // additional non-CRUD endpoints
    predictTags: builder.query<
      UnderstandingQueueTag[],
      { text: string; project_id: string }
    >({
      query: (requestBody) => ({
        url: "understanding/predict-tags/",
        method: "GET",
        params: requestBody,
      }),
    }),
  }),
});

export const {
  useListMessageTagDefinitionsQuery,
  useCreateMessageTagDefinitionMutation,
  useDeleteMessageTagDefinitionMutation,
  usePredictTagsQuery,
} = messageTagDefinitionApiSlice;
