import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown";

import { BsThreeDotsVertical } from "react-icons/bs";

import { Member } from "@/typings/member";
import DeleteMemberModal from "./DeleteMemberModal";
import MakeMemberOwner from "./MakeMemberOwner";
import React from "react";

type Props = {
  targetUser: any;
  onDeleteMember: (deleteMember: Member) => void;
  setActiveTab: React.Dispatch<React.SetStateAction<string>>;
};

function UserControlOptionsForOwner({
  targetUser,
  onDeleteMember,
  setActiveTab,
}: Props) {
  const focusRef = React.useRef<null | HTMLButtonElement>(null);

  return (
    <DropdownMenu modal={false}>
      <DropdownMenuTrigger asChild>
        <Button variant="outline">
          <BsThreeDotsVertical />
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent
        className="w-40"
        onCloseAutoFocus={(event: any) => {
          if (focusRef.current) {
            focusRef.current.focus();
            focusRef.current = null;
            event.preventDefault();
          }
        }}
      >
        <DeleteMemberModal
          userToDelete={targetUser}
          onDeleteMember={onDeleteMember}
        />
        <MakeMemberOwner
          userToUpdate={targetUser}
          setActiveTab={setActiveTab}
        />
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export default UserControlOptionsForOwner;
