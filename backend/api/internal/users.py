from django.utils.translation import gettext_lazy as _

from rest_framework import serializers, generics
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.hashers import check_password

from django_ratelimit.decorators import ratelimit
from rest_framework import status
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response

from accounts import models
from accounts.serializers import SpecialUserCreateSerializer

from api import models as api_models


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserAccount
        exclude = ("password",)


class UserDetailsRequestSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)


@method_decorator(
    ratelimit(key="ip", rate="10/m", method="GET", block=False), name="dispatch"
)
class UsersListView(generics.ListAPIView):
    serializer_class = SpecialUserCreateSerializer
    authentication_classes = []
    permission_classes = []

    def get_queryset(self):
        serializer = UserDetailsRequestSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        email = validated_data.get("email")

        queryset = models.UserAccount.objects.filter(email=email)

        return queryset

    def list(self, request, *args, **kwargs):
        if getattr(request, "limited", False):
            return JsonResponse(
                {"error": "Rate limit exceeded. Please try again later."}, status=429
            )

        return super().list(request, *args, **kwargs)


users_list_view = UsersListView.as_view()


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserAccount
        exclude = ("email",)

    password = serializers.CharField(required=False)
    name = serializers.CharField(required=False)
    current_workspace = serializers.UUIDField(required=False)


class UserUpdateView(generics.UpdateAPIView):
    """
    PATCH method is forbidden.
    """

    authentication_classe = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer

    def get_queryset(self):
        queryset = models.UserAccount.objects.filter(id=self.kwargs.get("pk"))
        return queryset

    def put(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = UserUpdateSerializer(data=data)
        serializer.is_valid(raise_exception=False)

        validated_data = serializer.validated_data
        instance = self.get_object()

        name = validated_data.get("name") or instance.name
        workspace = (
            api_models.Workspace.objects.filter(
                id=validated_data.get("current_workspace")
            ).first()
            or instance.current_workspace
        )

        models.UserAccount.objects.filter(id=self.get_object().id).update(
            name=name, current_workspace=workspace
        )

        instance = self.get_object()

        serializer = self.serializer_class(instance)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)


user_update_view = UserUpdateView.as_view()


class ResetUserPasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField()
    new_password = serializers.CharField()


class ResetUserPasswordView(generics.UpdateAPIView):
    """
    PATCH method is forbidden.
    """

    authentication_classe = []
    permission_classes = []
    serializer_class = UserSerializer

    def get_queryset(self):
        queryset = models.UserAccount.objects.filter(id=self.kwargs.get("pk"))
        return queryset

    def put(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = ResetUserPasswordSerializer(data=data)
        serializer.is_valid(raise_exception=False)

        validated_data = serializer.validated_data
        instance = self.get_object()

        passed_current_password = validated_data.get("current_password")

        passwords_match = check_password(passed_current_password, instance.password)
        if not passwords_match:
            return JsonResponse(
                "Current password is wrong.",
                status=status.HTTP_400_BAD_REQUEST,
                safe=False,
            )

        new_password = validated_data.get("new_password")

        instance.set_password(new_password)
        instance.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)


reset_user_password_view = ResetUserPasswordView.as_view()


class UserDeleteView(generics.DestroyAPIView):
    """Deletes the UserAccount associated with the PK passed via the URL"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = models.UserAccount.objects.all()
    serializer_class = UserSerializer
    lookup_field = "pk"


user_delete_view = UserDeleteView.as_view()


class UserDetailsView(generics.RetrieveAPIView):
    """Retrieves details of the UserAccount associated with the PK passed via the URL"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = api_models.UserAccount.objects.all()
    serializer_class = UserSerializer
    lookup_field = "pk"


user_details_view = UserDetailsView.as_view()
