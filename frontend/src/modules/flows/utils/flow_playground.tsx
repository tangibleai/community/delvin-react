import { Node } from "reactflow";
import { swapElseNodePosition } from "./shared";

const deleteSingleConditionNodes = (
  deletedNodes: Node[],
  nodes: Node[]
): Node[] => {
  // deletes single condition nodes and decrease the height of parent node
  const firstDeletedNode = deletedNodes[0];
  const parentNode = findParentNode(firstDeletedNode, nodes);
  const deletedNodeIds = deletedNodes.map((node) => node.id);

  // exclude deleted nodes from nodes array
  nodes = removeDeletedNodes(deletedNodeIds, nodes);
  // retrieve remained simgle conditio blocks for parent node
  const remainedSingleConditionNodes = getRemainedSingleConditionNodes(
    parentNode.id,
    nodes
  );

  const viewportHeight = window.innerHeight;

  if (remainedSingleConditionNodes.length === 0) {
    // reset parent node height to default value if no nodes are inside
    nodes = resetParentNodeHeight(parentNode, nodes, viewportHeight);
  } else {
    const nodeHeight = getNodeHeight(remainedSingleConditionNodes[0]);
    const gap = calculateGap(viewportHeight);
    let y = calculateInitialY(parentNode, gap);

    // go through remained single conditons and add place them one by one inside parent node to remove all gap remained after deleted node are excluded
    const res = repositionRemainingNodes(
      remainedSingleConditionNodes,
      nodes,
      y,
      nodeHeight,
      gap
    );
    y = res.y;
    nodes = res.nodes;
    y = updateYAfterReposition(y, nodeHeight, gap);

    // order of single condition blocks can become messed, so make sure else block comaes last
    nodes = swapElseNodePosition(parentNode, nodes);
    // decrease parent node height to remove empty space left from deleted nodes
    nodes = adjustParentHeightIfNeeded(parentNode, nodes, y, nodeHeight, gap);
  }

  return nodes;
};

const findParentNode = (deletedNode: Node, nodes: Node[]): Node => {
  return nodes.find((node) => node.id === deletedNode.parentId)!;
};

const removeDeletedNodes = (
  deletedNodeIds: string[],
  nodes: Node[]
): Node[] => {
  return nodes.filter((node) => !deletedNodeIds.includes(node.id));
};

const getRemainedSingleConditionNodes = (
  parentId: string,
  nodes: Node[]
): Node[] => {
  return nodes.filter((node) => node.parentId === parentId);
};

const resetParentNodeHeight = (
  parentNode: Node,
  nodes: Node[],
  viewportHeight: number
): Node[] => {
  return nodes.map((node) =>
    node.id === parentNode.id
      ? {
          ...node,
          style: {
            ...node.style,
            height: (6 / 100) * viewportHeight,
          },
        }
      : node
  );
};

const getNodeHeight = (node: Node): number => {
  return Number(String(node.style!.height));
};

const calculateGap = (viewportHeight: number): number => {
  return (viewportHeight * 0.5) / 100;
};

const calculateInitialY = (parentNode: Node, gap: number): number => {
  const parentNodeHeaderHeight = parentNode.data.headerHeight;
  const parentNodeBorderWidth = parentNode.data.borderWidth;
  return parentNodeHeaderHeight + parentNodeBorderWidth + gap;
};

const repositionRemainingNodes = (
  remainedNodes: Node[],
  nodes: Node[],
  y: number,
  nodeHeight: number,
  gap: number
): { y: number; nodes: Node[] } => {
  remainedNodes.forEach((currentNode) => {
    nodes = nodes.map((node) =>
      node.id === currentNode.id
        ? {
            ...node,
            position: { ...node.position, y: y },
          }
        : node
    );
    y += nodeHeight + gap;
  });
  return { y: y, nodes: nodes };
};

const updateYAfterReposition = (
  y: number,
  nodeHeight: number,
  gap: number
): number => {
  return y - nodeHeight - gap;
};

const adjustParentHeightIfNeeded = (
  parentNode: Node,
  nodes: Node[],
  y: number,
  nodeHeight: number,
  gap: number
): Node[] => {
  const maxCurrentBlockReachY =
    y + nodeHeight + gap + parentNode.data.borderWidth;
  if (Number(String(parentNode.style!.height)) > maxCurrentBlockReachY) {
    nodes = nodes.map((node) =>
      node.id === parentNode.id
        ? {
            ...node,
            style: {
              ...node.style,
              height: maxCurrentBlockReachY,
            },
          }
        : node
    );
  }
  return nodes;
};

export { deleteSingleConditionNodes };
