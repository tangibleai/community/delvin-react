import json
import logging
import os
import requests

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()
import requests
import json


logger = logging.getLogger(__name__)


OPENROUTER_API_KEY = os.environ["OPENROUTER_API_KEY"]


from abc import ABC, abstractmethod

from litellm import completion


class TextGenerationModel(ABC):
    @abstractmethod
    def generate_text(self, prompt: str) -> str:
        pass


class LLMResponseGeneration(TextGenerationModel):
    def __init__(
        self,
        model_name="nousresearch/nous-capybara-7b:free",
        api_key="",
    ):
        self.llm_api_key = api_key
        self.model_name = model_name
        self.fallback_message = "Sorry, we're having trouble processing your message."

    def generate_text(self, message_list: list):
        """Attempts to generate a response to a query

        Will automatically fall back to llama-3-70b if OpenRouter's moderation policy is tripped

        message_list
        A list of message dictionaries from the bot and user
        [{"role": "user", "content": "I love cats"}]

        """
        try:
            resp = completion(
                model=self.model_name, messages=message_list, api_key=self.llm_api_key
            )
            if resp:
                return resp["choices"][0]["message"]["content"]
            else:
                logger.warning(f"No LLM response to query- model: {self.model_name}")
                return self.fallback_message
        except Exception as e:
            logger.error(f"LLM generation failed- model: {self.model_name}, {e}")
            return self.fallback_message

    def __call__(self, *args, **kwargs):
        return self.generate_text(*args, **kwargs)


class ModerationModel:

    def __init__(self, api_key, model="meta-llama/llama-guard-2-8b"):
        self.model = model
        self.api_key = api_key

    def generate_text(self, prompt: str, categories: str) -> str:
        moderation_prompt = f"""
<|begin_of_text|>[INST] Task: Check if there is unsafe content in 'user' messages in conversations according our safety policy with the below categories.

<BEGIN UNSAFE CONTENT CATEGORIES>
{categories}
<END UNSAFE CONTENT CATEGORIES>

<BEGIN CONVERSATION>

User: {prompt}

<END CONVERSATION>

Provide your safety assessment for the message in the above conversation:
- First line must read 'safe' or 'unsafe'.
- If unsafe, a second line must include a comma-separated list of violated categories. [/INST]                            
                            """
        response = requests.post(
            url="https://openrouter.ai/api/v1/chat/completions",
            headers={
                "Authorization": f"Bearer {self.api_key}",
            },
            data=json.dumps({"model": self.model, "prompt": moderation_prompt}),
        )

        try:
            content = json.loads(response.content)
            result = content["choices"][0]["text"]
        except:
            result = ""

        return result

    def extract_first_category_code(self, result: str):
        lines = result.split("\n")
        safe = None
        if "unsafe" in lines[0]:
            safe = False
        elif "safe" in lines[0]:
            safe = True

        if not safe:
            return lines[1].split(",")[0]

        return None


def main():
    print("==================================")
    generate_text = LLMResponseGeneration(
        api_key="",
        model_name="nousresearch/nous-capybara-7b:free",
        # model_name="openai/gpt-3.5-turbo",
    )

    text = generate_text([{"role": "user", "content": "How are you?"}])
    print(text)

    # Switch model

    print("==================================")
    generate_text = LLMResponseGeneration(
        api_key="",
        model_name="google/gemma-7b-it:free",
        # model_name="mistralai/mistral-7b-instruct",
    )

    text = generate_text([{"role": "user", "content": "I love cats"}])
    print(text)


if __name__ == "__main__":
    main()
