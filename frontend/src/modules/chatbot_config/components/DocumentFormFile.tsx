import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";

import { ChatbotConfig } from "@/typings/chatbot_config";

import { Document } from "@/typings/document";
import ErrorScreen from "@/components/ErrorScreen";
import { IoReload } from "react-icons/io5";
import { useToast } from "@/components/ui/use-toast";

import {
  useCreateDocumentMutation,
  useUpdateDocumentMutation,
} from "@/slices/api/documentSlice";

type Props = {
  document: Document;
  selectedChatbot: ChatbotConfig;
  onSubmit: (newDocument: Document) => void;
  onCancel: any;
  submit_action: string;
};

function DocumentFormFile({
  document,
  selectedChatbot,
  onSubmit,
  onCancel,
  submit_action,
}: Props) {
  const { toast } = useToast();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [processingDocument, setProcessingDocument] = useState(false);

  const [createDocument] = useCreateDocumentMutation();

  const [updateDocument] = useUpdateDocumentMutation();

  // Form Text
  const modal_trigger_button_text =
    submit_action == "add" ? "Upload document" : document.title;
  const modal_text =
    submit_action == "add" ? "Upload document" : "Update document";

  const routeParams = useParams();
  const projectId = routeParams.projectId;

  const allowedFileTypes = [
    { mime: "application/pdf", extensions: ["pdf"] },
    {
      mime: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      extensions: ["docx"],
    },
    { mime: "text/plain", extensions: ["txt", "adoc"] },
  ];

  function isValidFileType(file: any) {
    const fileType = file.type;
    const fileName = file.name.toLowerCase();

    const isValidMimeType = allowedFileTypes.some(
      (type) => type.mime === fileType
    );
    const isValidExtension = allowedFileTypes.some((type) =>
      type.extensions.some((ext) => fileName.endsWith(`.${ext}`))
    );

    return isValidMimeType || isValidExtension;
  }

  const formSchema = z.object({
    documentFile: z
      .instanceof(FileList)
      .optional()
      .superRefine((fileList, ctx) => {
        if (!fileList || fileList.length === 0) {
          ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: "Please upload a file",
          });
        } else if (
          !Array.from(fileList).every((file) => isValidFileType(file))
        ) {
          ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: "Only .pdf, .docx, and .txt files are allowed",
          });
        }
      }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
  });
  const documentFileRef = form.register("documentFile");

  useEffect(() => {
    // Handles resetting the form after submit or x
    // Can't directly access x in Dialog
    if (!isModalOpen) {
      form.reset();
    }
  }, [isModalOpen]);

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    setProcessingDocument(true);

    const { documentFile } = values;

    let result: any = {};

    const file = documentFile?.[0];
    const formData = new FormData();
    formData.append("project", String(projectId));
    formData.append("chatbot", selectedChatbot.id);
    formData.append("title", "");
    if (file) {
      formData.append("content", file);
    }
    formData.append("document_type", "file");

    if (submit_action == "add") {
      try {
        result = await createDocument(formData).unwrap();
      } catch {
        return <ErrorScreen />;
      }
    } else {
      formData.append("document", String(document.id));
      try {
        result = await updateDocument(formData).unwrap();
      } catch {
        return <ErrorScreen />;
      }
    }

    if (result.error) {
      setProcessingDocument(false);
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: `Couldn't ${submit_action} document`,
      });
      return;
    }
    onSubmit(result);
    setIsModalOpen(false);
    setProcessingDocument(false);

    toast({
      title: "Success!",
      description: `Successfully ${
        submit_action === "add" ? "added" : "updated"
      } document`,
    });
  }

  return (
    <>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(handleSubmit)} className="space-y-8">
          <FormField
            control={form.control}
            name="documentFile"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Select your file</FormLabel>
                <FormControl>
                  <Input type="file" {...documentFileRef} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          {processingDocument ? (
            <Button disabled className="order-2 sm:order-1">
              <IoReload className="mr-2 h-4 w-4 animate-spin" />
              {modal_text}
            </Button>
          ) : (
            <>
              <Button type="submit" className="order-2 sm:order-1">
                {modal_text}
              </Button>
              <Button
                type="button"
                variant="outline"
                className="order-1 sm:order-2"
                onClick={onCancel}
              >
                Cancel
              </Button>
            </>
          )}
        </form>
      </Form>
    </>
  );
}

export default DocumentFormFile;
