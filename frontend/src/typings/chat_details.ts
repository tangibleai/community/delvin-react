import { ContactDetails } from "./contact_details";
import { Message } from "./message";

export interface ChatDetails {
  id: string;
  title: string;
  contact_id: ContactDetails;
  is_default: boolean;
  origin_id: string;
  latest_message: Message | null;
}
