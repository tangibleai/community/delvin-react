import { forwardRef, useEffect, useImperativeHandle } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";

import * as z from "zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";

import { AppDispatch, RootState } from "@/store";
import { setOnboardingData } from "@/slices/onboardingSlice";

const DocumentFormGoogle = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();

  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData,
  );

  const formSchema = z.object({
    url: z.string(),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      url: "",
    },
  });

  const { handleSubmit, reset } = form;

  async function onSubmit(values: z.infer<typeof formSchema>) {
    const { url } = values;
    dispatch(
      setOnboardingData({
        ...onboardingData,
        stepDocumentAdding: {
          file: onboardingData.stepDocumentAdding.file,
          text: onboardingData.stepDocumentAdding.text,
          google: url,
        },
      }),
    );

    return true;
  }

  useEffect(() => {
    if (onboardingData) {
      reset({
        url: onboardingData.stepDocumentAdding.google,
      });
    }
  }, [onboardingData, reset]);

  useImperativeHandle(ref, () => ({
    submitForm: () => {
      return new Promise<boolean>((resolve) => {
        handleSubmit(async (values) => {
          const result = await onSubmit(values);
          resolve(result);
        })();
      });
    },
    updateOnboardingDataWithDraft: () => {
      const { url } = form.getValues();
      dispatch(
        setOnboardingData({
          ...onboardingData,
          stepDocumentAdding: {
            file: onboardingData.stepDocumentAdding.file,
            text: onboardingData.stepDocumentAdding.text,
            google: url,
          },
        }),
      );
    },
  }));

  return (
    <>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
          <FormField
            control={form.control}
            name="url"
            defaultValue=""
            render={({ field }) => (
              <FormItem>
                <FormLabel>Public Google Doc URL</FormLabel>
                <FormControl>
                  <Input placeholder="The public share link" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
        </form>
      </Form>
    </>
  );
});

export default DocumentFormGoogle;
