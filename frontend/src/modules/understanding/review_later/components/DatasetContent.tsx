import { ScrollArea } from "@/components/ui/scroll-area";
import { useEffect, useState } from "react";
import { DatasetHeader } from "./DatasetHeader";
import { DatasetTaggedMessage } from "./DatasetTaggedMessage";
import {
  fetchTaggedMessages,
  removeTaggedMessage,
  retrainDataset,
} from "../utils/dataset_content";
import { useParams } from "react-router-dom";
import { Dataset } from "@/typings/dataset";
import { TaggedMessage } from "@/typings/tagged_message";

type Props = {
  activeDataset: Dataset;
  onDatasetDeleted: (datasetId: string) => Promise<void>;
};

const DatasetContent = ({ activeDataset, onDatasetDeleted }: Props) => {
  const [taggedMessages, setTaggedMessages] = useState(Array<TaggedMessage>);

  useEffect(() => {
    fetchAndSetTaggedMessages();
  }, [activeDataset]);

  const fetchAndSetTaggedMessages = async () => {
    if (!activeDataset || Object.keys(activeDataset).length === 0) return;
    const taggedMessages = await fetchTaggedMessages(activeDataset);
    // taggedMessages = removeCurrentTagDefinition(taggedMessages);
    setTaggedMessages(taggedMessages);
  };

  // const removeCurrentTagDefinition = (taggedMessages: []) => {
  //   const currentTagDefinitionName = activeDataset.name;
  //   taggedMessages.forEach((obj: TaggedMessage) => {
  //     obj.tag_definitions = obj.tag_definitions.filter(
  //       (sub) => sub.name !== currentTagDefinitionName
  //     );
  //   });

  //   return taggedMessages;
  // };

  const routeParams = useParams();
  const projectId = routeParams.projectId;

  const handleRetrainDataset = async () => {
    // await retrainDataset(projectId);  // uncomment later
    fetchAndSetTaggedMessages();
  };

  const handleTaggedMessageDeleted = async (taggedMessageId: string) => {
    await removeTaggedMessage(taggedMessageId);
    fetchAndSetTaggedMessages();
  };

  return (
    <div className="w-3/4 p-7">
      <DatasetHeader
        activeDataset={activeDataset}
        onDatasetDeleted={onDatasetDeleted}
        onRetrainDataset={handleRetrainDataset}
      />
      <p className="text-3xl font-bold mt-6">Examples</p>
      <ScrollArea className="flex flex-col justify-end flex-grow py-2.5 px-5 rounded-2xl bg-gray-100 p-4 h-[55vh] mt-4">
        {taggedMessages &&
          taggedMessages.map((taggedMessage, index) => (
            <DatasetTaggedMessage
              key={index}
              taggedMessage={taggedMessage}
              // onTaggedMessageDeleted={handleTaggedMessageDeleted}
            />
          ))}
      </ScrollArea>
    </div>
  );
};

export { DatasetContent };
