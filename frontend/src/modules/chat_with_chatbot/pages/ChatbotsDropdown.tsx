import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown";
import { cn, focusInput } from "@/lib/utils";
import { RiExpandUpDownLine } from "react-icons/ri";
import React from "react";
import { ScrollArea } from "@/components/ui/scroll-area";
import { composeProjectInitials } from "@/components/layout/utils/util";
import { ChatbotConfig } from "@/typings/chatbot_config";
import { useListChatbotsQuery } from "@/slices/api/chatbotsApiSlice";
import { useParams } from "react-router-dom";

type Props = {
  selectedChatbot: ChatbotConfig;
  setSelectedChatbot: React.Dispatch<React.SetStateAction<ChatbotConfig>>;
};

export default function ChatbotsDropdown({
  selectedChatbot,
  setSelectedChatbot,
}: Props) {
  const [dropdownOpen, setDropdownOpen] = React.useState(false);
  const focusRef = React.useRef<null | HTMLButtonElement>(null);

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const {
    data: chatbots = [],
    isLoading: retrievingChatbots,
    isError: errorRetrievingChatbots,
  } = useListChatbotsQuery({ project_id: projectId });

  const switchChatbot = async (chatbot: ChatbotConfig) => {
    setSelectedChatbot(chatbot);
  };

  return (
    <DropdownMenu
      open={dropdownOpen}
      onOpenChange={setDropdownOpen}
      modal={false}
    >
      <DropdownMenuTrigger asChild>
        <button
          className={cn(
            "flex w-full items-center  rounded-md  bg-white p-2 text-sm shadow-sm transition-all hover:bg-gray-50 dark:border-gray-800 dark:bg-gray-950 hover:dark:bg-gray-900",
            "border border-gray-300 gap-x-2.5",
            focusInput
          )}
        >
          <span
            className="flex aspect-square size-8 items-center justify-center rounded bg-indigo-600 p-2 text-xs font-medium text-white dark:bg-indigo-500"
            aria-hidden="true"
          >
            {selectedChatbot.origin!.slice(0, 2)}
          </span>
          <div className="flex w-full items-center justify-between gap-x-4 truncate">
            <div className="truncate">
              <p className="truncate whitespace-nowrap text-sm font-medium text-gray-900 dark:text-gray-50">
                {selectedChatbot.origin}
              </p>
            </div>
            <RiExpandUpDownLine
              className="size-5 shrink-0 text-gray-500"
              aria-hidden="true"
            />
          </div>
        </button>
      </DropdownMenuTrigger>
      <DropdownMenuContent
        onCloseAutoFocus={(event: any) => {
          if (focusRef.current) {
            focusRef.current.focus();
            focusRef.current = null;
            event.preventDefault();
          }
        }}
      >
        <DropdownMenuGroup>
          <DropdownMenuLabel>Assistants ({chatbots?.length})</DropdownMenuLabel>
          <ScrollArea
            key={"projects scroll area"}
            className="flex flex-col max-h-48"
          >
            {chatbots?.map((chatbot) => (
              <DropdownMenuItem key={chatbot.id}>
                <div
                  onClick={() => switchChatbot(chatbot)}
                  className="flex w-full items-center gap-x-2.5"
                >
                  <span
                    className={cn(
                      "bg-indigo-600 dark:bg-indigo-500",
                      "flex aspect-square w-[2rem] h-[2rem] items-center justify-center rounded p-2 text-xs font-medium text-white"
                    )}
                    aria-hidden="true"
                  >
                    {composeProjectInitials(chatbot.origin)}
                  </span>
                  <div>
                    <p
                      key={chatbot.id}
                      className="text-sm font-medium text-gray-900 dark:text-gray-50"
                    >
                      {chatbot.origin}
                    </p>
                  </div>
                </div>
              </DropdownMenuItem>
            ))}
          </ScrollArea>
        </DropdownMenuGroup>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}
