import { Navigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import { useState } from "react";
import Spinner from "@/components/Spinner";
import { Button } from "@/components/ui/button";
import { Checkbox } from "@/components/ui/checkbox";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { AppDispatch, RootState } from "@/store";
import logo from "@/assets/delvin_logo.png";
import { signup } from "@/actions/auth";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import { loadUserSuccess } from "@/slices/authSlice";
import { passwordStringValidator } from "../utils/util";

function Signup() {
  const [accountCreated, setAccountCreated] = useState(false);
  const [error, setError] = useState<String | null>(null);

  const dispatch: AppDispatch = useDispatch();

  const isLoading = useSelector((state: RootState) => state.auth.loading);

  const formSchema = z
    .object({
      name: z.string().min(1, "Please enter a name"),
      email: z.string().email("Please provide a valid email"),
      password: z
        .string()
        .min(8, { message: "The password should be at least 8 characters." })
        .refine(passwordStringValidator, {
          message: "String should not be all numbers",
        }),
      repassword: z.string(),
    })
    .superRefine(({ repassword, password }, ctx) => {
      if (password != repassword) {
        ctx.addIssue({
          path: ["repassword"],
          code: "custom",
          message: "The passwords did not match",
        });
      }
    });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      email: "",
      password: "",
      repassword: "",
    },
  });

  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated
  );

  if (isAuthenticated) {
    return <Navigate to={AUTH_PAGE_URLS.home} />;
  }

  if (accountCreated) {
    return <Navigate to={AUTH_PAGE_URLS.resend_email} />;
  }

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    const { name, email, password, repassword } = values;
    let result = await dispatch(signup(name, email, password, repassword));
    dispatch(loadUserSuccess({ email: email, name: "", id: "" }));
    if (!result.isSuccessful) {
      const errorDetail = result.message.includes("already exists")
        ? `${email} already exists.`
        : result.message;
      setError(errorDetail);
      return;
    }

    setAccountCreated(true);
  }

  return (
    <div className="flex flex-col items-center justify-center px-6 py-4 mx-auto md:h-screen lg:py-0">
      <img className="w-80 mb-3" src={logo} alt="logo" />
      <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700 p-2 space-y-2 md:space-y-6 sm:p-8">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          `{" "}
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
            Create an account
          </h1>
          {error && (
            <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
              Error: {error}
            </p>
          )}
          {isLoading && (
            <div className="mx-auto">
              <Spinner />
            </div>
          )}
          <Form {...form}>
            <form onSubmit={form.handleSubmit(handleSubmit)}>
              <FormField
                control={form.control}
                name="name"
                render={({ field }) => {
                  return (
                    <>
                      <FormItem>
                        <FormLabel>Name</FormLabel>
                        <FormControl>
                          <Input placeholder="Your name" {...field} />
                        </FormControl>
                      </FormItem>
                      {form?.formState?.errors?.name?.message && (
                        <FormMessage />
                      )}
                    </>
                  );
                }}
              />
              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel>Email</FormLabel>
                      <FormControl>
                        <Input placeholder="Your email" {...field} />
                      </FormControl>
                    </FormItem>
                    {form?.formState?.errors?.email?.message && <FormMessage />}
                  </>
                )}
              />
              <FormField
                control={form.control}
                name="password"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel>Password</FormLabel>
                      <FormControl>
                        <Input
                          type="password"
                          placeholder="Type a secure password"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  </>
                )}
              />
              <FormField
                control={form.control}
                name="repassword"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel>Confirm Password</FormLabel>
                      <FormControl>
                        <Input
                          type="password"
                          placeholder="Retype your password"
                          {...field}
                        />
                      </FormControl>
                    </FormItem>
                    {form?.formState?.errors?.repassword?.message && (
                      <FormMessage />
                    )}
                  </>
                )}
              />

              <div className="flex items-center space-x-2 h-10 my-4">
                <Checkbox id="terms" required />
                <label htmlFor="terms" className="">
                  I accept the{"  "}
                  <a
                    className="font-medium text-primary-600 hover:underline dark:text-primary-500"
                    href="https://tangibleai.com/delvintoc"
                    target="_blank"
                  >
                    Terms and Conditions
                  </a>
                  <br /> and the {"  "}
                  <a
                    className="font-medium text-primary-600 hover:underline dark:text-primary-500"
                    href="https://tangibleai.com/delvinprivacy"
                    target="_blank"
                  >
                    Privacy Policy.
                  </a>
                </label>
              </div>
              <Button type="submit">Sign Up</Button>
            </form>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default Signup;
