import { useForm } from "react-hook-form";
import * as z from "zod";
import {
  Form,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
  FormField,
  FormDescription,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Textarea } from "@/components/ui/textarea";
import { zodResolver } from "@hookform/resolvers/zod";
import { forwardRef, useEffect, useImperativeHandle } from "react";
import { AppDispatch, RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { setOnboardingData } from "@/slices/onboardingSlice";
import { Button } from "@/components/ui/button";
import { discussionAnswerBotSampleInput } from "../utils/onboardingSampleInput";

const SubstepDiscussionBotSettings = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();

  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData,
  );

  const formSchema = z.object({
    chatbotPersona: z.string().min(2, {
      message: "Chatbot persona must be at least 2 characters.",
    }),
    instructions: z.string().min(2, {
      message: "Instructions must be at least 2 characters.",
    }),
    constraints: z.string().min(2, {
      message: "Constraints must be at least 2 characters.",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      chatbotPersona: onboardingData.discussionBotSettings.chatbotPersona || "",
      instructions: onboardingData.discussionBotSettings.instructions || "",
      constraints: onboardingData.discussionBotSettings.constraints || "",
    },
  });

  const { handleSubmit, reset } = form;

  useEffect(() => {
    if (onboardingData) {
      reset({
        chatbotPersona:
          onboardingData.discussionBotSettings.chatbotPersona || "",
        instructions: onboardingData.discussionBotSettings.instructions || "",
        constraints: onboardingData.discussionBotSettings.constraints || "",
      });
    }
  }, [onboardingData, reset]);

  useImperativeHandle(ref, () => ({
    submitForm: () => {
      return new Promise<boolean>((resolve) => {
        handleSubmit(async (values) => {
          const result = await onSubmit(values);
          resolve(result);
        })();
      });
    },
    updateOnboardingDataWithDraft: () => {
      const { chatbotPersona, instructions, constraints } = form.getValues();
      dispatch(
        setOnboardingData({
          ...onboardingData,
          discussionBotSettings: {
            chatbotPersona: chatbotPersona,
            instructions: instructions,
            constraints: constraints,
          },
        }),
      );
    },
  }));

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const { chatbotPersona, instructions, constraints } = values;
    dispatch(
      setOnboardingData({
        ...onboardingData,
        discussionBotSettings: {
          chatbotPersona: chatbotPersona,
          instructions: instructions,
          constraints: constraints,
        },
      }),
    );

    return true;
  };

  const fillFieldsWithTemplate = () => {
    form.setValue(
      "chatbotPersona",
      discussionAnswerBotSampleInput["chatbotPersona"],
    );
    form.setValue(
      "instructions",
      discussionAnswerBotSampleInput["instructions"],
    );
    form.setValue("constraints", discussionAnswerBotSampleInput["constraints"]);
  };

  return (
    <div className="h-full w-full">
      <div className="w-[55%] mx-auto flex flex-col">
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-6">
            <div className="flex flex-col gap-2 w-full items-center">
              <p className="font-medium text-3xl">
                Create a prompt for your assistant
              </p>
              <span className="text-base font-light leading-none">
                The prompt contains all the instructions for your chatbot. You
                can change these later.
              </span>
            </div>
            <div className="flex flex-col items-center">
              <Button onClick={fillFieldsWithTemplate} className="text-lg">
                Use sample prompt
              </Button>
            </div>

            <div className="flex flex-col gap-6 text-lg">
              <FormField
                control={form.control}
                name="chatbotPersona"
                render={({ field }) => (
                  <FormItem className="flex flex-col">
                    <FormLabel>Chatbot persona</FormLabel>
                    <FormDescription>
                      Describe the assistant's personality, target audience, and
                      tone of voice.
                    </FormDescription>
                    <FormControl>
                      <Input
                        // placeholder="The assistant, named Syndee, helps women manage their"
                        {...field}
                        className="text-base"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="instructions"
                render={({ field }) => (
                  <FormItem className="flex flex-col">
                    <FormLabel>
                      <div className="flex flex-col gap-1 ">Instructions</div>
                    </FormLabel>
                    <FormDescription>
                      Provide detailed instructions on how the assistant should
                      conduct the conversation.
                    </FormDescription>
                    <FormControl>
                      <Textarea
                        // placeholder="The assistant provides information about imposter syndrome and facilitates exercises to help women think through their feelings related to imposter syndrome. The assistant speaks in an encouraging and upbeat manner, but is sensitive to what users say. The assistant leads the conversation and finishes its response with a question. Its replies are 2-3 sentences long."
                        {...field}
                        className="text-base"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="constraints"
                render={({ field }) => (
                  <FormItem className="flex flex-col">
                    <FormLabel>
                      <div className="flex flex-col gap-1">Constraints</div>
                    </FormLabel>
                    <FormDescription>
                      What topics your bot should not respond to? How long
                      should the answer be? What else the bot should not do?
                      etc.
                    </FormDescription>
                    <FormControl>
                      <Textarea
                        // placeholder="Sorry, I'm not sure what you meant"
                        {...field}
                        className="text-base"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
          </form>
        </Form>
      </div>
    </div>
  );
});

export default SubstepDiscussionBotSettings;
