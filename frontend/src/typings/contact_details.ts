export interface ContactDetails {
  id: string;
  origin: string;
  inserted_at: Date;
  last_message_timestamp: Date | null;
}
