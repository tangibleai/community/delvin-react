import litellm
from rest_framework import generics, serializers, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.exceptions import ValidationError

from api.models import Provider, ProviderChoices, Workspace


class ProviderSerializer(serializers.ModelSerializer):
    workspace = serializers.PrimaryKeyRelatedField(
        queryset=Workspace.objects.all(), required=False, allow_null=True
    )

    class Meta:
        model = Provider
        fields = "__all__"

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        if "api_key" in representation and representation["api_key"]:
            api_key = representation["api_key"]
            if len(api_key) > 6:
                masked_key = f"{api_key[:3]}{'*' * (len(api_key) - 6)}{api_key[-3:]}"
                representation["api_key"] = masked_key
            else:
                representation["api_key"] = "*" * len(api_key)
        return representation


class ProvidersListRequestSerializer(serializers.Serializer):
    workspace_id = serializers.UUIDField(
        required=True, help_text="UUID of the workspace."
    )


class ProvidersListView(generics.ListAPIView):
    serializer_class = ProviderSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        serializer = ProvidersListRequestSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)

        workspace_id = serializer.validated_data.get("workspace_id")

        try:
            workspace = Workspace.objects.get(id=workspace_id)
        except Workspace.DoesNotExist:
            raise ValidationError(f"Workspace with id {workspace_id} does not exist.")

        providers = Provider.objects.filter(workspace=workspace)

        if workspace.has_default_provider():
            default_provider = Provider.get_default_provider()
            providers = list(providers)
            providers.insert(0, default_provider)

        return providers

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        providers_data = self.get_serializer(queryset, many=True).data

        return Response(providers_data, status=status.HTTP_200_OK)


class ProviderCreateView(generics.CreateAPIView):
    serializer_class = ProviderSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        provider_type = self.request.data["type"]

        if provider_type not in ProviderChoices.values:
            raise ValueError(f"Invalid provider type: {provider_type}")

        workspace_id = self.request.data.get("workspace")
        workspace = Workspace.objects.get(id=workspace_id) if workspace_id else None

        serializer.save(type=provider_type, workspace=workspace)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        validated_data = serializer.validated_data
        provider = validated_data.get("type")
        api_key = validated_data.get("api_key")
        provider_api_key_valid = self.is_provider_api_key_valid(provider, api_key)
        if not provider_api_key_valid:
            return Response(
                {"detail": "Invalid API key for the specified provider."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def is_provider_api_key_valid(self, provider, api_key):
        provider_model = self.get_random_model_for_provider(provider)
        key_valid = litellm.check_valid_key(model=provider_model, api_key=api_key)

        return key_valid

    def get_random_model_for_provider(self, provider: str):
        provider_to_model = {
            ProviderChoices.OPENAI.value: "gpt-3.5-turbo",
            ProviderChoices.OPENROUTER.value: "openrouter/openai/gpt-3.5-turbo",
            ProviderChoices.ANTHROPIC.value: "claude-3-5-sonnet-20241022",
        }

        return provider_to_model.get(provider)


class ProviderDeleteView(generics.DestroyAPIView):
    queryset = Provider.objects.all()
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def delete(self, request, *args, **kwargs):
        provider = self.get_object()
        provider.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
