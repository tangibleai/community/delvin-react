from rest_framework import generics, serializers
from rest_framework import exceptions
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.internal.serializers import MessageTagDefinitionSerializer
from api.models import MessageTagDefinition, MessageTagGroup, Project, MLModel


class MessageTagDefinitionsViewGETRequestSerializer(serializers.Serializer):
    project_id = serializers.UUIDField(required=True)


class MessageTagDefinitionsViewPOSTRequestSerializer(serializers.Serializer):
    project_id = serializers.UUIDField(required=True)
    name = serializers.CharField(required=True)
    description = serializers.CharField(required=True)
    message_tag_group_id = serializers.UUIDField(required=False)

    def validate_message_tag_group_id(self, value):
        """
        Validates that passed value set to "tag_ids" field is an array of strings.
        """

        if not MessageTagGroup.objects.filter(id=value):
            raise serializers.ValidationError(
                "message tag group with specified id not found."
            )

        return value


class MessageTagDefinitionsView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    get_request_serializer_class = MessageTagDefinitionsViewGETRequestSerializer
    post_request_serializer_class = MessageTagDefinitionsViewPOSTRequestSerializer
    response_serializer = MessageTagDefinitionSerializer

    def get(self, request):
        serializer = self.get_request_serializer_class(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        project_id = validated_data.get("project_id")
        project = Project.objects.get(id=project_id)

        tags = MessageTagDefinition.objects.filter(project=project)

        response_serializer = self.response_serializer(tags, many=True)
        serialized_tags = response_serializer.data

        return Response(serialized_tags)

    def post(self, request):
        serializer = self.post_request_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        project_id = validated_data.get("project_id")
        project = Project.objects.get(id=project_id)
        name = validated_data.get("name")
        description = validated_data.get("description")

        # optional
        message_tag_group_id = validated_data.get("message_tag_group_id")
        message_tag_group_rec = MessageTagGroup.objects.filter(
            id=message_tag_group_id
        ).first()

        tag = MessageTagDefinition.objects.create(
            project=project,
            name=name,
            description=description,
            message_tag_group=message_tag_group_rec,
        )

        response_serializer = self.response_serializer(tag)
        serialized_tag = response_serializer.data

        ml_model = MLModel.objects.filter(project=project).first()
        ml_model.set_ml_model_status_to_needs_retraining()

        return Response(serialized_tag)


message_tag_definitions_view = MessageTagDefinitionsView.as_view()


class RemoveMessageTagDefinitionView(generics.DestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = MessageTagDefinition.objects.all()
    serializer_class = MessageTagDefinitionSerializer

    def perform_destroy(self, instance):
        project = instance.project

        super().perform_destroy(instance)

        ml_model = MLModel.objects.filter(project=project).first()
        ml_model.set_ml_model_status_to_needs_retraining()


remove_message_tag_definition_view = RemoveMessageTagDefinitionView.as_view()
