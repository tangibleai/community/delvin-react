import { Link } from "react-router-dom";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import { buttonVariants } from "@/components/ui/button";

export default function NotFound() {
  return (
    <div className="flex flex-col items-center justify-center px-20 py-8 mx-auto md:h-screen lg:py-0">
      <div className="not-found-container">
        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-4xl dark:text-white py-5">
          Oops! The page you were looking for does not exist.
        </h1>

        <p className="py-5">
          You might have mistyped the address, or the page might have been
          moved.
        </p>
        <Link
          to={AUTH_PAGE_URLS.home}
          className={buttonVariants({ variant: "default" })}
        >
          Back to Home{" "}
        </Link>
      </div>
    </div>
  );
}
