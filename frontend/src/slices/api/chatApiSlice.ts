import emptyApi from "../apiSlice";
import { ChatDetails } from "@/typings/chat_details";

type GetChatsRequestParams = {
  project_id: string;
  internal?: boolean;
};

const chatApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    getChats: builder.query<ChatDetails[], GetChatsRequestParams>({
      query: (requestParams) => {
        // Assign a default value for `internal` if not specified
        const params = { internal: true, ...requestParams };

        return {
          url: "chats/",
          method: "GET",
          params: params,
        };
      },
      providesTags: (result = [], error, arg) => [
        "Chat",
        ...result.map(({ id }) => ({ type: "Chat", id }) as const),
      ],
    }),
    createChat: builder.mutation<ChatDetails, string>({
      query: (contactId) => ({
        url: "chats/create/",
        method: "POST",
        data: { contact_id: contactId },
      }),
      invalidatesTags: ["Chat"],
    }),
  }),
});

export const { useGetChatsQuery, useCreateChatMutation } = chatApiSlice;
