from api import models
from api.etl import utils


def process(df_or_records_list, *args, **kwargs):
    users = df_or_records_list
    if not isinstance(users, list):
        users = df_or_records_list.to_dict("records")
    required_fields = {"email", "name", "password", "is_active"}
    utils.check_missing_fields(users, required_fields)

    duplicated_emails = check_duplicated_emails(users)
    if duplicated_emails:
        raise ValueError(f'Users with "{duplicated_emails}" emails already exist')

    db_records = save_users_to_db(users)

    return db_records


def check_duplicated_emails(users):
    duplicated_emails = []
    for u in users:
        email = u["email"]
        user_found = models.UserAccount.objects.filter(email=email)
        if user_found:
            duplicated_emails.append(email)

    return duplicated_emails


def save_users_to_db(records_as_dicts):
    db_records = []
    for u in records_as_dicts:
        rec = models.UserAccount.objects.create_user(
            **dict(
                email=u["email"],
                name=u["name"],
                password=u["password"],
            )
        )
        rec.is_active = u["is_active"]
        rec.is_admin = False
        db_records.append(rec)

    return db_records
