from utils.text_generator import ModerationModel
from api.utils.prompt_configuration import ChatbotConfig


class BuiltinModerationManager:
    def __init__(self, project, config: ChatbotConfig, chatbot):
        self.project = project
        self.config = config
        self.llm_api_key = chatbot.provider.api_key
        self.model_string = chatbot.generative_model.model_string

    def get_response(self, message):
        try:
            guardrails_builtin = self.config.guardrails.builtin
            if guardrails_builtin.enabled:
                categories = [
                    f"{cat.code}: {cat.name}"
                    for code, cat in guardrails_builtin.categories.items()
                    if cat.enabled
                ]

                model = ModerationModel(
                    api_key=self.llm_api_key, model=self.model_string
                )
                result = model.generate_text(message.text, categories)
                category = model.extract_first_category_code(result)
                response = guardrails_builtin.categories.get(category, {}).response
                if response:
                    return response
        except:
            return None
