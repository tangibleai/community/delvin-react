import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import { useParams, useNavigate } from "react-router-dom";

import { Button } from "@/components/ui/button";
import { Checkbox } from "@/components/ui/checkbox";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import logo from "@/assets/delvin_logo.png";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { AppDispatch, RootState } from "@/store";
import { verifyTeamMember } from "@/actions/auth";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";

const Activate = () => {
  const dispatch: AppDispatch = useDispatch();
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );
  const routeParams = useParams();
  const navigate = useNavigate();

  const user = useSelector((state: RootState) => state.auth.user);

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    const { name, password, repassword } = values;
    const uid = routeParams.uid!;
    const token = routeParams.token!;
    let data = {
      uid: uid,
      token: token,
      password: password,
      re_password: repassword,
    };

    const body = JSON.stringify(data);
    let response = await dispatch(
      // Updates the UserAccount with a password and activates
      verifyTeamMember(uid, token, name, password, repassword),
    );
    if (response?.status == 200) {
      return navigate("/");
    }
  }

  const passwordStringValidator = (value: any) => {
    return /\D/.test(value) ? true : false;
  };

  const formSchema = z
    .object({
      name: z.string().min(1, "Please enter a name"),
      password: z
        .string()
        .min(8, { message: "The password should be at least 8 characters." })
        .refine(passwordStringValidator, {
          message: "String should not be all numbers",
        }),
      repassword: z.string(),
    })
    .superRefine(({ repassword, password }, ctx) => {
      if (password != repassword) {
        ctx.addIssue({
          path: ["repassword"],
          code: "custom",
          message: "The passwords did not match",
        });
      }
    });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      password: "",
      repassword: "",
    },
  });

  return (
    <>
      <div className="flex flex-col items-center justify-center px-6 py-4 mx-auto md:h-screen lg:py-0">
        <img className="w-80 mb-3" src={logo} alt="logo" />
        <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
          <div className="p-2 space-y-2 md:space-y-6 sm:p-8">
            <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
              Account setup
            </h1>
            <Form {...form}>
              <form
                onSubmit={form.handleSubmit(handleSubmit)}
                className="space-y-4 md:space-y-4"
              >
                <FormField
                  control={form.control}
                  name="name"
                  render={({ field }) => {
                    return (
                      <>
                        <FormItem>
                          <FormLabel>Name</FormLabel>
                          <FormControl>
                            <Input placeholder="Your name" {...field} />
                          </FormControl>
                        </FormItem>
                        {form?.formState?.errors?.name?.message && (
                          <FormMessage />
                        )}
                      </>
                    );
                  }}
                />
                <FormField
                  control={form.control}
                  name="password"
                  render={({ field }) => (
                    <>
                      <FormItem>
                        <FormLabel>Password</FormLabel>
                        <FormControl>
                          <Input
                            type="password"
                            placeholder="Type a secure password"
                            {...field}
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    </>
                  )}
                />
                <FormField
                  control={form.control}
                  name="repassword"
                  render={({ field }) => (
                    <>
                      <FormItem>
                        <FormLabel>Confirm Password</FormLabel>
                        <FormControl>
                          <Input
                            type="password"
                            placeholder="Retype your password"
                            {...field}
                          />
                        </FormControl>
                      </FormItem>
                      {form?.formState?.errors?.repassword?.message && (
                        <FormMessage />
                      )}
                    </>
                  )}
                />
                <div className="flex items-center space-x-2 h-10">
                  <Checkbox id="terms" required />
                  <label htmlFor="terms" className="">
                    I accept the{" "}
                    <a
                      className="font-medium text-primary-600 hover:underline dark:text-primary-500"
                      href="#"
                    >
                      Terms and Conditions
                    </a>
                  </label>
                </div>
                <Button type="submit">Create an account</Button>
              </form>
            </Form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Activate;
