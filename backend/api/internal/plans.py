from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, serializers, status
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.response import Response

from api.models import Chatbot, GenerativeModel, Plan, Provider


class PlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = "__all__"


class PlanDetailsView(generics.RetrieveAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PlanSerializer
    queryset = Plan.objects.all()
    lookup_field = "id"


class UpdatePlanView(generics.UpdateAPIView):
    """
    PATCH method is forbidden.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PlanSerializer

    def get_queryset(self):
        queryset = Plan.objects.filter(id=self.kwargs.get("pk"))
        return queryset

    def put(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=False)

        plan = self.get_object()
        validated_data = serializer.validated_data
        serializer.update(plan, validated_data)

        new_plan_type = validated_data.get("type")
        if new_plan_type == Plan.Type.OWN_KEY.value:
            plan = self.get_object()
            workspace = plan.workspace
            default_provider = Provider.get_default_provider()
            chatbots_with_default_provider = Chatbot.objects.filter(
                project__workspace=workspace, provider=default_provider
            )
            first_user_added_provider = (
                Provider.objects.filter(workspace=workspace)
                .exclude(id=default_provider.id)
                .first()
            )
            generative_model = GenerativeModel.objects.filter(
                provider_type=first_user_added_provider.type, priority=1
            ).first()
            chatbots_with_default_provider.update(
                provider=first_user_added_provider, generative_model=generative_model
            )

        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)
