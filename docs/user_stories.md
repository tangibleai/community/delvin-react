# User Stories


## Reference Documents 

[Data Schema] (https://drive.google.com/file/d/1Be80tXMiaWGxYjmAW1fxtP8vCkNtSoRt/view?usp=sharing) 


## Up next




## Completed
* [x] User can sign up and log in.
* [x] User can see their list of projects
* [x] User can see the list of contacts in a given project
* [x] User can see conversation log for each conversation in the given project
* [x] User can see basic project stats (number of active & new users, number of messages) in a given project 
* [x] User can send a message to the API endpoint with an API token belonging **to the project** 
* [x] User can generate or change an api token for the project

## Backlog

### Epic: Security 

**Vision**: Delvin's security and encryption standards are on-par with similar solutions on the market. Delvin can be trusted by clients to store the chatbot data. 

* [ ] JWT tokens are stored in HTTP-only cookie instead of local storage. (At least refresh token)
    * [ ] Temporary fix: store access token in memory and refresh token in a cookie 

* [X] Backend and frontend communicate on https
    * [X] External task: Maria should provide SSL certificate for delvin.
      * Solved with CertManager (Cetin).


### Epic: Basic Usability

* [ ] Smooth Sign-In Process: 
    * [ ] Error Message if the user was not created


### Epic: Chatbot Platform Integrations 

**Vision**: Delvin can ingest messages from multiple platforms.

* [ ] User can send messages from a CustomGPT chatbot to Delvin using HTTP API.
    * [ ] External Task: implement a CustomGPT that uses an action to send every message
    * [ ] Public API can accept multiple messages in the same call 

* [ ] User can connect the project to a Turn.io BigQuery dataset
    * [x] User can store the Google API credentials 
    * [ ] When users connects to the project for the first time, the message data is synced from BigQuery to Delvin's database in batches 
    * [ ] After the initial sync, the projects pull data from BigQuery every hour. 
    * [ ] Initial sync and periodic sync for contact information. 

* [ ] User can connect the project a RapidPro (TextIt / Weni / Glyfic) chatbot
    * [ ] User can select a RapidPro connection and save their RapidPro API token 
    * [ ] When user connects from first time, they specify when to sync the data from and the platform sync it in batches. 
    * [ ] The platform syncs message data from RapidPro on a regular basis 



### Epic: Sharing and inviting users to projects 

**Vision**: Delvin is used by organizations where multiple people can access the same project from different accounts.  

* [ ] Multiple users can access the same project 
* [ ] User can see both projects they own and projects shared with them 
* [ ] User can invite other existing users to their project; only project owner can invite users 
* [ ] If the invited user does not exist yet, they need to sign up and will then see the invite in their home screen. 


### Epic: Natural Language - User utterances

**Vision**: User can analyze natural language utterances by the chatbot contacts, assign intents to them and train a natural language model for intent recognition. 
* [ ] User can configure to log natural language utterances from user in certain contexts. 
* [ ] User can see all the messages logged in particular context 
* [ ] User can assign intent / tag to a user utterance 
