import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface Response {
  tagDescription: string;
  examples: string[];
  tagName: string;
  tagID: string;
}

const initialState: Response = {
  tagDescription: "",
  examples: [""],
  tagName: "",
  tagID: "",
};

const responseSlice = createSlice({
  name: "response",
  initialState,
  reducers: {
    setResponse: (state, action: PayloadAction<Response>) => {
      return { ...state, ...action.payload };
    },
  },
});

export const { setResponse } = responseSlice.actions;
export default responseSlice.reducer;
