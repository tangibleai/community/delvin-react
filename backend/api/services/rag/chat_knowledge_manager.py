import json
import logging
import os

from datetime import datetime as dt

from django.db import connection
from llama_index.core import (
    Document as LlamaDocument,
)  # Note: Not to be confused with models.Document
from llama_index.core import (
    Settings,
    StorageContext,
    VectorStoreIndex,
)
from llama_index.core.extractors import TitleExtractor
from llama_index.core.ingestion import IngestionPipeline
from llama_index.core.node_parser import SentenceSplitter
from llama_index.core.prompts import PromptTemplate
from llama_index.core.vector_stores import ExactMatchFilter, MetadataFilters
from llama_index.llms.litellm import LiteLLM
from llama_index.readers.file import PDFReader
from llama_index.readers.google import GoogleDocsReader
from llama_index.vector_stores.postgres import PGVectorStore
from sqlalchemy import make_url

from api.models import Chatbot, Document
from api.services.object_storage_file_manager import (
    ObjectStorageDocumentManager,
)
from api.services.rag.custom_embedding_api import CustomTextEmbeddingsInference
from api.utils.prompt_configuration import ChatbotConfig
from utils.text_generator import LLMResponseGeneration


from llama_index.core.indices.keyword_table import KeywordTableIndex
from llama_index.storage.docstore.postgres import PostgresDocumentStore

from llama_index.retrievers.bm25 import BM25Retriever
from llama_index.core.retrievers import QueryFusionRetriever
from llama_index.core.query_engine import RetrieverQueryEngine


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class BaseKnowledgeDocument:
    """Initializes project, model, and database settings for Knowledge document-related classes"""

    def __init__(self, project_id: str, chatbot_id: int, document_data={}):
        self.project_id = project_id
        self.chatbot_id = chatbot_id
        self.metadata = {
            "delvin_project_id": str(self.project_id),
            "delvin_chatbot_id": str(self.chatbot_id),
            "delvin_document_id": document_data.get("id", ""),
            "delvin_document_type": document_data.get("properties", {}).get(
                "doc_type", ""
            ),
            "status": "active",
            "document_title": document_data.get("title", ""),
            "document_content": document_data.get("content", ""),
            "date": str(dt.now()),
        }

        project_chatbot = Chatbot.objects.filter(id=chatbot_id).first()
        if not project_chatbot:
            raise ValueError("No Chatbot configuration found for the given project ID.")

        document_count = Document.objects.filter(project__id=project_id).count()
        self.has_documents = document_count > 0

        # Embedding and generation models LLama-index will use
        self.llm_api_key = project_chatbot.provider.api_key
        self.llm_model = project_chatbot.generative_model.model_string

        Settings.llm = LiteLLM(model=self.llm_model, api_key=self.llm_api_key)

        self.embedding_url = "https://ml-staging.delvin.to/api/v1.0/predictions"
        Settings.embed_model = CustomTextEmbeddingsInference(
            model_name="Delvin Custom Embedding",
            timeout=60,
            embed_batch_size=10,
            base_url=self.embedding_url,
        )

        # Document embedding and metadata extraction settings
        self.chunk_size = 512
        self.chunk_overlap = 128
        self.extractors = [TitleExtractor(nodes=5, llm=Settings.llm)]

        # Connection to ChatbotKnowledge database table
        connection_string = os.environ.get("DJANGO_DATABASE_URL")

        database_url = make_url(connection_string)
        self.vector_store = PGVectorStore.from_params(
            database=os.environ.get("DATABASE_NAME", "postgres"),
            host=database_url.host,
            password=database_url.password,
            port=database_url.port,
            user=database_url.username,
            table_name="chatbot_knowledge",  # Prepends "data_" and lowercased
            embed_dim=384,  # Different models have different dimensions
        )

        self.docstore = PostgresDocumentStore.from_params(
            database=os.environ.get("DATABASE_NAME", "postgres"),
            host=database_url.host,
            port=database_url.port,
            user=database_url.username,
            password=database_url.password,
            table_name=f"docstore_chatbot_knowledge",  # Prepends "data_" and lowercased
        )


class KnowledgeDocumentProcessor(BaseKnowledgeDocument):
    """Handles receiving, splitting, adding metadata, and embedding knowledge document content"""

    def __init__(self, project_id: str, chatbot_id: int, document_data={}):
        super().__init__(project_id, chatbot_id, document_data)

    def convert_single_form_document_to_llamaindex_document(self):
        """Converts Delvin Document.content LlamaIndex Document objects with hardcoded, general metadata

        Note: Both Delvin and LlamaIndex have Document classes, so LlamaIndex is prefixed as LlamaDocument
        """
        doc = LlamaDocument(
            id=self.metadata["delvin_document_id"],
            text=self.metadata["document_content"],
            metadata={
                "delvin_project_id": self.metadata["delvin_project_id"],
                "delvin_chatbot_id": self.metadata["delvin_chatbot_id"],
                "delvin_document_id": self.metadata["delvin_document_id"],
                "status": "active",
            },
        )
        docs = []
        docs.append(doc)  # NOTE: Later bulk ingest documents, now only single
        doc_nodes = self.split_documents_into_nodes_with_metadata(docs)
        return doc_nodes

    def convert_single_file_document_to_llamaindex_document(
        self, project_id, data_dict
    ):
        """Ingests metadata from a downloaded PDF file"""
        object_storage_manager = ObjectStorageDocumentManager(project_id, data_dict)
        docs = object_storage_manager.load_s3_files_as_docs()
        doc_nodes = self.split_documents_into_nodes_with_metadata(docs)
        return doc_nodes

    def convert_single_google_document_to_llamaindex_document(self, document_id):
        """Ingests a single Google Doc
        Does not save Google Doc to object storage
        """
        document_ids = [document_id]
        docs = GoogleDocsReader().load_data(document_ids=document_ids)
        doc_nodes = self.split_documents_into_nodes_with_metadata(docs)
        return doc_nodes

    def split_documents_into_nodes_with_metadata(self, docs):
        """Breaks documents into chunks and uses LLM to extract metadata"""
        for doc in docs:
            doc.metadata.update(self.metadata)

        text_splitter = SentenceSplitter()
        transformations = [text_splitter] + self.extractors
        pipeline = IngestionPipeline(transformations=transformations)
        doc_nodes = pipeline.run(documents=docs)
        return doc_nodes


class KnowledgeDocumentIndexManager(BaseKnowledgeDocument):
    """Manages interactions with the knowledge document table - data_chat_knowledge

    Table Information
    - vector_store and docstore tables are unmanaged by Django ORM (no model)
    - llama_index creates and updates these tables

    - vector_store table
      * text - TextField - original source material embeddings generated from
      * metadata - JSONField - manual and LLM-generated information about the text (not compatible with Django ORM deserializer)
      * node_id - TextField - the id for a chunk of text that has been embedded
      * embedding - VectorField - a list of floats generated by Delvin's embedding service that represent the text

    - docstore table
      * key - vector_store.node_id
      * namespace - label of information type in the value column (docstore/data, docstore/metadata, docstore/ref_doc_info)
      * value - A JSON object containing information about a knowledge document
    """

    def __init__(self, project_id: str, chatbot_id: int, document_data={}):
        super().__init__(project_id, chatbot_id, document_data)

    # UTILS #####################################
    #############################################
    def set_query_engine(self, index, response_mode=""):
        """Applies settings for how to query the VectorStore index"""

        filters = MetadataFilters(
            filters=[
                ExactMatchFilter(
                    key="delvin_project_id", value=self.metadata["delvin_project_id"]
                ),
            ]
        )

        kwargs = dict(
            filters=filters,
            # similarity_top_k=3,
            # vector_store_query_mode="default",
            # alpha=None,
            # doc_ids=None,
        )
        if response_mode:
            kwargs["response_mode"] = response_mode
        query_engine = index.as_query_engine(**kwargs)

        return query_engine

    # RETRIEVE ##################################
    #############################################
    def retrieve_vector_store(self):
        """Retrieves existing nodes and metadata from a postgres vector store"""
        index = VectorStoreIndex.from_vector_store(vector_store=self.vector_store)
        return index

    def provide_knowledge_context(self, query):
        """Returns only the knowledge sources to incorporate into the chatbot's prompt"""

        if not self.has_documents:
            return None, 0.0

        index = self.retrieve_vector_store()
        query_engine = self.set_query_engine(index, response_mode="no_text")
        response = query_engine.query(query)

        try:
            max_confidence_score = response.source_nodes[0].score
            info = ["\n".join([node.get_content() for node in response.source_nodes])]
        except IndexError:
            max_confidence_score = 0.0
            info = None

        return (info, max_confidence_score)

    # CREATE / UPDATE #########################
    ###########################################
    def create_or_update_vector_store(self, nodes, db_action="create"):
        """Converts Document.content to vectors and saves it to db

        If a vector store already exists, the data will be added to existing data
        """
        if db_action == "update":
            self.delete_docstore_record()
            self.deactivate_old_knowledge_base_document()

        index = self.set_index(nodes)
        index = []
        return index

    def set_index(self, nodes):
        """Returns an index drawn from the VectorStore"""
        storage_context = StorageContext.from_defaults(vector_store=self.vector_store)
        index = VectorStoreIndex(
            nodes, storage_context=storage_context, show_progress=True
        )
        return index

    def set_docstore(self, nodes):
        """ """
        logger.info("start set_docstore")
        storage_context = StorageContext.from_defaults(docstore=self.docstore)
        keyword_index = KeywordTableIndex(
            nodes,
            storage_context=storage_context,
        )
        docstore = keyword_index
        logger.info("finish set_docstore")
        return docstore

    # DELETE ##################################
    ###########################################
    def deactivate_old_knowledge_base_document(self):
        """Deletes old version of the metadata when the user uploads a new version of the document

        Changes the metadata status key's value to 'inactive' when the user uploads a new version of the document
        """
        documents_to_update = []
        delvin_document_id = self.metadata["delvin_document_id"]

        with connection.cursor() as cursor:
            cursor.execute(
                f"""
                SELECT 
                    id, metadata_ 
                FROM 
                    data_chatbot_knowledge 
                WHERE 
                    COALESCE(NULLIF((metadata_->>'delvin_document_id')::text, ''), '0')::int = {delvin_document_id};
                """
            )
            documents_to_update = cursor.fetchall()

        # https://stackoverflow.com/questions/18209625/how-do-i-modify-fields-inside-the-new-postgresql-json-datatype
        for update_doc in documents_to_update:
            document_id = update_doc[0]
            with connection.cursor() as cursor:
                cursor.execute(
                    f"DELETE FROM data_chatbot_knowledge WHERE id={document_id}"
                )
                connection.commit()

    def delete_chat_knowledge_document(self):
        """Deletes ChatbotKnowledge documents that correspond to a Document scheduled for deletion

        Because Documents are split into nodes, there may be many ChatbotKnowledge documents for one Document
        """
        documents_to_update = []
        delvin_document_id = self.metadata["delvin_document_id"]
        with connection.cursor() as cursor:
            cursor.execute(
                f"""
                SELECT 
                    id, metadata_ 
                FROM 
                    data_chatbot_knowledge 
                WHERE 
                    COALESCE(NULLIF((metadata_->>'delvin_document_id')::text, ''), '0')::int = {delvin_document_id};
                """
            )
            documents_to_update = cursor.fetchall()

        for update_doc in documents_to_update:
            document_id = update_doc[0]
            with connection.cursor() as cursor:
                cursor.execute(
                    f"DELETE FROM data_chatbot_knowledge WHERE id={document_id}"
                )
                connection.commit()

    def delete_docstore_record(self):
        """Deletes three associated records that correspond to a vector_store node record

        The first two records use the node id
        The last node uses the record filepath
        """
        delvin_document_id = self.metadata["delvin_document_id"]

        with connection.cursor() as cursor:
            cursor.execute(
                f"""
                SELECT 
                    id, metadata_ 
                FROM 
                    data_chatbot_knowledge 
                WHERE 
                    COALESCE(NULLIF((metadata_->>'delvin_document_id')::text, ''), '0')::int = {delvin_document_id};
                """
            )
            documents = cursor.fetchall()

            ref_doc_id = documents[0][1]["ref_doc_id"]

            node_content = json.loads(documents[0][1]["_node_content"])
            node_id = node_content["id_"]

            cursor.execute(
                f"""
                DELETE FROM data_docstore_chatbot_knowledge WHERE key='{node_id}'
                """
            )
            connection.commit()

            cursor.execute(
                f"""
                DELETE FROM data_docstore_chatbot_knowledge WHERE key='{ref_doc_id}'
                """
            )
            connection.commit()

    def delete_all_project_document_metadata(self):
        """Removes all knowledge documents associated with a project ID"""
        delvin_document_id = self.metadata["delvin_document_id"]
        with connection.cursor() as cursor:
            cursor.execute(
                f"""
                DELETE FROM data_chatbot_knowledge
                WHERE
                    COALESCE(NULLIF((metadata_->>'delvin_document_id')::text, ''), '0')::int = {delvin_document_id};
                """
            )
            connection.commit()


class KnowledgeDocumentQueryResponder(BaseKnowledgeDocument):
    """Builds a query engine based on settings and builds a response to a user query"""

    def __init__(self, project_id: str, chatbot_id: int, document_data={}):
        super().__init__(project_id, chatbot_id, document_data)

    def create_prompt_from_config(self, chatbot_config: ChatbotConfig):
        knowledge_prompt = f"""
{chatbot_config.general_prompt.persona}

Your task is to answer a user's questions about the information you are provided.  
You will only answer the question using the Provided Information.
If a question cannot be answered with the provided information, {chatbot_config.knowledge.fallback}

Provided Information
---------------------
{{context_str}}
---------------------
Given the Provided Information and not prior knowledge, answer the query in a concise, clear way.
If a question cannot be answered with the provided information, {chatbot_config.knowledge.fallback}

{chatbot_config.general_prompt.constraints}


Query: {{query_str}}
Answer:
"""
        return PromptTemplate(knowledge_prompt)

    def retrieve_confidence_score(self, query_response):
        """Extracts the confidence score from an answer to a query"""
        try:
            confidence_score = query_response.source_nodes[0].score
        except IndexError:
            confidence_score = 0.0
        return confidence_score

    def build_basic_query_engine(self, index):
        """Queries the vector store index for items related to the user query"""
        knowledge_document_index_manager = KnowledgeDocumentIndexManager(
            self.project_id, self.chatbot_id
        )
        query_engine = knowledge_document_index_manager.set_query_engine(index)
        return query_engine

    def build_fusion_query_engine(self, index, k=1):
        """Searches both a vector store and a docstore items related to the user query

        Search types
        - vector search - embedding similarity
        - docstore - text similarity
        """
        docstore = self.docstore

        # NOTE: llama index will fail if k is larger than the available documents in the docstore
        vector_retriever = index.as_retriever(similarity_top_k=1)
        bm25_retriever = BM25Retriever.from_defaults(
            docstore=docstore, similarity_top_k=k
        )
        retriever = QueryFusionRetriever(
            [vector_retriever, bm25_retriever],
            similarity_top_k=k,
            num_queries=4,  # set this to 1 to disable query generation
            mode="reciprocal_rerank",
            use_async=False,
            verbose=True,
            # query_gen_prompt="...",  # we could override the query generation prompt here
        )
        query_engine = RetrieverQueryEngine.from_args(retriever)
        return query_engine

    def answer_user_query(
        self, query, chatbot_config: ChatbotConfig = None, query_engine_type="fusion"
    ):
        """Builds an index, queries it, and generates a response from the retrieve items"""
        if not self.has_documents:
            return None, 0.0

        knowledge_document_index_manager = KnowledgeDocumentIndexManager(
            self.project_id, self.chatbot_id
        )
        index = knowledge_document_index_manager.retrieve_vector_store()

        if query_engine_type == "fusion":
            query_engine = self.build_fusion_query_engine(index)
        else:
            query_engine = self.build_basic_query_engine(index)

        if chatbot_config:
            prompt_template = self.create_prompt_from_config(chatbot_config)
            query_engine.update_prompts(
                {"response_synthesizer:text_qa_template": prompt_template}
            )
        try:
            response = query_engine.query(query)
        except Exception as e:
            logging.warning("Unsuccessful LLM call during RAG:", e)
        confidence_score = self.retrieve_confidence_score(response)

        return str(response), confidence_score
