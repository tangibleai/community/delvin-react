import WorkspaceHome from "@/modules/workspace_home/pages/WorkspaceHome";

import withAuth from "@/lib/with-auth";

import { WORKSPACE_PAGE_URLS } from "./pageUrls";
import proceedToWorkspaceHomeOrShowErrorPage from "@/modules/workspace_home/pages/middlewares/checkAccess";

// the order must be preserved to make sure on user has access to workspace stage the user is loaded
const WorkspaceHomeWithCheckedAccess =
  proceedToWorkspaceHomeOrShowErrorPage(WorkspaceHome);
const WorkspaceHomeWithAuth = withAuth(WorkspaceHomeWithCheckedAccess);

export const ROUTES = [
  {
    path: WORKSPACE_PAGE_URLS.workspace,
    element: <WorkspaceHomeWithAuth />,
  },
];
