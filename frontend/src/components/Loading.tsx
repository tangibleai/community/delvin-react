import React from "react";
import { Oval } from "react-loader-spinner";

const Loading = () => {
  return (
    <div className="flex flex-col justify-center items-center min-h-screen bg-white">
      <Oval
        visible={true}
        height="50"
        color="#a29bfe"
        strokeWidth="5"
        secondaryColor="#f6f5fe"
        ariaLabel="oval-loading"
        wrapperStyle={{}}
        wrapperClass=""
      />
      <div className="mt-5 text-lg text-gray-600">Loading...</div>
    </div>
  );
};

export default Loading;
