from django.urls import path

from api.internal import (
    chats,
    chatbots,
    contacts,
    documents,
    generative_models,
    guardrail_actions,
    guardrail_responses,
    messages,
    message_tags,
    message_tag_definitions,
    message_tag_groups,
    message_tag_rules,
    ml_models,
    projects,
    project_collaborators,
    project_home,
    flows,
    provider_types,
    providers,
    users,
    workspaces,
    team_members,
    plans,
)
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path("charts/project-home/", project_home.get_home_charts),
    path("chats/", chats.chats_list_view, name="chats_list"),
    path(
        "chats/<uuid:chat_id>/messages/",
        chats.chat_message_list_view,
        name="chat_messages",
    ),
    path("chats/messages/", chats.chat_messages_view),
    path("chats/create/", chats.create_chat_view, name="create_chat_view"),
    # TODO Waits decision
    # The below URLs could be changed. But needs front and back-end update. But contact ID or chat ID has to be sent in the URL
    # path("chats/<uuid:pk>/earliest-chat-message-id/", chats.get_earliest_chat_message_id, ),
    # path("chats/<uuid:pk>/last-message/", chats.last_chat_message_view),
    # path("chats/<uuid:pk>/last-messages-by-timestamp/", chats.last_few_chat_messages_before_timestamp, ),
    # path("chats/<uuid:pk>/messages/", chats.chat_messages_view),
    # path("contacts/<uuid:pk>/chats/", chats.contact_chats_view),
    path("contact/chats/", chats.contact_chats_view),
    path("contacts/create/", contacts.create_contact_view, name="create_contact_view"),
    path(
        "documents/",
        documents.document_get_api_view,
    ),
    path("documents/create/", documents.create_knowledge_document_api_view),
    path("documents/<int:id>/update/", documents.update_knowledge_document_api_view),
    path("documents/<int:id>/delete/", documents.document_delete_api_view),
    path("guardrail-actions/", guardrail_actions.guardrail_action_list_view),
    path(
        "guardrail-actions/<uuid:pk>/update/",
        guardrail_actions.guardrail_action_update_view,
    ),
    path("guardrail-actions/create/", guardrail_actions.guardrail_action_create_view),
    path("guardrail-responses/", guardrail_responses.guardrail_response_list_view),
    path(
        "guardrail-responses/<uuid:pk>/",
        guardrail_responses.guardrail_response_retrieve_view,
    ),
    path(
        "guardrail-responses/<uuid:pk>/delete/",
        guardrail_responses.guardrail_response_delete_view,
    ),
    path(
        "guardrail-responses/create/",
        guardrail_responses.guardrail_response_create_view,
    ),
    path(
        "message-tag-definitions/",
        message_tag_definitions.message_tag_definitions_view,
        name="message_tag_definitions_view",
    ),
    path(
        "message-tag-definition/<uuid:pk>/delete/",
        message_tag_definitions.remove_message_tag_definition_view,
        name="alter_message_tag_definition_view",
    ),
    path("message-tag-group/create/", message_tag_groups.message_tag_group_create_view),
    path("message-tag-rule/create/", message_tag_rules.message_tag_rule_create_view),
    path("message-tags/", message_tags.message_tag_view, name="message_tags_view"),
    path(
        "message-tag/<int:pk>/delete/",
        message_tags.untag_message_view,
        name="alter_message_tag",
    ),
    path(
        "message-tags/with-tag/",
        message_tags.message_tags_with_tag_view,
        name="messages_with_specified_tag",
    ),
    path("understanding/predict-tags/", message_tags.predict_tags_view),
    path(
        "project/untag_message/",
        message_tags.untag_message_view,
    ),
    # TODO
    path("messages/", messages.message_list_view),
    path("ml-models/", ml_models.ml_model_view),
    path("project/retrain_model/", ml_models.retrain_model_view),
    # TODO Waits decision
    # path("ml-models/", ml_models.ml_model_view),
    # path("ml-models/retrain-model/", ml_models.retrain_model_view),
    path("project/", projects.project_list_view),
    path("project/<uuid:pk>/delete/", projects.project_delete_view),
    path("project/<uuid:id>/details/", projects.project_detail_view),
    path("project/<uuid:pk>/update-token/", projects.project_update_token_view),
    path("project/<uuid:pk>/update/", projects.project_update_view),
    path("project/create/", projects.project_create_view, name="project_create_view"),
    path(
        "project/<uuid:pk>/toggle-builtin-moderation/",
        projects.toggle_builtin_moderation_view,
    ),
    path(
        "project/<uuid:pk>/update-builtin-moderation-category/",
        projects.update_builtin_moderation_category_view,
    ),
    # TODO
    # path("projects/", projects.project_list_view),
    # path("projects/<uuid:pk>/", projects.project_detail_view),
    # path("projects/<uuid:pk>/delete/", projects.project_delete_view),
    # path("projects/<uuid:pk>/update-token/", projects.project_update_token_view),
    # path("projects/<uuid:pk>/update/", projects.project_update_view),
    # path("projects/create/", projects.project_create_view, name="project_create_view"),
    path("contact/<uuid:project_id>/", contacts.contact_list_view),
    path("contacts/", contacts.contact_list_view),
    # TODO (Maria will talk to Greg)
    # path("projects/<uuid:pk>/contacts/", contacts.contact_list_view),
    path(
        "project-collaborators/",
        project_collaborators.project_collaborator_by_project_view,
    ),
    path(
        "project-collaborators/create/",
        project_collaborators.project_collaborator_add_view,
    ),
    path(
        "project-collaborators/<int:id>/update/",
        project_collaborators.project_collaborator_update_view,
    ),
    path(
        "project-collaborators/<int:id>/delete/",
        project_collaborators.project_collaborator_delete_view,
    ),
    path(
        "team-members/",
        team_members.team_members_view,
    ),
    path(
        "team-members/create/",
        team_members.team_member_create_view,
    ),
    path(
        "team-members/<int:pk>/update/",
        team_members.team_member_update_view,
    ),
    path(
        "team-members/<int:id>/delete/",
        team_members.team_member_delete_view,
    ),
    path(
        "team-members/<str:uid>/confirm-invitation/",
        team_members.confirm_invitation,
    ),
    path(
        "project-collaborators/<str:uid>/confirm-invitation/",
        project_collaborators.confirm_invitation,
    ),
    path(
        "project/internal_usage/chatbot_response/",
        chatbots.get_chatbot_response_for_internal_usage,
        name="get_chatbot_response_for_internal_usage_view",
    ),
    path("chatbots/", chatbots.chatbots_list_view, name="chatbots_list_view"),
    path("chatbots/create/", chatbots.create_chatbot_view, name="create_chatbot_view"),
    path(
        "chatbots/<int:pk>/update/",
        chatbots.update_chatbot_view,
        name="update_chatbot_view",
    ),
    path(
        "chatbots/<int:pk>/delete/",
        chatbots.chatbot_delete_view,
        name="delete_chatbot_view",
    ),
    # TODO
    # The below URLs could be changed. But needs front and back-end update. Project ID has to be sent in the URL
    # path("projects/<uuid:pk>/chatbots/info/", chatbots.get_chatbot_info_view),
    # path("projects/<uuid:pk>/chatbots/responses/", chatbots.get_chatbot_response_for_internal_usage, ),
    # path("projects/<uuid:pk>/chatbots/reset-active-chat/", chatbots.reset_chatbot_active_chat_view),
    # path("projects/<uuid:pk>/chatbots/<uuid:pk>/update/", chatbots.update_chatbot_view),  # View has to be modified to, ID was sent in the body
    # path("projects/<uuid:pk>/chatbots/create/", chatbots.create_chatbot_view),  # not used
    path("projects/list/", projects.projects_list_view),
    path("users/", users.users_list_view),
    path("user/<uuid:pk>/delete/", users.user_delete_view),
    path("user/<uuid:pk>/details/", users.user_details_view),
    path("users/<uuid:pk>/update/", users.user_update_view),
    path("user/<uuid:pk>/reset_password/", users.reset_user_password_view),
    path("workspaces/", workspaces.workspaces_list_view),
    path("workspace/create/", workspaces.workspace_create_view),
    path("workspace/<uuid:id>/details/", workspaces.workspace_details_view),
    path("workspace/<uuid:pk>/update/", workspaces.workspace_update_view),
    path("workspace/<uuid:pk>/delete/", workspaces.workspace_delete_view),
    path(
        "flows/",
        flows.flows_list_view,
        name="flows_view",
    ),
    path(
        "flows/create/",
        flows.create_flow_view,
        name="create_flow_view",
    ),
    path("flows/<uuid:id>/details/", flows.flow_details_view),
    path(
        "flows/<uuid:pk>/delete/",
        flows.delete_flow_view,
        name="delete_flow_view",
    ),
    path(
        "flows/<uuid:id>/update/",
        flows.save_flow_graph_view,
        name="update_flow_graph",
    ),
    path("providers/", providers.ProvidersListView.as_view(), name="providers_list"),
    path(
        "providers/create/",
        providers.ProviderCreateView.as_view(),
        name="create_provider",
    ),
    path(
        "providers/<uuid:pk>/delete/",
        providers.ProviderDeleteView.as_view(),
        name="delete_provider",
    ),
    path(
        "provider-types/",
        provider_types.ProviderTypeListView.as_view(),
        name="provider_types_list",
    ),
    path(
        "generative-models/",
        generative_models.GenerativeModelListView.as_view(),
        name="generative_models",
    ),
    path("plans/<uuid:id>/details/", plans.PlanDetailsView.as_view()),
    path("plans/<uuid:pk>/update/", plans.UpdatePlanView.as_view()),
]
