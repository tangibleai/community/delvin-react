import React from "react";
import { Button } from "@/components/ui/button";
import { ScrollArea } from "@/components/ui/scroll-area";
import { PROJECT_PAGE_URLS } from "@/routes/pageUrls";
import { useNavigate, useParams } from "react-router-dom";
import { ChatbotConfig } from "@/typings/chatbot_config";
import { useListChatbotsQuery } from "@/slices/api/chatbotsApiSlice";
import { Skeleton } from "@/components/ui/skeleton";

type Props = {
  selectedChatbot: ChatbotConfig;
  setSelectedChatbot: React.Dispatch<React.SetStateAction<ChatbotConfig>>;
};

const ChatbotsPanel = ({ selectedChatbot, setSelectedChatbot }: Props) => {
  const navigate = useNavigate();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const { data: chatbots = [], isLoading: retrievingChatbots } =
    useListChatbotsQuery({ project_id: projectId });

  const redirectToOnboarding = () => {
    return navigate(`/project/${projectId}/init_assistant`);
  };

  const switchChatbot = async (chatbot: ChatbotConfig) => {
    setSelectedChatbot(chatbot);
  };

  return (
    <div className="flex flex-col w-1/4 p-4 border-r border-gray-200 gap-3">
      <div className="w-full text-center">
        <Button className="w-3/4 text-base" onClick={redirectToOnboarding}>
          Add Assistant
        </Button>
      </div>

      <ScrollArea className="h-full py-2 w-full">
        <div className="flex flex-col gap-3">
          {retrievingChatbots
            ? Array.from({ length: 5 }).map((_, index) => (
                <Skeleton key={index} className="h-10" />
              ))
            : chatbots.map((chatbot) => (
                <div
                  key={chatbot.id}
                  className={`p-2 cursor-pointer border rounded text-lg ${
                    selectedChatbot.id === chatbot.id ? "bg-muted" : ""
                  }`}
                  onClick={() => switchChatbot(chatbot)}
                >
                  {chatbot.origin}
                </div>
              ))}
        </div>
      </ScrollArea>
    </div>
  );
};

export default ChatbotsPanel;
