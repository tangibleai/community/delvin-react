export const MLModelStatuses = {
  UP_TO_DATE: "up-to-date",
  NEEDS_RETRAINING: "needs-retraining",
  TRAINING: "training",
} as const;

type MLModelStatus = (typeof MLModelStatuses)[keyof typeof MLModelStatuses];

export interface MLModel {
  id: string;
  project: string; // uuid
  name: string;
  description: string;
  dataset: string; // uuid
  status: MLModelStatus;
  created_by: string; // uuid
}
