from django.http import JsonResponse
from celery.result import AsyncResult
from rest_framework.response import Response
from rest_framework.decorators import api_view


def trigger_error(request):
    """Create an error to test sentry"""
    1 / 0


def api_404(request):
    """Returns 404 for paths that does not exist within api"""
    return JsonResponse({"error": "Not Found"}, status=404)


@api_view(["GET"])
def get_progress(request, task_id):
    result = AsyncResult(task_id)
    response_data = {
        "state": result.state,
        "details": result.info,
    }
    return Response(response_data)
