

# Stage 1: Node.js builder
FROM node:18-alpine AS frontend-builder

ARG URL
ARG SENTRY_ENVIRONMENT
ARG SENTRY_DSN

ENV VITE_REACT_APP_API_URL=${URL}
ENV VITE_SENTRY_ENVIRONMENT=${SENTRY_ENVIRONMENT}
ENV VITE_SENTRY_DSN=${SENTRY_DSN}

WORKDIR /app/frontend
COPY ./frontend/package*.json /app/frontend
RUN npm install
COPY ./frontend /app/frontend
RUN npm run build

# Stage 2: Python builder
FROM python:3.11-buster AS backend-builder
ENV PYTHONUNBUFFERED 1

WORKDIR /app/backend
COPY ./backend/requirements.txt /app/backend
RUN python -m venv /opt/venv
RUN /opt/venv/bin/pip install --upgrade pip
RUN /opt/venv/bin/pip install -r requirements.txt
COPY ./backend /app/backend

# Final stage
FROM python:3.11-slim-buster
WORKDIR /app/backend
COPY --from=frontend-builder /app/frontend /app/frontend
COPY --from=backend-builder /app/backend /app/backend
COPY --from=backend-builder /opt/venv /opt/venv
COPY ./entrypoint.sh /app

COPY ./test.sh /app

CMD ["/app/entrypoint.sh"]
