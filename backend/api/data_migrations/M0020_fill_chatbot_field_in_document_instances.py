from api.models import Chatbot, Document


def fill_chatbot_field():
    """Fills default value for Document.chatbot related to 0019 migration

    This prepares Delvin's existing data
    for Document.chatbot to be non-nullable
    """
    for document in Document.objects.all():
        project = document.project
        chatbots = Chatbot.objects.filter(project=project)

        default_assistant = chatbots.filter(origin="default_assistant").first()
        first_assistant = chatbots.first()

        # Prioritize default_assistant, but fallback to first() assistant
        if default_assistant:
            document.chatbot = default_assistant
        else:
            document.chatbot = first_assistant
        document.save()


if __name__ == "__main__":
    fill_chatbot_field()
