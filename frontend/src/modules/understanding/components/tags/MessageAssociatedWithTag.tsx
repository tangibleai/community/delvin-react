import { AiOutlineDelete } from "react-icons/ai";
import { Badge } from "@/components/ui/badge";
import { TaggedMessage } from "@/typings/tagged_message";
import { MutationTrigger } from "@reduxjs/toolkit/dist/query/react/buildHooks";
import { BaseQueryFn, MutationDefinition } from "@reduxjs/toolkit/query";
import { AxiosRequestConfig } from "axios";
import { useToast } from "@/components/ui/use-toast";

type Props = {
  taggedMessage: TaggedMessage;
  deleteMessageTag: MutationTrigger<
    MutationDefinition<
      string,
      BaseQueryFn<
        {
          url: string;
          method?: AxiosRequestConfig["method"];
          data?: AxiosRequestConfig["data"];
          params?: AxiosRequestConfig["params"];
          headers?: AxiosRequestConfig["headers"];
        },
        unknown,
        unknown
      >,
      "MessageTag",
      null,
      "api"
    >
  >;
};

const MessageAssociatedWithTag = ({
  taggedMessage,
  deleteMessageTag,
}: Props) => {
  const { toast } = useToast();

  const handleTagDeleted = async (messageTagId: string) => {
    try {
      await deleteMessageTag(messageTagId).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Could not untag message. Please, try again.",
      });
      return;
    }
    toast({
      title: "Success!",
      description: "Message was successfully untagged.",
    });
  };

  return (
    <div className="flex flex-col p-2 mb-2 bg-white rounded shadow cursor-pointer">
      <div className="flex justify-between items-center mb-2">
        <p>{taggedMessage.message.text}</p>
        <AiOutlineDelete
          size={"1.5rem"}
          onClick={() => {
            handleTagDeleted(taggedMessage.id);
          }}
        />
      </div>
      <div className="flex flex-wrap">
        {taggedMessage.message_tags.length > 0 &&
          taggedMessage.message_tags.map((tag, index) => (
            <Badge key={index}>{tag.name}</Badge>
          ))}
      </div>
    </div>
  );
};

export { MessageAssociatedWithTag };
