from django.utils.translation import gettext_lazy as _
from rest_framework import serializers, generics
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import GuardrailAction


class GuardrailActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = GuardrailAction
        fields = "__all__"


class GuardrailActionListAPIView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = GuardrailAction.objects.all()
    serializer_class = GuardrailActionSerializer


guardrail_action_list_view = GuardrailActionListAPIView.as_view()


class GuardrailActionCreateAPIView(generics.CreateAPIView):
    """Guardrail action is created within guardrail response
    This endpoint is optional"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = GuardrailAction.objects.all()
    serializer_class = GuardrailActionSerializer


guardrail_action_create_view = GuardrailActionCreateAPIView.as_view()


class GuardrailActionUpdateAPIView(generics.UpdateAPIView):
    """The frontend updates the guardrail action, not the guardrail response."""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = GuardrailAction.objects.all()
    serializer_class = GuardrailActionSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)


guardrail_action_update_view = GuardrailActionUpdateAPIView.as_view()
