import logging

from api.models import Plan, Workspace

logger = logging.getLogger(__name__)


def assign_default_plan():
    for workspace in Workspace.objects.filter(plan__isnull=True):
        logger.debug(workspace)
        # Create a default plan for each workspace without a plan
        default_plan = Plan.objects.create(
            type=Plan.Type.FREE,
            status=Plan.Status.ACTIVE,
            properties={"free_message": 0},
        )
        workspace.plan = default_plan
        workspace.save()


def remove_assigned_plans():
    for workspace in Workspace.objects.all():
        workspace.plan = None
        workspace.save()

    Plan.objects.all().delete()

if __name__ == "__main__":
    assign_default_plan()