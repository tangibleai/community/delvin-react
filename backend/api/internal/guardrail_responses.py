from django.db import transaction
from drf_spectacular.utils import OpenApiExample, extend_schema
from rest_framework import generics, serializers, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.internal.guardrail_actions import GuardrailActionSerializer
from api.internal.message_tag_definitions import MessageTagDefinitionSerializer
from api.models import (
    Contact,
    GuardrailAction,
    GuardrailResponse,
    MLModel,
    Message,
    MessageTag,
    MessageTagDefinition,
    Project
)
from api.utils.views import retrain_model
from core.constant import CONSTANTS
from scripts.ml import embedding


class GuardrailResponseListSerializer(serializers.ModelSerializer):
    message_tag_definition = MessageTagDefinitionSerializer(read_only=True)
    guardrail_action = GuardrailActionSerializer(read_only=True)

    class Meta:
        model = GuardrailResponse
        fields = [
            "id",
            "guardrail_action",
            "project",
            "message_tag_definition",
        ]


class GuardrailResponseCreateSerializer(serializers.ModelSerializer):
    examples = serializers.ListSerializer(child=serializers.CharField(), required=False)
    message_tags = serializers.ListSerializer(child=serializers.CharField(), required=False, write_only=True)

    def validate(self, data):
        """Remove message tags before validation and to the context"""
        message_tags = data.pop('message_tags', None)
        if message_tags:
            self.context['message_tags'] = message_tags
        return super().validate(data)

    def to_representation(self, instance):
        """Add message tags to the validated data
        Replace guardrail action id and message tag definition id with the serialized object.
        """
        data = super().to_representation(instance)
        tags = self.context.get('message_tags', [])
        data['message_tags'] = tags
        data['guardrail_action'] = GuardrailActionSerializer(
            GuardrailAction.objects.get(pk=data['guardrail_action'])).data
        data['message_tag_definition'] = MessageTagDefinitionSerializer(
            MessageTagDefinition.objects.get(pk=data['message_tag_definition'])).data
        return data

    class Meta:
        model = GuardrailResponse
        fields = '__all__'
        read_only_fields = ['id']


class GuardrailResponseDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = GuardrailResponse
        fields = "__all__"


class GuardrailResponseListAPIView(generics.ListAPIView):
    """Lists all project guardrail responses"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = GuardrailResponse.objects.all()
    serializer_class = GuardrailResponseListSerializer

    def get_queryset(self):
        project_id = self.request.query_params.get("project_id")
        queryset = self.queryset.filter(project=project_id)
        return queryset


guardrail_response_list_view = GuardrailResponseListAPIView.as_view()


class GuardrailResponseRetrieveAPIView(generics.RetrieveAPIView):
    """Returns one guardrail response. This view is optional"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = GuardrailResponse.objects.all()
    serializer_class = GuardrailResponseListSerializer
    lookup_field = "pk"


guardrail_response_retrieve_view = GuardrailResponseRetrieveAPIView.as_view()


@extend_schema(
    request=GuardrailResponseCreateSerializer,
    examples=[
        OpenApiExample(
            'GuardrailResponseExample',
            summary='GuardrailResponse request object',
            description='This is a detailed example GuardrailResponse request object.',
            value={
                'guardrail_action': {
                    'action_type': 'scripted | generative',
                    'content': 'Some content here'
                },
                'project': '123e4567-e89b-12d3-a456-426614174000',
                'message_tag_definition': {
                    'id': '123e4567-e89b-12d3-a456-426614174000',
                    'name': 'TagName',
                    'description': 'Description of the tag',
                    'project': '123e4567-e89b-12d3-a456-426614174000',
                    'message_tag_group': 'null'
                },
                'examples': ['hi', 'hello', 'hey', 'howdy', 'salut']
            },
            request_only=True,  # Optional: Show example only for request body
        ),
        OpenApiExample(
            'GuardrailRequestExample',
            summary='GuardrailResponse response object',
            description='This is a detailed example GuardrailResponse response object.',
            value={
                'guardrail_action': {
                    'action_type': 'scripted | generative',
                    'content': 'Some content here'
                },
                'project': '123e4567-e89b-12d3-a456-426614174000',
                'message_tag_definition': {
                    'id': '123e4567-e89b-12d3-a456-426614174000',
                    'name': 'TagName',
                    'description': 'Description of the tag',
                    'project': '123e4567-e89b-12d3-a456-426614174000',
                    'message_tag_group': 'null'
                },
                'message_tags': [155, 156, 157, 158, 159],
            },
            response_only=True,  # Optional: Show example only for request body
        )
    ]
)
class GuardrailResponseCreateAPIView(generics.CreateAPIView):
    queryset = GuardrailResponse.objects.all()
    serializer_class = GuardrailResponseCreateSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    @staticmethod
    def tag_message_examples(examples, project, message_tag_definition):
        contact = Contact.objects.get_or_create(
            origin=CONSTANTS.DEFAULT_DATASET_CONTACT_FOR_ANY_USER,
            project=project,
            internal=True,
        )[0]
        ml_model = MLModel.objects.filter(project=project.id).first()
        message_tags = []
        for message_text in examples:
            text_embedding = embedding.embed_text(message_text)
            message = Message.objects.create(
                direction=Message.Direction.OUTBOUND,
                sender_type=Message.SenderType.USER,
                channel_type=Message.ChannelType.WEB,
                text=message_text,
                embedding=text_embedding,
                project=project,
                contact=contact,
                chat=None,
            )
            message_tag = MessageTag.objects.create(message=message, tag=message_tag_definition, ml_model=ml_model)
            message_tags.append(message_tag.pk)

        retrain_model(project.id, ml_model)

        ml_model.status = MLModel.Status.UP_TO_DATE
        ml_model.save()

        return message_tags

    @transaction.atomic()
    def create(self, request, *args, **kwargs):
        data = request.data
        project_id = data.get("project")
        examples = data.pop("examples", None)

        if data.get("message_tag_definition") and data.get("message_tag_definition").get("id"):
            message_tag_definition_id = data['message_tag_definition']['id']
            try:
                message_tag_definition = MessageTagDefinition.objects.get(pk=message_tag_definition_id)
            except MessageTagDefinition.DoesNotExist:
                return Response({'error': 'Invalid message_tag_definition id'}, status=status.HTTP_400_BAD_REQUEST)
            data['message_tag_definition'] = message_tag_definition.id
        else:
            message_tag_definition_data = data.pop('message_tag_definition', None)

            message_tag_definition_serializer = MessageTagDefinitionSerializer(data=message_tag_definition_data)
            message_tag_definition_serializer.is_valid(raise_exception=True)
            message_tag_definition = message_tag_definition_serializer.save()
            data['message_tag_definition'] = message_tag_definition.id

            data["message_tags"] = self.tag_message_examples(examples, Project.objects.get(pk=project_id),
                                                             message_tag_definition)

        guardrail_action_data = data.pop('guardrail_action', None)

        guardrail_action_serializer = GuardrailActionSerializer(data=guardrail_action_data)
        guardrail_action_serializer.is_valid(raise_exception=True)
        guardrail_action = guardrail_action_serializer.save()
        data['guardrail_action'] = guardrail_action.id

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


guardrail_response_create_view = GuardrailResponseCreateAPIView.as_view()


class GuardrailResponseDeleteAPIView(generics.DestroyAPIView):
    """Deletes the guardrail response and related guardrail action"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = GuardrailResponse.objects.all()
    serializer_class = GuardrailResponseDeleteSerializer


guardrail_response_delete_view = GuardrailResponseDeleteAPIView.as_view()
