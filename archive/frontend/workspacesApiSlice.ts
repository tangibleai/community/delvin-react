// import { createEntityAdapter, createSelector, EntityState } from "@reduxjs/toolkit";
// import emptyApi from "../apiSlice";
// import { NewWorkspace, Workspace } from "@/typings/workspace";
// import { RootState } from "@/store";
// import { selectCurrentUserId } from "../authSlice";

// const apiWithTags = emptyApi.enhanceEndpoints({addTagTypes: ['Workspaces']})


// const workspacesAdapter = createEntityAdapter<Workspace>()
// const initialState = workspacesAdapter.getInitialState()


// export const workspacesApiSlice = apiWithTags.injectEndpoints({
//     endpoints: (builder) => ({
//         createWorkspace: builder.mutation<Workspace, NewWorkspace> ({
//             query: (newWorkspace: NewWorkspace) => ({
//                 url: "", 
//                 method: "POST",
//                 body: newWorkspace 
//             }), 
//             invalidatesTags:['Workspaces']
//         }),

//         listWorkspaces : builder.query<EntityState<Workspace>, string>({
//             query: (userId: string) => ({
//                 url: "workspaces/",
//                 params: { user_id: userId }
//             }),
//             providesTags: ['Workspaces'],
//             transformResponse (res: Workspace []) {
//                 return workspacesAdapter.setAll(initialState, res)
//             }
//         })

//     })
// })

// export const { useCreateWorkspaceMutation,
//                 useListWorkspacesQuery } = workspacesApiSlice;

// // Selector to get the result of `listWorkspaces` for the current user
// export const selectWorkspacesData = (state: RootState) => {
//     const currentUserId = selectCurrentUserId(state);
//     const result = currentUserId
//       ? workspacesApiSlice.endpoints.listWorkspaces.select(currentUserId)(state as any)
//       : undefined;
  
//       // Log the shape of the state and result to help debug
//     return result?.data ?? initialState;
//   };
  
  

//   export const { selectAll: selectWorkspacesForUser, selectById: selectWorkspaceById } = workspacesAdapter.getSelectors(
//     (state: RootState) => selectWorkspacesData(state)
//   );