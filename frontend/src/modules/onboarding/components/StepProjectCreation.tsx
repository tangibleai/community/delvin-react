import { forwardRef, useCallback, useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootState, AppDispatch } from "@/store";
import { PROJECT_PAGE_URLS } from "@/routes/pageUrls";
import { useToast } from "@/components/ui/use-toast";
import FadeLoader from "react-spinners/FadeLoader";
import { getCeleryTaskStatistics } from "../actions/api_call";
import { setWelcomeMessageShown } from "@/slices/projectSlice";
import { Progress } from "@/components/ui/progress";
import { composeConfigObject } from "../utils/projectCreation";
import { resetOnboarding } from "@/slices/onboardingSlice";
import { chatbotTypes } from "@/typings/chatbot_config";
import { useCreateChatbotMutation } from "@/slices/api/chatbotsApiSlice";
import { useCreateDocumentMutation } from "@/slices/api/documentSlice";
import { useCreateFlowMutation } from "@/slices/api/flowApiSlice";
import { useCreateContactMutation } from "@/slices/api/contactApiSlice";

const StepProjectCreation = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();
  const { toast } = useToast();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const userEmail = useSelector((state: RootState) => state.auth.user?.email);
  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData
  );

  const [createChatbot] = useCreateChatbotMutation();
  const [createDocument] = useCreateDocumentMutation();

  const [recordsCreationProgress, setRecordsCreationProgress] = useState(0);

  const PERCENT_PER_ACTION = 100 / 3;

  const [createFlow] = useCreateFlowMutation();
  const [createContact] = useCreateContactMutation();

  useEffect(() => {
    if (hasCalledCreateProjectAndDependencies.current) return;
    hasCalledCreateProjectAndDependencies.current = true;

    createRecordsAndResetSlice();
  }, []);

  const createRecordsAndResetSlice = async () => {
    await createProjectAndDependencies();
    dispatch(setWelcomeMessageShown(false));
  };

  const navigate = useNavigate();

  const hasCalledCreateProjectAndDependencies = useRef(false);

  const updateProgress = async (taskId: string) => {
    const taskStatistics = await getCeleryTaskStatistics(taskId);
    const currentProgress =
      PERCENT_PER_ACTION +
      PERCENT_PER_ACTION * (taskStatistics.details.percent / 100);
    if (
      recordsCreationProgress - 1 > currentProgress &&
      currentProgress < recordsCreationProgress + 1
    )
      return;

    setRecordsCreationProgress(currentProgress);
  };

  const delay = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  // const hasCalledCreateProjectAndDependencies = useRef(false);

  const createProjectAndDependencies = useCallback(async () => {
    setRecordsCreationProgress(
      (recordsCreationProgress) => recordsCreationProgress + PERCENT_PER_ACTION
    );
    let chatbotConfig = composeConfigObject(
      onboardingData,
      onboardingData.stepSetPrompt.chatbotType
    );
    const newContact = await createContact({
      project_id: projectId,
      origin: `${userEmail!} (internal)`,
      internal: true,
    }).unwrap();

    if (chatbotConfig.bot_type == chatbotTypes.GUIDED_CONVERSATION) {
      const flow = await createFlow({
        project_id: projectId,
        name: onboardingData.stepAssistantName.name,
        description: "",
        assistant_role: "",
      }).unwrap();
      chatbotConfig = { ...chatbotConfig, flow_id: flow.id };
    }
    const newChatbot = await createChatbot({
      project_id: projectId,
      llm_provider_id: onboardingData.stepAssistantName.llm_provider_id,
      model_id: onboardingData.stepAssistantName.model_id,
      config: chatbotConfig,
      origin: onboardingData.stepAssistantName.name,
    }).unwrap();

    await delay(250);
    setRecordsCreationProgress(
      (recordsCreationProgress) => recordsCreationProgress + PERCENT_PER_ACTION
    );

    let taskId: string | null = null;

    if (onboardingData.stepDocumentAdding.text !== "") {
      let body = {
        project: projectId,
        chatbot: newChatbot.id,
        title: "my title",
        content: onboardingData.stepDocumentAdding.text,
        document_type: "text",
      };
      const result = await createDocument(body).unwrap();
      taskId = result["celery_task_id"];
    } else if (onboardingData.stepDocumentAdding.file.length > 0) {
      const fileList: any = onboardingData.stepDocumentAdding.file;

      const file = fileList?.[0];

      const formData = new FormData();
      formData.append("project", String(projectId));
      formData.append("chatbot", newChatbot.id);
      formData.append("title", "");
      if (file) {
        formData.append("content", file);
      }
      formData.append("document_type", "file");

      const result = await createDocument(formData).unwrap();
      if ("error" in result) {
        toast({
          variant: "destructive",
          title: "Oops! Something went wrong",
          description:
            "Project was almost set up. Couldn't process document file. You can upload it again on chatbot configuration page",
        });
        navigate(
          `/project/${projectId}/${PROJECT_PAGE_URLS.project.children.chatbot_config}`
        );
        return;
      }
      taskId = result["celery_task_id"];
    } else if (onboardingData.stepDocumentAdding.google !== "") {
      let body = {
        project: projectId,
        chatbot: newChatbot.id,
        title: "",
        content: onboardingData.stepDocumentAdding.google,
        document_type: "google",
      };
      const result = await createDocument(body).unwrap();
      if ("error" in result) {
        toast({
          variant: "destructive",
          title: "Oops! Something went wrong",
          description:
            "Could not process the document.  Please make sure you provide a public Google Drive link.",
        });
        navigate(
          `/project/${projectId}/${PROJECT_PAGE_URLS.project.children.chatbot_config}`
        );
        return;
      }
      taskId = result["celery_task_id"];
    }

    if (taskId) {
      while (!(99 > recordsCreationProgress && recordsCreationProgress < 101)) {
        await delay(25);
        await updateProgress(taskId);
      }
    }

    await delay(250);
    toast({
      title: "Success!",
      description: "Assistant was successfully created.",
    });
    dispatch(resetOnboarding());
    navigate(
      `/project/${projectId}/${PROJECT_PAGE_URLS.project.children.chatbot_config}`
    );
  }, [onboardingData]);

  return (
    <div className="flex flex-col items-center justify-center h-screen">
      <div className="flex flex-col px-6 mx-auto gap-4">
        <p className="text-3xl font-bold">We are setting up your bot.</p>
        <p className="text-2xl">
          This could take a few minutes... <br></br>You can grab a coffee...
          we've got this.
        </p>
      </div>
      <div className="w-2/3 flex flex-row gap-5 items-center mt-10">
        <FadeLoader color="#5d6b68" className="" />
        <Progress
          value={recordsCreationProgress}
          className="w-full border-2 border-black h-6"
        />
      </div>
    </div>
  );
});

export default StepProjectCreation;
