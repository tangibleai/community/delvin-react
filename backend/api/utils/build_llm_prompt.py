def build_llm_prompt(config):
    """Combines Chatbot.config components into an LLM prompt"""
    prompt = ""

    persona = config["general_prompt"].get("persona", "")
    constraints = config["general_prompt"].get("constraints", "")
    instructions = config["general_prompt"].get("instructions", "")

    prompt = f"""
{persona}
    
{instructions}

{constraints}
"""

    return prompt
