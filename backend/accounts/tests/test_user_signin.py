"""
Test users and app views
"""

from django.contrib.auth import authenticate
from django.test import TestCase, Client

from accounts.models import UserAccount


class SigninTest(TestCase):
    """
    Tests user creation and authentication
    """

    user1 = {
        "email": "test_user@example.com",
        "name": "test_user",
        "password": "Pwd5544**",
    }

    def setUp(self):
        self.client = Client()
        self.user = UserAccount.objects.create_user(**self.user1)

    def tearDown(self):
        self.user.delete()

    def test_user_can_authenticate(self):
        """Test a user can authenticate"""
        user = authenticate(
            email=self.user1.get("email"), password=self.user1.get("password")
        )
        self.assertTrue(user.is_authenticated)

    def test_wrong_username(self):
        """Test a wrong username can not authenticate"""
        user = authenticate(email="wrong_email", password=self.user1.get("password"))
        self.assertFalse(user)

    def test_wrong_password(self):
        """Test a wrong password can not authenticate"""
        user = authenticate(email=self.user1.get("email"), password="wrong_password")
        self.assertFalse(user)
