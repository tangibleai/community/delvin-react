import logging
import numpy as np

log = logging.getLogger(__name__)


def ssr_gradient(x, y, coef, l2_regularization=0, activation_fun=linear):
    """Numerical partial derivative of the loss function relative to x (feature variables)

    Based on: https://12ft.io/proxy?q=https://realpython.com/gradient-descent-algorithm-python
    Similar to the math of Scikit-Learn's SGD: https://scikit-learn.org/stable/modules/sgd.html#id5

    x (np.array): 2-D matrix (2-D np.array or DataFrame) of example feature vectors
                  An extra feature at x[0] for the intercept (x[0,:] == 1)
                  x.shape == (n_obs, num_features + 1) or (n_batch_size, num_feature + 1)
    y (np.array): 1-D array of target values to be predicted

    >>  y_predictions = activation_fun(x.dot(coef))

    Residuals will be a column vector (matrix), one for each example (sample)
    >>  residuals = y_predictions - y
    >>  residuals.shape == (x.shape[0], 1)
    >>  regularized_residuals = residuals + l2_regularization * np.sqrt(np.sum(coef**2))
    >>  delta_residuals__over__delta_weights = np.mean(x * residuals, axis=0)  # gradient
    """
    log.debug(f"x: {x}")
    log.debug(f"in ssr_gradient() y: {y}")
    log.debug(f"in ssr_gradient() coef: {coef}")
    coef = coef.reshape(x.shape[1], 1)
    log.debug(f"in ssr_gradient() coef reshaped: {coef}")
    log.debug(f"in ssr_gradient() x.dot(coef): {x.dot(coef)}")
    log.debug(f"in ssr_gradient() y: {y}")
    # Residuals will be a column vector (matrix), one for each example (sample) residuals.shape == (batch_size, 1)
    residuals = activation_fun(x.dot(coef)) - y
    residuals_regularized = residuals + l2_regularization * np.sqrt(np.sum(coef**2))
    log.debug(f"in ssr_gradient() regularized residuals: {residuals_regularized}")
    grad = np.mean(x * residuals, axis=0)
    return (grad, residuals)


def fit(
    x,
    y,
    gradient_fun=ssr_gradient,
    activation_fun=linear,
    coef=None,
    learning_rate=0.1,
    momentum=0.2,
    batch_size=16,
    max_iter=50,
    tolerance=3e-04,
    dtype="float64",
    random_state=None,
    l2_regularization=0,
):
    """
    x (np.array): Dataset of embedding vectors with shape (n_obs, n_vars)
                  First column should be 1. for nonzero intercept fitting
    y (np.array): Array of target (observed output) values that model will learn to predict.
    coef (np.array|None): For a warm start, set coef to array of nonzero values, otherwise it will be initialized to random values.
    gradient_fun (function): outputs array of gradients (partial derivatives) of the loss function relative to x (feature variables)
                  function that computes the partial derivitive dy/dx1,dy/dx2,...dy/dxn
    activation_fun (function): 'logistic' or 'linear' or callable function that implements something like RELU or sigmoid.
    n_vars (int): Number of feature variables (embedding vector dimensions) + 1 (for intercept)
                  intercept weight (coef[0]) will be multiplied by first column
    coef (np.array): starting values for learned parameters (polynomial coeficients coef[0]*x[0] + coef[1]*x[1]...)
                  coef[0] == intercept
    batch_size (int): number of observations to calculate gradient and adjust coef for (gradient descent batch)
    learning_rate (float): 0 < learning_rate < 1, default = 0.1

    Intercept can be found in coef[0]
    feature vectors are homogenized with x[0] = 1 or x[:] = [1] + list(embedding_vector)

    Based on Real Python blog post: https://12ft.io/proxy?q=https://realpython.com/gradient-descent-algorithm-python
    """
    if gradient_fun in "ssr sse".split():
        gradient_fun = globals()[gradient_fun + "_gradient"]
    assert callable(
        gradient_fun
    ), "'gradient_fun' must be string 'ssr' or callable function that calculates the gradient"

    if activation_fun in "linear logistic".split():
        activation_fun = globals()[activation_fun]
    assert callable(
        activation_fun
    ), "'gradient_fun' must be string 'ssr' or callable function that calculates the gradient"

    n_vars = x.shape[1]

    # Setting up the data type for NumPy arrays
    dtype_ = np.dtype(dtype)

    # Converting x and y to NumPy arrays
    x, y = np.array(x, dtype=dtype_), np.array(y, dtype=dtype_)
    n_obs = x.shape[0]
    assert (
        x.shape == (n_obs, n_vars) == (y.shape[0], n_vars)
    ), f"'x' and 'y' shapes are incompatible: x.shape: {x.shape}, y.shape: {y.shape}"
    xy = np.c_[x.reshape(n_obs, -1), y.reshape(n_obs, 1)]

    # Initializing the random number generator
    seed = None if random_state is None else int(random_state)
    rng = np.random.default_rng(seed=seed)

    # Initializing the values of the variables
    coef = (
        rng.normal(size=int(n_vars)).astype(dtype_)
        if coef is None
        else np.array(coef, dtype=dtype_)
    )
    assert (
        coef.shape[0] == x.shape[1]
    ), f"Weights and x have mismatched shapes for x.dot(coef): {x}.dot({coef})"
    assert all(
        x[:, 0] == 1
    ), f"x doesn't appear to be homogenous our the 1's are not in the 0th dimension. x: {x}"

    learning_rate = np.array(learning_rate, dtype=dtype_)
    if np.any(learning_rate <= 0):
        log.warning(f"The 'learning_rate' ({learning_rate}) must be >= 0 and <= 1.")
        learning_rate = np.array([min(max(int(lr), 0, 1)) for lr in learning_rate])
        log.warning(f"Changed learning_rate to {learning_rate}")
        raise ValueError("The 'learning_rate' must be > 0")

    momentum = np.array(momentum, dtype=dtype_)
    if np.any(momentum < 0) or np.any(momentum > 1):
        log.warning(f"The 'momentum' ({momentum}) must be >= 0 and <= 1.")
        momentum = np.array([min(max(int(dr), 0, 1)) for dr in momentum])
        log.warning(f"Changed momentum to {momentum}")

    if not (0 < batch_size <= n_obs):
        log.warning(
            f"The 'batch_size' ({batch_size}) must be between 1 and the number of observations ({n_obs})."
        )
        batch_size = min(max(int(batch_size), 1, n_obs))
        log.warning(f"Changed batch_size to {batch_size}")
    assert (
        0 < batch_size <= n_obs
    ), f"The 'batch_size' ({batch_size}) must be between 1 and the number of observations ({n_obs})."

    # 10_000 iterations would take more than 3 hours, even for small datasets and feature vectors
    max_iter = int(max_iter)
    if not 0 < max_iter <= 10_000:
        log.error(
            f"'max_iter' ({max_iter}) must be > 0 and <= 10_000 because 10k iterations would take hours."
        )
        max_iter = min(max(int(max_iter), 1, n_obs))
        log.error(f"'max_iter' changed to a value of {max_iter}.")

    tolerance = np.array(tolerance, dtype=dtype_)
    if np.any(tolerance <= 0):
        raise ValueError("'tolerance' must be greater than zero")

    # Setting the difference to zero for the first iteration
    diff = np.array([0] * n_vars)
    assert (
        diff.shape == coef.shape
    ), f"Diff will be added to coef at each iteration and have the same shape (n_vars):' \
        f' diff.shape: {diff.shape} coef.shape: {coef.shape})"
    assert (
        diff.shape[0] == x.shape[1]
    ), f"Diff will be added to coef at each iteration and must have same number of columns (n_vars):' \
        f' diff.shape: {diff.shape} coef.shape: {coef.shape})"
    assert all(
        x[:, 0] == 1
    ), f"The x array doesn't appear to be homogenous or the 1's are not in the 0th dimension. x: {x}"

    grad_history = []

    rmse_history = []

    for i in range(max_iter):  # i = epoch number
        # Shuffle x and y
        rng.shuffle(xy)

        epoch_diffs = []
        # Performing minibatch moves
        for batch_start in range(
            0, n_obs, batch_size
        ):  # batch_start = start position of batch in X and Y
            batch_stop = batch_start + batch_size
            x_batch, y_batch = (
                xy[batch_start:batch_stop, :-1],
                xy[batch_start:batch_stop, -1:],
            )

            gradients, residuals = gradient_fun(
                x=x_batch, y=y_batch, coef=coef, l2_regularization=l2_regularization
            )
            gradients = np.array(gradients, dtype_)
            grad_history.append(gradients)
            log.debug(f"AFTER gradient_fun on batch gradients.shape: {gradients.shape}")
            diff = momentum * diff
            diff += -learning_rate * gradients

            epoch_diffs.append(list(diff))  # 2-D array, n_vars
            # Updating the values of the variables
            coef += diff

        """ Check if the absolute difference of all the weight adjustments (epoch_diffs) is smaller than tolerance
        >>> max((np.array(epoch_diffs)**2) **.5) / (n_obs // batch_size)
        0.000469
        >>> np.abs(epoch_diffs).max()
        0.023466
        """
        rmse_history.append(np.mean(residuals**2, axis=0) ** 0.5)
        log.debug(f"in ssr_gradient() residuals RMSE: {rmse_history[-1]}")
        if np.all(np.abs(epoch_diffs) <= tolerance * (n_obs // batch_size)):
            log.warning(
                f"Stopping fit early at iteration #{i}/{max_iter} because epoch_diffs.max()={np.abs(epoch_diffs).max()}"
            )
            break
    results = dict(
        coef=coef,
        num_iterations=i + 1,
        max_iter=max_iter,
        rmse_history=rmse_history,
        grad_history=grad_history,
        tolerance=tolerance,
        batch_start=batch_start,
        epoch_diffs=epoch_diffs,
        n_vars=n_vars,
        learning_rate=learning_rate,
        momentum=momentum,
        batch_size=batch_size,
        dtype=dtype,
        random_state=random_state,
        l2_regularization=l2_regularization,
    )
    log.info(results)
    return results
