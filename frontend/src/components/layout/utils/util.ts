const composeProjectInitials = (input: string): string => {
  // Split the input string by spaces
  const words = input.trim().split(/\s+/);

  if (words.length === 1) {
    // If there's only one word, capitalize the first two letters
    return words[0].slice(0, 2).toUpperCase();
  } else {
    // If there are multiple words, capitalize the first letters of initial 2 words
    return words
      .slice(0, 2)
      .map((word) => word.charAt(0).toUpperCase())
      .join("");
  }
};

export { composeProjectInitials };
