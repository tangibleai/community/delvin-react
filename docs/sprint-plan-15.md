## Cetin

* [X] Provision a PostreSQL for staging 
* [X] Squash migrations and recommit to staging
* [X] Move the staging instance to the new PostreSQL instance. 
* [X] Experimenting with putting Suspense outside the <Outlet /> component in NewProjectPage
* [X] Work on the errors in model retraining on the cluster.

## Vlad
* [ ] Continue the work on the Flows feature. 
* [ ] Complete the URL refactor. 

## Maria

* [ ] Set up a Kubernetes mentor meeting. 
* [ ] Finish the documentation (backend, frontend guidelines, deployment guidelines) and share with the team. 
