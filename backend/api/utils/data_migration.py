from pathlib import Path

from api.etl import (
    user_account,
    project,
    contact,
    message_tag_group,
    dataset,
    ml_model,
    message_tag_definition,
    utils,
)
from api.refer_later.etl import chat


def create_records(*args, **kwargs):
    filepath_to_user_account_csv = Path("etl") / "UserAccount.csv"
    user_account_df = utils.get_df(filepath_to_user_account_csv)
    user_account.process(user_account_df)

    filepath_to_project_csv = Path("etl") / "Project.csv"
    project_df = utils.get_df(filepath_to_project_csv)
    project.process(project_df)

    filepath_to_contact_csv = Path("etl") / "Contact.csv"
    contact_df = utils.get_df(filepath_to_contact_csv)
    contact.process(contact_df)

    filepath_to_chat_csv = Path("etl") / "Chat.csv"
    chat_df = utils.get_df(filepath_to_chat_csv)
    chat.process(chat_df)

    filepath_to_message_tag_group_csv = Path("etl") / "MessageTagGroup.csv"
    message_tag_group_df = utils.get_df(filepath_to_message_tag_group_csv)
    message_tag_group.process(message_tag_group_df)

    filepath_to_dataset_csv = Path("etl") / "Dataset.csv"
    dataset_df = utils.get_df(filepath_to_dataset_csv)
    dataset.process(dataset_df)

    filepath_to_mlmodel_csv = Path("etl") / "MLModel.csv"
    ml_model_df = utils.get_df(filepath_to_mlmodel_csv)
    ml_model.process(ml_model_df)

    filepath_to_message_tag_definition_csv = Path("etl") / "MessageTagDefinition.csv"
    message_tag_definition_df = utils.get_df(filepath_to_message_tag_definition_csv)
    message_tag_definition.process(message_tag_definition_df)
