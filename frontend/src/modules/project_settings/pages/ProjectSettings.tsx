import HttpApiSettings from "@/modules/project_settings/components/HttpApiSettings";
import { Tab, TabGroup, TabList, TabPanel, TabPanels } from "@tremor/react";
import DeleteProject from "../components/DeleteProject";


function ProjectSettings() {

  return (
    <>
      <div className="head-section">
        <h1 className="header">Settings</h1>
      </div>

      <TabGroup>
        <TabList className="mt-8">
          <Tab>General</Tab>
        </TabList>
        <TabPanels>
          <TabPanel className="text-left">

            <HttpApiSettings />
            <DeleteProject />
          </TabPanel>
        </TabPanels>
      </TabGroup>
    </>
  );
}

export default ProjectSettings;
