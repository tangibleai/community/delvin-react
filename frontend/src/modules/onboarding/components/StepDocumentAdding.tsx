import { forwardRef } from "react";
import { Tab, TabPanel, TabList, TabPanels, TabGroup } from "@tremor/react";

import DocumentFormFile from "./DocumentFormFile";
import DocumentFormText from "./DocumentFormText";
import { Button } from "@/components/ui/button";
import { AppDispatch, RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { setOnboardingData } from "@/slices/onboardingSlice";
import { sampleDocument } from "../utils/onboardingSampleInput";
import DocumentFormGoogle from "./DocumentFormGoogle";

const StepDocumentAdding = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();

  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData,
  );

  const fillDocumentTextWithTemplate = () => {
    dispatch(
      setOnboardingData({
        ...onboardingData,
        stepDocumentAdding: {
          file: onboardingData.stepDocumentAdding.file,
          text: sampleDocument,
          google: onboardingData.stepDocumentAdding.google,
        },
      }),
    );
  };

  return (
    <TabGroup defaultValue="text-tab" className="w-5/6 mx-auto flex flex-col">
      <div className="flex justify-between">
        <div className="flex flex-col gap-7">
          <h1 className="font-medium text-3xl">
            Add your first Knowledge document
          </h1>
          <TabList>
            <Tab value="text-tab" className="text-lg">
              Text
            </Tab>
            <Tab value="file-tab" className="text-lg">
              File
            </Tab>
            <Tab value="file-tab" className="text-lg">
              Google
            </Tab>
          </TabList>
        </div>
        <div className="w-[20%] flex flex-col items-center gap-1 text-lg">
          <p>No documents yet?</p>
          <Button className="text-lg" onClick={fillDocumentTextWithTemplate}>
            Use Sample Text
          </Button>
        </div>
      </div>

      <TabPanels className="w-full mx-auto text-base">
        <TabPanel className="space-y-2 text-sm leading-7 text-gray-600 dark:text-gray-500">
          <DocumentFormText ref={ref} />
        </TabPanel>

        <TabPanel className="space-y-2 text-sm leading-7 text-gray-600 dark:text-gray-500">
          <DocumentFormFile ref={ref} />
        </TabPanel>

        <TabPanel className="space-y-2 text-sm leading-7 text-gray-600 dark:text-gray-500">
          <DocumentFormGoogle ref={ref} />
        </TabPanel>
      </TabPanels>
    </TabGroup>
  );
});

export default StepDocumentAdding;
