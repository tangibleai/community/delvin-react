import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown";
import { cn, focusInput } from "@/lib/utils";
import { RiExpandUpDownLine } from "react-icons/ri";
import React from "react";
import { ScrollArea } from "@/components/ui/scroll-area";
import { composeProjectInitials } from "@/components/layout/utils/util";
import ModalAddWorkspace from "./ModalAddWorkspace";
import { useNavigate, useParams } from "react-router-dom";
import {
  useGetWorkspaceQuery,
  useListWorkspacesQuery,
} from "@/slices/api/workspacesApiSlice";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { Skeleton } from "@/components/ui/skeleton";

export const WorkspacesDropdown = () => {
  const navigate = useNavigate();
  const [dropdownOpen, setDropdownOpen] = React.useState(false);
  const focusRef = React.useRef<null | HTMLButtonElement>(null);

  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const user = useSelector((state: RootState) => state.auth.user);

  const { data: workspaces, isFetching: workspacesFetching } =
    useListWorkspacesQuery(user!.id);
  const { data: activeWorkspace, isFetching: activeWorkspaceFetching } =
    useGetWorkspaceQuery(workspaceId);

  return workspacesFetching || activeWorkspaceFetching ? (
    <div className="flex items-center space-x-2">
      <Skeleton className="h-8 w-8 rounded-full" />
      <div className="space-y-2">
        <Skeleton className="h-2 w-20" />
        <Skeleton className="h-2 w-20" />
      </div>
    </div>
  ) : (
    <DropdownMenu
      open={dropdownOpen}
      onOpenChange={setDropdownOpen}
      modal={false}
    >
      <DropdownMenuTrigger asChild>
        <div
          className={cn(
            "flex w-full items-center  rounded-md  bg-white p-2 text-sm shadow-sm transition-all hover:bg-gray-50 dark:border-gray-800 dark:bg-gray-950 hover:dark:bg-gray-900",
            "border border-gray-300 gap-x-2.5",
            focusInput
          )}
          role="button"
        >
          {activeWorkspace && (
            <>
              <span
                className="flex aspect-square size-8 items-center justify-center rounded bg-indigo-600 p-2 text-xs font-medium text-white dark:bg-indigo-500"
                aria-hidden="true"
              >
                {activeWorkspace.name.slice(0, 2)}
              </span>
              <div className="flex w-full items-center justify-between gap-x-4 truncate">
                <div className="truncate">
                  <p className="truncate whitespace-nowrap text-sm font-medium text-gray-900 dark:text-gray-50">
                    {activeWorkspace.name}
                  </p>
                </div>
                <RiExpandUpDownLine
                  className="size-5 shrink-0 text-gray-500"
                  aria-hidden="true"
                />
              </div>
            </>
          )}
        </div>
      </DropdownMenuTrigger>
      <DropdownMenuContent
        onCloseAutoFocus={(event: any) => {
          if (focusRef.current) {
            focusRef.current.focus();
            focusRef.current = null;
            event.preventDefault();
          }
        }}
      >
        <DropdownMenuGroup>
          <DropdownMenuLabel className="px-2">
            Other workspaces ({(workspaces?.length ?? 1) - 1})
          </DropdownMenuLabel>
          <ScrollArea
            key={"projects scroll area"}
            className="flex flex-col max-h-48 px-2"
          >
            {activeWorkspace ? (
              workspaces?.map((workspace) =>
                workspace.id === activeWorkspace?.id ? (
                  <React.Fragment key={workspace.id}></React.Fragment>
                ) : (
                  <DropdownMenuItem key={workspace.id}>
                    <div
                      onClick={() => navigate(`/workspace/${workspace.id}`)}
                      className="flex w-full items-center gap-x-2.5"
                    >
                      <span
                        className={cn(
                          "bg-indigo-600 dark:bg-indigo-500",
                          "flex aspect-square w-[2rem] h-[2rem] items-center justify-center rounded p-2 text-xs font-medium text-white"
                        )}
                        aria-hidden="true"
                      >
                        {composeProjectInitials(workspace.name)}
                      </span>
                      <div>
                        <p className="text-sm font-medium text-gray-900 dark:text-gray-50">
                          {workspace.name}
                        </p>
                      </div>
                    </div>
                  </DropdownMenuItem>
                )
              )
            ) : (
              <></>
            )}
          </ScrollArea>
        </DropdownMenuGroup>
        <DropdownMenuSeparator />
        <div className="w-full flex flex-col items-center text-center mt-2">
          <ModalAddWorkspace setDropdownOpen={setDropdownOpen} />
        </div>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
