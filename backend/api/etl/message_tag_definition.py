import pandas as pd

from api import models
from core.constant import CONSTANTS
from api.etl import utils


def process(df_or_records_list, *args, **kwargs):
    tag_definitions = df_or_records_list
    if not isinstance(df_or_records_list, list):
        tag_definitions = df_or_records_list.to_dict("records")
    required_fields = {
        "name",
        "description",
        "message_tag_group__name",
        "project__name",
        "project__owner__email",
        "dataset__name",
    }
    utils.check_missing_fields(tag_definitions, required_fields)

    tag_definitions = resolve_dependent_records(tag_definitions)
    check_duplicated_tag_definitions(tag_definitions)
    db_records = save_tag_definitions_to_db(tag_definitions)

    resolve_ml_model_for_each_record(tag_definitions)
    create_ml_weights_records(tag_definitions)

    return db_records


def resolve_dependent_records(tag_definitions):
    tag_definitions = utils.resolve_user_for_each_record(
        tag_definitions,
        user_email_field_name="project__owner__email",
    )
    tag_definitions = utils.resolve_project_for_each_record(tag_definitions)
    tag_definitions = resolve_message_tag_group_for_each_record(tag_definitions)

    return tag_definitions


def resolve_message_tag_group_for_each_record(tag_definitions):
    for t in tag_definitions:
        project_rec = t["project_rec"]
        message_tag_group_name = t["message_tag_group__name"]

        try:
            t["tag_group_rec"] = models.MessageTagGroup.objects.get(
                name=message_tag_group_name, project=project_rec
            )
        except models.MessageTagGroup.DoesNotExist:
            project_name = t["project__name"]
            raise ValueError(
                f'MessageTagGroup with "{message_tag_group_name}" name for "{project_name}" project doesn\'t exist for'
            )

    return tag_definitions


def check_duplicated_tag_definitions(tag_definitions):
    for t in tag_definitions:
        name = t["name"]
        tag_group_rec = t["tag_group_rec"]

        duplicated_tag_definitions = models.MessageTagDefinition.objects.filter(
            name=name, message_tag_group=tag_group_rec
        )
        if duplicated_tag_definitions:
            tag_group_name = t["message_tag_group__name"]
            project_name = t["project__name"]
            raise ValueError(
                f'MessageTagDefinition with "{name}" name already exists for "{tag_group_name}" message tag group within "{project_name}" project'
            )

    return []


def save_tag_definitions_to_db(tag_definitions):
    records_to_create = [
        models.MessageTagDefinition(
            name=t["name"],
            description=t["description"],
            project=t["project_rec"],
            message_tag_group=t["tag_group_rec"],
        )
        for t in tag_definitions
    ]
    created_tag_definition_records = models.MessageTagDefinition.objects.bulk_create(
        records_to_create
    )

    return created_tag_definition_records


def resolve_ml_model_for_each_record(tag_definitions):
    for t in tag_definitions:
        project_name = t["project__name"]
        project_rec = t["project_rec"]
        dataset_name = t["dataset__name"]

        try:
            dataset_rec = models.Dataset.objects.get(
                project=project_rec, name=dataset_name
            )
        except models.Dataset.DoesNotExist:
            raise ValueError(
                f'Dataset with "{dataset_name}" name within "{project_name}" project doesn\'t exist'
            )

        try:
            t["ml_model_rec"] = models.MLModel.objects.get(dataset=dataset_rec)
        except models.Dataset.MultipleObjectsReturned:
            raise ValueError(
                f'There are multiple MLModel records associated with "{dataset_name}" dataset within "{project_name}" project'
            )
        except models.Dataset.DoesNotExist:
            raise ValueError(
                f'There are no MLModel records associated with "{dataset_name}" dataset within "{project_name}" project'
            )

    return tag_definitions


def create_ml_weights_records(
    tag_definitions,
    filepath=CONSTANTS.DATA_DIR / "coef_and_intercepts.csv",
):
    """Reads csv, turns into df and creates if not exist MLWeigts records

    Args:
        filepath (str): path to csv with coef and intercepts
        project (Project): associated project record
        ml_model (MLModel): associated MLModel record
    Returns:
        list[MLModel]: list of created MLWeights records
    """

    df = pd.read_csv(filepath)
    ml_weights_records = []

    for t in tag_definitions:
        tag_name = t["name"]

        matched_row = df[df["label"] == tag_name]
        if matched_row.empty:
            continue
            raise ValueError(
                f'Not corresponding record with "{tag_name}" label found to retrieve coef and intercept'
            )

        project_rec = t["project_rec"]
        ml_model_rec = t["ml_model_rec"]

        row = matched_row.iloc[0]

        weights = row[1:-1].to_list()
        intercept = row[-1]

        tag_definition_rec = models.MessageTagDefinition.objects.get(
            name=tag_name, project=project_rec
        )
        ml_weights_rec = models.MLWeights.objects.create(
            tag_definition=tag_definition_rec,
            coef=weights,
            intercept=intercept,
            ml_model=ml_model_rec,
        )
        ml_weights_records.append(ml_weights_rec)

    return ml_weights_records
