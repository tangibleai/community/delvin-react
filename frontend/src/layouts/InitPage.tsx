import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { Navigate, Outlet } from "react-router-dom";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";

function InitPage() {
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );

  if (!isAuthenticated) {
    return <Navigate to={AUTH_PAGE_URLS.login} />;
  }

  return (
    <div>
      <Outlet />
    </div>
  );
}

export default InitPage;
