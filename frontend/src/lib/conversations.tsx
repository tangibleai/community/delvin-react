import axiosInstance from "@/api/axios";
import { Message } from "@/typings/message";
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import { processError } from "./utils";

// can't be turned to api slice
export const fetchMessages = async (
  chatId: string,
  offset: number,
  limit: number
): Promise<Message[]> => {
  try {
    const response = await axiosInstance.get(
      `/api/chats/${chatId}/messages/?offset=${offset}&limit=${limit}`
    );
    const data = await response.data.results;
    return data.reverse();
  } catch (error: any) {
    processError(`Failed to retrieve messages. Details: ${error}`);
    return [];
  }
};
