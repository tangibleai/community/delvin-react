import { useParams } from "react-router-dom";
import { useState } from "react";

import { Button } from "@/components/ui/button";
import { ClipboardDocumentCheckIcon } from "@heroicons/react/24/outline";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Flex, Icon, Text, TextInput, Badge } from "@tremor/react";

import { useResetProjectTokenMutation } from "@/slices/api/projectsApiSlice";

function ResetApiTokenModal() {
  const [newToken, setNewToken] = useState("");
  const [showSuccessBadge, setShowSuccessBadge] = useState(false);

  const params = useParams();
  const projectId = params.projectId!;

  const [resetProjectToken] = useResetProjectTokenMutation();

  const getNewToken = async () => {
    // Resets the success message if used
    setShowSuccessBadge(false);
    // Creates and sets a new token

    let newToken = "";
    try {
      newToken = await resetProjectToken(projectId).unwrap();
    } catch (error) {}

    setNewToken(newToken);
  };

  const onCopyClick = async () => {
    // copy the token to clipboard
    await navigator.clipboard.writeText(newToken);
    // show a success badge
    setShowSuccessBadge(true);
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button onClick={() => getNewToken()}>Reset API token</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>API token</DialogTitle>
        </DialogHeader>
        <Text>Copy and store this token in a safe place. </Text>
        <Text>
          You will not be able to recover it once you close this window.{" "}
        </Text>
        <Flex className="my-2">
          <TextInput
            className="mx-2"
            disabled={true}
            placeholder={newToken}
          ></TextInput>
          <Icon
            variant="light"
            icon={ClipboardDocumentCheckIcon}
            onClick={() => onCopyClick()}
          />
        </Flex>
        {showSuccessBadge && (
          <Badge color="emerald">Token copied to clipboard</Badge>
        )}
      </DialogContent>
    </Dialog>
  );
}

export default ResetApiTokenModal;
