import { ChatMessageType } from "./chat_message_type";
import { TaggedMessage } from "./tagged_message";

export interface Dataset {
  id: string;
  name: string;
  description: string;
  project: string;
  message: ChatMessageType;
  tagged_messages: Array<TaggedMessage>;
}
