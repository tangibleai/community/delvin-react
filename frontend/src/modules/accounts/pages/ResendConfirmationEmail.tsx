import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { load_user } from "@/actions/auth";
import { Button } from "@/components/ui/button";
import { RootState, AppDispatch } from "@/store";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import { resendActivationEmail } from "../actions/action";
import { useToast } from "@/components/ui/use-toast";

function ResendConfirmationEmail() {
  const { toast } = useToast();

  const [disabled, setDisabled] = useState(false);
  const [timeLeftToResendEmail, setTimeLeftToResendEmail] = useState(0);

  useEffect(() => {
    let timer: string | number | NodeJS.Timeout | undefined;
    if (disabled && timeLeftToResendEmail > 0) {
      timer = setInterval(() => {
        setTimeLeftToResendEmail((prevTime) => {
          if (prevTime <= 1) {
            clearInterval(timer);
            setDisabled(false);
            return 0;
          }
          return prevTime - 1;
        });
      }, 1000);
    }
    return () => clearInterval(timer);
  }, [disabled, timeLeftToResendEmail]);

  const dispatch: AppDispatch = useDispatch();
  // Replace with the appropriate state shape or selector function
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );

  const userEmail = useSelector(
    (state: RootState) => state.auth.user!.email ?? "",
  );

  const onResendButtonClicked = async (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    e.preventDefault();
    setDisabled(true);
    setTimeLeftToResendEmail(60);
    const statusCode = await resendActivationEmail(userEmail);

    if (statusCode >= 200 && statusCode <= 299) {
      toast({
        title: "Success!",
        description: "Confirmation email was successfully sent.",
      });
      return statusCode;
    }

    toast({
      variant: "destructive",
      title: "Oops! Something went wrong",
      description: "Couldn't send confirmation email.",
    });
  };

  if (isAuthenticated) {
    dispatch(load_user());
    return <Navigate to={AUTH_PAGE_URLS.home} />;
  }

  return (
    <>
      <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0 gap-10">
        <p className="text-5xl">Please, verify your email!</p>
        <p className="text-2xl">We sent a verification email on {userEmail}</p>
        <p className="text-2xl">
          If you didn't receive an email, please check your "Spam" folder.
        </p>
        <Button
          className="text-2xl"
          onClick={onResendButtonClicked}
          disabled={disabled}
        >
          {disabled && timeLeftToResendEmail} Resend Verification Email
        </Button>
      </div>
    </>
  );
}

export default ResendConfirmationEmail;
