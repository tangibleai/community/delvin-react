import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";

import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogFooter,
  DialogClose,
} from "@/components/ui/dialog";

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useToast } from "@/components/ui/use-toast";
import { useParams } from "react-router-dom";
import { useCreateFlowMutation } from "@/slices/api/flowApiSlice";

const NewFlowDialog = () => {
  const { toast } = useToast();
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const [isNewtagDialogOpen, setIsNewTagDialogOpen] = useState(false);

  const toggleDialog = () => setIsNewTagDialogOpen(!isNewtagDialogOpen);

  const [createFlow] = useCreateFlowMutation();

  const newFlowFormSchema = z.object({
    flow_name: z.string().min(1, "Flow name is required"),
    flow_description: z.string(),
    assistant_role: z.string(),
  });

  const newFlowForm = useForm<z.infer<typeof newFlowFormSchema>>({
    resolver: zodResolver(newFlowFormSchema),
    defaultValues: {
      flow_name: "",
      flow_description: "",
      assistant_role: "",
    },
  });

  async function handleSubmit(values: z.infer<typeof newFlowFormSchema>) {
    const { flow_name, flow_description, assistant_role } = values;

    try {
      await createFlow({
        project_id: projectId,
        name: flow_name,
        description: flow_description,
        assistant_role,
      }).unwrap();
      toast({
        title: "Success!",
        description: "New flow was successfully created.",
      });
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Could not create a new flow. Please, try again.",
      });
    }

    setIsNewTagDialogOpen(false); // Close the dialog
    newFlowForm.reset();
  }

  return (
    <Dialog open={isNewtagDialogOpen} onOpenChange={setIsNewTagDialogOpen}>
      <DialogTrigger asChild>
        <Button className="mt-2" onClick={toggleDialog}>
          Add flow
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <Form {...newFlowForm}>
          <form onSubmit={newFlowForm.handleSubmit(handleSubmit)}>
            <DialogHeader>
              <DialogTitle>Create flow</DialogTitle>
            </DialogHeader>
            <div className="grid gap-4 py-4">
              <FormField
                control={newFlowForm.control}
                name="flow_name"
                render={({ field }) => {
                  return (
                    <>
                      <FormItem>
                        <FormLabel>Flow name</FormLabel>
                        <FormControl>
                          <Input placeholder="A unique title" {...field} />
                        </FormControl>
                      </FormItem>
                      {newFlowForm?.formState?.errors?.flow_name?.message && (
                        <FormMessage />
                      )}
                    </>
                  );
                }}
              />
              <FormField
                control={newFlowForm.control}
                name="flow_description"
                render={({ field }) => {
                  return (
                    <>
                      <FormItem>
                        <FormLabel>Flow description (optional)</FormLabel>
                        <FormControl>
                          <Textarea
                            placeholder="Notes to help the team remember what the flow is and important information about it"
                            {...field}
                            rows={5}
                          />
                        </FormControl>
                      </FormItem>
                      {newFlowForm?.formState?.errors?.flow_description
                        ?.message && <FormMessage />}
                    </>
                  );
                }}
              />
              <FormField
                control={newFlowForm.control}
                name="assistant_role"
                render={({ field }) => {
                  return (
                    <>
                      <FormItem>
                        <FormLabel>Assistant role (optional)</FormLabel>
                        <FormControl>
                          <Textarea
                            placeholder="Briefly describe the role the chatbot should perform throughout this entire flow. This role will be sent with every prompt, unless you override it"
                            {...field}
                            rows={5}
                          />
                        </FormControl>
                      </FormItem>
                      {newFlowForm?.formState?.errors?.assistant_role
                        ?.message && <FormMessage />}
                    </>
                  );
                }}
              />
            </div>
            <DialogFooter>
              <Button type="submit">Create flow</Button>
              <DialogClose asChild>
                <Button type="button">Cancel</Button>
              </DialogClose>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export { NewFlowDialog };
