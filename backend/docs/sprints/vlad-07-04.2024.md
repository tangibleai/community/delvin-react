1. Create a data fixture on staging (`api`, `accounts`, look for others)
2. Move those files to local machine
3. Checkout staging branch on local machine
4. Delete db and load staging data into local machine
   SKIP 5. Delete all migrations, make one for staging, load data
5. Switch to current branch, run makemigrations and apply it
6. Dump data in a new fixture
7. Merge code to staging and test new data fields
8. Add `created_by` field to `MLModel`
