import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Card } from "@tremor/react";

import axiosInstance from "@/api/axios";

import ChatHistory from "../components/ChatHistory";

function ChatPage() {
  const { chatId } = useParams<{ chatId: string }>();

  return (
    <div className="flex flex-col h-screen">
      <div>
        <Card className="justify-start my-2">
          <h1 className="header"> {chatId} </h1>
        </Card>
      </div>
      <ChatHistory chatId={chatId!} />
    </div>
  );
}

export default ChatPage;
