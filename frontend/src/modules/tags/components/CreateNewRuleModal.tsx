import { XMarkIcon } from "@heroicons/react/24/outline";
import {
  Flex,
  Icon,
  Text,
  TextInput,
  Badge,
  Title,
  Button,
  Select,
  SelectItem,
} from "@tremor/react";
import ReactModal from "react-modal";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",

    transform: "translate(-50%, -50%)",
    background: "white",
    borderRadius: "4px",
    padding: "25px",
    maxWidth: "700px",
    width: "auto",
  },
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.75)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
};

type Props = {
  isNewRuleModalOpen: boolean;
  setIsNewRuleModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
};

function CreateNewRuleModal({
  isNewRuleModalOpen,
  setIsNewRuleModalOpen,
}: Props) {
  const onCloseClick = () => {
    setIsNewRuleModalOpen(false);
  };

  return (
    <ReactModal
      appElement={document.getElementById("root") as HTMLElement}
      isOpen={isNewRuleModalOpen}
      style={customStyles}
    >
      <button className="absolute top-2 right-2" onClick={onCloseClick}>
        <XMarkIcon className="h-4 w-4" />
      </button>

      <Title className="my-5"> Create new rule </Title>

      <div className="flex flew-row justify-between">
        <div className="flex my-2">
          <TextInput></TextInput>
          <Button> + New tag</Button>
        </div>

        <div className="flex flex-grow mx-10">
          <Text> or use existing tag: </Text>
          <Select>
            <SelectItem value="tag1">Tag1</SelectItem>
          </Select>
        </div>
      </div>
    </ReactModal>
  );
}

export default CreateNewRuleModal;
