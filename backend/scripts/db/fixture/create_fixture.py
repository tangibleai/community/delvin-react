import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()


import json

from django.conf import settings


def filter_json_data(file_path, output_file_path):
    print("here")
    with open(file_path, "r") as file:
        data = json.load(file)

    print(data)

    filtered_data = [
        obj
        for obj in data
        if obj["model"].startswith("api") or obj["model"].startswith("accounts")
    ]

    with open(output_file_path, "w") as file:
        json.dump(filtered_data, file, indent=4)


file_path = output_file_path = (
    settings.BASE_DIR / "fixtures" / "default_account_data.json"
)

# file_path = "fixtures/default_account_data.json"
# output_file_path = "fixtures/default_account_data.json"

filter_json_data(file_path, output_file_path)
