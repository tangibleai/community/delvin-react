import { useEffect, useRef } from "react";
import { Input } from "@/components/ui/input";
import { InputToolbox, SendButton } from "@chatscope/chat-ui-kit-react";

type Props = {
  userPrompt: string;
  setUserPrompt: React.Dispatch<React.SetStateAction<string>>;
  handleSubmittedPrompt: (
    e:
      | React.KeyboardEvent<HTMLInputElement>
      | React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void;
  className?: string;
  isDisabled?: boolean;
};

export function ChatInput({
  userPrompt,
  setUserPrompt,
  handleSubmittedPrompt,
  className,
  isDisabled = false,
}: Props) {
  const inputRef = useRef<HTMLInputElement>(null);

  const handleUserPromptChanged: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    const userPrompt = event.target.value;
    setUserPrompt(userPrompt);
  };

  // Use useEffect to focus the input when isDisabled is false
  useEffect(() => {
    if (!isDisabled && inputRef.current) {
      inputRef.current.focus();
    }
  }, [isDisabled]);

  return (
    <div
      className={`flex my-2 mx-auto w-full items-center space-x-2 ${className}`}
    >
      <Input
        ref={inputRef}
        type="text"
        placeholder="Type your message here ..."
        value={userPrompt}
        onChange={handleUserPromptChanged}
        onKeyDown={handleSubmittedPrompt}
        disabled={isDisabled}
      />
      <InputToolbox>
        <SendButton disabled={isDisabled} onClick={handleSubmittedPrompt} />
      </InputToolbox>
    </div>
  );
}
