import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { TagDefinitionsSidebar } from "./TagDefinitionsSidebar";
import { TagDefinitionContent } from "./TagDefinitionContent";
import { useToast } from "@/components/ui/use-toast";
import { useListMessageTagDefinitionsQuery } from "@/slices/api/messageTagDefinition";
import { MessageTagDefinition } from "@/typings/message_tag_definition";

const TagsTabContent = () => {
  const { toast } = useToast();
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const [activeTagDefinition, setActiveTagDefinition] = useState<
    Partial<MessageTagDefinition>
  >({});

  const { data: tags = [], isError: errorRetrievingTags } =
    useListMessageTagDefinitionsQuery(projectId);

  useEffect(() => {
    if (!errorRetrievingTags) return;

    toast({
      variant: "destructive",
      title: "Oops! Something went wrong",
      description: "Could not fetch tags. Please, try to reopen this page.",
    });
  }, [errorRetrievingTags]);

  useEffect(() => {
    if (tags.length) return; // Only fires if no tags in list

    resolveActiveTagDefinition();
  }, []);

  const resolveActiveTagDefinition = async () => {
    if (tags.length === 0) {
      setActiveTagDefinition({});
      return;
    }

    setActiveTagDefinition(tags[0]);
  };

  return (
    <div className="flex flex-1 pt-[3vh]">
      <TagDefinitionsSidebar
        activeTagDefinition={activeTagDefinition as MessageTagDefinition}
        setActiveTagDefinition={setActiveTagDefinition}
      />
      <TagDefinitionContent
        activeTagDefinition={activeTagDefinition as MessageTagDefinition}
        setActiveTagDefinition={setActiveTagDefinition}
      />
    </div>
  );
};

export { TagsTabContent };
