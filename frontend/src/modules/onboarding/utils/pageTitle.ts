import { OnboardingData } from "@/typings/onboarding_data";
import { StateName } from "../types/state_names";
import { OnboardingState } from "./onboardingStateManager";

export const getPageTitleForState = (
  onboardingData: OnboardingData,
  currentOnboardingState: OnboardingState
) => {
  if (currentOnboardingState.name === StateName.StepAssistantName)
    return "Set up your assistant";
  const chatbotName = onboardingData.stepAssistantName.name;
  if (
    new Set([StateName.StepSetPrompt, StateName.StepDocumentAdding]).has(
      currentOnboardingState.name
    )
  )
    return `Set up ${chatbotName}`;

  return "";
};
