from pydantic import BaseModel
from enum import Enum
from typing import List, Dict

from api.models import Project

from api.utils.tags_prediction import predict_tags


class NodeTypeEnum(str, Enum):
    MessageNode = "promptNode"
    StartNode = "startNode"
    ConditionBlock = "conditionBlock"


UiNodeTypeEnum = Enum(
    "UiNodeTypeEnum",
    {**NodeTypeEnum.__members__, "SingleConditionBlock": "singleCondition"},
)


class Edge(BaseModel):
    source_node_id: str
    target_node_id: str

    @classmethod
    def deserialize(cls, edge_dict: dict):
        edge_data = {
            "source_node_id": edge_dict["source_node_id"],
            "target_node_id": edge_dict["target_node_id"],
        }
        return cls(**edge_data)


class ConditionProperties(BaseModel):
    tag_name: str
    tag_id: str


class ConditionTypeEnum(str, Enum):
    TAG_DETECTED = "tag_detected"
    ELSE = "else"


class EdgeCondition(BaseModel):
    type: ConditionTypeEnum
    properties: ConditionProperties


class ConditionEdge(Edge):
    id: str
    condition: EdgeCondition

    @classmethod
    def deserialize(cls, edge_dict: dict):
        parent_edge = Edge.deserialize(edge_dict)
        edge_data = {
            **dict(parent_edge),
            "id": edge_dict["id"],
            "condition": edge_dict["condition"],
        }
        return cls(**edge_data)

    def condition_is_true(self, predicted_tags: list[str]):
        condition_type = self.condition.type
        tag_name = self.condition.properties.tag_name

        if condition_type == ConditionTypeEnum.TAG_DETECTED:
            return tag_name in predicted_tags

        return True


class Node(BaseModel):
    id: str
    type: NodeTypeEnum
    edges: List[Edge] = []

    class Config:
        extra = "allow"

    @classmethod
    def from_ui_node(cls, node_dict: dict):
        node_data = {
            "id": node_dict["data"]["nodeId"],
            "type": node_dict["type"],
        }
        return cls(**node_data)

    def get_next_node_id(self):
        return self.edges[0].target_node_id

    @classmethod
    def resolve_node_type(cls, node: "Node"):
        typed_node = node

        if node["type"] == NodeTypeEnum.ConditionBlock:
            typed_node = ConditionBlockNode.deserialize(node)
            for edge in node["edges"]:
                typed_edge = ConditionEdge.deserialize(edge)
                typed_node.edges.append(typed_edge)
        else:
            typed_node = PromptNode.deserialize(node)
            for edge in node["edges"]:
                typed_edge = Edge.deserialize(edge)
                typed_node.edges.append(typed_edge)

        return typed_node


class PromptNode(Node):
    content: str

    @classmethod
    def from_ui_node(cls, node_dict: dict):
        parent_node = Node.from_ui_node(node_dict)
        node_data = {
            **dict(parent_node),
            "content": node_dict["data"]["nodePrompt"],
        }
        return cls(**node_data)

    @classmethod
    def deserialize(cls, node_dict: dict):
        node_data = {
            "id": node_dict["id"],
            "content": node_dict["content"],
            "type": node_dict["type"],
        }
        return cls(**node_data)


class ConditionBlockNode(Node):
    @classmethod
    def deserialize(cls, node_dict: dict):
        node_data = {
            "id": node_dict["id"],
            "type": node_dict["type"],
        }
        return cls(**node_data)

    def figure_next_node_id(self, predicted_tags: list[str]) -> str:
        if_condition_edges = self.get_edges_for_if_conditions()
        for edge in if_condition_edges:
            condition_is_true = edge.condition_is_true(predicted_tags)
            if condition_is_true:
                next_node_id = edge.target_node_id
                return next_node_id

        else_condition_edge = self.get_edge_for_else_condition()
        next_node_id = else_condition_edge.target_node_id

        return next_node_id

    def get_edges_for_if_conditions(self) -> list[ConditionEdge]:
        if_condition_edges = [
            edge for edge in self.edges if edge.condition.type != ConditionTypeEnum.ELSE
        ]

        if_condition_edges.sort(key=lambda edge: int(edge.id))

        return if_condition_edges

    def predict_tags(self, project: Project, user_message: dict) -> list[str]:
        message = user_message["content"]
        predicted_tag_records = predict_tags(message, project)
        predicted_tags = [tag_rec["tag"].name for tag_rec in [predicted_tag_records][0]]

        return predicted_tags

    def get_edge_for_else_condition(self) -> ConditionEdge:
        else_edge = [
            edge for edge in self.edges if edge.condition.type == ConditionTypeEnum.ELSE
        ][0]

        return else_edge


class FlowGraph(BaseModel):
    nodes: Dict[str, Node]
    source_node_id: str

    @classmethod
    def from_first_node(cls, first_node: Node):
        return cls(nodes={first_node.id: first_node}, source_node_id=first_node.id)

    @classmethod
    def instantiate(cls, source_node_id: str, nodes: Dict[str, Node]):
        typed_nodes = {}
        for id, node in nodes.items():
            typed_node = Node.resolve_node_type(node)
            typed_nodes[id] = typed_node

        return cls(nodes=typed_nodes, source_node_id=source_node_id)

    def add_node(self, new_node: Node, source=False):
        self.nodes[new_node.id] = new_node
        if source:
            self.source_node_id = new_node.id

    def has_node(self, node_id: str):
        return node_id in self.nodes.keys()

    def get_node(self, node_id: str):
        try:
            return self.nodes[node_id]
        except KeyError:
            return None

    def get_next_node(self, node: Node, project: Project, user_message: dict):
        next_node_id = node.get_next_node_id()
        next_node = self.get_node(next_node_id)

        predicted_tags = self.predict_tags_for_condition_block(project, user_message)
        while next_node.type == NodeTypeEnum.ConditionBlock:
            next_node_id = next_node.figure_next_node_id(predicted_tags)
            next_node = self.get_node(next_node_id)

        return next_node

    def predict_tags_for_condition_block(self, project: Project, user_message: dict):
        condition_block_occurs_in_conversation = any(
            node.type == NodeTypeEnum.ConditionBlock for node in self.nodes.values()
        )

        predicted_tags = []

        if not condition_block_occurs_in_conversation:
            return predicted_tags

        message = user_message["content"]
        predicted_tag_records = predict_tags(message, project)
        predicted_tags = [tag_rec["tag"].name for tag_rec in [predicted_tag_records][0]]

        return predicted_tags
