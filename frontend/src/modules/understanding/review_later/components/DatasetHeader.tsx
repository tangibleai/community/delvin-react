import { Button } from "@/components/ui/button";
import { Dataset } from "@/typings/dataset";

type Props = {
  activeDataset: Dataset;
  onDatasetDeleted: (datasetId: string) => Promise<void>;
  onRetrainDataset: () => Promise<void>;
};

const DatasetHeader = ({
  activeDataset,
  onDatasetDeleted,
  onRetrainDataset,
}: Props) => {
  return (
    <div className="flex justify-between items-center">
      <div>
        <h1 className="text-xl font-bold">{activeDataset?.name}</h1>
        <p className="text-gray-600">{activeDataset?.description}</p>
      </div>
      <Button
        variant="destructive"
        onClick={() => {
          onRetrainDataset();
        }}
      >
        Retrain dataset
      </Button>
      {/* <Button
        variant="destructive"
        onClick={() => {
          onDatasetDeleted(activeDataset?.id);
        }}
      >
        Delete dataset
      </Button> */}
    </div>
  );
};

export { DatasetHeader };
