import json
import os
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from django.test.testcases import SerializeMixin
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from api.models import *
from api.tests import (
    mock,
    utils as test_utils,
)  # since "utils" is reserved name in python
from core.constant import CONSTANTS


class TestViews(TestCase, SerializeMixin):
    """Test the API views.
    To test in order using the SerializeMixin
    """

    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()

    fixtures = [
        "default_account_data.json"
    ]  # to populate database with company account records to be able to test "copy dataset" feature

    @classmethod
    @patch("api.tasks.generate_message_embeddings_task.delay", return_value=1)
    @patch("api.tasks.apply_message_rules_task.delay", return_value=1)
    def setUpTestData(cls, *args, **kwargs):
        """setUp function to create test data.
        Use of setUpClass to create objects only once.
        """
        # Disable celery tasks
        # patch("celery.app.task.Task.delay", return_value=1)
        # patch("celery.app.task.Task.apply_async", return_value=1)

        # Create user1
        user = test_utils.filter_by_fields(
            mock.mock["user1"], ["email", "name", "password"]
        )
        cls.user1 = UserAccount.objects.create_user(**user)
        response = cls.client.post("/auth/jwt/create/", user)
        cls.user1_access_token = response.json()["access"]

        # Create user2
        user2 = test_utils.filter_by_fields(
            mock.mock["user2"], ["email", "name", "password"]
        )
        cls.user2 = UserAccount.objects.create_user(**user2)
        response = cls.client.post("/auth/jwt/create/", user2)
        cls.user2_access_token = response.json()["access"]

        # Create workspace1
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["workspace1"]["name"],
            "properties": mock.mock["workspace1"]["properties"],
            "owner": str(cls.user1.id),
        }
        json_data = json.dumps(req_body)
        serialized_workspace1 = cls.client.post(
            "/api/workspace/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        cls.workspace1_id = serialized_workspace1["id"]

        # Create workspace2
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["workspace2"]["name"],
            "properties": mock.mock["workspace2"]["properties"],
            "owner": str(cls.user2.id),
        }
        json_data = json.dumps(req_body)
        serialized_workspace2 = cls.client.post(
            "/api/workspace/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        workspace2_id = serialized_workspace2["id"]

        # Create project1
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["user1"]["project_name"],
            "workspace_id": cls.workspace1_id,
        }
        json_data = json.dumps(req_body)
        serialized_project = cls.client.post(
            "/api/project/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        cls.project1_id = serialized_project["id"]
        cls.project1_token_key = serialized_project["token_key"]

        # Create project2
        headers = {"Authorization": "JWT " + cls.user2_access_token}
        req_body = {
            "name": mock.mock["user2"]["project_name"],
            "workspace_id": workspace2_id,
        }
        json_data = json.dumps(req_body)
        serialized_project = cls.client.post(
            "/api/project/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        cls.project2_id = serialized_project["id"]
        cls.project2_token_key = serialized_project["token_key"]

        # Create message tag group
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        response = cls.client.post(
            "/api/message-tag-group/create/",
            json.dumps(
                {
                    "name": mock.mock["user1"]["message_tag_group_name"],
                    "project": cls.project1_id,
                }
            ),
            headers=headers,
            content_type="application/json",
        ).json()
        cls.message_tag_group1_id = response["id"]

        # Create message tag definition
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        response = cls.client.post(
            "/api/message-tag-definitions/",
            json.dumps(
                {
                    "name": mock.mock["user1"]["message_tag_definition_name"],
                    "description": "Test Description",
                    "project_id": cls.project1_id,
                    "message_tag_group_id": cls.message_tag_group1_id,
                }
            ),
            headers=headers,
            content_type="application/json",
        ).json()
        cls.message_tag_definition1_id = response["id"]

        # Create message tag rule
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        response = cls.client.post(
            "/api/message-tag-rule/create/",
            json.dumps(
                {
                    "direction": "inbound",
                    "rule_type": "contains",
                    "rule_content": "Summary",
                    "project": cls.project1_id,
                    "message_tag_definition": cls.message_tag_definition1_id,
                }
            ),
            headers=headers,
            content_type="application/json",
        ).json()
        cls.message_tag_rule1_id = response["id"]

        # set up for test message creation
        project = Project.objects.get(id=cls.project1_id)
        contact = Contact.objects.get_or_create(
            id=mock.mock["origin"],
            project=project,
            origin=mock.mock["origin"],
            internal=True,
        )[0]
        Chat.objects.get_or_create(id=mock.mock["chat"], contact_id=contact)

        # Create guardrail response with message tag definition id
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        response = cls.client.post(
            "/api/guardrail-responses/create/",
            json.dumps(
                {
                    "guardrail_action": mock.mock["user1"]["guardrail_action"],
                    "project": cls.project1_id,
                    "message_tag_definition": {
                        "id": cls.message_tag_definition1_id,
                        "name": "",
                        "description": "",
                        "project": "",
                        "message_tag_group": "",
                    },
                }
            ),
            headers=headers,
            content_type="application/json",
        ).json()
        cls.guardrail_response1 = response

        # TODO Is not working due to retrain_model
        # # Create guardrail response with message tag definition data
        # headers = {"Authorization": "JWT " + cls.user1_access_token}
        # response = cls.client.post(
        #     "/api/guardrail-responses/create/",
        #     json.dumps(
        #         {
        #             "guardrail_action": mock.mock["user1"]["guardrail_action2"],
        #             "project": cls.project1_id,
        #             "message_tag_definition": {
        #                 "id": None,
        #                 "name": "greet",
        #                 "description": "User greets the bot",
        #                 "project": cls.project1_id,
        #                 "message_tag_group": None,
        #             },
        #             "examples": ["hey", "hi", "hello", "sup", "salut"],
        #         }
        #     ),
        #     headers=headers,
        #     content_type="application/json",
        # ).json()
        # cls.guardrail_response2 = response

    def test_01_access_token_length(self):
        self.assertGreater(len(self.user1_access_token), 250)
        self.assertGreater(len(self.user2_access_token), 250)

    def test_02_project_token_length(self):
        self.assertEqual(len(self.project1_token_key), 10)
        self.assertEqual(len(self.project2_token_key), 10)

    def test_03_user1_can_list_projects(self):
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.get(
            "/api/project/",
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertEqual(len(response), 1)
        self.assertEqual(response[0]["id"], self.project1_id)

    def test_04_user2_can_list_projects(self):
        headers = {"Authorization": "JWT " + self.user2_access_token}
        response = self.client.get(
            "/api/project/",
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertEqual(len(response), 1)
        self.assertEqual(response[0]["id"], self.project2_id)

    def test_05_project1_token_update(self):
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.put(
            f"/api/project/{self.project1_id}/update-token/",
            {"name": mock.mock["user1"]["project_name"]},
            headers=headers,
            content_type="application/json",
        ).json()
        token = response["token"]
        self.assertNotEqual(
            token, self.project1_token_key
        )  # assert if token has changed
        self.project1_token_key = token  # update project1 token for the class
        self.assertGreater(len(token), 30)

    def test_06_project1_name_update(self):
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.put(
            f"/api/project/{self.project1_id}/update/",
            {"name": mock.mock["user1"]["project_name"] + " update"},
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertEqual(
            response["name"], mock.mock["user1"]["project_name"] + " update"
        )

    def test_07_message_tag_group_uuid(self):
        self.assertTrue(test_utils.is_uuid(self.message_tag_group1_id))

    def test_08_message_tag_group_wrong_project_id(self):
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.post(
            f"/api/message-tag-group/create/",
            {
                "name": mock.mock["user1"]["message_tag_group_name"],
                "project": mock.mock["uuid"],
            },
            headers=headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)

    def test_09_message_tag_definition_uuid(self):
        self.assertTrue(test_utils.is_uuid(self.message_tag_definition1_id))

    def test_10_message_tag_rule_uuid(self):
        self.assertTrue(test_utils.is_uuid(self.message_tag_rule1_id))

    def test_11_message_tag_rule_wrong_message_tag_definition_id(self):
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.post(
            "/api/message-tag-rule/create/",
            json.dumps(
                {
                    "direction": "inbound",
                    "rule_type": "contains",
                    "rule_content": "summary",
                    "project": self.project1_id,
                    "message_tag_definition": mock.mock["uuid"],
                }
            ),
            headers=headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)

    def test_13_project_detail(self):
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.get(
            f"/api/project/{self.project1_id}/details/",
            {"name": mock.mock["user1"]["project_name"]},
            headers=headers,
        ).json()
        self.assertEqual(response["id"], self.project1_id)

    def test_14_project_delete(self):
        self.assertEqual(
            len(Project.objects.all()), 3
        )  # 2 projects created for user and 1 for default user
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.delete(
            f"/api/project/{self.project1_id}/delete/",
            {"name": mock.mock["user1"]["project_name"]},
            headers=headers,
        )
        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            len(Project.objects.all()), 2
        )  # one for copied and one created during testing

    def test_17_guardrail_action_object(self):
        """Tests if the guardrail action object created during setup is correct"""
        self.assertTrue(
            test_utils.is_uuid(self.guardrail_response1["guardrail_action"]["id"])
        )
        self.assertEqual(
            self.guardrail_response1["guardrail_action"]["action_type"],
            mock.mock["user1"]["guardrail_action"]["action_type"],
        )
        self.assertEqual(
            self.guardrail_response1["guardrail_action"]["content"],
            mock.mock["user1"]["guardrail_action"]["content"],
        )

    def test_18_user1_can_list_guardrail_actions(self):
        """Tests if user can list guardrail actions"""
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.get(
            "/api/guardrail-actions/",
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertEqual(len(response), 1)
        self.assertEqual(
            response[0]["id"], self.guardrail_response1["guardrail_action"]["id"]
        )

    def test_19_user1_can_create_guardrail_action(self):
        """Tests guardrail action create view"""
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.post(
            "/api/guardrail-actions/create/",
            json.dumps({"action_type": "scripted", "content": "__start__"}),
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertTrue(test_utils.is_uuid(response["id"]))

    def test_20_guardrail_action_create_view_fails_on_wrong_action_type(self):
        """Tests guardrail action create view fails on wrong action type"""
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.post(
            "/api/guardrail-actions/create/",
            json.dumps({"action_type": "fail-here", "content": "__start__"}),
            headers=headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)

    def test_21_user1_can_update_guardrail_action(self):
        """Tests if user can update guardrail action"""
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.put(
            f"/api/guardrail-actions/{self.guardrail_response1['guardrail_action']['id']}/update/",
            json.dumps({"action_type": "generative", "content": "__end__"}),
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertEqual(response["action_type"], "generative")
        self.assertEqual(response["content"], "__end__")

    def test_22_guardrail_response_object(self):
        """Tests if the guardrail response1 object created during setup is correct"""
        self.assertTrue(test_utils.is_uuid(self.guardrail_response1["id"]))
        self.assertEqual(self.guardrail_response1["project"], self.project1_id)
        self.assertEqual(
            self.guardrail_response1["message_tag_definition"]["id"],
            self.message_tag_definition1_id,
        )

    # def test_23_guardrail_response_object(self):
    #     """Tests if the guardrail response2 object created during setup is correct"""
    #     self.assertTrue(test_utils.is_uuid(self.guardrail_response2["id"]))
    #     self.assertTrue(test_utils.is_uuid(self.guardrail_response2["message_tag_definition"]["id"]))
    #     self.assertEqual(self.guardrail_response2["project"], self.project1_id)

    def test_24_user1_can_list_project_guardrail_responses(self):
        """Tests if user can list project guardrail responses"""
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.get(
            f"/api/guardrail-responses/?project_id={self.project1_id}",
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertEqual(len(response), 1)
        self.assertEqual(response[0]["id"], self.guardrail_response1["id"])

    def test_25_user1_can_not_list_all_guardrail_responses(self):
        """If project_id is not present as query parameter
        Response should be an empty list"""
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.get(
            "/api/guardrail-responses/",
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertEqual(len(response), 0)

    def test_26_user1_can_retrieve_one_guardrail_response(self):
        """Tests guardrail response retrieve view"""
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.get(
            f"/api/guardrail-responses/{self.guardrail_response1['id']}/",
            headers=headers,
            content_type="application/json",
        ).json()
        self.assertEqual(response["id"], self.guardrail_response1["id"])
        self.assertEqual(
            response["guardrail_action"]["id"],
            self.guardrail_response1["guardrail_action"]["id"],
        )
        self.assertEqual(response["project"], self.project1_id)
        self.assertEqual(
            response["message_tag_definition"]["id"], self.message_tag_definition1_id
        )

    def test_27_user1_can_delete_guardrail_response(self):
        """Tests guardrail response delete view
        It should delete the guardrail response, and related guardrail action"""
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.delete(
            f"/api/guardrail-responses/{self.guardrail_response1['id']}/delete/",
            headers=headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 204)
        self.assertIsNone(GuardrailAction.objects.first())
        self.assertIsNone(GuardrailResponse.objects.first())

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()


class ProjectCreateAPITests(TestCase):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    @classmethod
    def setUpTestData(cls):
        set_up_data = test_utils.user_and_project_set_up(cls.client)
        cls.user = set_up_data["user"]
        cls.workspace = set_up_data["workspace"]
        cls.headers = set_up_data["headers"]

        cls.chatbot_model = Chatbot.GenerativeModel.OPENAI_GPT_TURBO
        cls.chatbot_description = "test chatbot"

    def test_create_project_successfully(self):
        url = reverse("project_create_view")
        data = {
            "name": mock.mock["user1"]["project_name"],
            "workspace_id": str(self.workspace.id),
        }
        json_data = json.dumps(data)
        response = self.client.post(
            url, json_data, content_type="application/json", headers=self.headers
        )
        created_project = response.data

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        project = Project.objects.get(id=created_project["id"])
        self.verify_records_copied(project)

    def verify_records_copied(self, project):
        s_project = Project.objects.get(
            name=CONSTANTS.DEFAULT_USER_DEFAULT_PROJECT_NAME,
            workspace__owner__email=CONSTANTS.DEFAULT_USER_EMAIL,
        )
        s_dataset = Dataset.objects.filter(project=s_project)
        t_dataset = Dataset.objects.filter(project=project)
        self.assertEqual(
            len(s_dataset),
            len(t_dataset),
            "Number of copied `Dataset` records doesn't match source one",
        )
        s_ml_model = MLModel.objects.filter(project=s_project)
        t_ml_model = MLModel.objects.filter(project=project)
        self.assertEqual(
            len(s_ml_model),
            len(t_ml_model),
            "Number of copied `MLModel` records doesn't match source one",
        )
        s_msg_tag_groups = MessageTagGroup.objects.filter(project=s_project)
        t_msg_tag_groups = MessageTagGroup.objects.filter(project=project)
        self.assertEqual(
            len(s_msg_tag_groups),
            len(t_msg_tag_groups),
            "Number of copied `MessageTagGroup` records doesn't match source one",
        )
        s_messages = Message.objects.filter(project=s_project)
        t_messages = Message.objects.filter(project=project)
        self.assertEqual(
            len(s_messages),
            len(t_messages),
            "Number of copied `Message` records doesn't match source one",
        )
        s_tag_definitions = MessageTagDefinition.objects.filter(project=s_project)
        t_tag_definitions = MessageTagDefinition.objects.filter(project=project)
        self.assertEqual(
            len(s_tag_definitions),
            len(t_tag_definitions),
            "Number of copied `MessageTagDefinition` records doesn't match source one",
        )
        s_message_tags = MessageTag.objects.filter(ml_model=s_ml_model.first())
        t_message_tags = MessageTag.objects.filter(ml_model=t_ml_model.first())
        self.assertEqual(
            len(s_message_tags),
            len(t_message_tags),
            "Number of copied `MessageTag` records doesn't match source one",
        )

    def test_create_project_missing_name(self):
        url = reverse("project_create_view")
        data = {}  # Missing 'name'
        response = self.client.post(url, data, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_project_unauthorized(self):
        url = reverse("project_create_view")
        data = {"name": "New Project"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def tearDown(self):
        UserAccount.objects.all().delete()
        Project.objects.filter(workspace__owner=self.user).delete()


class ChatbotCreateAPITests(TestCase):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    @classmethod
    def setUpTestData(cls):
        set_up_data = test_utils.user_and_project_set_up(cls.client)
        cls.user = set_up_data["user"]
        cls.project = set_up_data["project"]
        cls.headers = set_up_data["headers"]

        cls.provider = Provider.objects.create(
            label="Test",
            type=ProviderChoices.OPENROUTER.value,
            workspace=set_up_data["workspace"],
            api_key="abcd",
        )
        cls.generative_model, _ = GenerativeModel.objects.get_or_create(
            label="Test Generative model",
            model_string="openrouter/meta-llama/llama-3-70b-instruct",
            priority=1,
            provider_type=ProviderChoices.OPENROUTER.value,
        )

        cls.chatbot_description = "test chatbot"

    def test_create_chatbot_successfully(self):
        url = reverse("create_chatbot_view")
        data = {
            "project_id": str(self.project.id),
            "description": self.chatbot_description,
            "config": {"prompt_settings": {}, "knowledge_settings": {}},
            "origin": "first_chatbot",
            "llm_provider_id": str(self.provider.id),
            "model_id": str(self.generative_model.id),
        }
        json_data = json.dumps(data)
        response = self.client.post(
            url, json_data, content_type="application/json", headers=self.headers
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.verify_chatbot_created()

    def verify_chatbot_created(self):
        project_chatbots = Chatbot.objects.filter(project=self.project)
        self.assertTrue(project_chatbots.exists(), "No chatbot found for project.")
        self.assertEqual(
            len(project_chatbots), 1, "Amount of created chatbots should be 1"
        )
        chatbot = project_chatbots.first()
        self.assertEqual(
            chatbot.generative_model,
            self.generative_model,
            "Chatbot generative model doesn't match the one specified during creation",
        )
        self.assertEqual(
            chatbot.provider,
            self.provider,
            "Chatbot provider doesn't match the one specified during creation",
        )
        self.assertIn(
            "general_prompt",
            chatbot.config,
            "Chatbot config doesn't contain 'general_prompt' key",
        )
        self.assertIn(
            "knowledge",
            chatbot.config,
            "Chatbot config doesn't contain 'knowledge' key",
        )
        self.assertIn(
            "bot_type", chatbot.config, "Chatbot config doesn't contain 'bot_type' key"
        )

    def tearDown(self):
        Chatbot.objects.filter(project=self.project).delete()

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()
