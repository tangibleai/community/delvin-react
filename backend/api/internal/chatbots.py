import logging

from django.http import JsonResponse
from django.utils.translation import gettext_lazy as _
from rest_framework import generics, serializers, status
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import Chat, Chatbot, GenerativeModel, Plan, Project, Provider
from api.serializers import ChatbotInternalResponseViewRequestSerializer
from api.services.chatbot_engine import engine_response
from api.tasks import create_response_logs, evaluate_plan_type_and_properties
from api.utils.prompt_configuration import (
    create_chatbot_config_object,
)
from utils.chatbot_utils import (
    create_message_from_request,
    save_response_message,
    get_or_create_contact_and_chat,
)


class ChatbotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chatbot
        fields = "__all__"


class ChatbotsListRequestSerializer(serializers.Serializer):
    project_id = serializers.UUIDField(required=False)
    provider_id = serializers.UUIDField(required=False)


class ChatbotsListView(generics.ListAPIView):
    serializer_class = ChatbotSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        serializer = ChatbotsListRequestSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        provider_id = validated_data.get("provider_id")
        if provider_id:
            provider_chatbots = Chatbot.objects.filter(provider__id=provider_id)
            return provider_chatbots

        project_id = validated_data.get("project_id")

        queryset = Chatbot.objects.filter(project__id=project_id)

        return queryset


chatbots_list_view = ChatbotsListView.as_view()


class CreateChatbotView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ChatbotSerializer

    def post(self, request, *args, **kwargs):
        data = request.data

        try:
            chatbot_config = create_chatbot_config_object(
                prompt_settings=data["config"],
            )
        except Exception as e:
            logging.error(f"Error creating a chatbot config object: {data}", e)
            chatbot_config = {}

        project_id = request.query_params.get("project_id", None) or data.get(
            "project_id", None
        )
        project = Project.objects.filter(id=project_id).first()
        if not project:
            raise serializers.ValidationError("Project not found.")

        generative_model_id = data.get("model_id")
        generative_model = GenerativeModel.objects.filter(
            id=generative_model_id
        ).first()
        if not generative_model:
            raise serializers.ValidationError("Provided model isn't recognized.")

        origin = data.get("origin")

        llm_provider_id = data.get("llm_provider_id")
        provider_rec = Provider.objects.get(id=llm_provider_id)

        chatbot = Chatbot.objects.create(
            project=project,
            generative_model=generative_model,
            config=chatbot_config,
            origin=origin,
            provider=provider_rec,
        )
        serializer = self.serializer_class(chatbot)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


create_chatbot_view = CreateChatbotView.as_view()


class UpdateChatbotView(generics.UpdateAPIView):
    """
    PATCH method is forbidden.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ChatbotSerializer

    def get_queryset(self):
        queryset = Chatbot.objects.filter(id=self.kwargs.get("pk"))
        return queryset

    def put(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=False)

        validated_data = serializer.validated_data
        instance = self.get_object()
        Chatbot.objects.filter(id=instance.id).update(**validated_data)
        instance = self.get_object()

        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)


update_chatbot_view = UpdateChatbotView.as_view()


class ChatbotResponseForInternalUsage(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ChatbotInternalResponseViewRequestSerializer

    def post(self, request):
        user_message_data = request.data

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        project = Project.objects.filter(id=user_message_data["project_id"]).first()

        message, errors = create_message_from_request(
            user_message_data, project, internal=True
        )

        chatbot_origin = validated_data.get("assistant", "default_assistant")
        chatbot = Chatbot.objects.filter(origin=chatbot_origin, project=project).first()

        plan = project.workspace.plan

        if not message:
            return Response({"errors": errors})

        if not chatbot:
            return JsonResponse(
                {"error": "Assistant with this ID not found."}, status=404
            )

        # When expired allows free plan to continue with free models.
        # Paid plan is blocked if status is not ACTIVE.
        if plan.type == Plan.Type.PAID and plan.status != Plan.Status.ACTIVE:
            response = "Thank you for using our service. To send more messages, please consider upgrading your plan."
            response_dict = {"response_text": response, "source": "P", "context": None}
        else:
            response_dict = engine_response(message, chatbot, message.chat)

        # Evaluate plan type for generative messages and calculate quota
        chatbot_uses_default_llm_provider = (
            chatbot.provider == Provider.get_default_provider()
        )
        evaluate_plan_type_and_properties.delay(
            project.id, chatbot_uses_default_llm_provider
        )

        # Creates a message object for the response
        response_message_object = save_response_message(
            response_dict["response_text"], message
        )

        # Creates a response log for the user message and the response.
        # Message is the incoming message from the user and response is the bot response.
        # Both are message objects.
        create_response_logs.delay(
            user_message_id=message.id,
            response_id=response_message_object.id,
            source=response_dict["source"],
            ai_usage={"usage_records": [], "cost": -1},
            metadata={},
            context=response_dict["context"],
        )

        return Response(response_dict["response_text"])


get_chatbot_response_for_internal_usage = ChatbotResponseForInternalUsage.as_view()


class ChatbotDeleteView(generics.DestroyAPIView):
    """Deletes the Chatbot associated with the PK passed via the URL"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Chatbot.objects.all()
    serializer_class = ChatbotSerializer
    lookup_field = "pk"


chatbot_delete_view = ChatbotDeleteView.as_view()
