from django.db.models import Prefetch
from rest_framework import generics, serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.internal.serializers import (MessageTagDefinitionSerializer, MessageTagSerializer)
from api.models import (Contact, MLModel, Message, MessageTag, MessageTagDefinition, Project)
from api.utils import tags_prediction
from core.constant import CONSTANTS
from scripts.ml import embedding


class MessageTagPOSTRequestSerializer(serializers.Serializer):
    project_id = serializers.UUIDField()
    message_text = serializers.CharField()
    tag_ids = serializers.ListField(child=serializers.CharField())

    def validate_tag_ids(self, value):
        """
        Validates that passed value set to "tag_ids" field is an array of strings.
        """

        if not isinstance(value, list):
            raise serializers.ValidationError("tag_ids must be a list.")
        if not all(isinstance(tag_id, str) for tag_id in value):
            raise serializers.ValidationError("All items in tag_ids must be strings.")

        return value


class MessageTagView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    post_request_serializer_class = (
        MessageTagPOSTRequestSerializer  # for validating request payload/query_params
    )
    response_serializer = MessageTagSerializer

    def post(self, request):
        serializer = self.post_request_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        project_id = validated_data.get("project_id")
        message_text = validated_data.get("message_text")
        tag_ids = validated_data.get("tag_ids")
        project = Project.objects.get(id=project_id)
        tags = [MessageTagDefinition.objects.get(id=tag_id) for tag_id in tag_ids]

        contact = Contact.objects.get_or_create(
            origin=CONSTANTS.DEFAULT_DATASET_CONTACT_FOR_ANY_USER,
            project=project,
            internal=True,
        )[0]
        text_embedding = embedding.embed_text(message_text)
        message = Message.objects.create(
            direction=Message.Direction.OUTBOUND,
            sender_type=Message.SenderType.USER,
            channel_type=Message.ChannelType.WEB,
            text=message_text,
            embedding=text_embedding,
            project=project,
            contact=contact,
            chat=None,
        )

        serialized_recs = []

        for tag in tags:
            message_tag = MessageTag.objects.create(message=message, tag=tag)
            serialized_rec = self.response_serializer(message_tag).data
            serialized_recs.append(serialized_rec)

        ml_model = MLModel.objects.filter(project=project).first()
        ml_model.set_ml_model_status_to_needs_retraining()

        return Response(serialized_recs)


message_tag_view = MessageTagView.as_view()


class MessageTagsWithTagRequestSerializer(serializers.Serializer):
    message_tag_definition_id = serializers.UUIDField(required=True)


class MessageTagsWithTagResponseSerializer(MessageTagSerializer):
    message_tags = serializers.SerializerMethodField()

    def get_message_tags(self, obj):
        message_tag_definitions = []
        for item in obj.message.tagged_messages:
            serialized_tag = MessageTagDefinitionSerializer(item.tag).data
            message_tag_definitions.append(serialized_tag)

        return message_tag_definitions


class MessageTagsWithTagView(APIView):
    """Searches for `MessageTag` records containing provided tag. For each `MessageTag.message` record looks up for all other tags associated with this message"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    request_serializer_class = MessageTagsWithTagRequestSerializer  # for validating request payload/query_params
    response_serializer = MessageTagsWithTagResponseSerializer

    def get(self, request):
        serializer = self.request_serializer_class(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        message_tag_definition_id = validated_data.get("message_tag_definition_id")

        message_tags_prefetch = Prefetch(
            "message__messagetag_set",
            queryset=MessageTag.objects.select_related("tag"),
            to_attr="tagged_messages",
        )
        message_tag_recs_with_fetched_recs = (
            MessageTag.objects.filter(tag__id=message_tag_definition_id)
            .select_related("message", "tag")
            .prefetch_related(message_tags_prefetch)
        )
        serialized_response = self.response_serializer(
            message_tag_recs_with_fetched_recs, many=True
        )

        return Response(serialized_response.data)


message_tags_with_tag_view = MessageTagsWithTagView.as_view()


class UntagMessageView(generics.DestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = MessageTag.objects.all()
    serializer_class = MessageTagSerializer

    def perform_destroy(self, instance):
        project = instance.message.project

        super().perform_destroy(instance)

        ml_model = MLModel.objects.filter(project=project).first()
        ml_model.set_ml_model_status_to_needs_retraining()


untag_message_view = UntagMessageView.as_view()


class PredictTagsViewRequestSerializer(serializers.Serializer):
    text = serializers.CharField(required=True)
    project_id = serializers.UUIDField(required=True)


class PredictTagsViewResponseSerializer(serializers.Serializer):
    tag = MessageTagDefinitionSerializer(required=True)
    score = serializers.FloatField(required=True)


class PredictTagsView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = (
        PredictTagsViewRequestSerializer  # for validating request payload/query_params
    )
    response_serializer = PredictTagsViewResponseSerializer

    def get(self, request):
        serializer = self.serializer_class(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        text = validated_data.get("text", "")
        project_id = validated_data.get("project_id")

        project = Project.objects.get(id=project_id)

        predicted_tags = tags_prediction.predict_tags(text, project)

        response_serializer = self.response_serializer(predicted_tags, many=True)
        serialized_predicted_tags = response_serializer.data

        return Response(serialized_predicted_tags)


predict_tags_view = PredictTagsView.as_view()
