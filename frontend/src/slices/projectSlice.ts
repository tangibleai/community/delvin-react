import { Project } from "@/typings/project";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface SliceProject {
  welcomeMessageShown: boolean;
}

const initialState: SliceProject = {
  welcomeMessageShown: false,
};

const projectSlice = createSlice({
  name: "project",
  initialState,
  reducers: {
    setWelcomeMessageShown: (state, action: PayloadAction<boolean>) => {
      state.welcomeMessageShown = action.payload;
    },
  },
});

export const { setWelcomeMessageShown } = projectSlice.actions;
export const updateCurrentProject = (state: { project: Project | null }) =>
  state.project;
export default projectSlice.reducer;
