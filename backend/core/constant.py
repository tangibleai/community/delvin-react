import hashlib
from pathlib import PurePosixPath

from django.conf import settings


class CONSTANTS:
    """
    Constants cannot be changed at runtime
    """

    MAXIMUM_TOKEN_PREFIX_LENGTH = 5  # Down from 10
    TOKEN_KEY_LENGTH = 10  # Down from 15
    AUTH_TOKEN_CHARACTER_LENGTH = 32  # Down from 64
    DIGEST_LENGTH = 128
    SECURE_HASH_ALGORITHM = hashlib.sha512
    TOKEN_PREFIX = "Token"

    # for api application
    DEFAULT_CHAT_TITLE = "Default"
    DEFAULT_CHAT_ORIGIN_ID = "default chat"

    DATA_DIR = settings.BASE_DIR / "data"

    # constants to load a joblib model from s3. Not in use ATM
    MODEL_NAME = "multi_intent_recognizer"
    INTENT_RECOGNIZER_PATH = PurePosixPath(MODEL_NAME).with_suffix(".pkl")
    S3_BUCKET = "nlpia"
    S3_REGION = "fra1"
    S3_FQDN = f"{S3_BUCKET}.{S3_REGION}.digitaloceanspaces.com"

    S3_CREDS_DICT = dict(
        # https://nlpia.fra1.digitaloceanspaces.com/multi_intent_recognizer.pkl
        # OR https://fra1.digitaloceanspaces.com/nlpia/multi_intent_recognizer.pkl
        endpoint_url=f"https://{S3_REGION}.digitaloceanspaces.com",
        region_name=S3_REGION,
        aws_access_key_id="DO0092LZ...........",
        aws_secret_access_key="fms.....................................",
    )

    CONTACT_ORIGIN_FOR_CHATBOT = (
        "chatbot contact"  # origin of contact that stores all chatbot conversations
    )

    CHAT_FOR_MANUALLY_TAGGED_MESSAGES = "manually tagged messages"

    DEFAULT_USER_EMAIL = "engineering@tangibleai.com"
    DEFAULT_USER_DEFAULT_PROJECT_NAME = "Default Project"
    DEFAULT_USER_DATASET_NAME = "Default Dataset"
    DEFAULT_DATASET_CONTACT_FOR_ANY_USER = "Default Contact"

    KNOWLEDGE_TAG_THRESHOLD = 0.5

    TEST_USER_EMAIL_PATTERN = r"user\d+@example.com"

    MAX_FREE_MESSAGES = 2000
    WARNING_RATIO = 0.8
    DEFAULT_CACHE_TIMEOUT = 60 * 15

    def __setattr__(self, *args, **kwargs):
        raise Exception(
            """
            Constant values must NEVER be changed at runtime, as they are
            integral to the structure of database tables
            """
        )


CONSTANTS = CONSTANTS()
