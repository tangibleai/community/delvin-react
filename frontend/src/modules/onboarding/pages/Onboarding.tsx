import { useRef } from "react";
import { MdWest, MdEast } from "react-icons/md";

import { Button } from "@/components/ui/button";
import { Progress } from "@/components/ui/progress";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/store";
import {
  setNextOnboardingState,
  setPreviousOnboardingState,
} from "@/slices/onboardingSlice";
import { StateName } from "../types/state_names";
import componentMap from "../utils/componentMap";
import { getPageTitleForState } from "../utils/pageTitle";
import StopOnboardingModal from "../components/StopOnboardingModal";

type StepAssistantNameRef = {
  submitForm: () => Promise<boolean>; // updated onboardingData with verified data
  updateOnboardingDataWithDraft: () => void; // updated onboardingData with non-finished draft
};

function Onboarding() {
  const dispatch: AppDispatch = useDispatch();
  const formRef = useRef<StepAssistantNameRef>(null);
  const currentOnboardingState = useSelector(
    (state: RootState) => state.onboarding.currentOnboardingState,
  );
  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData,
  );

  const onSubmitButtonClick = async ({ next = true }) => {
    if (!formRef.current) return;

    if (next) {
      const isFormValid = await formRef.current.submitForm();
      if (!isFormValid) return;

      if (currentOnboardingState.name !== StateName.StepProjectCreation) {
        dispatch(setNextOnboardingState());
      }
    } else {
      await formRef.current.updateOnboardingDataWithDraft();
      dispatch(setPreviousOnboardingState());
    }
  };

  const ComponentToRender = componentMap[currentOnboardingState.name];

  const pageTitle = getPageTitleForState(
    onboardingData,
    currentOnboardingState,
  );

  return (
    <div className="flex flex-col min-h-screen">
      <div className="flex-grow">
        <div className="absolute top-0 right-0 m-4">
          <StopOnboardingModal />
        </div>
        {currentOnboardingState.name !== StateName.StepProjectCreation && (
          <div className="flex flex-col mt-4 items-center mx-auto gap-3">
            <Progress
              value={currentOnboardingState.progress}
              className="w-1/5 border-2 border-black h-6"
            />
          </div>
        )}

        <div className="items-center mx-auto flex flex-col mt-[2.5vh] w-full">
          {ComponentToRender ? <ComponentToRender ref={formRef} /> : null}
        </div>
      </div>
      {currentOnboardingState.name !== StateName.StepProjectCreation && (
        <div className="sticky bottom-0 py-8 flex justify-center gap-16">
          {currentOnboardingState.name !== StateName.StepAssistantName && (
            <Button
              className="gap-2 text-xl"
              onClick={() => onSubmitButtonClick({ next: false })}
            >
              <MdWest />
              Back
            </Button>
          )}
          <Button
            className="gap-2 text-xl"
            onClick={() => onSubmitButtonClick({})}
          >
            Next
            <MdEast />
          </Button>
        </div>
      )}
    </div>
  );
}

export default Onboarding;
