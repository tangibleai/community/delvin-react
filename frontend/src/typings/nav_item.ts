export interface NavItem {
  title: string;
  href: string;
  icon: any;
  color?: string;
}
