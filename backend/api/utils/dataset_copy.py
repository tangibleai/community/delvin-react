from api import models
from core.constant import CONSTANTS

from django.db.models import Count, Prefetch


def copy_dataset_and_dependencies(
    t_project_id,
    t_owner_email,
    s_project_name=CONSTANTS.DEFAULT_USER_DEFAULT_PROJECT_NAME,
    s_owner_email=CONSTANTS.DEFAULT_USER_EMAIL,
    s_dataset_name=CONSTANTS.DEFAULT_USER_DATASET_NAME,
    t_dataset_name=CONSTANTS.DEFAULT_USER_DATASET_NAME,
):
    """Creates a copy of dataset and related records and assignes them to target user project

    Every variable starting with "s_" is referred to source, the same with "_t" refers to target
    """

    t_records = {
        "dataset": None,
        "message_tag_groups": [],
        "message_tag_definitions": [],
        "messages": [],
        "message_tag_recs": [],
        "ml_weights": [],
    }

    s_project = models.Project.objects.get(
        name=s_project_name, workspace__owner__email=s_owner_email
    )
    s_dataset = models.Dataset.objects.get(project=s_project, name=s_dataset_name)

    t_user = models.UserAccount.objects.get(email=t_owner_email)
    t_project = models.Project.objects.get(id=t_project_id, workspace__owner=t_user)
    t_dataset = models.Dataset.objects.get_or_create(
        name=t_dataset_name,
        description=s_dataset.description,
        project=t_project,
    )[0]
    t_records["dataset"] = t_dataset
    t_ml_model = models.MLModel.objects.get_or_create(
        project=t_project, created_by=t_user, dataset=t_dataset
    )[0]
    t_ml_model.dataset = t_dataset
    t_ml_model.save()

    s_tag_defs = (
        models.MessageTagDefinition.objects.filter(project=s_project)
        .prefetch_related(Prefetch("mlweights_set"))
        .select_related("message_tag_group")
    )

    tag_name_related_objects_mapping = {
        tag.name: {"s_tag_def": tag} for tag in s_tag_defs
    }

    # creating `MessageTagGroup` record
    t_msg_tag_group = models.MessageTagGroup.objects.create(
        name=s_tag_defs.first().message_tag_group.name, project=t_project
    )
    t_records["message_tag_groups"].append(t_msg_tag_group)

    for tag in s_tag_defs:
        tag_recs_group = tag_name_related_objects_mapping.get(tag.name)
        tag_recs_group.setdefault("t_msg_tag_group", t_msg_tag_group)

    # creating `MessageTagDefinition` records
    for t_name, recs in tag_name_related_objects_mapping.items():
        t_tag_def = models.MessageTagDefinition(
            name=t_name,
            description=recs["s_tag_def"].description,
            project=t_project,
            message_tag_group=recs["t_msg_tag_group"],
        )
        t_records["message_tag_definitions"].append(t_tag_def)
        recs["t_tag_def"] = t_tag_def
    models.MessageTagDefinition.objects.bulk_create(
        t_records["message_tag_definitions"]
    )

    # creating `MLWeights` records
    for rec, mapping in zip(s_tag_defs, tag_name_related_objects_mapping.items()):
        s_ml_weights = rec.mlweights_set.first()
        if s_ml_weights is None:
            continue

        coef = s_ml_weights.coef
        intercept = s_ml_weights.intercept

        tag = mapping[-1]["t_tag_def"]

        t_ml_weights = models.MLWeights(
            tag_definition=tag,
            coef=coef,
            intercept=intercept,
            ml_model=t_ml_model,
        )
        t_records["ml_weights"].append(t_ml_weights)
    models.MLWeights.objects.bulk_create(t_records["ml_weights"])

    # creating Message and MessageTag
    contact = models.Contact.objects.get_or_create(
        project=t_project,
        origin=CONSTANTS.DEFAULT_DATASET_CONTACT_FOR_ANY_USER,
        internal=True,
    )[0]
    message_tags_prefetch = Prefetch(
        "messagetag_set",  # The related name for accessing MessageTag from Message
        queryset=models.MessageTag.objects.select_related("tag"),
    )
    s_messages = (
        models.Message.objects.filter(project=s_project)
        .annotate(message_tag_group_count=Count("messagetag"))
        .filter(message_tag_group_count__gt=0)
    ).prefetch_related(message_tags_prefetch)

    for s_msg in s_messages:
        init_t_msg_params = dict(
            project=t_project,
            direction=s_msg.direction,
            sender_type=s_msg.sender_type,
            channel_type=s_msg.channel_type,
            text=s_msg.text,
            embedding=s_msg.embedding,
            contact=contact,
            chat=None,
        )
        t_msg = models.Message(**init_t_msg_params)
        t_records["messages"].append(t_msg)

        s_message_tag_group = s_msg.messagetag_set.all()

        for s_message_tag in s_message_tag_group:
            s_tag = s_message_tag.tag
            t_tag = tag_name_related_objects_mapping.get(s_tag.name).get("t_tag_def")

            t_message_tag_rec = models.MessageTag(
                message=t_msg, tag=t_tag, ml_model=t_ml_model
            )
            t_records["message_tag_recs"].append(t_message_tag_rec)
    models.Message.objects.bulk_create(t_records["messages"])
    models.MessageTag.objects.bulk_create(t_records["message_tag_recs"])

    return t_records
