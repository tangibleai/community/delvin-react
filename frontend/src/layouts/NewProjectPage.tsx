import { useState } from "react";
import { Outlet, useParams } from "react-router-dom";

import Sidebar from "@/components/layout/Sidebar";
import Loading from "@/components/Loading";
import OutOfBanners from "@/modules/project_home/components/OutofBanners";
import { RootState } from "@/store";
import { useSelector } from "react-redux";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";
import { Project } from "@/typings/project";

function ProjectPage() {
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated
  );
  const user = useSelector((state: RootState) => state.auth.user);

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const { data: project = {} as Project, isFetching: retrievingProject } =
    useGetCurrentProjectDetailsQuery(projectId);

  if (retrievingProject) {
    return <Loading />;
  }

  if (isAuthenticated && user && project.id) {
    return (
      <>
        <div className="flex border-collapse overflow-hidden">
          <Sidebar />
          <main className="flex-1 overflow-y-auto overflow-x-hidden min-h-full pb-1 px-10">
            <OutOfBanners />
            <Outlet />
          </main>
        </div>
      </>
    );
  }

  return <Loading />;
}

export default ProjectPage;
