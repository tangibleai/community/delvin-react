import { useParams } from "react-router-dom";
import { useState } from "react";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { HiTrash } from "react-icons/hi2";

import { useDeleteDocumentMutation } from "@/slices/api/documentSlice";
import { Document } from "@/typings/document";

const styles = {
  icon_size: 24,
};

type Props = {
  documentToDelete: any;
  onDeleteDocument: (deleteDocument: Document) => void;
};

function DeleteDocumentModal({ documentToDelete, onDeleteDocument }: Props) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const routeParams = useParams();
  const projectId = routeParams.projectId;

  const [deleteDocument, { isLoading, error }] = useDeleteDocumentMutation();

  function handleClickDelete(documentToDelete: Document) {
    try {
      deleteDocument(documentToDelete.id).unwrap();
      onDeleteDocument(documentToDelete);
      setIsModalOpen(false);
    } catch (err) {
      console.log("Document failed to delete");
    }
  }

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button variant="ghost">
          <HiTrash size={styles.icon_size} title="Delete document" />
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-md">
        <DialogHeader>
          <DialogTitle>Delete document</DialogTitle>
        </DialogHeader>
        <p>Are you sure you want to delete the following document?</p>
        <li className="ml-4 font-style: italic list-disc">
          "{documentToDelete.title}"
        </li>
        <DialogFooter>
          <Button
            variant="destructive"
            className="order-2 sm:order-1"
            onClick={() => handleClickDelete(documentToDelete)}
          >
            Delete
          </Button>
          <Button
            variant="outline"
            className="order-1 sm:order-2"
            onClick={() => setIsModalOpen(false)}
          >
            Cancel
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}

export default DeleteDocumentModal;
