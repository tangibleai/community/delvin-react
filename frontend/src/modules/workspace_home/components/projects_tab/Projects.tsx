import { Link, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";

import { Card, Title, Subtitle } from "@tremor/react";

import { RootState } from "@/store";
import { Workspace } from "@/typings/workspace";
import { Member, MemberRoles } from "@/typings/member";
import { useToast } from "@/components/ui/use-toast";
import {
  useLazyListProjectsQuery,
  useListProjectsQuery,
} from "@/slices/api/projectsApiSlice";
import { NewProjectModal } from "./NewProjectModal";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { Project } from "@/typings/project";
import Loading from "@/components/Loading";
import ErrorScreen from "@/components/ErrorScreen";
import { useGetTeamMembersQuery } from "@/slices/api/teamMemberApiSlice";
import { Skeleton } from "@/components/ui/skeleton";

function Projects() {
  const dispatch = useDispatch();
  const { toast } = useToast();
  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const { data: activeWorkspace, isError: errorRetrievingWorkspace } =
    useGetWorkspaceQuery(workspaceId);

  const {
    data: projects = [],
    isLoading: projectsLoading,
    isError: errorRetrievingProjects,
  } = useListProjectsQuery(activeWorkspace?.id ?? "", {
    skip: !activeWorkspace?.id, // Skip the query if there's no workspace ID
  });
  const [userCanCreateProject, setUserCanCreateProject] = useState(false);

  const userEmail =
    useSelector((state: RootState) => state.auth.user?.email) ?? "";

  const { data: teamMembers = [], isError: errorRetrievingTeamMembers } =
    useGetTeamMembersQuery(
      {
        workspace_id: workspaceId,
        email: userEmail,
      },
      {
        skip: userEmail === "",
      }
    );

  useEffect(() => {
    if (!errorRetrievingWorkspace) return;

    const errorMessage = figureErrorMessage();

    if (errorMessage)
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: errorMessage,
      });
  }, [
    errorRetrievingWorkspace,
    errorRetrievingProjects,
    errorRetrievingTeamMembers,
  ]);

  const figureErrorMessage = () => {
    if (errorRetrievingWorkspace)
      return "Couldn't retrieve workspace. Please, try to reload the page.";
    if (errorRetrievingProjects)
      return "Couldn't retrieve projects. Please, try to reload the page.";
    if (errorRetrievingTeamMembers)
      return "Couldn't retrieve team members. Please, try to reload the page.";

    return "";
  };

  useEffect(() => {
    if (!activeWorkspace) return;

    const decideUserCanCreateProject = async () => {
      let userCanCreateProject = activeWorkspace!.owner === userEmail;

      if (!userCanCreateProject) {
        userCanCreateProject = await checkTeamMemberIsEditor();
      }

      setUserCanCreateProject(userCanCreateProject);
    };

    const checkTeamMemberIsEditor = async () => {
      const teamMemberIsEditor = teamMembers.find(
        (member) => member.role === MemberRoles.EDITOR
      );
      return Boolean(teamMemberIsEditor);
    };

    decideUserCanCreateProject();
  }, [activeWorkspace]);

  if (errorRetrievingProjects || errorRetrievingWorkspace) {
    return <ErrorScreen />;
  }

  return (
    <div className="flex flex-col gap-5 max-w-md mx-auto">
      <div className="flex flex-row justify-between items-center mt-14 w-full">
        <Title> Your Projects </Title>
        {userCanCreateProject && <NewProjectModal />}
      </div>
      <div>
        {projectsLoading ? (
          <div className="space-y-3">
            {Array.from({ length: 3 }).map((_, index) => (
              <Skeleton key={index} className="h-20 max-w-md mx-auto" />
            ))}
          </div>
        ) : projects?.length ? (
          projects.map((project) => (
            <Link to={`/project/${project.id}/home`} key={project.id}>
              <Card className="max-w-md mx-auto mt-3">
                <Subtitle> {project.name} </Subtitle>
              </Card>
            </Link>
          ))
        ) : (
          <Card className="max-w-md mx-auto mt-3 border-white ring-0">
            <Subtitle> Set up a project to create an assistant. </Subtitle>
          </Card>
        )}
      </div>
    </div>
  );
}

export default Projects;
