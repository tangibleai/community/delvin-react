import { useForm } from "react-hook-form";
import { useState } from "react";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { QuestionMarkCircleIcon } from "@heroicons/react/24/outline";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { zodResolver } from "@hookform/resolvers/zod";

import { Project } from "@/typings/project";
import { Member } from "@/typings/member";
import { IoReload } from "react-icons/io5";
import { useParams } from "react-router-dom";
import { useListProjectsQuery } from "@/slices/api/projectsApiSlice";
import { useCreateTeamMemberMutation } from "@/slices/api/teamMemberApiSlice";
import { useCreateChatMutation } from "@/slices/api/chatApiSlice";
import { useCreateContactMutation } from "@/slices/api/contactApiSlice";

type Props = {
  onAddNewMember: (newMember: Member) => void;
};

function NewMemberModal({ onAddNewMember }: Props) {
  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const [error, setError] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [showTooltip, setShowTooltip] = useState(false);
  const [processing, setProcessing] = useState(false); // when user clicks "Add team member" button set this to true

  const [createChat] = useCreateChatMutation();
  const [createContact] = useCreateContactMutation();

  const {
    data: projects = [],
    isLoading: projectsLoading,
    isError: errorLoadingProjects,
  } = useListProjectsQuery(workspaceId);
  const [createTeamMember] = useCreateTeamMemberMutation();

  const formSchema = z.object({
    email: z.string().email(),
    role: z.string().min(4, {
      message: "Please choose a role",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      role: "",
    },
  });

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    setProcessing(true);
    const { email, role } = values;
    let newMember = {} as Member;
    try {
      newMember = await createTeamMember({
        email,
        role,
        workspace_id: workspaceId,
      }).unwrap();
    } catch (error) {
      setError(String(error));
      setProcessing(false);
      return;
    }

    projects.forEach(async (project: Project) => {
      const newContactForCollaborator = await createContact({
        project_id: project.id,
        origin: `${email} (internal)`,
        internal: true,
      }).unwrap();
      await createChat(newContactForCollaborator.id).unwrap();
    });

    onAddNewMember(newMember);
    setError("");
    setProcessing(false);
    setIsModalOpen(false);
  }

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button>+ New member</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Add a new workspace member</DialogTitle>
          <DialogDescription>
            Add team members to work on the workpace projects together.
          </DialogDescription>
        </DialogHeader>
        {error && (
          <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
            Error: {error}
          </p>
        )}
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(handleSubmit)}
            className="space-y-8"
          >
            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Email</FormLabel>
                  <FormControl>
                    <Input placeholder="Your email" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="role"
              render={({ field }) => (
                <FormItem>
                  <div className="relative pt-3 flex items-center">
                    <FormLabel>Role</FormLabel>
                    <QuestionMarkCircleIcon
                      className="h-4 w-4 cursor-pointer"
                      onMouseEnter={() => setShowTooltip(true)}
                      onMouseLeave={() => setShowTooltip(false)}
                    />
                  </div>
                  {showTooltip && (
                    <div
                      data-tooltip="tooltip"
                      className="bg-gray-800 text-white p-2 rounded-md absolute bottom-28 left-1/2 transform -translate-x-1/2"
                    >
                      The role determines the permissions a team member has for
                      this workspace.
                    </div>
                  )}
                  <Select
                    onValueChange={field.onChange}
                    defaultValue={field.value}
                  >
                    <FormControl>
                      <SelectTrigger className="w-[180px]">
                        <SelectValue placeholder="Select a role" />
                      </SelectTrigger>
                    </FormControl>
                    <SelectContent>
                      <SelectGroup>
                        <SelectItem value="Editor">Editor</SelectItem>
                        <SelectItem value="Viewer">Viewer</SelectItem>
                      </SelectGroup>
                    </SelectContent>
                  </Select>
                </FormItem>
              )}
            />
            <DialogFooter>
              <Button disabled={processing} type="submit">
                {processing && (
                  <IoReload className="mr-2 h-4 w-4 animate-spin" />
                )}
                Add team member
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}

export default NewMemberModal;
