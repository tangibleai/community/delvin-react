import emptyApi from "../apiSlice";
import { Message } from "@/typings/message";

type GetMessagesRequestParams = {
  chatOrigin: string | undefined;
  projectId: string;
};

const messageApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    getMessages: builder.query<Message[], GetMessagesRequestParams>({
      query: ({ chatOrigin, projectId }) => ({
        url: `messages/`,
        method: "GET",
        params: { project_id: projectId, chat_origin: chatOrigin },
      }),
      transformResponse: (response: { results: Message[] }) =>
        response.results.reverse(),
    }),
  }),
});

export const { useGetMessagesQuery } = messageApiSlice;
