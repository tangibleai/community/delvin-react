import { useEffect, useState } from "react";
import { useForm, useWatch } from "react-hook-form";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Switch } from "@/components/ui/switch";
import { Textarea } from "@/components/ui/textarea";
import { useToast } from "@/components/ui/use-toast";
import { ChatbotConfig, chatbotTypes } from "@/typings/chatbot_config";
import { PlanStatus } from "@/typings/plan";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useUpdateChatbotMutation } from "@/slices/api/chatbotsApiSlice";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { Label } from "@/components/ui/label";
import { useParams } from "react-router-dom";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";
import { Provider } from "@/typings/provider";
import { useFetchProvidersQuery } from "@/slices/api/providerApiSlice";
import { useFetchGenerativeModelsQuery } from "@/slices/api/generativeModelsApiSlice";
import { GenerativeModel } from "@/typings/generative_model";

type Props = {
  knowledgeEnabled: boolean;
  selectedChatbot: ChatbotConfig;
  handleUpdateEnable: (enabled_setting: boolean) => void;
  handleUpdateConfig: (updatedConfig: ChatbotConfig) => void;
};

function AISettings({
  knowledgeEnabled,
  selectedChatbot,
  handleUpdateEnable,
  handleUpdateConfig,
}: Props) {
  const { toast } = useToast();
  const [error, setError] = useState("");

  const routeParams = useParams();
  const projectId = routeParams.projectId!;
  const { data: project } = useGetCurrentProjectDetailsQuery(projectId, {
    skip: !projectId,
  });
  const { data: workspace } = useGetWorkspaceQuery(project!.workspace, {
    skip: !project?.workspace,
  });
  const planStatus = workspace?.plan?.status;
  const workspacePlanExpired = planStatus !== PlanStatus.ACTIVE;

  const [availableLLMs, setAvailableLLMs] = useState<GenerativeModel[]>([]);

  const { data: providers = [] as Provider[] } = useFetchProvidersQuery(
    project!.workspace,
    {
      skip: !project || !project.workspace,
    }
  );
  const { data: generativeModels = [] as GenerativeModel[] } =
    useFetchGenerativeModelsQuery();

  const [updateChatbot] = useUpdateChatbotMutation();

  const formSchema = z.object({
    llm_provider_id: z.string(),
    model_id: z.string(),
    chatbotPersona: z.string().min(2, {
      message: "Chatbot persona must be at least 2 characters.",
    }),
    instructions: z.string(),
    constraints: z.string().min(2, {
      message: "Constraints must be at least 2 characters.",
    }),
    chatbotType: z.string().min(2, {
      message: "Please select a bot type",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      llm_provider_id: selectedChatbot.provider,
      model_id: String(selectedChatbot.generative_model),
      chatbotPersona: selectedChatbot.config.general_prompt.persona,
      instructions: selectedChatbot.config.general_prompt.instructions,
      constraints: selectedChatbot.config.general_prompt.constraints,
      chatbotType: selectedChatbot.config.bot_type,
    },
  });

  useEffect(() => {
    if (!selectedChatbot || !availableLLMs.length) return;

    const modelExists = availableLLMs.some(
      (model) => String(model.id) === String(selectedChatbot.generative_model)
    );

    form.setValue(
      "model_id",
      modelExists
        ? String(selectedChatbot.generative_model)
        : String(availableLLMs[0].id)
    );
  }, [selectedChatbot, availableLLMs]);

  const llmProviderId = useWatch({
    control: form.control,
    name: "llm_provider_id",
  });
  useEffect(() => {
    const selectedProviderId = form.getValues("llm_provider_id");
    if (
      !selectedProviderId ||
      !generativeModels ||
      !Object.keys(generativeModels).length
    )
      return;

    const selectedProvider = providers.find(
      (provider) => provider.id === selectedProviderId
    );
    if (!selectedProvider) return;

    const providerLLMs = generativeModels.filter(
      (model) => model.provider_type === selectedProvider.type
    );
    setAvailableLLMs(providerLLMs);

    if (providerLLMs.length > 0) {
      form.setValue(
        "model_id",
        selectedProviderId === selectedChatbot.provider
          ? String(selectedChatbot.generative_model)
          : String(providerLLMs[0].id)
      );
    }
  }, [llmProviderId, providers, generativeModels]);

  useEffect(() => {
    repopulateForm();
  }, [selectedChatbot]);

  const repopulateForm = () => {
    form.setValue("llm_provider_id", selectedChatbot.provider);
    if (availableLLMs.length)
      form.setValue("model_id", String(selectedChatbot.generative_model));
    form.setValue(
      "chatbotPersona",
      selectedChatbot.config.general_prompt.persona
    );
    form.setValue(
      "instructions",
      selectedChatbot.config.general_prompt.instructions
    );
    form.setValue(
      "constraints",
      selectedChatbot.config.general_prompt.constraints
    );
    form.setValue("chatbotType", selectedChatbot.config.bot_type);
    handleUpdateEnable(selectedChatbot.config.knowledge.enabled);
  };

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    const {
      llm_provider_id,
      chatbotType,
      model_id,
      chatbotPersona,
      instructions,
      constraints,
    } = values;

    const prompt_settings = {
      chatbotPersona: chatbotPersona,
      constraints: constraints,
      instructions: chatbotType == chatbotTypes.DISCUSSION ? instructions : "",
      fallbackResponses: selectedChatbot.config.knowledge.fallback,
    };

    const knowledge_settings = {
      enabled: knowledgeEnabled,
      type: selectedChatbot.config.knowledge.type,
      threshold: 0,
    };

    const updatedChatbot = {
      ...selectedChatbot,
      provider: llm_provider_id,
      generative_model: model_id,
      config: {
        ...selectedChatbot.config,
        bot_type: chatbotType,
        general_prompt: {
          constraints: prompt_settings.constraints,
          instructions: prompt_settings.instructions,
          persona: prompt_settings.chatbotPersona,
        },
        knowledge: {
          enabled: knowledge_settings?.enabled ?? false,
          fallback: prompt_settings?.fallbackResponses ?? "",
          activation: {
            type: knowledge_settings?.type ?? "",
            threshold: knowledge_settings?.threshold ?? 1,
          },
        },
      },
    } as ChatbotConfig;

    try {
      await updateChatbot(updatedChatbot).unwrap();
    } catch (error) {
      console.error("Error updating assistant:", error);
      setError("Error updating assistant");
      return;
    }

    setError("");
    setTimeout(() => {
      toast({
        title: "Success!",
        description: "Successfully saved chatbot settings",
      });
    }, 10);
    handleUpdateConfig({ ...selectedChatbot, ...updatedChatbot });
  }

  const chatbotType = form.watch("chatbotType");

  return (
    <div className="p-6 space-y-4 md:space-y-6 sm:p-8 h-full overflow-y-hidden">
      {error && (
        <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
          Error: {error}
        </p>
      )}
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleSubmit)}
          className="h-5/6 overflow-y-auto space-y-8 flex-1"
        >
          <FormField
            control={form.control}
            name="llm_provider_id"
            render={({ field }) => (
              <FormItem className="flex flex-col gap-4 w-5/6">
                <FormLabel>Provider</FormLabel>
                <Select
                  onValueChange={field.onChange}
                  value={field.value}
                  disabled={workspacePlanExpired}
                >
                  <FormControl>
                    <SelectTrigger className="text-base">
                      <SelectValue placeholder="Select a provider" />
                    </SelectTrigger>
                  </FormControl>
                  <SelectContent>
                    {providers.map((provider) => (
                      <SelectItem key={provider.id} value={provider.id}>
                        {provider.label}
                      </SelectItem>
                    ))}
                  </SelectContent>
                </Select>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="model_id"
            render={({ field }) => (
              <FormItem className="flex flex-col gap-1 w-5/6">
                <FormLabel>Model</FormLabel>
                {availableLLMs && availableLLMs.length ? (
                  <Select
                    onValueChange={field.onChange}
                    value={field.value}
                    disabled={workspacePlanExpired}
                  >
                    <FormControl>
                      <SelectTrigger className="text-base">
                        <SelectValue placeholder="Select a model to display" />
                      </SelectTrigger>
                    </FormControl>

                    <SelectContent>
                      {availableLLMs.map((model) => (
                        <SelectItem
                          key={String(model.id)}
                          value={String(model.id)}
                          className="text-base"
                        >
                          {model.label}
                        </SelectItem>
                      ))}
                    </SelectContent>
                  </Select>
                ) : (
                  <></>
                )}

                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="chatbotPersona"
            render={({ field }) => (
              <FormItem className="w-5/6">
                <FormLabel>Chatbot persona</FormLabel>
                <FormDescription>
                  Describe the assistant's personality, target audience, and
                  tone of voice.
                </FormDescription>
                <FormControl>
                  <Textarea
                    placeholder="You are a helpful assistant that offers concise responses..."
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          {chatbotType === chatbotTypes.DISCUSSION && (
            <FormField
              control={form.control}
              name="instructions"
              render={({ field }) => (
                <FormItem className="w-5/6">
                  <FormLabel>Instructions</FormLabel>
                  <FormDescription>
                    Tell your assistant what it should do
                  </FormDescription>
                  <FormControl>
                    <Textarea
                      placeholder="You offer advice about what volunteer activities that users might enjoy based on their interests..."
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          )}

          <FormField
            control={form.control}
            name="constraints"
            render={({ field }) => (
              <FormItem className="w-5/6">
                <FormLabel>Constraints</FormLabel>
                <FormDescription>
                  What topics should your assistant not respond to? How long
                  should the answer be? What else should the assistant not do?
                  etc.
                </FormDescription>
                <FormControl>
                  <Textarea
                    placeholder="You should not use hateful language..."
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <div className="flex items-center gap-2">
            <Label>Knowledge</Label>
            <Switch
              checked={knowledgeEnabled}
              onCheckedChange={(checked) => {
                handleUpdateEnable(checked);
              }}
            />
          </div>
          <Button type="submit" className="order-2 sm:order-1">
            Save
          </Button>
        </form>
      </Form>
    </div>
  );
}

export default AISettings;
