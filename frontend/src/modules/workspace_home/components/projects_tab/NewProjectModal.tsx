import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogFooter,
  DialogClose,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useToast } from "@/components/ui/use-toast";
import { useCreateProjectMutation } from "@/slices/api/projectsApiSlice";
import { Project } from "@/typings/project";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";

const NewProjectModal = () => {
  const navigate = useNavigate();
  const { toast } = useToast();
  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const { data: activeWorkspace } = useGetWorkspaceQuery(workspaceId);

  const [isNewProjectDialogOpen, setIsNewProjectDialogOpen] = useState(false);
  const [createProject, { isLoading }] = useCreateProjectMutation();

  const newProjectFormSchema = z.object({
    name: z.string().min(1, "Project name is required"),
  });

  const newProjectForm = useForm<z.infer<typeof newProjectFormSchema>>({
    resolver: zodResolver(newProjectFormSchema),
    defaultValues: {
      name: "",
    },
  });

  async function handleSubmit(values: z.infer<typeof newProjectFormSchema>) {
    const { name } = values;
    const newProject = await createNewProject(name);

    if (Object.keys(newProject).length > 0) {
      setTimeout(() => {
        toast({
          title: "Success!",
          description: "New project was successfully created.",
        });
      }, 300);
      navigate(`/project/${newProject.id}/init_assistant`);
    } else {
      setTimeout(() => {
        toast({
          variant: "destructive",
          title: "Oops! Something went wrong",
          description: "Could not create a new project. Please, try again.",
        });
      }, 300);
    }
    setIsNewProjectDialogOpen(false); // Close the dialog
    newProjectForm.reset();
  }

  const createNewProject = async (name: string) => {
    let newProject: Project = {} as Project;

    try {
      newProject = await createProject({
        name,
        workspace_id: activeWorkspace!.id,
      }).unwrap();
    } catch (err) {
      ("");
    }

    return newProject;
  };

  const toggleDialog = () => setIsNewProjectDialogOpen(!isNewProjectDialogOpen);

  return (
    <Dialog
      open={isNewProjectDialogOpen}
      onOpenChange={setIsNewProjectDialogOpen}
    >
      <DialogTrigger asChild>
        <Button className="mt-2" onClick={toggleDialog}>
          + New Project
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <Form {...newProjectForm}>
          <form onSubmit={newProjectForm.handleSubmit(handleSubmit)}>
            <DialogHeader>
              <DialogTitle>Create new project</DialogTitle>
            </DialogHeader>
            <div className="grid gap-4 py-4">
              <FormField
                control={newProjectForm.control}
                name="name"
                render={({ field }) => {
                  return (
                    <>
                      <FormItem>
                        <FormLabel>Project name</FormLabel>
                        <FormControl>
                          <Input placeholder="Project Name" {...field} />
                        </FormControl>
                      </FormItem>
                      {newProjectForm?.formState?.errors?.name?.message && (
                        <FormMessage />
                      )}
                    </>
                  );
                }}
              />
            </div>
            <DialogFooter>
              <Button type="submit" disabled={isLoading}>
                Create
              </Button>
              <DialogClose asChild>
                <Button variant="outline" type="button">
                  Cancel
                </Button>
              </DialogClose>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export { NewProjectModal };
