"""Test database models creation for api application"""

import os
import uuid

from django.utils import timezone

from api.models import (
    UserAccount,
    Project,
    Contact,
    Chat,
    Message,
    MessageTagGroup,
    MessageTagDefinition,
    TaggedMessage,
    Dataset,
    MLModel,
)
from core.constant import CONSTANTS


def get_or_create_user_account(
    email="test@example.com", name="", is_active=True, is_admin=False, **kwargs
):
    user_account, created = UserAccount.objects.get_or_create(
        email=email, name=name, is_active=is_active, is_admin=is_admin
    )
    assert user_account is not None, "Failed to create user account"
    return user_account


def get_or_create_project(owner, name="Test Project"):
    project, token_key, created = Project.objects.get_or_create(name=name, owner=owner)
    assert (
        project is not None and len(token_key) == CONSTANTS.TOKEN_KEY_LENGTH
    ), "Failed to create project"
    return project


def get_or_create_contact(project, origin="Test Contact"):
    contact_uuid = uuid.uuid4()
    while Contact.objects.filter(id=contact_uuid):
        contact_uuid = uuid.uuid4()
    contact, created = Contact.objects.get_or_create(
        id=contact_uuid, origin=origin, project=project
    )
    assert contact is not None, "Failed to create contact"
    return contact


def get_or_create_chat(
    contact, title="Test Chat", is_default="False", origin_id="Test Chat"
):
    chat_uuid = uuid.uuid4()
    while Chat.objects.filter(id=chat_uuid):
        chat_uuid = uuid.uuid4()
    chat, created = Chat.objects.get_or_create(
        id=chat_uuid,
        title=title,
        contact_id=contact,
        is_default=is_default,
        origin_id=origin_id,
    )
    assert chat is not None, "Failed to create chat"
    return chat


def get_or_create_message(
    project,
    contact,
    chat,
    direction=Message.Direction.INBOUND,
    sender_type=Message.SenderType.USER,
    channel_type=Message.ChannelType.WEB,
    text="Test message text",
):
    message_uuid = uuid.uuid4()
    while Message.objects.filter(id=message_uuid):
        message_uuid = uuid.uuid4()
    message, created = Message.objects.get_or_create(
        id=message_uuid,
        direction=direction,
        sender_type=sender_type,
        channel_type=channel_type,
        timestamp=timezone.now(),
        text=text,
        project=project,
        contact=contact,
        chat=chat,
    )
    assert message is not None, "Failed to create message"
    return message


def get_or_create_message_tag_group(project, name="Test Tag Group"):
    message_tag_group, created = MessageTagGroup.objects.get_or_create(
        name=name, project=project
    )
    assert message_tag_group is not None, "Failed to create message tag group"
    return message_tag_group


def get_or_create_message_tag_definition(
    project, message_tag_group, name="Test Tag", description="A test tag description"
):
    message_tag_definition, create = MessageTagDefinition.objects.get_or_create(
        name=name,
        description=description,
        project=project,
        message_tag_group=message_tag_group,
    )
    assert message_tag_definition is not None, "Failed to create message tag definition"
    return message_tag_definition


def get_or_create_tagged_message(message):
    tagged_message, created = TaggedMessage.objects.get_or_create(message=message)
    assert tagged_message is not None, "Failed to create tagged message"
    return tagged_message


def get_or_create_dataset(
    project, tagged_messages, name="Test Dataset", description="A test dataset"
):
    dataset, created = Dataset.objects.get_or_create(
        name=name, description=description, project=project
    )
    dataset.tagged_messages.set(tagged_messages)
    dataset.save()
    assert (
        dataset is not None and dataset.tagged_messages.count() > 0
    ), "Failed to create dataset or add tagged messages"
    return dataset


def get_or_create_ml_model(project, dataset, description="Test ML Model"):
    dummy_weights = os.urandom(64)
    ml_model = MLModel.objects.create(
        weights=dummy_weights,
        project=project,
        description=description,
        dataset=dataset,
    )
    assert ml_model is not None, "Failed to create ML model"
    return ml_model


def create_dataset_and_dependencies():
    user_account = get_or_create_user_account()
    project = get_or_create_project(user_account)
    contact = get_or_create_contact(project)
    chat = get_or_create_chat(contact)
    message = get_or_create_message(project, contact, chat)
    message_tag_group = get_or_create_message_tag_group(project)
    message_tag_definition = get_or_create_message_tag_definition(
        project, message_tag_group
    )
    tagged_message = get_or_create_tagged_message(message)
    dataset = get_or_create_dataset(project, [tagged_message])
    return dataset
