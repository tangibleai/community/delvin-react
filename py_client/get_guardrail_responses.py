from json import JSONDecodeError
from pathlib import Path
from pprint import pprint
from py_client.create_tokens import get_access_tokens

import requests

ROOT = Path(__file__).resolve().parent.parent

refresh, access = get_access_tokens()


endpoint = "http://localhost:8000/api/chats/guardrail-responsex/?project_id=a758ea27-7a02-44ca-b062-716cd3fc1865"

headers = {"Authorization": f"JWT {access}"}

response = requests.get(
    endpoint,
    headers=headers,
)

if response.status_code == 200:
    try:
        pprint(response.json())
    except JSONDecodeError:
        print(response.status_code)
        print(response.text)
else:
    print(response.status_code)
    print(response.text)
