import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { ConditionTypes } from "../typing/condition";

type Props = { addCondition: (type: string) => void };

function ConditionDropdown({ addCondition }: Props) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant="outline" className="w-full">
          Add Condition
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent>
        <DropdownMenuItem
          onClick={() => addCondition(ConditionTypes.TAG_DETECTED)}
          className="items-center justify-center"
        >
          Tag is detected
        </DropdownMenuItem>
        {/* <DropdownMenuItem
          onClick={() => addCondition("not_detected")}
          className="items-center justify-center"
        >
          Tag isn't detected
        </DropdownMenuItem> */}
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export default ConditionDropdown;
