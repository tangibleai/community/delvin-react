from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient
from accounts.models import UserAccount
from api.models import Flow, Project, Workspace
from rest_framework_simplejwt.tokens import RefreshToken

from api.tests.mock_assets import flow


class SaveFlowGraphViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()

        # Create a user and authenticate with JWT
        self.user = UserAccount.objects.create_user(
            email="testuser@example.com", name="Test", password="password"
        )
        refresh = RefreshToken.for_user(self.user)
        self.token = str(refresh.access_token)

        # Set the Authorization header
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + self.token)

        self.workspace = Workspace.objects.create(
            name="Test Workspace", owner=self.user
        )
        self.project, _ = Project.objects.create(
            name="Test Project", workspace=self.workspace
        )

        self.flow = Flow.objects.create(
            project=self.project,
            name="Test Flow",
            description="Test flow description",
            assistant_role="Assistant Role",
        )

        self.url = reverse("update_flow_graph", kwargs={"id": self.flow.id})

        self.nodes = flow.ui_nodes
        self.edges = flow.ui_edges
        self.expected_flow_graph = flow.expected_flow_graph

    def test_save_flow_graph(self):
        data = {
            "nodes": self.nodes,
            "edges": self.edges,
        }

        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.flow.refresh_from_db()

        self.assertEqual(self.flow.ui["nodes"], self.nodes)
        self.assertEqual(self.flow.ui["edges"], self.edges)

        flow_graph = self.flow.flow_graph

        self.assertEqual(
            flow_graph["source_node_id"], self.expected_flow_graph["source_node_id"]
        )
