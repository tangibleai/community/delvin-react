import django
import os
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from sklearn.metrics import f1_score
from sklearn.linear_model import LogisticRegression
import timeit

from api.ml import sgd

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()


from api.models import (
    UserAccount,
    Project,
    MLModel,
    # MLWeights,
    MessageTag,
    Message,
    MessageTagDefinition,
)


def calculate_sklearn_score_for_tag(tag, project_messages=None):
    x = []
    y = []

    if project_messages is None:
        project_messages = Message.objects.filter(
            project=t.project,
            id__in=MessageTag.objects.values_list("message_id", flat=True).distinct(),
        )

    for m in project_messages:
        embedding = [1] + list(m.embedding)
        x.append(embedding)
        message_tagged_with_tag = (
            1 if MessageTag.objects.filter(message=m, tag__id=tag.id) else 0
        )
        y.append(message_tagged_with_tag)

    x = np.array(x)
    y = np.array(y)
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.33)

    reg = LogisticRegression(class_weight="balanced").fit(X_train, y_train)
    y_predict = reg.predict(X_test)
    score = f1_score(y_test, y_predict)

    return score


def calculate_sgd_y_predict(X_test, coef, intercept):
    X_test = X_test[:, 1:]
    coef.reshape(384, 1)  # to turn (384,) shape into (384, 1)

    predicted_vals = X_test @ coef + intercept
    binary_targets = [1 if elem > 0.5 else 0 for elem in predicted_vals]

    return binary_targets


def run():
    tag_col_name = "Tag"
    sk_score_col_name = "Scikit score"
    sgd_score_col_name = "Sgd score"
    sk_time_col_name = "Scikit time"
    sgd_time_col_name = "Sgd time"

    metrics_df = pd.DataFrame(
        columns=[
            tag_col_name,
            sk_score_col_name,
            sgd_score_col_name,
            sk_time_col_name,
            sgd_time_col_name,
        ]
    )

    metrics = []

    DEFAULT_USER_EMAIL = "engineering@tangibleai.com"
    DEFAULT_PROJECT_NAME = "Default Project"

    project_owner = UserAccount.objects.get(email=DEFAULT_USER_EMAIL)
    project = Project.objects.get(name=DEFAULT_PROJECT_NAME, owner=project_owner)

    measurements_per_tag = 10  # number of measurements per each tag

    ml_model = MLModel.objects.filter(project=project).first()

    tag_definitions = MessageTagDefinition.objects.filter(project=project)
    for t in tag_definitions:
        x = []
        y = []

        messages = Message.objects.filter(
            project=t.project,
            id__in=MessageTag.objects.values_list("message_id", flat=True).distinct(),
        )
        for m in messages:
            embedding = [1] + list(m.embedding)
            x.append(embedding)
            message_tagged_with_tag = (
                1 if MessageTag.objects.filter(message=m, tag__id=t.id) else 0
            )
            y.append(message_tagged_with_tag)

        x = np.array(x)
        y = np.array(y)
        X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.33)

        sklearn_execution_time_in_seconds = (
            timeit.timeit(
                lambda: LogisticRegression(class_weight="balanced").fit(
                    X_train, y_train
                ),
                number=measurements_per_tag,
            )
            / measurements_per_tag
        )

        reg = LogisticRegression(class_weight="balanced").fit(X_train, y_train)
        y_predict = reg.predict(X_test)
        sklearn_score = f1_score(y_test, y_predict)

        sgd_execution_time_in_seconds = (
            timeit.timeit(
                lambda: sgd.fit(X_train, y_train), number=measurements_per_tag
            )
            / measurements_per_tag
        )

        sgd_metrics = sgd.fit(X_train, y_train)["coef"]
        intercept = sgd_metrics[0]
        coef = sgd_metrics[1:]
        y_sgd_predict = calculate_sgd_y_predict(X_test, coef, intercept)
        sgd_score = f1_score(y_test, y_sgd_predict)

        metrics.append(
            {
                tag_col_name: t.name,
                sk_time_col_name: sklearn_execution_time_in_seconds,
                sgd_time_col_name: sgd_execution_time_in_seconds,
                sk_score_col_name: sklearn_score,
                sgd_score_col_name: sgd_score,
            },
        )

    metrics_df = pd.DataFrame(metrics)
    print(metrics_df)


if __name__ == "__main__":
    run()
