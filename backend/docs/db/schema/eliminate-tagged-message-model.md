This docs describes how we eliminated `models.TaggedMessage` record preserving data in db.

`models.TaggedMessage` schema:

```
class TaggedMessage(models.Model):
    """Datasets will contain only `TaggedMessage`s that have been tagged by the intent tagging model"""

    message = models.OneToOneField(
        Message, null=False, blank=False, on_delete=models.CASCADE
    )
    tag_definitions = models.ManyToManyField("MessageTagDefinition")
```

The model was created to represent a connection between `models.Message` and `models.MessageTagDefinition` models in relation one to many. Here is a group of models involved:

```
class TaggedMessage(models.Model):
    """Datasets will contain only `TaggedMessage`s that have been tagged by the intent tagging model"""

    message = models.OneToOneField(
        Message, null=False, blank=False, on_delete=models.CASCADE
    )
    tag_definitions = models.ManyToManyField("MessageTagDefinition")


class Dataset(models.Model):
    """Django data model, Dataset.project_id, Dataset.message_labels - many to many relation

    Dataset.tagged_messages - Collection of tagged messages used to train a model (create model weights)
    """

    name = models.CharField(max_length=50, default="")
    description = models.TextField(
        default="",
        help_text="User describes the conversation domain",
    )
    project = models.ForeignKey(
        Project, null=False, blank=False, on_delete=models.CASCADE
    )
    tagged_messages = models.ManyToManyField(
        "TaggedMessage",
        help_text="Collection of tagged messages used to train a model (create model weights)",
    )
```

We would like to move tag definitions reference directly to `Message` model and eliminate `TaggedMessage` record persisting user data (connections between message and tags). Here is a bunch of steps undertaken:

1. Create `MessageTag` model representing a connection between `Message` and `MessageTagDefinition`:

```
class MessageTag(TimeStampMixin):
    """Represents a connection between `Message` and `MessageTagDefinition`"""

    message = models.ForeignKey(
        Message, null=False, blank=False, on_delete=models.CASCADE
    )
    tag = models.ForeignKey(
        MessageTagDefinition, null=False, blank=False, on_delete=models.CASCADE
    )
    ml_model = models.ForeignKey(
        MLModel, null=True, blank=True, default=None, on_delete=models.CASCADE
    )
```

We inherit it from `TimeStampMixin` model to keep track of modifications through time and add a reference to `MLModel` model (for now it's null since only one model allowed per project)

2. Add `tags` field to `Message` model:

```
class Message(models.Model):
    ...
    tags = models.ManyToManyField("MessageTagDefinition", through="MessageTag")
```

3. Remove `Dataset.tagged_messages` field and run:

```
python manage.py makemigrations
python manage.py migrate
```

4. Let's copy data from `TaggedMessage` to newly created `MessageTag` model to preserve data. Supposed you are in `{project root}/backend` folder run:

```
python manage.py shell_plus
from scripts.db.schema import eliminate_tagged_message_model
eliminate_tagged_message_model.move_from_tagged_message_to_message_tag_model()
```

5. Finally, remove `TaggedMessage` model, all serializer and substitude all occurencies in code with `MessageTag` model accordingly to logic
