import { Navigate, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Navbar from "../components/Navbar";

import {
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@/components/ui/tremor-tabs";
import Projects from "../components/projects_tab/Projects";
import Settings from "../components/settings_tab/Settings";
import { useEffect, useState } from "react";
import TeamSettings from "../components/team_tab/TeamSettings";
import { WorkspaceTab } from "../utils/workspaceTabs";
import { loadUserSuccess } from "@/slices/authSlice";
import Providers from "../components/providers/Providers";
import { AppDispatch, RootState } from "@/store";
import { useToast } from "@/components/ui/use-toast";
import { Workspace } from "@/typings/workspace";
import Loading from "@/components/Loading";
import { updateUser } from "@/lib/api";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import {
  useListWorkspacesQuery,
  useGetWorkspaceQuery,
  useCreateWorkspaceMutation,
} from "@/slices/api/workspacesApiSlice";

function WorkspaceHome() {
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated
  );
  if (!isAuthenticated) {
    return <Navigate to={AUTH_PAGE_URLS.login} />;
  }

  const navigate = useNavigate();
  const dispatch: AppDispatch = useDispatch();
  const { toast } = useToast();

  const user = useSelector((state: RootState) => state.auth.user);
  const { data: workspaces } = useListWorkspacesQuery(user!.id);
  // >>>>>>> 78b17d9501120add80808097c5cf63515bec74fb
  const [activeTab, setActiveTab] = useState<string>(WorkspaceTab.PROJECTS);

  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const {
    data: activeWorkspace,
    isFetching,
    isError,
  } = useGetWorkspaceQuery(workspaceId);

  const [createWorkspace] = useCreateWorkspaceMutation();

  //   // RTK Query hooks
  //   const { data: workspaces, isLoading, isError } = useListWorkspacesQuery(user!.id)
  //   const { data: activeWorkspace, isLoading: isCurrentLoading, isError: isCurrentError}  = useGetWorkspaceQuery(workspaceId);

  //   if (isLoading || isCurrentLoading) {
  //     return <Loading />
  //   }

  // <<<<<<< HEAD
  //   if (isError || isCurrentError) {
  //     return <ErrorScreen/>
  //   }

  // =======
  //   const user = useSelector((state: RootState) => state.auth.user);

  // useEffect(() => {
  //   setActiveW;
  // }, [activeWorkspace]);

  useEffect(() => {
    if (!user?.id) return;

    setActiveTab(WorkspaceTab.PROJECTS);
    resolveActiveWorkspace();
  }, [workspaceId, user?.id]);

  const resolveActiveWorkspace = async () => {
    if (!workspaces) return;

    let activeWorkspace;
    if (!workspaces.length) {
      try {
        const workspace = await createWorkspace({
          owner: user!.id,
          name: `${user!.name}'s Workspace`,
        }).unwrap();
        activeWorkspace = workspace;
      } catch (error: any) {
        toast({
          variant: "destructive",
          title: "Oops! Something went wrong",
          description:
            "Couldn't create workspace for a user. Please, try to log out and sign in again.",
        });
        return;
      }
    }

    // user switched to another workspace
    if (workspaceId) {
      activeWorkspace = workspaces.find((w: Workspace) => w.id === workspaceId);
      // update user `current_workspace` field on backend and in redux slice
      if (activeWorkspace && activeWorkspace.id !== user?.current_workspace) {
        const updatedUser = await updateUser(user!.id, {
          newCurrentWorkspaceId: activeWorkspace.id,
        });
        dispatch(loadUserSuccess(updatedUser));
      }
      return navigate(`/workspace/${activeWorkspace!.id}`);
    }

    // user just joined joined platform via invitation to workspace - `current_workspace` field not set for user which means user never visited this page before, activeWorkspace is null and workspaces.length !== 0 which means user has some workspace
    if (!user?.current_workspace && !activeWorkspace && workspaces.length) {
      activeWorkspace = workspaces[0];
      const updatedUser = await updateUser(user!.id, {
        newCurrentWorkspaceId: activeWorkspace.id,
      });
      dispatch(loadUserSuccess(updatedUser));
      return navigate(`/workspace/${activeWorkspace.id}`);
    }

    // user just signed up and new workspace set to `activeWorkspace` is created for him
    if (!user?.current_workspace) {
      const updatedUser = await updateUser(user!.id, {
        newCurrentWorkspaceId: activeWorkspace!.id,
      });
      dispatch(loadUserSuccess(updatedUser));
      return navigate(`/workspace/${activeWorkspace!.id}`);
    }

    // user signed in after loggin out and his last active workspace is resolved
    activeWorkspace = workspaces.find(
      (w: Workspace) => w.id === user!.current_workspace
    );
    return navigate(`/workspace/${activeWorkspace!.id}`);
  };

  // >>>>>>> 78b17d9501120add80808097c5cf63515bec74fb
  return activeWorkspace ? (
    <div>
      <Navbar withLogo={true} />
      <div className="w-[80%] mx-auto text-lg mt-7">
        <Tabs
          defaultValue={WorkspaceTab.PROJECTS}
          value={activeTab}
          onValueChange={setActiveTab}
        >
          <TabsList>
            <TabsTrigger value={WorkspaceTab.PROJECTS}>Projects</TabsTrigger>
            {user?.email === activeWorkspace.owner && (
              <>
                <TabsTrigger value={WorkspaceTab.LLM_PROVIDERS}>
                  LLM Providers
                </TabsTrigger>
                <TabsTrigger value={WorkspaceTab.SETTINGS}>
                  Settings
                </TabsTrigger>
                <TabsTrigger value={WorkspaceTab.TEAM}>Team</TabsTrigger>
              </>
            )}
          </TabsList>
          <div>
            <TabsContent value={WorkspaceTab.PROJECTS}>
              <Projects />
            </TabsContent>
            {user?.email === activeWorkspace.owner && (
              <>
                <TabsContent value={WorkspaceTab.LLM_PROVIDERS}>
                  {activeWorkspace && (
                    <Providers activeWorkspace={activeWorkspace} />
                  )}
                </TabsContent>
                <TabsContent
                  className="text-left"
                  value={WorkspaceTab.SETTINGS}
                >
                  {workspaces?.length && <Settings />}
                </TabsContent>
                <TabsContent className="text-left" value={WorkspaceTab.TEAM}>
                  {activeWorkspace && (
                    <TeamSettings setActiveTab={setActiveTab} />
                  )}
                </TabsContent>
              </>
            )}
          </div>
        </Tabs>
      </div>
    </div>
  ) : (
    <Loading />
  );
}

export default WorkspaceHome;
