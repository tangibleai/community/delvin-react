## Cetin 

* [X] Create a 404 page and make sure incorrect requests redirect there.
* [X] Standardizing the url for Guardrail Response and Guardrail Actions. 
* [X] Experiment with `react-router` useLoaderData().

## Vlad 
* [ ] Finish the Create Flows issue. 

## Greg 
* [ ] Refactor the project-collaborator and document urls.


## Maria
* [ ] Questions to the mentor
    - Do we log frontend errors implictly of explicitly?
    - Should we use useLoaderData and how?
