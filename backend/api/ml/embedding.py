"""Turn text into embedding vector"""

import logging

import requests

from django.conf import settings


def embed_text(text):
    """Computes embedding vector for text

    Args:
        text (str): Input text
    Returns:
        list[float]: Embedding vector represented as list of floats
    """

    url = settings.ENV["SENTENCE_EMBEDDINGS_URL"]

    sentences = [text]
    data = {"data": {"ndarray": sentences}}

    embedding = None
    try:
        response = requests.post(url=url, json=data)
        sentences_embedding = response.json()["data"]["ndarray"]
        embedding = sentences_embedding[0]
    except Exception as e:
        logging.error("An error occurred: %s", e)

    return embedding
