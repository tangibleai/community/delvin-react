import { useState } from "react";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";

import { Member, MemberRoles } from "@/typings/member";
import { useToast } from "@/components/ui/use-toast";
import { useNavigate, useParams } from "react-router-dom";
import { WorkspaceTab } from "../../utils/workspaceTabs";
import { useUpdateTeamMemberRoleMutation } from "@/slices/api/teamMemberApiSlice";

type Props = {
  userToUpdate: any;
  setActiveTab: React.Dispatch<React.SetStateAction<string>>;
};

function MakeMemberOwner({ userToUpdate, setActiveTab }: Props) {
  const navigate = useNavigate();
  const { toast } = useToast();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId;

  const [updateTeamMember, { isError }] = useUpdateTeamMemberRoleMutation();

  async function onConfirmButtonClick(userToUpdate: Member) {
    try {
      await updateTeamMember({
        id: userToUpdate.id,
        role: MemberRoles.OWNER,
      }).unwrap();
      toast({
        title: "Success!",
        description: "Team member role was successfully updated.",
      });
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Couldn't make team member an owner.",
      });
    }

    setIsModalOpen(false);
    setActiveTab(WorkspaceTab.PROJECTS);
    return navigate(`/workspace/${workspaceId}`);
  }

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button variant="ghost" className="w-full">
          Make Owner
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-md">
        <DialogHeader>
          <DialogTitle>Make member an owner</DialogTitle>
        </DialogHeader>
        <p>
          Are you sure you want to make {userToUpdate.delvin_user} an owner of
          this workspace?
        </p>
        <DialogFooter>
          <Button
            variant="destructive"
            onClick={() => onConfirmButtonClick(userToUpdate)}
          >
            Yes, I'm sure
          </Button>
          <Button variant="outline" onClick={() => setIsModalOpen(false)}>
            Cancel
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}

export default MakeMemberOwner;
