import emptyApi from "../apiSlice";
import { ContactDetails } from "@/typings/contact_details";
import { processError } from "@/lib/utils";

type GetContactSearchParams = {
  project_id: string;
  origin: string;
};

type CreateContactRequestBody = {
  project_id: string;
  origin: string;
  internal: boolean;
};

const contactApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    getContact: builder.query<ContactDetails, GetContactSearchParams>({
      async queryFn(
        { project_id, origin },
        _queryApi,
        _extraOptions,
        fetchWithBQ
      ) {
        try {
          const params = new URLSearchParams({
            project_id,
            origin,
            internal: "true",
          });

          // Fetch contact details
          const response = await fetchWithBQ({
            url: `contacts/?${params}`,
            method: "GET",
          });

          if (response.error) throw response.error;

          const contacts = response.data as ContactDetails[];
          const contact = contacts[0];

          return { data: contact };
        } catch (err) {
          const errorMessage = `Failed to retrieve contact information: ${err}`;
          processError(errorMessage);
          return { error: { status: "CUSTOM_ERROR", message: errorMessage } };
        }
      },
      providesTags: ["Contact"],
    }),
    createContact: builder.mutation<ContactDetails, CreateContactRequestBody>({
      query: (requestBody) => ({
        url: "contacts/create/",
        method: "POST",
        data: requestBody,
      }),
      invalidatesTags: ["Contact"],
    }),
  }),
});

export const { useGetContactQuery, useCreateContactMutation } = contactApiSlice;
