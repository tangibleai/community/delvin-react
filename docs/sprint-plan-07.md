## Maria
* [ ] Create pull request from `frontend`
* [ ] Create task for a initial sync from BigQuery to Delvin


## Cetin 
* [X] Merge Maria's pull request
* [X] Try and send a request to public api from another domain
  * Tried with pyclient from shell.cloud.google.com. Created tokens, project and a message.
  * Please find some information in the link. [Do I need CSRF](https://security.stackexchange.com/questions/170388/do-i-need-csrf-token-if-im-using-bearer-jwt)
* [X] Create a task to run the initial sync
* [X] Create ingress to split route for seldon.
* [X] Follow the tutorial at (https://medium.com/@vinayakshanawad/serving-hugging-face-transformers-optimizing-custom-model-deployment-with-seldon-core-a593f6ea7549) to create an embedding service, but use the filed in `feat-ml` branch for the model instead of the tutorial.
  * I did this with seldon-core-microservice and nginx ingress.
    * Couldn't implement as in the tutorial, because we don't have a model storage and it failed.
    * Seldon deployment failed due to resources.


## Greg 

* [ ] Get familiar with repository and be able to run it locally 
* [ ] Play with CustomGPT and try to make it log the messages to Delvin endpoint.
