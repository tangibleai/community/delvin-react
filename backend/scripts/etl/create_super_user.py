import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from django.db.utils import IntegrityError
from accounts.models import UserAccount

try:
    super_user = UserAccount.objects.create_superuser(
        email="abc@gmail.com",  # os.environ.get("ADMIN_EMAIL"),
        name="vlad",  # os.environ.get("ADMIN_USER"),
        password="Abcdefgh!",  # os.environ.get("ADMIN_PASSWORD"),
    )
    print("Super user has been created!")
except IntegrityError as exc:
    print(f"{str(exc)}. Super user already exists!")
