TODO for Message and MessageTag data import and export (ETL - extract transform load)

See backend/api/etl/

1. Create a git tag with-MessageTag
2. git commit more often
3. Eliminate MessageTag from all code
4. Generate new diagram
5. Create user account, project and contact within one script with hard-coded data
6. CSV file to create user's, project's and contact's for me, Hobs, Maria and Greg (to_dio)
7. Delete comments
