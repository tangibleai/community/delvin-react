import { useParams } from "react-router-dom";

import { IoMdRefresh } from "react-icons/io";

import ChatContainer from "@/components/modules/chatbot/ChatContainer";

import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { useDispatch } from "react-redux";
import { resolveChatForChatbotMapping } from "@/components/modules/actions/chatbot";
import {
  ChatbotToChatMapping,
  removeChatbotToChatMapping,
  setChatbotToChatMapping,
} from "@/slices/chatbotSlice";
import ChatbotsDropdown from "./ChatbotsDropdown";
import { useEffect, useState } from "react";
import { ChatbotConfig } from "@/typings/chatbot_config";
import { useListChatbotsQuery } from "@/slices/api/chatbotsApiSlice";
import { useGetContactQuery } from "@/slices/api/contactApiSlice";
import { ContactDetails } from "@/typings/contact_details";

function ChatWithChatbot() {
  const dispatch = useDispatch();
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const userEmail = useSelector(
    (state: RootState) => state.auth.user!.email ?? ""
  );
  const { data: contact = {} as ContactDetails } = useGetContactQuery(
    {
      project_id: projectId,
      origin: `${userEmail!} (internal)`,
    },
    { skip: !userEmail }
  );
  const chatbotToChatMapping = useSelector(
    (state: RootState) => state.chatbot.chatbotToChatMapping
  );

  const { data: chatbots = [] } = useListChatbotsQuery({
    project_id: projectId,
  });

  const [selectedChatbot, setSelectedChatbot] = useState(
    chatbots?.[0] ?? ({} as ChatbotConfig)
  );
  const [chatOrigin, setChatOrigin] = useState<string>();

  useEffect(() => {
    const chatbotConfig = async () => {
      if (!chatbots.length) return;

      setSelectedChatbot(chatbots[0]);
    };

    chatbotConfig();
  }, [chatbots]);

  useEffect(() => {
    if (!selectedChatbot || !Object.keys(selectedChatbot).length || !contact.id)
      return;

    resolveAndSetChatForChatbotMapping(chatbotToChatMapping);
  }, [selectedChatbot, contact?.id]);

  const resetActiveChat = async () => {
    const mappingWithExcludedChatbot = chatbotToChatMapping.filter(
      (mapping) => mapping.chatbotId !== selectedChatbot.id
    );
    dispatch(removeChatbotToChatMapping(selectedChatbot.id));
    resolveAndSetChatForChatbotMapping(mappingWithExcludedChatbot);
  };

  const resolveAndSetChatForChatbotMapping = async (
    updatedMapping: ChatbotToChatMapping[]
  ) => {
    const mapping = await resolveChatForChatbotMapping(
      selectedChatbot,
      updatedMapping
    );
    setChatOrigin(mapping.chatOrigin);
    dispatch(setChatbotToChatMapping(mapping));
  };

  return (
    <div className="mx-36 pt-7 h-screen">
      {chatbots.length ? (
        <div className="h-full flex flex-col gap-3">
          <div className="flex justify-between items-center px-5 py-5 bg-slate-200 rounded-xl">
            <div className="text text-2xl font-bold">Assistant Preview</div>
            {selectedChatbot && Object.keys(selectedChatbot).length && (
              <div className="w-2/5">
                <ChatbotsDropdown
                  selectedChatbot={selectedChatbot}
                  setSelectedChatbot={setSelectedChatbot}
                />
              </div>
            )}

            <div className="flex gap-2">
              <IoMdRefresh onClick={resetActiveChat} size={"2rem"} />
            </div>
          </div>
          {projectId && (
            <div className="flex flex-col font-sans w-full h-[85%] overflow-y-auto bg-slate-100 rounded-xl">
              {selectedChatbot &&
                Object.keys(selectedChatbot).length &&
                Object.keys(contact).length && (
                  <ChatContainer
                    projectId={projectId}
                    selectedChatbot={selectedChatbot}
                    chatOrigin={chatOrigin}
                    contactOrigin={contact.origin}
                  />
                )}
            </div>
          )}
        </div>
      ) : (
        <div className="flex flex-row items-center gap-2 justify-center py-2.5 rounded-2xl bg-gray-200 p-4 h-full w-full">
          <p className="text-xl">
            Create first chatbot on "Chat With Assistant" sidebar menu item
          </p>
        </div>
      )}
    </div>
  );
}

export default ChatWithChatbot;
