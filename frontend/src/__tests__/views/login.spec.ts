import { test, expect, Browser, Page } from "@playwright/test";

test.describe("Login Page", () => {
  let page: Page;

  test.beforeAll(async ({ browser }) => {
    page = await browser.newPage();
  });

  test.afterAll(async () => {
    await page.close();
  });

  test("should render the login form", async () => {
    await page.goto("/login");
    const signInText = await page.textContent("h1");
    await expect(signInText).toBe("Sign in to your account");
  });
});
