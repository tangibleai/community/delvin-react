import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { useToast } from "@/components/ui/use-toast";
import { PlanType } from "@/typings/plan";
import { zodResolver } from "@hookform/resolvers/zod";
import { Card } from "@tremor/react";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import DeleteWorkspaceModal from "./DeleteWorkspaceModal";
import FreePlanCard from "./FreePlanCard";
import PaidPlanCard from "./PaidPlanCard";
import { useParams } from "react-router-dom";
import {
  useGetWorkspaceQuery,
  useUpdateWorkspaceMutation,
} from "@/slices/api/workspacesApiSlice";
import OwnPlanCard from "./OwnPlanCard";

const formSchema = z.object({
  workspaceName: z.string().min(1, "Workspace name is required"),
});

function Settings() {
  const { toast } = useToast();

  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const { data: activeWorkspace } = useGetWorkspaceQuery(workspaceId);

  const [updateWorkspace] = useUpdateWorkspaceMutation();

  useEffect(() => {
    // Handles resetting the form after submit or x
    // Can't directly access x in Dialog
    form.reset();

    form.setValue("workspaceName", activeWorkspace!.name);
  }, [activeWorkspace]);

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      workspaceName: activeWorkspace?.name || "",
    },
  });

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    try {
      await updateWorkspace({
        id: activeWorkspace!.id,
        name: values.workspaceName,
      }).unwrap();
    } catch (err) {
      toast({
        variant: "destructive",
        title: "Error",
        description: "Failed to update workspace name. Please try again.",
      });
      return;
    }

    toast({
      title: "Success!",
      description: "Workspace name updated successfully.",
    });
  };

  return (
    <div className="flex flex-col gap-4 m-2">
      <Card className="p-4 mt-4 max-w-lg">
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onSubmit)}
            className="flex flex-col gap-6"
          >
            <FormField
              control={form.control}
              name="workspaceName"
              render={({ field }) => (
                <FormItem>
                  <Label
                    htmlFor="workspace-name"
                    className="text-lg font-semibold"
                  >
                    Workspace name
                  </Label>
                  <div className="flex gap-3">
                    <FormControl>
                      <Input id="workspace-name" {...field} className="w-1/3" />
                    </FormControl>
                    <Button type="submit" className="w-fit">
                      Save
                    </Button>
                  </div>
                  <FormMessage />
                </FormItem>
              )}
            />
          </form>
        </Form>
      </Card>

      {activeWorkspace && activeWorkspace.plan.type === PlanType.FREE && (
        <FreePlanCard plan={activeWorkspace.plan} />
      )}
      {activeWorkspace && activeWorkspace.plan.type === PlanType.PAID && (
        <PaidPlanCard plan={activeWorkspace.plan} />
      )}
      {activeWorkspace && activeWorkspace.plan.type === PlanType.OWN_KEY && (
        <OwnPlanCard plan={activeWorkspace.plan} />
      )}

      <Card className="mb-2 max-w-lg">
        <div className="max-w-md">
          <h3 className="text-lg font-semibold">Delete workspace</h3>
          <p className="my-2 text-sm">
            Deleting this workspace will remove all assistants you have built,
            including all chats, messages and other data assosiated with each
            project. Make sure you have backed up any information you would like
            to keep.
          </p>
        </div>

        <DeleteWorkspaceModal />
      </Card>
    </div>
  );
}

export default Settings;
