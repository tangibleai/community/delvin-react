import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export type ChatbotToChatMapping = { chatbotId: string; chatOrigin: string };

// TODO: replace this with Project type
interface SliceChatbot {
  chatbotToChatMapping: ChatbotToChatMapping[];
}

const initialState: SliceChatbot = {
  chatbotToChatMapping: [],
};

const chatbotSlice = createSlice({
  name: "chatbot",
  initialState,
  reducers: {
    setChatbotToChatMapping: (
      state,
      action: PayloadAction<ChatbotToChatMapping>
    ) => {
      const mappingFound = state.chatbotToChatMapping.find(
        (mapping) => action.payload.chatbotId === mapping.chatbotId
      );
      if (!mappingFound) {
        state.chatbotToChatMapping = [
          ...state.chatbotToChatMapping,
          action.payload,
        ];
        return state;
      }

      state.chatbotToChatMapping = state.chatbotToChatMapping.map((mapping) =>
        mapping.chatbotId == action.payload.chatbotId
          ? { ...mapping, chatId: action.payload.chatOrigin }
          : mapping
      );

      return state;
    },
    removeChatbotToChatMapping: (state, action: PayloadAction<string>) => {
      state.chatbotToChatMapping = state.chatbotToChatMapping.filter(
        (mapping) => mapping.chatbotId !== action.payload
      );
    },
  },
});

export const { setChatbotToChatMapping, removeChatbotToChatMapping } =
  chatbotSlice.actions;
export default chatbotSlice.reducer;
