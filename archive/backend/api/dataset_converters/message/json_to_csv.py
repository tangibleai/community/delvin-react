import json
import csv


def json_to_csv(json_file, csv_file):
    with open(json_file, "r", encoding="utf-8") as file:
        data = json.load(file)

    with open(csv_file, "w", newline="", encoding="utf-8") as file:
        fieldnames = ["model", "pk"] + [
            "fields." + key for key in data[0]["fields"].keys()
        ]
        writer = csv.DictWriter(file, fieldnames=fieldnames)

        # Write the columns
        writer.writeheader()

        for item in data:
            row_data = {"model": item["model"], "pk": item["pk"]}
            row_data.update(
                {"fields." + key: value for key, value in item["fields"].items()}
            )
            writer.writerow(row_data)
