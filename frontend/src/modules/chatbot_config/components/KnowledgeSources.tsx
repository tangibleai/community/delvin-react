import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { FaFilePdf, FaFileWord, FaFileLines, FaFilePen } from "react-icons/fa6";
import { FaExternalLinkAlt } from "react-icons/fa";
import { SiGoogledocs } from "react-icons/si";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableHeaderCell,
  TableBody,
  Flex,
} from "@tremor/react";

import { ChatbotConfig } from "@/typings/chatbot_config";
import DeleteDocumentModal from "./DeleteDocumentModal";
import { Document } from "@/typings/document";
import DocumentUploadAndEditFormModal from "./DocumentUploadAndEditFormModal";
import StatusComponent from "./StatusComponent";
import { useGetDocumentsQuery } from "@/slices/api/documentSlice";
const styles = {
  icon_size: 24,
};

type Props = {
  selectedChatbot: ChatbotConfig;
};

function KnowledgeSources({ selectedChatbot }: Props) {
  const params = useParams();
  const [documents, setDocuments] = useState<Document[]>([]);
  const [success, setSuccess] = useState<string | null>(null);
  const [isPolling, setIsPolling] = useState(false);

  const {
    data: docs = [],
    isLoading,
    isError,
    refetch,
  } = useGetDocumentsQuery({ chatbotId: selectedChatbot.id });

  useEffect(() => {
    const getDocuments = async (): Promise<Document[]> => {
      if (docs.length > 0) {
        setDocuments(docs);
        return docs as Document[];
      } else {
        setDocuments([]);
        return [];
      }
    };

    // Load the page the first time
    getDocuments();

    // Set a regular time to get documents
    let intervalDocuments: number;
    if (isPolling) {
      const intervalDocuments = setInterval(async () => {
        const currentDocs: Document[] = await getDocuments();
        // document.status "P" or "E" indicates processing finished
        const isProcessingDocs = currentDocs.some(
          (doc) => doc.status === "C" || doc.status === "U"
        );
        if (!isProcessingDocs) {
          clearInterval(intervalDocuments);
          setIsPolling(false);
        } else {
          refetch();
        }
      }, 5000);
      return () => {
        if (intervalDocuments) {
          clearInterval(intervalDocuments);
        }
      };
    }
  }, [selectedChatbot.id, isPolling, docs]);

  const handleAddNewDocument = (newDocument: Document) => {
    setDocuments([...documents, newDocument]);
    let successDetail = "";
    successDetail = `Successfully added ${newDocument.title}!`;
    setIsPolling(true);
    setSuccess(successDetail);
  };

  const handleDeleteDocument = (deletedDoc: Document) => {
    let updated_documents = documents.filter((doc) => doc.id !== deletedDoc.id);

    setDocuments(updated_documents);
    const successDetail = `Successfully deleted ${deletedDoc.title}!`;

    setSuccess(successDetail);
  };

  const handleUpdateDocument = (updatedDocument: Document) => {
    let updated_documents = documents.filter(
      (document) => document.id !== updatedDocument.id
    );
    setIsPolling(true);
    setDocuments([...updated_documents, updatedDocument]);
  };

  return (
    <div className="flex-1 flex-col">
      <div className="text-left my-5 mx-2">
        {documents.length > 0 ? (
          <>
            <DocumentUploadAndEditFormModal
              document={{ id: 0, title: "", content: "" } as Document}
              selectedChatbot={selectedChatbot}
              handleAddNewDocument={handleAddNewDocument}
              submit_action="add"
            />
            <Table>
              <TableHead>
                <TableRow>
                  <TableHeaderCell>File</TableHeaderCell>
                  <TableHeaderCell>Type</TableHeaderCell>
                  <TableHeaderCell>Status</TableHeaderCell>
                  <TableHeaderCell></TableHeaderCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {documents.map((document) => (
                  <TableRow key={document.id}>
                    <TableCell className="inline-flex items-center">
                      <DocumentUploadAndEditFormModal
                        document={document}
                        selectedChatbot={selectedChatbot}
                        handleAddNewDocument={handleUpdateDocument}
                        submit_action="update"
                      />
                      {document.properties.doc_type == "google" ? (
                        <a
                          href={document.content}
                          target="_blank"
                          className="ml-3"
                        >
                          <FaExternalLinkAlt />
                        </a>
                      ) : (
                        ""
                      )}
                    </TableCell>

                    <TableCell>
                      {document.properties.doc_type == "text" ? (
                        <FaFilePen
                          size={styles.icon_size}
                          title="Text submission"
                        />
                      ) : document.properties.doc_type == "google" ? (
                        <SiGoogledocs
                          size={styles.icon_size}
                          title="GoogleDoc"
                        />
                      ) : document.title.includes(".pdf") ? (
                        <FaFilePdf size={styles.icon_size} title="PDF" />
                      ) : document.title.includes(".docx") ? (
                        <FaFileWord size={styles.icon_size} title="DOC" />
                      ) : (
                        <FaFileLines size={styles.icon_size} title="TXT" />
                      )}
                    </TableCell>
                    <TableCell>
                      <StatusComponent document_status={document.status} />
                    </TableCell>
                    <TableCell>
                      <DeleteDocumentModal
                        documentToDelete={document}
                        onDeleteDocument={handleDeleteDocument as any}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </>
        ) : (
          <div
            className={`w-[300px] mx-auto text-left flex flex-col items-center`}
          >
            <span className={`mb-[15px]`}>
              Knowledge sources help you ground your assistant in information
              you provide. Your assistant will search documents for a reliable
              answer to return to your users.
            </span>
            <DocumentUploadAndEditFormModal
              document={
                {
                  id: 0,
                  title: "",
                  content: "",
                  properties: { doc_type: "text" },
                } as Document
              }
              selectedChatbot={selectedChatbot}
              handleAddNewDocument={handleAddNewDocument}
              submit_action="add"
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default KnowledgeSources;
