export const chatbotTypes = {
  PROMPT_BASED: "prompt-based",
  KNOWLEDGE_BASED: "knowledge-based",
  EVERYTHING: "everything", // use onyl in frontend to represent a user-friendly string, means same as hybrid
  HYBRID: "hybrid",
  DISCUSSION: "discussion",
  QUESTION_ANSWER: "question-answer",
  GUIDED_CONVERSATION: "guided-conversation",
} as const;

export type ChatbotType =
  | "prompt-based"
  | "knowledge-based"
  | "everything"
  | "hybrid"
  | "discussion"
  | "question-answer"
  | "guided-conversation";

export interface ChatbotConfig {
  id: string;
  config: {
    bot_type: string;
    general_prompt: {
      persona: string;
      instructions: string;
      constraints: string;
    };
    knowledge: {
      enabled: boolean;
      type: string; // isn't provided by backend
      activation: { type: string; threshold: number };
      fallback: string;
    };
    fallbackResponses: string[]; // isn't provided by backend
    flow: string;
  };

  project: string;
  generative_model: string;
  active_chat: string;
  template: string;
  origin: string;
  provider: string;
}
