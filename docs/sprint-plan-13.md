# Cetin
* [X] Suggest the URL refactor
* [X] Research Sentry logging for frontend errors
* [X] Disabling the warnings
* [X] Checking which views need customer decorators for Swagger compliance

# Vlad 
* [ ] Remove errors in tests 

# Maria 
 * [ ] Finish the chatbot engine merge request 
 * [ ] Share with the team examples of chatbots we can build 
 * [ ] Find kubernetes mentor to review the cluster settings and suggest best practices. 
