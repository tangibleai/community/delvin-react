from rest_framework import generics
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import (
    MessageTagGroup,
)


class MessageTagGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageTagGroup
        fields = "__all__"


class MessageTagGroupCreateAPIView(generics.CreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = MessageTagGroup.objects.all()
    serializer_class = MessageTagGroupSerializer


message_tag_group_create_view = MessageTagGroupCreateAPIView.as_view()
