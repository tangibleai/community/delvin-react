import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import {
  useCreateWorkspaceMutation,
  useListWorkspacesQuery,
} from "@/slices/api/workspacesApiSlice";
import { RootState } from "@/store";
import Loading from "@/components/Loading";
import ErrorScreen from "@/components/ErrorScreen";
import { useToast } from "@/components/ui/use-toast";
import { Workspace } from "@/typings/workspace";

function Home() {
  const navigate = useNavigate();
  const { toast } = useToast();

  const user = useSelector((state: RootState) => state.auth.user);
  const {
    data: workspaces,
    isLoading: retrievingWorkspaces,
    isError: errorRetrievingWorkspaces,
  } = useListWorkspacesQuery(user!.id);
  const [
    createWorkspace,
    { isLoading: creatingWorkspace, isError: errorCreatingWorkspace },
  ] = useCreateWorkspaceMutation();

  useEffect(() => {
    if (!errorRetrievingWorkspaces) return;

    toast({
      variant: "destructive",
      title: "Oops! Something went wrong",
      description:
        "Couldn't retrieve workspaces. Please, try to reload the page.",
    });
  }, [errorRetrievingWorkspaces]);

  useEffect(() => {
    if (retrievingWorkspaces) return;

    if (workspaces?.length === 0) {
      handleNoWorkspaces();
    } else {
      const currentWorkspaceId = user?.current_workspace || workspaces![0].id;
      navigate(`/workspace/${currentWorkspaceId}`);
    }
  }, [retrievingWorkspaces, errorRetrievingWorkspaces, workspaces, user]);

  const handleNoWorkspaces = async () => {
    const workspaceName = `${user!.name}'s Workspace`;
    let newWorkspace = { id: "1" } as Workspace;
    try {
      newWorkspace = await createWorkspace({
        owner: user!.id,
        name: workspaceName,
      }).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description:
          "Couldn't create workspace for a user. Please, try to log out and sign in again.",
      });
      return;
    }

    toast({
      title: "Success!",
      description: "Workspace successfully created!",
    });

    navigate(`/workspace/${newWorkspace.id}`);
  };

  if (retrievingWorkspaces || creatingWorkspace) {
    return <Loading />;
  }

  if (errorRetrievingWorkspaces || errorCreatingWorkspace) {
    return <ErrorScreen />;
  }

  return <Loading />; // Render nothing as this is a redirect-only component.
}

export default Home;
