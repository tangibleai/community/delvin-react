import copy

from pydantic import BaseModel
from typing import Optional, Dict
from enum import Enum

from api.utils.moderation import BuiltinCategory, BUILTIN_DEFAULT_CATEGORIES


# NOTE: backwards-compatibility for transitioning to new bot types
class BotTypeEnum(str, Enum):
    PromptBased = "prompt"  # Remove later
    KnowledgeBased = "knowledge"  # Remove later
    Hybrid = "hybrid"
    Discussion = "discussion"
    QuestionAnswer = "question-answer"
    GuidedConversations = "guided-conversation"


class Prompt(BaseModel):
    persona: str
    constraints: str
    instructions: str


class KnowledgeActivation(BaseModel):
    type: str
    threshold: float


class Knowledge(BaseModel):
    enabled: bool
    activation: Optional[KnowledgeActivation]
    fallback: str


class BuiltinGuardrails(BaseModel):
    enabled: bool
    categories: Optional[Dict[str, BuiltinCategory]]


class Guardrails(BaseModel):
    builtin: BuiltinGuardrails


class ChatbotConfig(BaseModel):
    bot_type: BotTypeEnum
    general_prompt: Prompt
    knowledge: Knowledge
    flow: Optional[str]

    def get_general_prompt_str(self):
        return f"""
{self.general_prompt.persona}
                
{self.general_prompt.instructions}
                
## Constraints
{self.general_prompt.constraints}
"""


class ProjectConfig(BaseModel):
    guardrails: Guardrails


def create_knowledge_settings(bot_type, knowledge_settings, prompt_settings):
    """Creates the Chatbot.config 'knowledge' object

    >>> create_knowledge_settings("discussion", {}, {})
    {'enabled': False, 'fallback': '', 'activation': {'type': 'never', 'threshold': 1}}

    >>> create_knowledge_settings("question-answer", {"type": "always"}, {"fallbackResponses": "I dont know"})
    {'enabled': True, 'activation': {'type': 'always', 'threshold': 0.0}, 'fallback': 'I dont know'}

    >>> create_knowledge_settings("question-answer", {"type": "threshold", "threshold": 0.75}, {"fallbackResponses": "I dont know"})
    {'enabled': True, 'activation': {'type': 'threshold', 'threshold': 0.75}, 'fallback': 'I dont know'}
    """
    updated_knowledge_settings = {
        "enabled": False,
        "fallback": "",
        "activation": {"type": "never", "threshold": 1},
    }
    if bot_type == BotTypeEnum.QuestionAnswer:
        updated_knowledge_settings = {
            "enabled": True,
            "activation": {
                "type": knowledge_settings.get("type", "always"),
                "threshold": knowledge_settings.get("threshold", 0.0),
            },
            "fallback": prompt_settings["fallbackResponses"],
        }
    return updated_knowledge_settings


def create_chatbot_config_object(
    prompt_settings: dict = {}, knowledge_settings: dict = {}, guardrails: dict = {}
) -> ChatbotConfig:
    """Combines chatbot settings into a Chatbot.config object for onboarding form

    Input
    - bot_type: str
    - prompt_settings: dict
    - bot_settings: dict

    Output
    - config: dict

    >>> create_chatbot_config_object({}, {})
    {'bot_type': 'discussion', 'general_prompt': {'persona': '', 'instructions': '', 'constraints': ''}, 'knowledge': {'enabled': False, 'fallback': '', 'activation': {'type': 'never', 'threshold': 1}}}

    >>> create_chatbot_config_object({"chatbotPersona": "You are helpful.", "constraints": "You are not mean.", "instructions": "Talk about cats."}, {})
    {'bot_type': 'discussion', 'general_prompt': {'constraints': 'You are not mean.', 'instructions': 'Talk about cats.', 'persona': 'You are helpful.'}, 'knowledge': {'enabled': False, 'fallback': '', 'activation': {'type': 'never', 'threshold': 1}}}

    # works without knowledge settings for knowledge bots (transition)
    >>> create_chatbot_config_object({"chatbotPersona": "Answer my question.", "constraints": "Don't cuss", "fallbackResponses": "I didn't find anything"}, {})
    {'bot_type': 'question-answer', 'general_prompt': {'constraints': "Don't cuss", 'persona': 'Answer my question.', 'instructions': ''}, 'knowledge': {'enabled': True, 'activation': {'type': 'always', 'threshold': 0.0}, 'fallback': "I didn't find anything"}}
    """
    config = {}

    # Hybrid bot
    if knowledge_settings == {} and prompt_settings == {}:
        return {
            "bot_type": "discussion",
            "general_prompt": {"persona": "", "instructions": "", "constraints": ""},
            "knowledge": {
                "enabled": False,
                "fallback": "",
                "activation": {"type": "never", "threshold": 1},
            },
            "guardrails": {
                "builtin": {
                    "enabled": False,
                    "categories": copy.deepcopy(BUILTIN_DEFAULT_CATEGORIES),
                }
            },
        }

    bot_type = prompt_settings.get("bot_type", BotTypeEnum.Discussion)
    if bot_type == BotTypeEnum.QuestionAnswer:
        bot_type = BotTypeEnum.QuestionAnswer
    elif bot_type == BotTypeEnum.GuidedConversations:
        bot_type = BotTypeEnum.GuidedConversations

    config["bot_type"] = bot_type

    flow_id = prompt_settings.get("flow_id", "")
    config["flow"] = flow_id

    config["general_prompt"] = prompt_settings
    config["general_prompt"]["persona"] = config["general_prompt"].pop(
        "chatbotPersona", ""
    )
    config["general_prompt"]["instructions"] = config["general_prompt"].get(
        "instructions", ""
    )

    updated_knowledge_settings = create_knowledge_settings(
        bot_type, knowledge_settings, prompt_settings
    )
    config["knowledge"] = updated_knowledge_settings

    if config["general_prompt"].get("fallbackResponses", ""):
        del config["general_prompt"]["fallbackResponses"]

    config["guardrails"] = {
        "builtin": {"enabled": False, "categories": BUILTIN_DEFAULT_CATEGORIES}
    }
    config["constraints"] = ""
    return config


def update_chatbot_config(
    bot_type: str = "discussion",
    prompt_settings: dict = {},
    knowledge_settings: dict = {},
    guardrails: dict = {},
) -> ChatbotConfig:
    """Updates the Chatbot.config when saved from the AI Settings form"""

    if guardrails == {}:
        guardrails = {
            "builtin": {"enabled": False, "categories": BUILTIN_DEFAULT_CATEGORIES}
        }

    config = {
        "bot_type": bot_type,
        "general_prompt": {
            "constraints": prompt_settings.get("constraints", ""),
            "instructions": prompt_settings.get("instructions", ""),
            "persona": prompt_settings.get("chatbotPersona", ""),
        },
        "knowledge": {
            "enabled": knowledge_settings.get("enabled", False),
            "fallback": prompt_settings.get("fallbackResponses", ""),
            "activation": {
                "type": knowledge_settings.get("type", ""),
                "threshold": knowledge_settings.get("threshold", 1),
            },
        },
        "guardrails": guardrails,
    }
    return config
