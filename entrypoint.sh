#!/bin/bash

echo "Number of cores: $CORES"

echo "Activating virtual environments"
source /opt/venv/bin/activate

echo "Collecting static files"
python manage.py collectstatic --noinput

echo "Applying migrations"
python manage.py migrate

echo "Creating api schema"
python manage.py spectacular --file schema.yml 2>&1 | grep Error > schema.log

echo "Starting gunicorn!"
gunicorn --workers $((2 * CORES + 1)) --bind "0.0.0.0:8000" core.wsgi:application
