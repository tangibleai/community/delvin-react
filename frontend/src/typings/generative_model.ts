export interface GenerativeModel {
  id: number;
  label: string;
  model_string: string;
  priority: number;
  provider_type: string;
}
