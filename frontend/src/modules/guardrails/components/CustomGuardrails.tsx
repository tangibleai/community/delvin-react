import { useState } from "react";
import { useParams } from "react-router-dom";

import { Button } from "@/components/ui/button";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeaderCell,
  TableRow,
} from "@tremor/react";

import { GuardrailResponse } from "@/typings/guardrail_response";
import NewAction from "../components/NewActionFormModal";
import NewTagDefinition from "../components/NewTagDefinitionFormModal";
import UpdateGuardrailResponse from "../components/UpdateGuardrailResponseFormModal";
import DeleteGuardrailModal from "./DeleteGuardrailModal";
import {
  useDeleteGuardrailResponseMutation,
  useListGuardrailResponsesQuery,
} from "@/slices/api/guardrailResponsesApiSlice";
import { Loader } from "lucide-react";
import { useToast } from "@/components/ui/use-toast";
import ErrorScreen from "@/components/ErrorScreen";

export default function CustomGuardrails() {
  const routeParams = useParams();
  const projectId = routeParams.projectId!;
  const {
    data: guardrailResponses,
    isLoading,
    isError,
    error,
    isUninitialized,
  } = useListGuardrailResponsesQuery(projectId);
  const [
    deleteGuardrailResponse,
    { isError: isDeleteError, error: deleteError },
  ] = useDeleteGuardrailResponseMutation();
  const { toast } = useToast();
  const [tagDefinitionOpen, setTagDefinitionOpen] = useState<boolean>(false);
  const [guardrailActionOpen, setGuardrailActionOpen] =
    useState<boolean>(false);

  const handleDelete = async (guardrailResponseId: string) => {
    try {
      await deleteGuardrailResponse(guardrailResponseId).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Guardrail response",
        description: "There was an error while creating guardrail response!",
      });
    }
  };

  if (isError) {
    return <ErrorScreen />;
  }

  if (isLoading || isUninitialized) {
    return <Loader />;
  }

  return (
    <div className="w-full relative">
      <div className="my-2">
        <div className="flex flex-row justify-between">
          <h1 className="header justify-start">Responses</h1>
          <Button
            onClick={() => {
              setTagDefinitionOpen(true);
            }}
          >
            New response
          </Button>
          <NewTagDefinition
            guardrailResponses={guardrailResponses}
            tagDefinitionOpen={tagDefinitionOpen}
            guardrailActionOpen={guardrailActionOpen}
            setTagDefinitionOpen={setTagDefinitionOpen}
            setGuardrailActionOpen={setGuardrailActionOpen}
          />
          <NewAction
            guardrailResponses={guardrailResponses}
            tagDefinitionOpen={tagDefinitionOpen}
            guardrailActionOpen={guardrailActionOpen}
            setTagDefinitionOpen={setTagDefinitionOpen}
            setGuardrailActionOpen={setGuardrailActionOpen}
          />
        </div>
      </div>
      <div className="w-full mx-auto">
        <Table>
          <TableHead>
            <TableRow>
              <TableHeaderCell>When</TableHeaderCell>
              <TableHeaderCell>Tag</TableHeaderCell>
              <TableHeaderCell>Response</TableHeaderCell>
              <TableHeaderCell></TableHeaderCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {guardrailResponses.map((guardrailResponse: GuardrailResponse) => {
              return (
                <TableRow
                  className="hover:bg-slate-100"
                  key={guardrailResponse.id}
                >
                  <TableCell>
                    {guardrailResponse.message_tag_definition.description}
                  </TableCell>
                  <TableCell>
                    {guardrailResponse.message_tag_definition.name}
                  </TableCell>
                  <TableCell className="max-w-2xl">
                    {guardrailResponse.guardrail_action.action_type ===
                    "scripted"
                      ? `Say "${guardrailResponse.guardrail_action.content.slice(
                          0,
                          60
                        )}..."`
                      : `${guardrailResponse.guardrail_action.content.slice(
                          0,
                          60
                        )}...`}
                  </TableCell>
                  <TableCell className="flex flex-row gap-5">
                    <UpdateGuardrailResponse
                      guardrailResponse={guardrailResponse}
                    />
                    <DeleteGuardrailModal
                      guardRailID={guardrailResponse.id}
                      onDelete={handleDelete}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    </div>
  );
}
