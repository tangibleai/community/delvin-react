import { forwardRef, useEffect, useImperativeHandle } from "react";
import { useForm } from "react-hook-form";

import * as z from "zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";
import { AppDispatch, RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { setOnboardingData } from "@/slices/onboardingSlice";

const DocumentFormFile = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();

  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData,
  );

  const allowedFileTypes = [
    "application/pdf",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "text/plain",
  ];
  const formSchema = z.object({
    documentFile: z
      .instanceof(FileList)
      .optional()
      .superRefine((fileList, ctx) => {
        if (!fileList || fileList.length === 0) {
          ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: "Please upload a file",
          });
        } else if (
          !Array.from(fileList).every((file) =>
            allowedFileTypes.includes(file.type),
          )
        ) {
          ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: "Only .pdf, .docx, and .txt files are allowed",
          });
        }
      }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      documentFile: onboardingData.stepDocumentAdding.file || ({} as FileList),
    },
  });

  const { handleSubmit } = form;

  const documentFileRef = form.register("documentFile");

  async function onSubmit(values: z.infer<typeof formSchema>) {
    const { documentFile } = values;

    dispatch(
      setOnboardingData({
        ...onboardingData,
        stepDocumentAdding: {
          file: documentFile!,
          text: onboardingData.stepDocumentAdding.text,
          google: onboardingData.stepDocumentAdding.google,
        },
      }),
    );

    return true;
  }

  useImperativeHandle(ref, () => ({
    submitForm: () => {
      return new Promise<boolean>((resolve) => {
        handleSubmit(async (values) => {
          const result = await onSubmit(values);
          resolve(result);
        })();
      });
    },
    updateOnboardingDataWithDraft: () => {
      const { documentFile } = form.getValues();
      dispatch(
        setOnboardingData({
          ...onboardingData,
          stepDocumentAdding: {
            file: documentFile!,
            text: onboardingData.stepDocumentAdding.text,
            google: onboardingData.stepDocumentAdding.google,
          },
        }),
      );
    },
  }));

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
        <FormField
          control={form.control}
          name="documentFile"
          render={({ field }) => (
            <FormItem>
              <FormLabel className="text-3xl">Select your file</FormLabel>
              <FormControl className="flex text-2xl text-center items-center">
                <Input
                  type="file"
                  {...documentFileRef}
                  className="text-2xl border border-black"
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
      </form>
    </Form>
  );
});

export default DocumentFormFile;
