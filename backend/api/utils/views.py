from api.ml import sgd
from api import models
import numpy as np


# def get_retrained_coef_and_intercept(dataset, tag_definition):
#     x = []
#     y = []

#     messages = models.Message.objects.filter(dataset=dataset)
#     for msg in messages:
#         embedding = msg.embedding
#         x.append(embedding)
#         message_tagged_with_tag = 1 if msg.tags.filter(id=tag_definition.id) else 0
#         y.append(message_tagged_with_tag)

#     result = sgd.fit(x, y)["coef"]

#     coef_and_intercept = dict(intercept=result[0], coef=result[1:])

#     return coef_and_intercept

from api import models
from api.utils import views


def retrain_model(project, ml_model):
    tag_definitions = models.MessageTagDefinition.objects.filter(project=project)
    for t in tag_definitions:
        models.MLWeights.objects.filter(tag_definition=t).delete()
        coef_and_intercept = views.get_retrained_coef_and_intercept(t)
        models.MLWeights.objects.create(
            tag_definition=t,
            coef=coef_and_intercept["coef"],
            intercept=coef_and_intercept["intercept"],
            ml_model=ml_model,
        )


# def get_retrained_coef_and_intercept(tag_definition):
#     x = []
#     y = []

#     message_tag_recs = models.MessageTag.objects.filter(
#         tag__project=tag_definition.project
#     )
#     for r in message_tag_recs:
#         message = r.message
#         embedding = [1] + list(message.embedding)
#         x.append(embedding)
#         message_tagged_with_tag = (
#             1 if models.MessageTag.objects.filter(tag__id=tag_definition.id) else 0
#         )
#         y.append(message_tagged_with_tag)

#     x = np.array(x)
#     y = np.array(y)
#     result = sgd.fit_with_sklearn_linear_regression(x, y)
#     # coef = result["coef"]
#     # intercept = result["intercept"]
#     # result = sgd.fit(x, y)["coef"]

#     # coef_and_intercept = dict(intercept=result[0], coef=coef)

#     return result


def get_retrained_coef_and_intercept(tag_definition):
    x = []
    y = []

    messages = models.Message.objects.filter(
        project=tag_definition.project,
        id__in=models.MessageTag.objects.values_list(
            "message_id", flat=True
        ).distinct(),
    )

    for m in messages:
        embedding = list(m.embedding)
        x.append(embedding)
        message_tagged_with_tag = (
            1
            if models.MessageTag.objects.filter(message=m, tag__id=tag_definition.id)
            else 0
        )
        y.append(message_tagged_with_tag)

    x = np.array(x)
    y = np.array(y)
    result = sgd.fit_with_sklearn_linear_regression(x, y)

    return result
