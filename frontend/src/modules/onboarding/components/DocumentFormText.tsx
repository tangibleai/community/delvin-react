import { forwardRef, useEffect, useImperativeHandle } from "react";
import { useForm } from "react-hook-form";

import * as z from "zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { zodResolver } from "@hookform/resolvers/zod";
import { Textarea } from "@/components/ui/textarea";
import { AppDispatch, RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { setOnboardingData } from "@/slices/onboardingSlice";

const DocumentFormText = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();

  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData,
  );

  const formSchema = z.object({
    text: z
      .string()
      .min(1, "Please enter some information your bot should know."),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      text: "",
    },
  });

  const { handleSubmit, reset } = form;

  async function onSubmit(values: z.infer<typeof formSchema>) {
    const { text } = values;
    dispatch(
      setOnboardingData({
        ...onboardingData,
        stepDocumentAdding: {
          file: onboardingData.stepDocumentAdding.file,
          text: text,
          google: onboardingData.stepDocumentAdding.google,
        },
      }),
    );

    return true;
  }

  useEffect(() => {
    if (onboardingData) {
      reset({
        text: onboardingData.stepDocumentAdding.text,
      });
    }
  }, [onboardingData, reset]);

  useImperativeHandle(ref, () => ({
    submitForm: () => {
      return new Promise<boolean>((resolve) => {
        handleSubmit(async (values) => {
          const result = await onSubmit(values);
          resolve(result);
        })();
      });
    },
    updateOnboardingDataWithDraft: () => {
      const { text } = form.getValues();
      dispatch(
        setOnboardingData({
          ...onboardingData,
          stepDocumentAdding: {
            file: onboardingData.stepDocumentAdding.file,
            text: text,
            google: onboardingData.stepDocumentAdding.google,
          },
        }),
      );
    },
  }));

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
        <FormField
          control={form.control}
          name="text"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Textarea
                  placeholder="The facts your bot should use to respond to users"
                  {...field}
                  className=" text-xl"
                  rows={16}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
      </form>
    </Form>
  );
});

export default DocumentFormText;
