import { GoAlertFill } from "react-icons/go";

export default function DisabledBannerApiEditor() {
    return (
        <div className="bg-red-200 flex items-center p-6 rounded-md">

            <div className="mr-4 flex-shrink-0 text-3xl">
                <GoAlertFill />
            </div>

            <div className="flex-1">
                <h2 className="text-lg font-semibold">
                    API integration is disabled for this project.
                </h2>
                <p className="text-sm mt-1">
                    Your projects can now only use free generative models on limited availability basis and API usage has been disabled.
                    Ask the workspace owner to upgrade the plan to get access to all the models, unlimited messages and more!
                </p>
            </div>

        </div>
    );
};


