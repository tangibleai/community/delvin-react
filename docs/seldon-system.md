#### Tutorial
[Official](https://docs.seldon.io/projects/seldon-core/en/latest/workflow/github-readme.html)
[GitHub-Repository](https://github.com/SeldonIO/seldon-core)
[Python-Seldon-Core-Module](https://docs.seldon.io/projects/seldon-core/en/latest/python/index.html)
[Packaging a Model Using Docker](https://docs.seldon.io/projects/seldon-core/en/latest/python/python_wrapping_docker.html)
[Non-Official-Tutorial](https://towardsdatascience.com/to-serve-man-60246a82d953)

#### Installation Commands
```commandline
helm install seldon-core seldon-core-operator     --repo https://storage.googleapis.com/seldon-charts     --set usageMetrics.enabled=true     --namespace seldon-system 
kubectl apply -f ml2/sentence-embedder-seldon.yaml 
kubectl get pods
kubectl get pods --help
kubectl get pods --namespace seldon-system
kubectl describe pods sentence-embedder-model-default-0-sentence-embedder-graph-9xnlg 
kubectl delete -f ml2/sentence-embedder-seldon.yaml 
```
