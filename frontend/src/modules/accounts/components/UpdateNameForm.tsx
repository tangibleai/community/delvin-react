import { Button } from "@/components/ui/button";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/store";
import { useEffect } from "react";
import { updateUser } from "@/lib/api";
import { useToast } from "@/components/ui/use-toast";
import { useDispatch } from "react-redux";
import { loadUserSuccess } from "@/slices/authSlice";

// Form schema for validation
const formSchema = z.object({
  name: z.string().min(1, "Please enter a name that is more than 1 character"),
});

const UpdateNameForm = () => {
  const { toast } = useToast();
  const dispatch: AppDispatch = useDispatch();

  const user = useSelector((state: RootState) => state.auth.user);

  useEffect(() => {
    if (!user) return;
    form.setValue("name", user!.name);
  }, [user]);

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
    },
  });

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const { name } = values;

    try {
      const updatedUser = await updateUser(user!.id, { name: name });
      dispatch(loadUserSuccess(updatedUser));
      toast({
        title: "Success!",
        description: "Name was successfully updated.",
      });
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Couldn't update name. Please, try again later.",
      });
    }
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
        <div className="flex items-center gap-4">
          <FormField
            control={form.control}
            name="name"
            render={({ field }) => (
              <FormItem className="flex items-center text-center gap-4 w-5/6">
                <FormLabel className="text-lg">Name</FormLabel>
                <FormControl>
                  <Input
                    {...field}
                    placeholder="Enter your name"
                    className="text-lg"
                    // className="border-2 border-black"
                  />
                </FormControl>
                {form.formState.errors.name && <FormMessage />}
              </FormItem>
            )}
          />
          <Button type="submit" className="text-lg">
            Edit
          </Button>
        </div>
      </form>
    </Form>
  );
};

export default UpdateNameForm;
