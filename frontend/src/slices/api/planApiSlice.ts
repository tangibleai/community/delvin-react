import { Plan } from "@/typings/plan";
import emptyApi from "../apiSlice";
import { Workspace } from "@/typings/workspace";

export const planApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    fetchPlan: builder.query<Plan, string>({
      query: (planId: string) => ({
        url: `plans/${planId}/details/`,
        method: "GET",
      }),
      providesTags: (result, error, arg) => [{ type: "Plan", id: arg }],
    }),
    updatePlan: builder.mutation<Plan, { plan: Plan; workspace: Workspace }>({
      query: (records) => ({
        url: `plans/${records.plan.id}/update/`,
        method: "PUT",
        data: records.plan,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: "Plan", id: arg.plan.id },
        { type: "Workspaces", id: arg.workspace.id },
        "Chatbot",
        "Provider",
      ],
    }),
  }),
});

export const { useFetchPlanQuery, useUpdatePlanMutation } = planApiSlice;
