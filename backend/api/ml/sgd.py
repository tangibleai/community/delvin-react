""" Based on Real Python blog post: https://12ft.io/proxy?q=https://realpython.com/gradient-descent-algorithm-python

>>> X = np.random.randn(100, 3)
>>> X.shape
(100, 3)
>>> X = np.concatenate([np.ones((100, 1)), X], axis=1)
>>> X.shape
(100, 4)
>>> X  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
array([[ 1..., ..., ..., ...],...]])
>>> coef = np.array([-4, +3, -2, +1])
>>> Y_true = X.dot(coef)
>>> Y = Y_true + .1 * np.random.randn(*Y_true.shape)
>>> results = fit(X, Y,
...     batch_size=16,
...     tolerance=0.001
...     learning_rate=.1,
...     momentum=.5,
...     l2_regularization=.0001,
...     activation_fun='logistic')
>>> np.mean((results['coef'] - np.array(weights)) ** 2) ** .5 < .01
True
>>> 0 < results['num_iterations'] <= 50
True
"""

from sklearn.linear_model import LogisticRegression


def fit_with_sklearn_linear_regression(x, y):
    reg = LogisticRegression(class_weight="balanced").fit(x, y)
    coef = reg.coef_
    intercept = reg.intercept_

    coef = coef[0]
    intercept = intercept[0]

    return dict(coef=coef, intercept=intercept)
