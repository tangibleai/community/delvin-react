import { ChatbotConfig } from "@/typings/chatbot_config";
import emptyApi from "../apiSlice";
import { Flow } from "@/typings/flow";
import { Node, Edge } from "reactflow";

interface CreateFlowRequestBody {
  project_id: string;
  name: string;
  description: string;
  assistant_role: string;
}

type UpdateFlowRequestBody = {
  flowId: string;
  nodes?: Node[];
  edges?: Edge[];
};

const flowApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    getFlows: builder.query<Flow[], string>({
      query: (projectId: string) => ({
        url: `flows/`,
        method: "GET",
        params: { project_id: projectId },
      }),
      providesTags: (result, error, arg) => [{ type: "Flow", id: arg }],
    }),
    getFlow: builder.query<Flow, string>({
      query: (flowId: string) => ({
        url: `flows/${flowId}/details/`,
        method: "GET",
      }),
      providesTags: (result, error, arg) => [{ type: "Flow", id: arg }],
    }),
    createFlow: builder.mutation<Flow, CreateFlowRequestBody>({
      query: (requestBody) => ({
        url: "flows/create/",
        method: "POST",
        data: requestBody,
      }),
      invalidatesTags: ["Flow"],
    }),
    updateFlow: builder.mutation<ChatbotConfig[], UpdateFlowRequestBody>({
      query: (requestBody) => ({
        url: `flows/${requestBody.flowId}/update/`,
        method: "POST",
        data: requestBody,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: "Flow", id: arg.flowId },
      ],
    }),
    deleteFlow: builder.mutation<null, string>({
      query: (flowId: string) => ({
        url: `flows/${flowId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: ["Flow"],
    }),
  }),
});

export const {
  useGetFlowsQuery,
  useGetFlowQuery,
  useCreateFlowMutation,
  useUpdateFlowMutation,
  useDeleteFlowMutation,
} = flowApiSlice;
