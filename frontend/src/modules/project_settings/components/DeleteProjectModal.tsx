import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector } from "react-redux";

import { RootState } from "@/store";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";

import { IoReload } from "react-icons/io5";
import { useToast } from "@/components/ui/use-toast";
import {
  useDeleteProjectMutation,
  useGetCurrentProjectDetailsQuery,
} from "@/slices/api/projectsApiSlice";
import { Project } from "@/typings/project";

function DeleteProjectModal() {
  const { toast } = useToast();

  const [error, setError] = useState<string | null>(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [deleteButtonClicked, setDeleteButtonClicked] = useState(false);

  const routeParams = useParams();
  const projectId = routeParams.projectId!;
  const { data: project = {} as Project } =
    useGetCurrentProjectDetailsQuery(projectId);

  const [deleteProject] = useDeleteProjectMutation();

  const navigate = useNavigate();

  useEffect(() => {
    // Handles resetting the form after submit or x
    // Can't directly access x in Dialog
    if (!isModalOpen) {
      form.reset();
    }
  }, [isModalOpen]);

  async function handleClickDelete(values: z.infer<typeof formSchema>) {
    setDeleteButtonClicked(true);
    const { userDeleteConfirmation } = values;

    if (userDeleteConfirmation == confirmationString) {
      try {
        await deleteProject(projectId).unwrap();
        toast({
          title: "Success!",
          description: "Project was successfully removed.",
        });
        return navigate("/");
      } catch (error) {}
    } else {
      setError("The confirmation string does not match");
    }
    setDeleteButtonClicked(false);
  }

  const confirmationString = `delete ${project?.name}`;

  const formSchema = z.object({
    userDeleteConfirmation: z
      .string()
      .min(4, {
        message: "Please type the confirmation phrase",
      })
      .refine((val) => val.includes(`${confirmationString}`), {
        message: `The confirmation phrase must be "${confirmationString}"`,
      })
      .refine((val) => !/\s+$/.test(val), {
        message: "The confirmation phrase should not end with spaces.",
      }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      userDeleteConfirmation: "",
    },
  });

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button variant="destructive">Delete project</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-md">
        <DialogHeader>
          <DialogTitle>Delete project</DialogTitle>
        </DialogHeader>
        {error && (
          <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
            Error: {error}
          </p>
        )}
        <p>This action cannot be undone.</p>

        <p>
          All data associated with this project will be permanently deleted and
          unrecoverable.
        </p>

        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(handleClickDelete)}
            className="space-y-8"
          >
            <FormField
              control={form.control}
              name="userDeleteConfirmation"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>
                    Type <pre className="inline">{confirmationString}</pre> to
                    confirm.
                  </FormLabel>
                  <FormControl>
                    <Input placeholder={confirmationString} {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <DialogFooter>
              <Button
                disabled={deleteButtonClicked}
                type="submit"
                variant="destructive"
                className="order-2 sm:order-1"
              >
                {deleteButtonClicked && (
                  <IoReload className={"mr-2 h-4 w-4 animate-spin"} />
                )}
                Delete project
              </Button>
              <Button
                type="button"
                variant="outline"
                className="order-1 sm:order-2"
                onClick={() => setIsModalOpen(false)}
              >
                Cancel
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}

export default DeleteProjectModal;
