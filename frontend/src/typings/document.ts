interface DocumentProperties {
  doc_type: string;
}
export interface Document {
  id: number;
  title: string;
  content: string;
  status: string;
  properties: DocumentProperties;
}
