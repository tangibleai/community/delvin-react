import requests
from asgiref.timeout import timeout
from django.conf import settings

from api.internal.serializers import MessageSerializer
from api.models import Chat, Contact, Message
from django.core.cache import cache

from core.constant import CONSTANTS

DEFAULT_CACHE_TIMEOUT = CONSTANTS.DEFAULT_CACHE_TIMEOUT


def get_or_create_contact_and_chat(project, data, internal=True):
    contact_origin = data.get("contact")
    contact = Contact.objects.get_cached(contact_origin, project, internal)

    chat_origin_id = data.get("chat")
    chat = Chat.objects.get_cached(chat_origin_id, contact)

    return contact, chat


def create_message_from_request(data, project, internal=False):
    contact, chat = get_or_create_contact_and_chat(project, data, internal)

    url = settings.ENV["SENTENCE_EMBEDDINGS_URL"]
    embedding_data = {"data": {"ndarray": [data["user_message"]]}}
    r = requests.post(url=url, json=embedding_data)
    embeddings = r.json()["data"]["ndarray"]

    # Create and validate the message record
    message_data = {
        "text": data["user_message"],
        "project": project,
        "contact": contact,
        "chat": chat,
        "direction": Message.Direction.INBOUND,
        "sender_type": Message.SenderType.USER,
        "channel_type": Message.ChannelType.WEB,
        "embedding": embeddings[0],
    }
    serializer = MessageSerializer(data=message_data)
    if serializer.is_valid():
        message = Message.objects.create(**message_data)
        return message, None
    else:
        return (None, serializer.errors)


def previous_messages_gen_format(message):
    message_records = Message.objects.filter(chat=message.chat).order_by("timestamp")
    messages = []
    for msg in message_records:
        message_data = {
            "role": "user" if msg.direction == "inbound" else "assistant",
            "content": msg.text,
        }
        messages.append(message_data)
    return messages


def save_response_message(response_text, original_message):
    direction = (
        Message.Direction.OUTBOUND
        if original_message.direction == Message.Direction.INBOUND
        else original_message.Direction.INBOUND
    )
    sender_type = (
        Message.SenderType.BOT
        if original_message.sender_type == Message.SenderType.USER
        else Message.SenderType.User
    )

    response_message_data = {
        "project": original_message.project,
        "contact": original_message.contact,
        "chat": original_message.chat,
        "text": response_text,
        "direction": direction,
        "sender_type": sender_type,
        "channel_type": original_message.channel_type,
    }

    message = Message.objects.create(**response_message_data)

    return message
