"""Test UI login flow on http://127.0.0.1:3000/login"""

import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.conf import settings
from django.test import tag
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service

# from selenium.webdriver.firefox import


@tag("exclude_me")
class SeleniumTestCase(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        path_to_chrome_driver = settings.BASE_DIR / "geckodriver"
        print(path_to_chrome_driver)
        service = Service(path_to_chrome_driver)
        opts = webdriver.FirefoxOptions()
        opts.add_argument("--headless")  # Ensures Firefox runs in headless mode
        opts.add_argument(
            "--no-sandbox"
        )  # Bypass OS security model, necessary on Linux if running as root
        opts.add_argument(
            "--disable-dev-shm-usage"
        )  # Overcome limited resource problems
        cls.driver = webdriver.Firefox(options=opts)
        cls.corrent_email = settings.ENV["TEST_AUTH_EMAIL"]
        cls.wrong_email = "wrong@user.com"
        cls.correct_password = settings.ENV["TEST_USER_PASSWORD"]
        cls.wrong_password = "Abcdefgh!"
        cls.home_url = "http://127.0.0.1:3000"
        cls.home_urls = (
            cls.home_url,
            cls.home_url + "/",
        )

    def setUp(self):
        self.driver.get(f"{self.home_url}/login")
        self.email_input = self.driver.find_element(value="email")
        self.password_input = self.driver.find_element(value="password")
        self.submit_button = self.driver.find_element(
            By.XPATH, value="//button[@type='submit']"
        )

    def test_corrent_email_and_password(self):
        self.email_input.send_keys(self.corrent_email)
        self.password_input.send_keys(self.correct_password)
        self.submit_button.click()

        WebDriverWait(self.driver, 10).until(
            lambda _: self.driver.current_url in self.home_urls
        )

    def test_wrong_email(self):
        self.email_input.send_keys(self.wrong_email)
        self.password_input.send_keys(self.correct_password)
        self.submit_button.click()

        self.assert_url_unchanged()

    def test_wrong_password(self):
        self.email_input.send_keys(self.corrent_email)
        self.password_input.send_keys(self.wrong_password)
        self.submit_button.click()

        self.assert_url_unchanged()

    def test_wrong_email_and_password(self):
        self.email_input.send_keys(self.wrong_email)
        self.password_input.send_keys(self.wrong_password)
        self.submit_button.click()

        self.assert_url_unchanged()

    def assert_url_unchanged(self):
        for _ in range(10):
            time.sleep(1)
            self.assertTrue(self.driver.current_url not in self.home_urls)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()
