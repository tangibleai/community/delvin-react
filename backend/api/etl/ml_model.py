from api import models
from api.etl import utils


def process(df_or_records_list, *args, **kwargs):
    ml_models = df_or_records_list
    if not isinstance(ml_models, list):
        ml_models = df_or_records_list.to_dict("records")
    required_fields = {
        "name",
        "description",
        "project__name",
        "project__owner__email",
        "dataset__name",
    }
    utils.check_missing_fields(ml_models, required_fields)

    ml_models = resolve_dependent_records(ml_models)
    check_no_ml_model_connected_to_dataset_per_each_record(ml_models)
    db_records = save_ml_models_to_db(ml_models)

    return db_records


def resolve_dependent_records(ml_models):
    ml_models = utils.resolve_user_for_each_record(
        ml_models, user_email_field_name="project__owner__email"
    )
    ml_models = utils.resolve_project_for_each_record(ml_models)
    ml_models = resolve_dataset_for_each_record(ml_models)

    return ml_models


def resolve_dataset_for_each_record(ml_models):
    for m in ml_models:
        project_rec = m["project_rec"]
        dataset_name = m["dataset__name"]
        try:
            m["dataset_rec"] = models.Dataset.objects.get(
                name=dataset_name, project=project_rec
            )
        except models.Dataset.DoesNotExist:
            project_name = m["project__name"]
            raise ValueError(
                f'Dataset with "{dataset_name}" name doesn\'t exist for "{project_name}" project'
            )

    return ml_models


def check_no_ml_model_connected_to_dataset_per_each_record(ml_models):
    for m in ml_models:
        dataset_name = m["dataset__name"]
        project_rec = m["project_rec"]
        dataset_rec = models.Dataset.objects.get(name=dataset_name, project=project_rec)
        ml_model_connected = models.MLModel.objects.filter(
            project=project_rec, dataset=dataset_rec
        )
        if ml_model_connected:
            project_name = m["project__name"]
            raise ValueError(
                f'Dataset with "{dataset_name}" name already has MLModel connected within "{project_name}" project'
            )

    return ml_models


def save_ml_models_to_db(ml_models):
    initialized_records = [
        models.MLModel(
            name=m["name"],
            description=m["description"],
            project=m["project_rec"],
            dataset=m["dataset_rec"],
            created_by=m["owner_rec"],
        )
        for m in ml_models
    ]
    saved_records = models.MLModel.objects.bulk_create(initialized_records)

    return saved_records
