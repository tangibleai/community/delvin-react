# Generated by Django 4.2.13 on 2024-08-28 20:59

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):
    dependencies = [
        ("api", "0010_document_properties"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="chatbot",
            name="active_chat",
        ),
        migrations.AddField(
            model_name="chat",
            name="created_at",
            field=models.DateTimeField(
                auto_now_add=True, default=django.utils.timezone.now
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="chat",
            name="updated_at",
            field=models.DateTimeField(auto_now=True),
        ),
    ]
