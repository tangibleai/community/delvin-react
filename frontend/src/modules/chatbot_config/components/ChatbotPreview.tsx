import ChatContainer from "@/components/modules/chatbot/ChatContainer";
import { IoMdRefresh } from "react-icons/io";
import { RxCross1 } from "react-icons/rx";

import { Button } from "@/components/ui/button";

import {
  Sheet,
  SheetContent,
  SheetTrigger,
} from "@/components/ui/custom/sheet-without-close";
import { ChatbotConfig } from "@/typings/chatbot_config";
import { resolveChatForChatbotMapping } from "@/components/modules/actions/chatbot";
import { useDispatch, useSelector } from "react-redux";
import {
  ChatbotToChatMapping,
  removeChatbotToChatMapping,
  setChatbotToChatMapping,
} from "@/slices/chatbotSlice";
import { RootState } from "@/store";
import { useEffect, useState } from "react";
import { useGetContactQuery } from "@/slices/api/contactApiSlice";
import { ContactDetails } from "@/typings/contact_details";

type Props = {
  projectId: string;
  selectedChatbot: ChatbotConfig;
};

function ChatbotPreview({ projectId, selectedChatbot }: Props) {
  const dispatch = useDispatch();

  const chatbotToChatMapping = useSelector(
    (state: RootState) => state.chatbot.chatbotToChatMapping
  );

  const [chatOrigin, setChatOrigin] = useState<string>();

  const userEmail = useSelector(
    (state: RootState) => state.auth.user!.email ?? ""
  );
  const { data: contact = {} as ContactDetails } = useGetContactQuery(
    {
      project_id: projectId,
      origin: `${userEmail!} (internal)`,
    },
    { skip: !userEmail }
  );

  useEffect(() => {
    if (
      !selectedChatbot ||
      !Object.keys(selectedChatbot).length ||
      !contact?.origin
    )
      return;

    resolveAndSetChatForChatbotMapping(chatbotToChatMapping);
  }, [selectedChatbot, contact?.origin]);

  const resetActiveChat = async () => {
    const mappingWithExcludedChatbot = chatbotToChatMapping.filter(
      (mapping) => mapping.chatbotId !== selectedChatbot.id
    );
    dispatch(removeChatbotToChatMapping(selectedChatbot.id));
    resolveAndSetChatForChatbotMapping(mappingWithExcludedChatbot);
  };

  const resolveAndSetChatForChatbotMapping = async (
    updatedMapping: ChatbotToChatMapping[]
  ) => {
    const mapping = await resolveChatForChatbotMapping(
      selectedChatbot,
      updatedMapping
    );
    setChatOrigin(mapping.chatOrigin);
    dispatch(setChatbotToChatMapping(mapping));
  };

  return (
    <Sheet>
      <SheetTrigger asChild>
        <Button variant="secondary">Preview Bot</Button>
      </SheetTrigger>
      <SheetContent className="px-1 pt-2 w-2/5">
        <div className="flex justify-between items-center px-5 pt-1">
          <div className="text text-2xl font-bold">Chatbot Preview</div>
          <div className="flex gap-2">
            <IoMdRefresh onClick={resetActiveChat} size={"1.5rem"} />

            <SheetTrigger asChild>
              <span>
                <RxCross1 size={"1.5rem"} />
              </span>
            </SheetTrigger>
          </div>
        </div>
        <div className="flex flex-row font-sans w-full h-full pb-2">
          {Object.keys(contact).length && chatOrigin && (
            <ChatContainer
              projectId={projectId}
              selectedChatbot={selectedChatbot}
              chatOrigin={chatOrigin}
              contactOrigin={contact?.origin}
            />
          )}
        </div>
      </SheetContent>
    </Sheet>
  );
}

export { ChatbotPreview };
