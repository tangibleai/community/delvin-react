import emptyApi from "../apiSlice";
import { NewWorkspace, Workspace } from "@/typings/workspace";

export type UpdateWorkspaceBody = Pick<Workspace, "id" | "name">;

export const workspacesApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    createWorkspace: builder.mutation<Workspace, NewWorkspace>({
      query: (newWorkspace: NewWorkspace) => ({
        url: "workspace/create/",
        method: "POST",
        data: newWorkspace,
      }),
      invalidatesTags: ["Workspaces"],
    }),

    getWorkspace: builder.query<Workspace, string>({
      query: (workspaceId: string) => ({
        url: `workspace/${workspaceId}/details/`,
        method: "GET",
      }),
      providesTags: (result, error, arg) => [{ type: "Workspaces", id: arg }],
    }),

    listWorkspaces: builder.query<Workspace[], string>({
      query: (userId: string) => ({
        url: "workspaces/",
        method: "GET",
        params: { user_id: userId },
      }),
      providesTags: (result = [], error, arg) => [
        "Workspaces",
        ...result.map(({ id }) => ({ type: "Workspaces", id }) as const),
      ],
    }),
    updateWorkspace: builder.mutation<Workspace, UpdateWorkspaceBody>({
      query: (workspace) => ({
        url: `workspace/${workspace.id}/update/`,
        method: "PUT",
        data: workspace,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: "Workspaces", id: arg.id },
      ],
    }),
    deleteWorkspace: builder.mutation<null, string>({
      query: (workspaceId: string) => ({
        url: `workspace/${workspaceId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: ["Workspaces"],
    }),
  }),
});

export const {
  useCreateWorkspaceMutation,
  useGetWorkspaceQuery,
  useLazyGetWorkspaceQuery,
  useListWorkspacesQuery,
  useUpdateWorkspaceMutation,
  useDeleteWorkspaceMutation,
} = workspacesApiSlice;
