import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";

import DeleteProjectModal from "./DeleteProjectModal";

export default function DeleteProject() {
  return (
    <Card>
      <CardHeader>
        <CardTitle>Delete project</CardTitle>
        <CardDescription>
          Deleting this project will remove all chats, messages, and other data
          associated with this project.
        </CardDescription>
        <CardDescription>
          Make sure you have backed up any information you would like to keep.
        </CardDescription>
      </CardHeader>
      <CardContent className="grid gap-4"></CardContent>
      <CardFooter>
        <DeleteProjectModal />
      </CardFooter>
    </Card>
  );
}
