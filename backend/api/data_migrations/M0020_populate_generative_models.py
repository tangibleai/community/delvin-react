"Apply after api/migrations/0020_provider.py migration"

import logging

from api.models import GenerativeModel, ProviderChoices

logger = logging.getLogger(__name__)


def populate_generative_models():
    models_data = [
        {
            "provider_type": ProviderChoices.OPENAI.value,
            "name": "GPT-4o",
            "model_string": "gpt-4o",
            "priority": 2,
        },
        {
            "provider_type": ProviderChoices.OPENAI.value,
            "name": "GPT-4o mini",
            "model_string": "gpt-4o-mini",
            "priority": 1,
        },
        {
            "provider_type": ProviderChoices.ANTHROPIC.value,
            "name": "Claude 3.5 Sonnet",
            "model_string": "claude-3-5-sonnet-20241022",
            "priority": 2,
        },
        {
            "provider_type": ProviderChoices.ANTHROPIC.value,
            "name": "Claude 3 Haiku",
            "model_string": "claude-3-haiku-20240307",
            "priority": 1,
        },
        {
            "provider_type": ProviderChoices.OPENROUTER.value,
            "name": "GPT 4o",
            "model_string": "openrouter/openai/gpt-4o",
            "priority": 2,
        },
        {
            "provider_type": ProviderChoices.OPENROUTER.value,
            "name": "GPT 3.5 Turbo",
            "model_string": "openrouter/openai/gpt-3.5-turbo",
            "priority": 2,
        },
        {
            "provider_type": ProviderChoices.OPENROUTER.value,
            "name": "Claude 3.5 Sonnet",
            "model_string": "openrouter/anthropic/claude-3.5-sonnet",
            "priority": 2,
        },
        {
            "provider_type": ProviderChoices.OPENROUTER.value,
            "name": "Llama 3 70B Instruct",
            "model_string": "openrouter/meta-llama/llama-3-70b-instruct",
            "priority": 1,
        },
        {
            "provider_type": ProviderChoices.OPENROUTER.value,
            "name": "Gemini 1.5 Pro",
            "model_string": "openrouter/google/gemini-pro-1.5",
            "priority": 2,
        },
    ]

    for model_data in models_data:
        generative_model, created = GenerativeModel.objects.get_or_create(
            label=model_data["name"],
            model_string=model_data["model_string"],
            priority=model_data["priority"],
            provider_type=model_data["provider_type"],
        )
        if created:
            logger.info(f"Created GenerativeModel: {generative_model.label}")
        else:
            logger.info(f"Skipped existing GenerativeModel: {generative_model.label}")

    print("Generative models have been populated successfully.")


if __name__ == "__main__":
    populate_generative_models()
