import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Textarea } from "@/components/ui/textarea";
import { useToast } from "@/components/ui/use-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect } from "react";
import { useForm, useWatch } from "react-hook-form";
import { z } from "zod";
import { Switch } from "@/components/ui/switch";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { BuiltinCategory } from "@/typings/project";
import { useUpdateModerationCategoryMutation } from "@/slices/api/moderationApiSlice";
import { useParams } from "react-router-dom";

interface Props {
  dialogOpened: boolean; // state to open tag definition modal
  setDialogOpened: React.Dispatch<React.SetStateAction<boolean>>; // state to open guardrail action modal
  moderation: BuiltinCategory;
}

export default function EditModerationCategoryModal(props: Props) {
  const { toast } = useToast();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const formSchema = z.object({
    enabled: z.boolean(),
    chatbotResponse: z.string().min(1, "Please enter a chatbot response"),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
  });

  const enabled = useWatch({
    control: form.control,
    name: "enabled",
    defaultValue: props.moderation.enabled,
  });

  const [updateModerationCategory] = useUpdateModerationCategoryMutation();

  const onSubmit = async (data: z.infer<typeof formSchema>) => {
    // closing the modals
    props.setDialogOpened(false);

    const { enabled, chatbotResponse } = data;

    try {
      await updateModerationCategory({
        project_id: projectId,
        details: {
          code: props.moderation.code,
          name: props.moderation.name,
          enabled: enabled,
          response: chatbotResponse,
        },
      }).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Couldn't toggle moderation category. Try again later.",
      });
      return;
    }

    toast({
      title: "Success!",
      description: "Moderation category was toggled!",
    });
  };

  function resetForm() {
    form.setValue("enabled", props.moderation.enabled);
    form.setValue("chatbotResponse", props.moderation.response);
  }

  useEffect(() => {
    resetForm();
  }, [props.moderation.enabled, props.moderation.response]);

  return (
    <Dialog open={props.dialogOpened} onOpenChange={props.setDialogOpened}>
      <DialogContent className="flex flex-col gap-7">
        <DialogHeader>
          <DialogTitle className="text-xl">
            Edit Built-In Moderation Category
          </DialogTitle>
        </DialogHeader>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="flex flex-col gap-6">
              <div className="flex items-center gap-3">
                <Label className="text-base">Category</Label>
                <Input
                  disabled
                  value={props.moderation.name}
                  className="border-2 border-black"
                />
              </div>
              <FormField
                control={form.control}
                name="enabled"
                defaultValue={props.moderation.enabled}
                render={({ field }) => (
                  <FormItem className="flex items-center text-center gap-3">
                    <Label className="text-base">Enabled</Label>
                    <FormControl>
                      <Switch
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="chatbotResponse"
                defaultValue={props.moderation.response}
                render={({ field }) => (
                  <FormItem>
                    <div className="flex flex-col gap-2">
                      <FormLabel className="text-base">
                        Chatbot Response
                      </FormLabel>
                      <div className="flex flex-col">
                        <FormControl>
                          {enabled ? (
                            <Textarea className="resize-none" {...field} />
                          ) : (
                            <Textarea
                              disabled
                              className="resize-none"
                              {...field}
                            />
                          )}
                        </FormControl>
                        <FormMessage />
                      </div>
                    </div>
                  </FormItem>
                )}
              />
              <div className="flex flex-row justify-evenly">
                <Button type="submit" className="w-32 text-base">
                  Save
                </Button>
                <Button
                  type="button"
                  variant="outline"
                  className="w-32 text-base"
                  onClick={() => props.setDialogOpened(false)}
                >
                  Cancel
                </Button>
              </div>
            </div>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}
