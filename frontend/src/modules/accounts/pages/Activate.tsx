import { useState } from "react";
import { Navigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import { verify } from "@/actions/auth";

import logo from "@/assets/delvin_logo.png";
import { AppDispatch } from "@/store";
import { useDispatch } from "react-redux";
import { Button } from "@/components/ui/button";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";

const Activate = () => {
  const [verified, setVerified] = useState(false);

  const dispatch: AppDispatch = useDispatch();

  const routeParams = useParams();

  const verify_account = (e: any) => {
    const uid = routeParams.uid!;
    const token = routeParams.token!;

    dispatch(verify(uid, token));
    setVerified(true);
  };

  if (verified) {
    return <Navigate to={AUTH_PAGE_URLS.home} />;
  }

  return (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
      <img className="w-80 mb-2" src={logo} alt="logo" />
      <div className="w-full p-6 bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md dark:bg-gray-800 dark:border-gray-700 sm:p-8">
        <h2 className="mb-1 text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
          Verify Your Account
        </h2>
        <Button type="submit" onClick={(e) => verify_account(e)}>
          Verify
        </Button>
      </div>
    </div>
  );
};

export default Activate;
