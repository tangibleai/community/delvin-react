from api import models


def copy_tag_intents_to_user(user_project_name, user_owner_email):
    source_project_name = "Vlad's Test Project"
    source_owner_email = "vlad@tangibleai.com"
    source_owner_rec = models.UserAccount.objects.get(email=source_owner_email)
    source_project_rec = models.Project.objects.get(
        name=source_project_name, owner=source_owner_rec
    )
    source_tag_group_rec = models.MessageTagGroup.objects.filter(
        project=source_project_rec
    ).first()
    source_tag_definition_recs = models.MessageTagDefinition.objects.filter(
        message_tag_group=source_tag_group_rec, project=source_project_rec
    )

    user_project_rec = models.Project.objects.get(
        name=user_project_name, owner__email=user_owner_email
    )
    user_tag_group_rec = models.MessageTagGroup.objects.get_or_create(
        name=source_tag_group_rec.name, project=user_project_rec
    )[0]
    copied_tag_records = []
    for rec in source_tag_definition_recs:
        tag_rec = models.MessageTagDefinition.objects.get_or_create(
            name=rec.name,
            description=rec.description,
            project=user_project_rec,
            message_tag_group=user_tag_group_rec,
        )[0]
        copied_tag_records.append(tag_rec)

    return copied_tag_records
