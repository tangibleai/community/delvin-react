from django.apps import AppConfig
from django.db.models.signals import post_delete, post_save


class ApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "api"

    def ready(self):
        """After guardrail response is deleted calls the signal to delete the guardrail action"""
        from api.models import GuardrailResponse, Contact, Project, Chatbot, Chat
        from api.signals import (
            clear_contact_cache_on_update,
            clear_contact_cache_on_delete,
            clear_project_cache_on_update,
            clear_project_cache_on_delete,
            clear_chatbot_cache_on_update,
            clear_chatbot_cache_on_delete,
            clear_chat_cache_on_update,
            clear_chat_cache_on_delete,
            delete_associated_guardrail_action,
        )

        # Connect signals for deleting guardrail action
        post_delete.connect(
            delete_associated_guardrail_action, sender=GuardrailResponse
        )

        # Connect signals for Project
        post_save.connect(clear_project_cache_on_update, sender=Project)
        post_delete.connect(clear_project_cache_on_delete, sender=Project)

        # Connect signals for Chatbot
        post_save.connect(clear_chatbot_cache_on_update, sender=Chatbot)
        post_delete.connect(clear_chatbot_cache_on_delete, sender=Chatbot)

        # Connect signals for Contact
        post_save.connect(clear_contact_cache_on_update, sender=Contact)
        post_delete.connect(clear_contact_cache_on_delete, sender=Contact)

        # Connect signals for Chat
        post_save.connect(clear_chat_cache_on_update, sender=Chat)
        post_delete.connect(clear_chat_cache_on_delete, sender=Chat)
