from api.models import UserAccount, Workspace, Project, ProjectCollaborator, TeamMember


# forward migration function
def create_workspace_for_each_user():
    for user in UserAccount.objects.all():
        workspace = Workspace.objects.create(
            name=f"{user.name}'s Workspace", owner=user, properties={}
        )
        user.current_workspace = workspace
        user.save()

        user_projects = Project.objects.filter(owner=user)
        collaborators_emails = {}
        for proj in user_projects:
            proj.workspace = workspace
            proj.save()
            project_collaborators = ProjectCollaborator.objects.filter(project=proj)
            for c in project_collaborators:
                if (
                    c.delvin_user.email in collaborators_emails
                    and collaborators_emails.get(c.delvin_user.email) == "E"
                ):
                    continue
                TeamMember.objects.filter(
                    delvin_user=c.delvin_user,
                    workspace=workspace,
                ).delete()
                TeamMember.objects.create(
                    delvin_user=c.delvin_user,
                    workspace=workspace,
                    verification_status=c.verification_status,
                    role=c.role,
                    properties=c.properties,
                )
                collaborators_emails[c.delvin_user.email] = c.role


# reverse migration function
def remove_workspace_for_each_user():
    for user in UserAccount.objects.all():
        user.current_workspace = None
        user.save()

        user_workspaces = Workspace.objects.filter(owner=user)
        for workspace in user_workspaces:
            projects = Project.objects.filter(workspace=workspace)
            for p in projects:
                p.workspace = None
                p.save()
            workspace.delete()


if __name__ == "__main__":
    create_workspace_for_each_user()
