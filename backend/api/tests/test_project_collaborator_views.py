# import json
# import os
# from unittest.mock import patch

# from django.conf import settings
# from django.test import TestCase
# from django.test.testcases import SerializeMixin
# from rest_framework.test import APIClient

# from api.models import *
# from api.tests import utils as test_utils  # since "utils" is reserved name in python
# from api.tests import mock


# class TestProjectCollaboratorViews(TestCase, SerializeMixin):
#     """Test the API views.
#     To test in order using the SerializeMixin
#     """

#     lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
#     client = APIClient()
#     fixtures = ["default_account_data.json"]

#     def setUp(self):
#         # Set up user accounts for authentication
#         user = test_utils.filter_by_fields(
#             mock.mock["user1"], ["email", "name", "password"]
#         )
#         UserAccount.objects.create_user(**user)
#         user2 = test_utils.filter_by_fields(
#             mock.mock["user2"], ["email", "name", "password"]
#         )
#         UserAccount.objects.create_user(**user2)
#         response = self.client.post("/auth/jwt/create/", user)
#         self.user1_access_token = response.json()["access"]

#         # Create User1's Project
#         data = {
#             "name": mock.mock["user1"]["project_name"],
#             "chatbot": mock.mock["user1"]["chatbot"],
#         }
#         serialized_project = self.send_post_request(
#             "/api/project/create/",
#             self.user1_access_token,
#             data,
#         )
#         self.project1_id = serialized_project["id"]
#         self.project = Project.objects.get(id=self.project1_id)
#         self.project1_token_key = serialized_project["token_key"]
#         self.project1_owner = serialized_project["owner"]

#         # Add User2 to User1's Project
#         self.create_user_response = self.send_post_request(
#             "/api/project-collaborators/create/",
#             self.user1_access_token,
#             {
#                 "role": mock.mock["user2"]["role"],
#                 "delvin_user": mock.mock["user2"]["email"],
#                 "project": self.project1_id,
#                 "properties": mock.mock["user2"]["properties"],
#             },
#         )

#     def send_post_request(self, endpoint, auth_id, payload):
#         headers = {"Authorization": "JWT " + auth_id}
#         response = self.client.post(
#             endpoint,
#             json.dumps(payload),
#             headers=headers,
#             content_type="application/json",
#         ).json()
#         return response

#     def test_view_project_collaborator(self):
#         headers = {"Authorization": "JWT " + self.user1_access_token}
#         response = self.client.get(
#             f"/api/project-collaborators/?project_id={self.project1_id}",
#             headers=headers,
#             # content_type="application/json",
#         ).json()
#         assert len(response) == 1

#     def test_add_verified_project_collaborator(self):
#         """User1 adds User2 to the Project.  Use2 has a Delvin account."""
#         assert self.create_user_response["project"] == self.project1_id
#         assert self.create_user_response["delvin_user"] == "user2@example.com"
#         assert self.create_user_response["role"] == "Editor"
#         assert self.create_user_response["verification_status"] == "V"

#     def test_add_unverified_project_collaborator(self):
#         """User1 adds User3 to the Project.  User3 doesn't have a Delvin account."""
#         response = self.send_post_request(
#             "/api/project-collaborators/create/",
#             self.user1_access_token,
#             {
#                 "role": mock.mock["user3"]["role"],
#                 "delvin_user": mock.mock["user3"]["email"],
#                 "project": self.project1_id,
#                 "properties": mock.mock["user3"]["properties"],
#             },
#         )
#         assert response["project"] == self.project1_id
#         assert response["properties"]["email"] == "user3@example.com"
#         assert response["role"] == "Viewer"
#         assert response["verification_status"] == "U"

#     def test_edit_project_collaborator(self):
#         """User1 adds User3 to the Project.  User3 doesn't have a Delvin account."""
#         response = self.send_post_request(
#             f"/api/project-collaborators/{self.create_user_response['id']}/update/",
#             self.user1_access_token,
#             {
#                 "delvin_user": mock.mock["user2"]["email"],
#                 "role": "Viewer",
#                 "project": self.project1_id,
#             },
#         )
#         assert response["project"] == self.project1_id
#         assert response["delvin_user"] == "user2@example.com"
#         assert response["role"] == "Viewer"

#     def test_delete_project_collaborator(self):
#         headers = {"Authorization": "JWT " + self.user1_access_token}
#         response = self.client.delete(
#             f"/api/project-collaborators/{self.create_user_response['id']}/delete/",
#             headers=headers,
#         )
#         assert response.status_code == 204

#     def tearDown(self):
#         ProjectCollaborator.objects.all().delete()
#         UserAccount.objects.all().delete()
