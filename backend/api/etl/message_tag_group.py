from api import models
from api.etl import utils


def process(df_or_records_list, *args, **kwargs):
    tag_groups = df_or_records_list
    if not isinstance(df_or_records_list, list):
        tag_groups = df_or_records_list.to_dict("records")
    required_fields = {"name", "project__name", "project__owner__email"}
    utils.check_missing_fields(tag_groups, required_fields)

    tag_groups = resolve_dependent_records(tag_groups)
    check_duplicated_tag_groups(tag_groups)
    db_records = save_tag_groups_to_db(tag_groups)

    return db_records


def resolve_dependent_records(tag_groups):
    tag_groups = utils.resolve_user_for_each_record(
        tag_groups,
        user_email_field_name="project__owner__email",
    )
    tag_groups = utils.resolve_project_for_each_record(tag_groups)

    return tag_groups


def check_duplicated_tag_groups(tag_groups):
    for t in tag_groups:
        name = t["name"]
        project_rec = t["project_rec"]

        duplicated_tag_groups = models.MessageTagGroup.objects.filter(
            name=name, project=project_rec
        )
        if duplicated_tag_groups:
            project_name = t["project__name"]
            raise ValueError(
                f'MessageTagGroup with "{name}" name and "{project_name}" project already exists'
            )

    return []


def save_tag_groups_to_db(tag_groups):
    initialized_records = [
        models.MessageTagGroup(
            name=t["name"],
            project=t["project_rec"],
        )
        for t in tag_groups
    ]
    saved_records = models.MessageTagGroup.objects.bulk_create(initialized_records)

    return saved_records
