import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import ErrorScreen from "@/components/ErrorScreen";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";

import { ChatbotConfig } from "@/typings/chatbot_config";
import { Document } from "@/typings/document";
import {
  useCreateDocumentMutation,
  useUpdateDocumentMutation,
} from "@/slices/api/documentSlice";

type Props = {
  document: Document;
  selectedChatbot: ChatbotConfig;
  onSubmit: (newDocument: Document) => void;
  onCancel: any;
  submit_action: string;
};

function DocumentFormGoogle({
  document,
  selectedChatbot,
  onSubmit,
  onCancel,
  submit_action,
}: Props) {
  const [error, setError] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const defaultURL = "content" in document ? document.content : "";

  const [createDocument] = useCreateDocumentMutation();

  const [updateDocument] = useUpdateDocumentMutation();

  // Form Text
  const modal_text =
    submit_action == "add" ? "Add document" : "Update document";

  const routeParams = useParams();
  const projectId = routeParams.projectId;

  const formSchema = z.object({
    url: z.string().url({
      message: "Please enter a URL",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      url: defaultURL,
    },
  });

  useEffect(() => {
    // Handles resetting the form after submit or x
    // Can't directly access x in Dialog
    if (!isModalOpen) {
      form.reset();
    }
  }, [isModalOpen]);

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    const { url } = values;
    let result: any = {};

    const documentContent = { title: "", content: url };

    if (submit_action == "add") {
      let body = {
        project: projectId,
        chatbot: selectedChatbot.id,
        title: documentContent.title,
        content: documentContent.content,
        document_type: "google",
      };

      try {
        result = await createDocument(body).unwrap();
      } catch (error) {
        return <ErrorScreen />;
      }
    } else {
      let body = {
        project: projectId,
        chatbot: selectedChatbot.id,
        title: documentContent.title,
        content: documentContent.content,
        document_type: "google",
        document: document.id,
      };
      try {
        result = await updateDocument(body).unwrap();
      } catch (error) {
        return <ErrorScreen />;
      }
    }

    if (result.error) {
      setError(result.error);
      return;
    }
    onSubmit(result);
    setError(null);
    setIsModalOpen(false);
  }

  return (
    <>
      {error && (
        <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
          Error: {error}
        </p>
      )}
      <Form {...form}>
        <form onSubmit={form.handleSubmit(handleSubmit)} className="space-y-8">
          <FormField
            control={form.control}
            name="url"
            defaultValue=""
            render={({ field }) => (
              <FormItem>
                <FormLabel>Public Google Doc URL</FormLabel>
                <FormControl>
                  <Input placeholder="The public share link" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <Button type="submit" className="order-2 sm:order-1">
            {modal_text}
          </Button>
          <Button
            type="button"
            variant="secondary"
            className="order-1 sm:order-2"
            onClick={onCancel}
          >
            Cancel
          </Button>
        </form>
      </Form>
    </>
  );
}

export default DocumentFormGoogle;
