import error from "@/assets/browser.png"

const ErrorScreen = () => {
  return (
    <div className="flex flex-col justify-center items-center min-h-screen bg-white">
      <img src={error} alt="error" className="w-32" />
      <div className="mt-5 text-xl text-gray-600">Oops! Something went wrong.</div>
      <div className="mt-5 text-base text-gray-600">Please reload the page.</div>
    </div>
  );
};

export default ErrorScreen;
