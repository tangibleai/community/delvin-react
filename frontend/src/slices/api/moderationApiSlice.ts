import emptyApi from "../apiSlice";
import { BuiltinCategory, Project } from "@/typings/project";

type ToggleBuiltinModerationRequestBody = {
  project_id: string;
  enabled: boolean;
};

type UpdateModerationCategoryRequestBody = {
  project_id: string;
  details: BuiltinCategory;
};

const apiWithModerationSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    toggleBuiltinModeration: builder.mutation<
      Project,
      ToggleBuiltinModerationRequestBody
    >({
      query: (requestBody) => ({
        url: `project/${requestBody.project_id}/toggle-builtin-moderation/`,
        method: "POST",
        data: requestBody,
      }),
      invalidatesTags: ["Moderation"],
    }),
    updateModerationCategory: builder.mutation<
      Project,
      UpdateModerationCategoryRequestBody
    >({
      query: (requestBody) => ({
        url: `project/${requestBody.project_id}/update-builtin-moderation-category/`,
        method: "POST",
        data: requestBody.details,
      }),
      invalidatesTags: ["Moderation"],
    }),
  }),
});

export const {
  useToggleBuiltinModerationMutation,
  useUpdateModerationCategoryMutation,
} = apiWithModerationSlice;
