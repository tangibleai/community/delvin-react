## Maria 
* [x] User can create a new project from the Project List screen
* [ ] User can browse to "Project Settings" tab in the menu and see the project's API token (not in database yet )

## Cetin
* [X] Create a  K8s cluster in TangibleAI DigitalOcean
* [X] Make the cluster run 
* [X] Connect to Dockerhub to store the images (try the DigitalOcean again)
  * Dockerhub is better.
* [X] Adjust the gitlab pipeline to accomodate the frontend
