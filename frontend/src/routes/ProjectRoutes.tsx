import NewProjectPage from "@/layouts/NewProjectPage";
import ChatbotConfig from "@/modules/chatbot_config/pages/ChatbotConfig";
import ChatPage from "@/modules/conversations/pages/ChatPage";
import Conversations from "@/modules/conversations/pages/Conversations";
import GuardrailResponses from "@/modules/guardrails/pages/GuardrailResponses";
import ProjectHome from "@/modules/project_home/pages/ProjectHome";
import ProjectSettings from "@/modules/project_settings/pages/ProjectSettings";
import Tags from "@/modules/tags/pages/Tags";
import Understanding from "@/modules/understanding/pages/Understanding";
import withAuth from "@/lib/with-auth";
import ChatWithChatbot from "@/modules/chat_with_chatbot/pages/ChatWithChatbot";

import { PROJECT_PAGE_URLS } from "./pageUrls";
import { FlowEditor } from "@/modules/flows/pages/FlowEditor";
import Onboarding from "@/modules/onboarding/pages/Onboarding";
import proceedToProjectOrShowErrorPage from "@/layouts/middlewares/checkAccess";

// The order is important since we want first to check auth access and then if user has access to project
const ProjectPageWithCheckedAccess =
  proceedToProjectOrShowErrorPage(NewProjectPage);
const NewProjectPageWithAuth = withAuth(ProjectPageWithCheckedAccess);

const InitAssistantPageWithAuth = withAuth(Onboarding);

export const ROUTES = [
  {
    path: PROJECT_PAGE_URLS.project.path,
    element: <NewProjectPageWithAuth />,
    children: [
      {
        path: PROJECT_PAGE_URLS.project.children.home,
        element: <ProjectHome />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.conversations,
        element: <Conversations />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.chat_with_assistant,
        element: <ChatWithChatbot />,
      },
      { path: PROJECT_PAGE_URLS.project.children.tags, element: <Tags /> },
      {
        path: PROJECT_PAGE_URLS.project.children.chat,
        element: <ChatPage />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.settings,
        element: <ProjectSettings />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.chatbot_config,
        element: <ChatbotConfig />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.understanding,
        element: <Understanding />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.guardrail_responses,
        element: <GuardrailResponses />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.flow_playground,
        element: <FlowEditor />,
      },
    ],
  },
  {
    path: PROJECT_PAGE_URLS.init_assistant,
    element: <InitAssistantPageWithAuth />,
  },
];
