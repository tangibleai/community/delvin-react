import CONSTANTS from "@/constants";
import { RootState } from "@/store";
import { PlanStatus, PlanType } from "@/typings/plan";
import { useSelector } from "react-redux";
import OutOfBannerEditor from "./OutOfBannerEditor";
import OutOfBannerOwner from "./OutOfBannerOwner";
import RunningOutOfBannerEditor from "./runningOutOfBannerEditor";
import RunningOutOfBannerOwner from "./runningOutOfBannerOwner";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { useParams } from "react-router-dom";

export default function OutOfBanners() {
  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;
  const user = useSelector((state: RootState) => state.auth.user);
  const { data: workspace } = useGetWorkspaceQuery(workspaceId);
  const isWorkspaceOwner = workspace?.owner === user?.email;
  const planType = workspace?.plan?.type;
  const planStatus = workspace?.plan?.status;
  const planProperties = workspace?.plan?.properties;
  const freeMessageCount = planProperties?.free_message
    ? parseInt(planProperties?.free_message)
    : null;

  if (planStatus === PlanStatus.EXPIRED) {
    if (isWorkspaceOwner) {
      return <OutOfBannerOwner />;
    } else {
      return <OutOfBannerEditor />;
    }
  } else {
    if (
      planType === PlanType.FREE &&
      freeMessageCount &&
      freeMessageCount >= CONSTANTS.MAX_FREE_MESSAGES * CONSTANTS.WARNING_RATIO
    ) {
      if (isWorkspaceOwner) {
        return <RunningOutOfBannerOwner />;
      } else {
        return <RunningOutOfBannerEditor />;
      }
    }
  }
  return null;
}
