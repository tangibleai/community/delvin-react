from rest_framework import generics
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import (
    MessageTagRule,
)


class MessageTagRuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageTagRule
        fields = "__all__"


class MessageTagRuleCreateAPIView(generics.CreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = MessageTagRule.objects.all()
    serializer_class = MessageTagRuleSerializer


message_tag_rule_create_view = MessageTagRuleCreateAPIView.as_view()
