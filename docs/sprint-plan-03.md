## Maria 

* [ ] Create a single frontend test with Playwright   
* [ ] Create a simple Select Contact page in Project section 
* [ ] Clicking on a contact shows the contact conversation 



## Cetin

* [X] Finish in-container testing
* [X] Experiment with Project Token-based authentication for a public API endpoint
