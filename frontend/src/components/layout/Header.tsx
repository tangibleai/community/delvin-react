import { Link, useNavigate } from "react-router-dom";
import { Button } from "@/components/ui/button";
import icon from "@/assets/delvin_icon.png";
import { useDispatch } from "react-redux";
import { logout } from "@/actions/auth";
import { AppDispatch } from "@/store";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";

type Props = {
  header?: string;
};

export default function Header({ header }: Props) {
  const navigate = useNavigate();
  const dispatch: AppDispatch = useDispatch();

  const user_logout = () => {
    dispatch(logout());
    navigate(AUTH_PAGE_URLS.login);
  };

  return (
    <div className="supports-backdrop-blur:bg-background/60 fixed left-0 right-0 top-0 z-20 border-b bg-background/95 backdrop-blur">
      <nav className="flex h-16 items-center justify-between px-4">
        <Link
          to={"/"}
          className="hidden items-center justify-between gap-2 md:flex"
        >
          <img src={icon} alt="Logo" className="w-8 mr-2" />
          <h1 className="text-lg font-semibold">{header}</h1>
        </Link>

        <div className="flex items-center gap-2">
          <Button onClick={() => user_logout()}>Logout</Button>
        </div>
      </nav>
    </div>
  );
}
