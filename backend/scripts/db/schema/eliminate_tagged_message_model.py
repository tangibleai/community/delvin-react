"""Contains a bunch of functions needed to eliminate `models.TaggedMessage` record retaining user data in db. See docs/db/schema/eliminate-tagged-message-model.md for detailed steps"""

from api import models


def move_from_tagged_message_to_message_tag_model():
    """Copies existing data from `models.TaggedMessage` model to `models.MessageTag` to retain user data in db before removing first model"""

    message_tag_recs = []

    for tagged_msg in models.TaggedMessage.objects.all():
        msg = tagged_msg.message
        tags = tagged_msg.tag_definitions.all()
        for tag in tags:
            message_tag_rec = models.MessageTag.objects.create(message=msg, tag=tag)
            message_tag_recs.append(message_tag_rec)

    return message_tag_recs
