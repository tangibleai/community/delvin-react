"use client";

import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown";
import { RiArrowRightUpLine } from "react-icons/ri";
import * as React from "react";
import { Link, useNavigate } from "react-router-dom";
import { AppDispatch } from "@/store";
import { useDispatch } from "react-redux";
import { logout } from "@/actions/auth";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";

export type DropdownUserProfileProps = {
  children: React.ReactNode;
  align?: "center" | "start" | "end";
  userEmail: string;
};

export function DropdownUserProfile({
  children,
  align = "start",
  userEmail,
}: DropdownUserProfileProps) {
  const [mounted, setMounted] = React.useState(false);
  React.useEffect(() => {
    setMounted(true);
  }, []);

  const navigate = useNavigate();
  const dispatch: AppDispatch = useDispatch();

  const user_logout = () => {
    dispatch(logout());
    navigate(AUTH_PAGE_URLS.login);
  };

  if (!mounted) {
    return null;
  }
  return (
    <>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>{children}</DropdownMenuTrigger>
        <DropdownMenuContent align={align}>
          <DropdownMenuLabel>{userEmail}</DropdownMenuLabel>
          <DropdownMenuSeparator />
          <DropdownMenuGroup>
            <DropdownMenuItem>
              <Link to={"/account_settings"}>Account settings</Link>
            </DropdownMenuItem>
            <DropdownMenuItem>
              <Link
                to={"https://docs.delvin.to/"}
                target="_blank"
                className="flex items-center"
              >
                Documentation
                <RiArrowRightUpLine
                  className="mb-1 ml-1 size-2.5 shrink-0 text-gray-500"
                  aria-hidden="true"
                />
              </Link>
            </DropdownMenuItem>
          </DropdownMenuGroup>
          <DropdownMenuSeparator />
          <DropdownMenuGroup>
            <DropdownMenuItem onClick={() => user_logout()}>
              Sign out
            </DropdownMenuItem>
          </DropdownMenuGroup>
        </DropdownMenuContent>
      </DropdownMenu>
    </>
  );
}
