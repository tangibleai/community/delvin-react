from api import models
from core.constant import CONSTANTS


def process(df):
    message_tag_recs = []

    user = models.UserAccount.objects.get(email=CONSTANTS.DEFAULT_USER_EMAIL)
    project = models.Project.objects.get(
        workspace__owner=user, name=CONSTANTS.DEFAULT_USER_DEFAULT_PROJECT_NAME
    )
    contact = models.Contact.objects.get(
        origin="Engineering Default Contact", project=project, internal=True
    )
    dataset = models.Dataset.objects.get(
        project=project, name=CONSTANTS.DEFAULT_USER_DATASET_NAME
    )
    ml_model = models.MLModel.objects.get_or_create(project=project, dataset=dataset)[0]

    for index, row in df.iterrows():
        utterance = row["Utterance"]
        label = row["Label"]

        try:
            tag = models.MessageTagDefinition.objects.get(project=project, name=label)
        except models.MessageTagDefinition.DoesNotExist:
            continue

        message = models.Message.objects.get_or_create(
            project=project, text=utterance, contact=contact
        )[0]

        message_tag_rec = models.MessageTag.objects.create(
            message=message, tag=tag, ml_model=ml_model
        )
        message_tag_recs.append(message_tag_rec)

    return message_tag_recs
