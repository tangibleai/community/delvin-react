import { logout } from "@/actions/auth";
import icon from "@/assets/delvin_icon.png";
import { Link, useNavigate, useParams } from "react-router-dom";
import { AppDispatch, RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown";
import { Avatar, AvatarFallback } from "@/components/ui/avatar";
import { WorkspacesDropdown } from "./WorkspacesDropdown";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";

type Props = {
  withLogo: boolean;
};

function Navbar({ withLogo }: Props) {
  const navigate = useNavigate();
  const dispatch: AppDispatch = useDispatch();
  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const user = useSelector((state: RootState) => state.auth.user);

  const { data: activeWorkspace } = useGetWorkspaceQuery(workspaceId);

  const user_logout = () => {
    dispatch(logout());
    navigate(AUTH_PAGE_URLS.login);
  };

  return (
    <nav className=" top-0 left-0 w-full flex items-center justify-between p-4 bg-white shadow-md">
      <div className="flex items-center gap-5">
        {withLogo && <img src={icon} alt="Logo" className="w-8 mr-2" />}{" "}
        {activeWorkspace && <WorkspacesDropdown />}
      </div>
      <span className="flex items-center ">
        <DropdownMenu>
          <DropdownMenuTrigger className="flex items-center gap-2 text-xl font-bold">
            <Avatar>
              <AvatarFallback>{user?.name?.charAt(0) ?? ""}</AvatarFallback>
            </Avatar>
          </DropdownMenuTrigger>
          <DropdownMenuContent>
            <DropdownMenuItem className="sm:text-base font-extralight">
              {user?.email}
            </DropdownMenuItem>
            <DropdownMenuSeparator />
            <DropdownMenuItem className="sm:text-base">
              <Link to={"/account_settings"}>Account settings</Link>
            </DropdownMenuItem>
            <DropdownMenuItem className="sm:text-base">
              Documentation
            </DropdownMenuItem>
            <DropdownMenuItem
              className="sm:text-base"
              onClick={() => user_logout()}
            >
              Sign out
            </DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
      </span>
    </nav>
  );
}

export default Navbar;
