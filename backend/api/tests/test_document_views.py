import json
import os
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from django.test.testcases import SerializeMixin
from rest_framework.test import APIClient

from api.models import *
from api.tests import utils as test_utils  # since "utils" is reserved name in python
from api.tests import mock


class TestDocumentViews(TestCase, SerializeMixin):
    """Test the API views.
    To test in order using the SerializeMixin
    """

    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    def setUp(self):
        # Set up user account for authentication
        user = test_utils.filter_by_fields(
            mock.mock["user1"], ["email", "name", "password"]
        )
        user_rec = UserAccount.objects.create_user(**user)
        response = self.client.post("/auth/jwt/create/", user)
        self.user1_access_token = response.json()["access"]
        self.headers = {"Authorization": "JWT " + self.user1_access_token}

        # Create workspace
        headers = {"Authorization": "JWT " + self.user1_access_token}
        req_body = {
            "name": mock.mock["workspace1"]["name"],
            "properties": mock.mock["workspace1"]["properties"],
            "owner": str(user_rec.id),
        }
        json_data = json.dumps(req_body)
        serialized_workspace = self.client.post(
            "/api/workspace/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()

        workspace_id = serialized_workspace["id"]

        # Create User1's Project
        data = {
            "name": mock.mock["user1"]["project_name"],
            "workspace_id": workspace_id,
        }
        serialized_project = self.send_put_or_post_request(
            "/api/project/create/",
            data,
        )
        self.project1_id = serialized_project["id"]
        self.project = Project.objects.get(id=self.project1_id)
        self.project1_token_key = serialized_project["token_key"]

        self.chatbot = Chatbot.objects.create(project=self.project)

        # Create an initial document within the Project
        response = self.send_put_or_post_request(
            "/api/documents/create/",
            {
                "project": self.project1_id,
                "chatbot": self.chatbot.id,
                "title": mock.mock["document1"]["title"],
                "content": mock.mock["document1"]["content"],
                "document_type": mock.mock["document1"]["document_type"],
            },
        )
        self.create_document_response = response

    def send_put_or_post_request(self, endpoint, payload, method="post"):
        request_data = {
            "path": endpoint,
            "data": json.dumps(payload),
            "headers": self.headers,
            "content_type": "application/json",
        }

        if method == "put":
            response = self.client.put(**request_data).json()
        else:
            response = self.client.post(**request_data).json()
        return response

    def test_view_document(self):
        response = self.client.get(
            f"/api/documents/?chatbot_id={self.chatbot.id}",
            headers=self.headers,
            content_type="application/json",
        ).json()
        assert len(response) == 1

    def test_add_document(self):
        assert self.create_document_response["id"] is not None
        assert self.create_document_response["title"] == mock.mock["document1"]["title"]
        assert (
            self.create_document_response["content"]
            == mock.mock["document1"]["content"]
        )
        assert self.create_document_response["status"] == "C"

    def test_edit_document(self):
        assert self.create_document_response["title"] == mock.mock["document1"]["title"]
        assert (
            self.create_document_response["content"]
            == mock.mock["document1"]["content"]
        )

        modified_title = "Modified First Document"
        modified_content = "Modified Content of First Document"
        response = self.send_put_or_post_request(
            f"/api/documents/{self.create_document_response['id']}/update/",
            {
                "id": self.create_document_response["id"],
                "title": modified_title,
                "content": modified_content,
                "project": self.project1_id,
                "document_type": mock.mock["document1"]["document_type"],
                "document": self.create_document_response["id"],
            },
            "put",
        )
        assert response["title"] == modified_title
        assert response["content"] == modified_content

    def test_delete_project_collaborator(self):
        response = self.client.delete(
            f"/api/documents/{self.create_document_response['id']}/delete/",
            headers=self.headers,
        )
        assert response.status_code == 204

    def tearDown(self):
        Document.objects.all().delete()
        UserAccount.objects.all().delete()
