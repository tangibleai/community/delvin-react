import { useParams } from "react-router-dom";
import { useToast } from "@/components/ui/use-toast";

import { FlowsTable } from "../components/FlowsTable";
import { NewFlowDialog } from "../components/NewFlowDialog";
import { Flow } from "@/typings/flow";
import {
  useDeleteFlowMutation,
  useGetFlowsQuery,
} from "@/slices/api/flowApiSlice";

const Flows = () => {
  const { toast } = useToast();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const { data: flows = [] as Flow[] } = useGetFlowsQuery(projectId);
  const [deleteFlow] = useDeleteFlowMutation();

  const handleDeleteFlow = async (flowId: string) => {
    try {
      await deleteFlow(flowId).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Could not remove a flow. Please, try again.",
      });
      return;
    }

    toast({
      title: "Success!",
      description: "Flow was successfully removed.",
    });
  };

  return (
    <div className="flex flex-col h-screen p-5">
      <div className="flex items-center justify-between pr-7">
        <p className="text-3xl font-bold">Flows</p>
        <NewFlowDialog />
      </div>
      <p className="pt-3">
        Flows are guided conversations that follow a general script for a safe,
        focused, multi-turn chats.
      </p>
      <div className="w-full mx-auto py-10">
        <FlowsTable flows={flows} handleDeleteFlow={handleDeleteFlow} />
      </div>
    </div>
  );
};

export default Flows;
