import { useSelector } from "react-redux";

import {
  Card,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableHeaderCell,
  TableBody,
  Title,
} from "@tremor/react";
import { RootState } from "@/store";

import NewMemberModal from "./NewMemberModal";
import UpdateMemberRoleModal from "./UpdateMemberRoleModal";
import UserControlOptionsForOwner from "./UserControlOptionsForOwner";
import { useToast } from "@/components/ui/use-toast";
import { useParams } from "react-router-dom";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { useRetrieveTeamListQuery } from "@/slices/api/teamMemberApiSlice";
import { Workspace } from "@/typings/workspace";
import { Skeleton } from "@/components/ui/skeleton";

type Props = {
  setActiveTab: React.Dispatch<React.SetStateAction<string>>;
};

function TeamSettings({ setActiveTab }: Props) {
  const { toast } = useToast();
  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const { data: activeWorkspace = {} as Workspace } =
    useGetWorkspaceQuery(workspaceId);

  const userEmail =
    useSelector((state: RootState) => state.auth.user?.email) ?? "";

  const { data: teamMembers = [], isFetching: retrievingTeamMembers } =
    useRetrieveTeamListQuery(activeWorkspace, {
      skip: !Object.keys(activeWorkspace).length,
    });

  const handleAddNewMember = (newMember: any) => {
    let successDetail = "";
    if (newMember.verification_status == "U") {
      successDetail = `Sent ${newMember.properties.email} an invitation to Delvin`;
    } else {
      successDetail = `Successfully added ${newMember.email}!`;
    }
    toast({
      title: "Success!",
      description: successDetail,
    });
  };

  const handleDeleteMember = (deletedMember: any) => {
    let successDetail = "";
    if (deletedMember.delvin_user) {
      successDetail = `Successfully deleted ${deletedMember.delvin_user}!`;
    } else {
      successDetail = `Successfully deleted ${deletedMember.email}!`;
    }
    toast({
      title: "Success!",
      description: successDetail,
    });
  };

  const isOwner = userEmail === activeWorkspace?.owner;

  return (
    <div className="flex flex-col max-w-md mx-auto mt-20">
      <div className="flex flex-row justify-between my-5 ">
        <Title> Team members </Title>
        {isOwner ? <NewMemberModal onAddNewMember={handleAddNewMember} /> : ""}
      </div>
      <Card>
        <Table>
          <TableHead>
            <TableRow>
              <TableHeaderCell>Email</TableHeaderCell>
              <TableHeaderCell>Permissions</TableHeaderCell>
              {isOwner ? <TableHeaderCell></TableHeaderCell> : ""}
            </TableRow>
          </TableHead>
          <TableBody>
            {retrievingTeamMembers
              ? Array.from({ length: 3 }).map((_, index) => (
                  <TableRow key={index}>
                    <TableCell className="w-1/2">
                      <Skeleton className="h-10" />
                    </TableCell>
                    <TableCell className="w-1/2">
                      <Skeleton className="h-10" />
                    </TableCell>
                  </TableRow>
                ))
              : teamMembers.map((member: any) => (
                  <TableRow key={member.id}>
                    <TableCell>
                      {member.delvin_user
                        ? member.delvin_user
                        : member.email
                        ? member.email
                        : member.properties.email + " (pending)"}
                    </TableCell>
                    <TableCell>
                      {member.role == "Owner" ? (
                        <span className="inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm h-10 px-4 py-2">
                          {member.role}
                        </span>
                      ) : (
                        <>
                          {isOwner ? (
                            <UpdateMemberRoleModal userToUpdate={member} />
                          ) : (
                            <span className="inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm h-10 px-4 py-2">
                              {member.role}
                            </span>
                          )}
                        </>
                      )}
                    </TableCell>
                    {isOwner ? (
                      <TableCell>
                        {member.role != "Owner" ? (
                          <UserControlOptionsForOwner
                            targetUser={member}
                            onDeleteMember={handleDeleteMember}
                            setActiveTab={setActiveTab}
                          />
                        ) : (
                          ""
                        )}
                      </TableCell>
                    ) : (
                      ""
                    )}
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </Card>
    </div>
  );
}

export default TeamSettings;
