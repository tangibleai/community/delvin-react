import { useForm } from "react-hook-form";
import { useState } from "react";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
} from "@/components/ui/form";
import { QuestionMarkCircleIcon } from "@heroicons/react/24/outline";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { zodResolver } from "@hookform/resolvers/zod";

import { Member } from "@/typings/member";
import { useToast } from "@/components/ui/use-toast";
import { IoReload } from "react-icons/io5";
import { useUpdateTeamMemberRoleMutation } from "@/slices/api/teamMemberApiSlice";

type Props = {
  userToUpdate: Member;
};

function UpdateMemberRoleModal({ userToUpdate }: Props) {
  const { toast } = useToast();

  const [error, setError] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [showTooltip, setShowTooltip] = useState(false);
  const [processing, setProcessing] = useState(false); // when user clicks "Add team member" button set this to true

  const [updateTeamMemberRole, { isError }] = useUpdateTeamMemberRoleMutation();

  const formSchema = z.object({
    role: z.string().min(4, {
      message: "Please choose a role",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      role: "",
    },
  });

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    setProcessing(true);
    const { role } = values;

    try {
      await updateTeamMemberRole({
        id: userToUpdate.id,
        role: role,
      }).unwrap();
    } catch (error) {
      setProcessing(false);
      return setError(String(error));
    }

    toast({
      title: "Success!",
      description: "Team member role was successfully updated.",
    });
    setProcessing(false);
    setIsModalOpen(false);
  }

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button variant="ghost">{userToUpdate.role}</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>
            Change the role for {userToUpdate.delvin_user}
          </DialogTitle>
        </DialogHeader>
        {error && (
          <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
            Error: {error}
          </p>
        )}
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(handleSubmit)}
            className="space-y-8"
          >
            <FormField
              control={form.control}
              name="role"
              render={({ field }) => (
                <FormItem>
                  <div className="relative pt-3 flex items-center">
                    <FormLabel>Role</FormLabel>
                    <QuestionMarkCircleIcon
                      className="h-4 w-4 cursor-pointer"
                      onMouseEnter={() => setShowTooltip(true)}
                      onMouseLeave={() => setShowTooltip(false)}
                    />
                  </div>
                  {showTooltip && (
                    <div
                      data-tooltip="tooltip"
                      className="bg-gray-800 text-white p-2 rounded-md absolute bottom-28 left-1/2 transform -translate-x-1/2"
                    >
                      The role determines the permissions a team member has for
                      this workspace.
                    </div>
                  )}
                  <Select
                    onValueChange={field.onChange}
                    defaultValue={field.value}
                  >
                    <FormControl>
                      <SelectTrigger className="w-[180px]">
                        <SelectValue placeholder="Select a role" />
                      </SelectTrigger>
                    </FormControl>
                    <SelectContent>
                      <SelectGroup>
                        <SelectItem value="Editor">Editor</SelectItem>
                        <SelectItem value="Viewer">Viewer</SelectItem>
                      </SelectGroup>
                    </SelectContent>
                  </Select>
                </FormItem>
              )}
            />
            <DialogFooter>
              <Button disabled={processing} type="submit">
                {processing && (
                  <IoReload className="mr-2 h-4 w-4 animate-spin" />
                )}
                Update role
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}

export default UpdateMemberRoleModal;
