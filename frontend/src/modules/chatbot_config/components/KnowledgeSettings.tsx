import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Textarea } from "@/components/ui/textarea";
import { useToast } from "@/components/ui/use-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { ChatbotConfig } from "@/typings/chatbot_config";

type Props = {
  selectedChatbot: ChatbotConfig;
  updateChatbot: (updatedChatbot: ChatbotConfig) => void;
};

function KnowledgeSettings({ selectedChatbot, updateChatbot }: Props) {
  if (!selectedChatbot || !Object.keys(selectedChatbot).length) return <></>;

  const { toast } = useToast();

  const default_threshold = selectedChatbot.config.knowledge.activation
    .threshold
    ? selectedChatbot.config.knowledge.activation.threshold
    : 0.5;
  const default_type = selectedChatbot.config.knowledge.activation.type
    ? selectedChatbot.config.knowledge.activation.type
    : "never";

  const [selectedThreshold, setSelectedThreshold] = useState(default_threshold);

  const [selectedUploadOption, setSelectedUploadOption] =
    useState(default_type);

  const formSchema = z.object({
    frequency: z.string().min(1, {
      message: "Please select an option",
    }),
    fallback: z.string().min(1, {
      message: "Add enter instructions for the bot",
    }),
    threshold: z
      .number()
      .optional()
      .refine((value) => value || 1, {
        message: "Please write a confidence value",
        path: ["threshold"],
      }),
  });
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      frequency: selectedUploadOption,
      fallback: selectedChatbot.config.knowledge.fallback || "",
      threshold: selectedThreshold,
    },
  });

  const { register } = form;

  useEffect(() => {
    form.reset({
      frequency: selectedChatbot.config.knowledge.activation.type || "never",
      fallback: selectedChatbot.config.knowledge.fallback || "",
      threshold: selectedChatbot.config.knowledge.activation.threshold || 0.5,
    });
  }, [selectedChatbot]);

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    const { fallback, threshold } = values;

    const prompt_settings = {
      chatbotPersona: selectedChatbot.config.general_prompt.persona,
      constraints: selectedChatbot.config.general_prompt.constraints,
      instructions: selectedChatbot.config.general_prompt.instructions,
      fallbackResponses: fallback,
    };

    const knowledge_settings = {
      enabled: selectedChatbot.config.knowledge.enabled,
      type: selectedUploadOption,
      threshold: threshold,
    };

    const updatedChatbot = {
      ...selectedChatbot,
      config: {
        ...selectedChatbot.config,
        general_prompt: {
          constraints: prompt_settings.constraints,
          instructions: prompt_settings.instructions,
          persona: prompt_settings.chatbotPersona,
        },
        knowledge: {
          enabled: knowledge_settings?.enabled ?? false,
          fallback: prompt_settings?.fallbackResponses ?? "",
          activation: {
            type: knowledge_settings?.type ?? "",
            threshold: knowledge_settings?.threshold ?? 1,
          },
        },
      },
    } as ChatbotConfig;

    updateChatbot(updatedChatbot);
    setTimeout(() => {
      toast({
        title: "Success!",
        description: "Successfully saved knowledge settings",
      });
    }, 10);
  }

  const handleRadioChange = (event: any) => {
    setSelectedUploadOption(event);
  };

  return (
    <>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(handleSubmit)} className="space-y-8">
          {false ? (
            <FormField
              control={form.control}
              name="frequency"
              render={({ field }) => (
                <FormItem>
                  <RadioGroup
                    defaultValue={selectedUploadOption}
                    onValueChange={handleRadioChange}
                  >
                    <p>When to add knowledge context</p>
                    <div className="flex items-center space-x-2">
                      <RadioGroupItem
                        value="always"
                        id="option-one"
                        checked={selectedUploadOption === "always"}
                      />
                      <Label htmlFor="option-one">always</Label>
                    </div>
                    <div className="flex items-center space-x-2">
                      <RadioGroupItem
                        value="threshold"
                        id="option-two"
                        checked={selectedUploadOption === "threshold"}
                      />
                      <Label htmlFor="option-two">
                        only if the confidence is above threshold
                      </Label>
                    </div>
                    <div className="flex items-center space-x-2">
                      <RadioGroupItem
                        value="never"
                        id="option-three"
                        checked={selectedUploadOption === "never"}
                      />
                      <Label htmlFor="option-two">
                        never (disable knowledge)
                      </Label>
                    </div>
                  </RadioGroup>
                </FormItem>
              )}
            />
          ) : (
            <></>
          )}

          {selectedUploadOption === "threshold" && (
            <div>
              <label>How often?</label>
              <FormField
                control={form.control}
                name="threshold"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>
                      Over what threshold should a result be returned?
                    </FormLabel>
                    <FormControl>
                      <Input
                        type="number"
                        step="any"
                        {...register("threshold", {
                          valueAsNumber: true,
                          required: true,
                        })}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
          )}

          <FormField
            control={form.control}
            name="fallback"
            render={({ field }) => (
              <FormItem>
                <FormLabel>
                  If the answer is not in the provided context...
                </FormLabel>
                <FormControl>
                  <Textarea
                    placeholder="Say 'Sorry I don't have information about this topic'"
                    {...field}
                  />
                </FormControl>
              </FormItem>
            )}
          />

          <Button type="submit" className="order-2 sm:order-1">
            Save
          </Button>
        </form>
      </Form>
    </>
  );
}

export default KnowledgeSettings;
