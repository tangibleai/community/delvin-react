import { Button } from "@/components/ui/button";
import CONSTANTS from "@/constants";
import { Plan } from "@/typings/plan";
import moment from 'moment';

import { Callout } from "@/components/ui/callout";
import { Card } from "@tremor/react";

type Props = {
    plan: Plan;
};

export default function PaidPlanCard({ plan }: Props) {

    var formattedDate = ""

    if (plan.renewal_date !== null) {
        const dateObject = moment(plan.renewal_date, "YYYY-MM-DD HH:mm:ss.SSS Z").toDate();
        formattedDate = dateObject.toLocaleDateString('en-US', { year: 'numeric', month: 'long', day: 'numeric' })
    }

    const handleEmailClick = () => {
        window.location.href = `mailto:${CONSTANTS.CONTACT_EMAIL}?subject=change plan`;
    }

    return (
        <Card className="p-6 shadow-lg max-w-lg flex flex-col gap-4">
            <div className="flex items-center justify-between">
                <span className="text-2xl font-bold">Current Plan</span>
                <span className="bg-mute text-primary font-semibold text-sm px-3 py-1 rounded-full border-2">
                    Paid
                </span>
            </div>

                <div className="text-sm text-gray-600">
                    {`Renewal Date: ${formattedDate}`}
                </div>

                <Callout
                    variant="default"
                    title="You are on a paid plan"
                    className="w-full"
                >
                    Contact us to cancel or change your plan.
                </Callout>

                <div className="flex justify-end">
                    <Button onClick={handleEmailClick} >Contact Us</Button>
                </div>
            
        </Card>
    )
};
