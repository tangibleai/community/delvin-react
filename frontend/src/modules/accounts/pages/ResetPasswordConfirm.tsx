import { useNavigate, useParams } from "react-router-dom";

import { reset_password_confirm } from "@/actions/auth";
import logo from "@/assets/delvin_logo.png";
import { Button } from "@/components/ui/button";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { useToast } from "@/components/ui/use-toast";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import { passwordStringValidator } from "../utils/util";

function ResetPasswordConfirm() {
  const { toast } = useToast();
  const routeParams = useParams();
  const navigate = useNavigate();

  const formSchema = z
    .object({
      password: z
        .string()
        .min(8, { message: "The password should be at least 8 characters." })
        .refine(passwordStringValidator, {
          message: "String should not be all numbers",
        }),
      repassword: z.string(),
    })
    .superRefine(({ repassword, password }, ctx) => {
      if (password != repassword) {
        ctx.addIssue({
          path: ["repassword"],
          code: "custom",
          message: "The passwords did not match",
        });
      }
    });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      password: "",
      repassword: "",
    },
  });

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const { password, repassword } = values;
    const uid = routeParams.uid!;
    const token = routeParams.token!;

    const statusCode = await reset_password_confirm(
      uid,
      token,
      password,
      repassword,
    );

    if (statusCode >= 200 && statusCode <= 299) {
      toast({
        title: "Success!",
        description: "Password was successfully reset.",
      });
      navigate(AUTH_PAGE_URLS.login);
      return statusCode;
    }

    toast({
      variant: "destructive",
      title: "Oops! Something went wrong",
      description: "Couldn't reset password.",
    });
  };

  return (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
      <img className="w-80 mb-2" src={logo} alt="logo" />
      <div className="w-full p-6 bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md dark:bg-gray-800 dark:border-gray-700 sm:p-8">
        <h2 className="mb-1 text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
          Request Password Reset
        </h2>
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onSubmit)}
            className="mt-4 space-y-4 lg:mt-5 md:space-y-5"
          >
            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <>
                  <FormItem>
                    <FormLabel>Password</FormLabel>
                    <FormControl>
                      <Input
                        type="password"
                        placeholder="Type a secure password"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                </>
              )}
            />
            <FormField
              control={form.control}
              name="repassword"
              render={({ field }) => (
                <>
                  <FormItem>
                    <FormLabel>Confirm Password</FormLabel>
                    <FormControl>
                      <Input
                        type="password"
                        placeholder="Retype your password"
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                  {form?.formState?.errors?.repassword?.message && (
                    <FormMessage />
                  )}
                </>
              )}
            />

            <Button type="submit">Reset password</Button>
          </form>
        </Form>
      </div>
    </div>
  );
}

export default ResetPasswordConfirm;
