import axiosInstance from "@/api/axios";

const fetchDatasets = async (projectId: string) => {
  const response = await axiosInstance.get(
    `/api/project/get_datasets/?project_id=${projectId}`,
  );
  const datasets = response.data;
  return datasets;
};

const removeDatasetFromDb = async (projectId: string, datasetId: string) => {
  const response = await axiosInstance.post(
    `/api/dataset/delete/?project_id=${projectId}&dataset_id=${datasetId}`,
  );
  const removedDataset = response.data;
  return removedDataset;
};

export { fetchDatasets, removeDatasetFromDb };
