import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { Card, Title } from "@tremor/react";
import { AppDispatch, RootState } from "@/store";

import NewProviderModal from "./NewProviderModal";
import { BsFillTrash3Fill } from "react-icons/bs";
import { useToast } from "@/components/ui/use-toast";

import { Workspace } from "@/typings/workspace";
import {
  useDeleteProviderMutation,
  useFetchProvidersQuery,
} from "@/slices/api/providerApiSlice";
import { useFetchProviderTypesQuery } from "@/slices/api/providerTypeApiSlice";
import { Skeleton } from "@/components/ui/skeleton";
import { useDispatch } from "react-redux";
import { chatbotsApiSlice } from "@/slices/api/chatbotsApiSlice";
import { projectsApiSlice } from "@/slices/api/projectsApiSlice";
import { useFetchPlanQuery } from "@/slices/api/planApiSlice";
import { Plan, PlanType } from "@/typings/plan";
import axiosInstance from "@/api/axios";
import { Provider } from "@/typings/provider";

function TruncatedText({ text }: { text: string }) {
  const [truncatedText, setTruncatedText] = useState(text);
  const containerRef = useRef<HTMLSpanElement | null>(null); // Specify the correct type

  function truncateToFit(text: string, containerWidth: number, charWidth = 8) {
    const maxChars = Math.floor(containerWidth / charWidth);

    if (text.length <= maxChars) return text;

    const partLength = Math.floor((maxChars - 3) / 2);
    return `${text.slice(0, partLength)}...${text.slice(-partLength)}`;
  }

  useEffect(() => {
    const container = containerRef.current;
    if (!container) return;

    const updateTruncation = () => {
      const containerWidth = container.offsetWidth;
      setTruncatedText(truncateToFit(text, containerWidth));
    };

    const resizeObserver = new ResizeObserver(updateTruncation);
    resizeObserver.observe(container);

    updateTruncation();

    return () => resizeObserver.disconnect();
  }, [text]);

  return (
    <span ref={containerRef} className="truncate">
      {truncatedText}
    </span>
  );
}

type Props = {
  activeWorkspace: Workspace;
};

function Providers({ activeWorkspace }: Props) {
  const dispatch = useDispatch<AppDispatch>();
  const { toast } = useToast();

  const userEmail =
    useSelector((state: RootState) => state.auth.user?.email) ?? "";
  const isOwner = userEmail === activeWorkspace.owner;

  const { data: providers = [], isFetching: fetchingProviders } =
    useFetchProvidersQuery(activeWorkspace.id, {
      skip: !activeWorkspace || !activeWorkspace.id,
    });

  const [deleteProvider] = useDeleteProviderMutation();
  const {
    data: workspacePlan = {} as Plan,
    isFetching: fetchingWorkspacePlan,
  } = useFetchPlanQuery(activeWorkspace?.plan?.id || "", {
    skip: !activeWorkspace,
  });

  const { data: providerTypes = [], isFetching: fetchingProviderTypes } =
    useFetchProviderTypesQuery();

  const handleDeleteProvider = async (providerId: string) => {
    const providerChatbots = await dispatch(
      chatbotsApiSlice.endpoints.listChatbots.initiate({
        provider_id: providerId,
      })
    ).unwrap();

    try {
      if (providerChatbots && providerChatbots.length > 0) {
        const projectNamesWithChatbotDependingOnProvider = await Promise.all(
          providerChatbots.map(async (chatbot) => {
            const project = await dispatch(
              projectsApiSlice.endpoints.getCurrentProjectDetails.initiate(
                chatbot.project
              )
            ).unwrap();
            return project.name;
          })
        );
        toast({
          title: "Error",
          variant: "destructive",
          description: (
            <div>
              <p>
                Could not delete Provider. The following projects have
                assistants depending on the provider:
              </p>
              <ul>
                {projectNamesWithChatbotDependingOnProvider.map(
                  (name: string) => (
                    <li>- {name}</li>
                  )
                )}
              </ul>
              <p>
                Make sure no assistants rely on the provider before removing it.
              </p>
            </div>
          ),
        });
        return;
      }

      await deleteProvider(providerId).unwrap();
      toast({
        title: "Success!",
        description: "Provider deleted successfully",
      });
    } catch (error: any) {
      toast({ title: "Error deleting provider", description: error.message });
    }
  };

  const defaultProvider = providers.find((provider) => !provider.workspace);

  return (
    <div className="flex flex-col max-w-6xl mt-10 gap-14">
      <div className="flex flex-row justify-between my-5">
        <Title className="font-bold text-[1.3rem]">LLM Providers</Title>
        {isOwner ? (
          <NewProviderModal
            activeWorkspace={activeWorkspace}
            providerTypes={providerTypes}
          />
        ) : null}
      </div>
      <div className="w-full">
        {fetchingProviderTypes ? (
          <div className="space-y-3">
            {Array.from({ length: 3 }).map((_, index) => (
              <Skeleton key={index} className="h-36 w-full bg-gray-300" />
            ))}
          </div>
        ) : (
          <>
            {defaultProvider && (
              <Card className="w-full mb-4">
                <div className="flex flex-row justify-between items-center">
                  <div className="space-y-3">
                    <p className="font-bold text-2xl">
                      {defaultProvider.label}
                    </p>
                    <div>
                      <p>
                        <span className="font-bold">Type: </span>
                        {defaultProvider.type}
                      </p>
                      <p>
                        <span className="font-bold">API Key: </span>
                        {defaultProvider.api_key}
                      </p>
                    </div>
                  </div>
                </div>
              </Card>
            )}

            {providers
              .filter((provider) => provider.workspace)
              .map((provider) => (
                <Card key={provider.id} className="w-full mb-4">
                  <div className="flex flex-row justify-between items-center">
                    <div className="space-y-3 w-full">
                      <div className="flex justify-between w-full">
                        <p className="font-bold text-2xl">{provider.label}</p>
                        {provider.id === providers[0].id &&
                        workspacePlan.type === PlanType.OWN_KEY ? (
                          <></>
                        ) : provider.workspace ? (
                          <BsFillTrash3Fill
                            className="w-8 h-8 cursor-pointer"
                            onClick={() => handleDeleteProvider(provider.id)}
                          />
                        ) : (
                          <></>
                        )}
                      </div>

                      <div>
                        <p>
                          <span className="font-bold">Type: </span>
                          {provider.type}
                        </p>
                        <p className="truncate max-w-full">
                          <span className="font-bold">API Key: </span>
                          <TruncatedText text={provider.api_key} />
                        </p>
                      </div>
                    </div>
                  </div>
                </Card>
              ))}
          </>
        )}
      </div>
    </div>
  );
}

export default Providers;
