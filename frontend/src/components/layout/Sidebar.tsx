import React, { useState } from "react";
import { SideNav } from "@/components/layout/SideNav";
import { NavItems } from "@/components/layout/NavItems";
import { UserProfileDesktop } from "./UserProfile";
import icon from "@/assets/delvin_icon.png";
import logo from "@/assets/delvin_logo.png";

import { cn } from "@/lib/utils";
import { BsArrowLeftShort } from "react-icons/bs";
import { ProjectsDropdownDesktop } from "./SidebarProjectsDropdown";
import { Separator } from "../ui/separator";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { Link, useParams } from "react-router-dom";
import { PAGE_URLS } from "@/routes";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";

interface SidebarProps {
  className?: string;
}

export default function Sidebar({ className }: SidebarProps) {
  const [isOpen, setOpen] = useState(true);
  const [status, setStatus] = useState(false);
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const user = useSelector((state: RootState) => state.auth.user);

  const { data: project, isLoading } =
    useGetCurrentProjectDetailsQuery(projectId);

  const handleToggle = () => {
    setStatus(true);
    setOpen((prev) => !prev);
    setTimeout(() => setStatus(false), 500);
  };

  if (isLoading) {
    return <></>;
  }

  return (
    <nav
      className={cn(
        `flex flex-col relative min-h-screen border-r-2  md:block`,
        status && "duration-500",
        isOpen ? "w-72" : "w-20",
        className
      )}
    >
      <BsArrowLeftShort
        className={cn(
          "absolute -right-3 top-5 cursor-pointer rounded-full border bg-background text-3xl text-foreground",
          !isOpen && "rotate-180"
        )}
        onClick={handleToggle}
      />

      <div className="px-3 my-5 flex justify-center items-center">
        <Link to={AUTH_PAGE_URLS.home}>
          {isOpen ? (
            <img src={logo} alt="Logo" className="w-32" />
          ) : (
            <img src={icon} alt="Logo" className="w-8" />
          )}
        </Link>
      </div>
      <div className="my-5">
        <Separator />
      </div>
      <div className="px-3">
        {project && (
          <ProjectsDropdownDesktop isOpen={isOpen} currentProject={project} />
        )}
      </div>

      <div className="flex flex-col">
        <div className="flex-grow">
          <div className="px-3 py-2">
            <div className="mt-3 space-y-1">
              <SideNav
                className="text-background opacity-0 transition-all duration-300 group-hover:z-50 group-hover:ml-4 group-hover:rounded group-hover:bg-foreground group-hover:p-2 group-hover:opacity-100"
                items={NavItems}
                isOpen={isOpen}
                setOpen={setOpen}
              />
            </div>
          </div>
        </div>
        <div className="px-3 pt-20 pb-4">
          <UserProfileDesktop
            isOpen={isOpen}
            userName={user!.name}
            userEmail={user!.email}
          />
        </div>
      </div>
    </nav>
  );
}
