import { NavItem } from "@/typings/nav_item";
import { HiHome, HiShieldCheck } from "react-icons/hi";
import {
  HiChatBubbleLeftRight,
  HiCog8Tooth,
  HiLanguage,
  HiMiniWrenchScrewdriver,
  HiChatBubbleLeftEllipsis,
} from "react-icons/hi2";
import { PiTreeStructure } from "react-icons/pi";
import { BiSolidBrain } from "react-icons/bi";

export const NavItems: NavItem[] = [
  {
    title: "Home",
    icon: HiHome,
    href: "home",
    color: "#4141F7",
  },

  // {
  //   title: "Chat with Assistants",
  //   icon: HiChatBubbleLeftEllipsis,
  //   href: "chat_with_assistant",
  //   color: "#4141F7",
  // },

  {
    title: "Assistants Configuration",
    icon: HiMiniWrenchScrewdriver,
    href: "chatbot_config",
    color: "#4141F7",
  },

  {
    title: "Guardrails",
    icon: HiShieldCheck,
    href: "guardrail_responses",
    color: "#4141F7",
  },

  {
    title: "AI Training (Advanced)",
    icon: BiSolidBrain,
    href: "understanding",
    color: "#4141F7",
  },

  {
    title: "Conversations",
    icon: HiChatBubbleLeftRight,
    href: "conversations",
    color: "#4141F7",
  },

  {
    title: "Settings",
    icon: HiCog8Tooth,
    href: "settings",
    color: "#4141F7",
  },
];
