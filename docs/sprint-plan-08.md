## Maria
* [ ] Deploy GPT messenger on Render and alter it to log all messages to Delvin. 
* [ ] Check that Cetin's implementation of ml.delvin.to
* [ ] Do more planning about the future roadmap 
* [ ] Reach out to 3 people with Generative chatbots


## Cetin 
* [X] Check how we can have https on app.delvin.to and ml.delvin.to
* [X] Research `pg-vector` (PostgreSQL format for storing vectors) and check out `pgvector-python`(https://github.com/pgvector/pgvector-python)
* [X] Stretch - implement an Utterance object (Maria will provide the specs) that gets created every time the message is created and contains the message embedding. 

## Greg
* [ ] Plan the architecture and the API (and the interface) - create an issue with the user story and the API calls definition.
* [ ] Implement the permissions object.
* [ ] ...
