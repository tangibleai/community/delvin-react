# import json
# import os
# from unittest.mock import patch

# from django.conf import settings
# from django.db import connection, connections
# from django.test import override_settings, TransactionTestCase
# from django.test.testcases import SerializeMixin
# from rest_framework.test import APIClient

# from api.models import (
#     Chatbot,
#     Document,
#     GenerativeModel,
#     Project,
#     Provider,
#     ProviderChoices,
#     UserAccount,
#     Workspace,
# )
# from api.tests import mock
# from api.tests import utils as test_utils


# @override_settings(
#     # Celery settings run this synchronously for easier testing
#     CELERY_TASK_ALWAYS_EAGER=True,
#     CELERY_TASK_EAGER_PROPAGATES=True,
#     DATABASES={
#         "default": {
#             **settings.DATABASES["default"],
#             "CONN_MAX_AGE": 0,  # This setting facilitates closing database connections
#         }
#     },
# )
# class TestChatKnowledgeDocument(TransactionTestCase, SerializeMixin):
#     """Test the creation of chat knowledge records when Documents are created

#     Throughout this test suite, raw SQL is used because
#     LlamaIndex manages the knowledge document table, not Django
#     """

#     lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
#     client = APIClient()
#     fixtures = ["default_account_data.json"]

#     @classmethod
#     def setUpClass(cls):
#         """Ensures that LlamaIndex uses the correct database and table
#         through the test, as Django does not manage the chat_knowledge_table
#         """
#         super().setUpClass()
#         # Get the test database connection string
#         test_db_settings = connections["default"].settings_dict
#         cls.test_db_url = f"postgresql://{test_db_settings['USER']}:{test_db_settings['PASSWORD']}@{test_db_settings['HOST']}:{test_db_settings.get('PORT', 5432)}/{test_db_settings['NAME']}"

#         # Patch .env variable database settings
#         cls.patcher = patch.dict(
#             "os.environ",
#             {
#                 "DJANGO_DATABASE_URL": cls.test_db_url,
#                 "DATABASE_NAME": test_db_settings["NAME"],
#             },
#         )
#         cls.patcher.start()

#     def setUp(self):
#         """Creates the necessary database records to facilitate testing

#         An initial Document instance is created,
#         which should trigger the creation of a knowledge document instance
#         """
#         super().setUp()
#         # Set up user account for authentication
#         user = test_utils.filter_by_fields(
#             mock.mock["user1"], ["email", "name", "password"]
#         )
#         user_rec = UserAccount.objects.create_user(**user)
#         response = self.client.post("/auth/jwt/create/", user)
#         self.user1_access_token = response.json()["access"]
#         self.headers = {"Authorization": "JWT " + self.user1_access_token}

#         # Create workspace for the user
#         req_body = {
#             "name": mock.mock["workspace1"]["name"],
#             "properties": mock.mock["workspace1"]["properties"],
#             "owner": str(user_rec.id),
#         }
#         json_data = json.dumps(req_body)
#         serialized_workspace = self.client.post(
#             "/api/workspace/create/",
#             json_data,
#             content_type="application/json",
#             headers=self.headers,
#         ).json()

#         workspace_id = serialized_workspace["id"]
#         workspace = Workspace.objects.get(id=workspace_id)

#         # Create a project associated with the workspace
#         data = {
#             "name": mock.mock["user1"]["project_name"],
#             "workspace_id": workspace_id,
#         }
#         serialized_project = self.send_put_or_post_request(
#             "/api/project/create/",
#             data,
#         )
#         self.project1_id = serialized_project["id"]
#         self.project = Project.objects.get(id=self.project1_id)
#         self.project1_token_key = serialized_project["token_key"]

# self.provider = Provider.objects.create(
#     label="Test",
#     type=ProviderChoices.OPENAI.value,
#     workspace=workspace,
#     api_key="abcd",
# )
# self.generative_model, _ = GenerativeModel.objects.get_or_create(
#     label="Test Generative model",
#     model_string="gpt-4o-mini",
#     priority=1,
#     provider_type=ProviderChoices.OPENAI.value,
# )
# # NOTE: Unlike other user artifacts, this is created manually, not through an endpoint
# self.chatbot = Chatbot.objects.create(
#     project=self.project,
#     generative_model=self.generative_model,
#     config={
#         "general_prompt": {
#             "persona": "AI Assistant",
#             "constraints": "Be helpful and concise",
#         },
#         "knowledge": {
#             "fallback": "I cannot answer that based on the available information"
#         },
#     },
# )

#         # Create an initial document within the Project
#         response = self.send_put_or_post_request(
#             "/api/documents/create/",
#             {
#                 "project": self.project1_id,
#                 "chatbot": self.chatbot.id,
#                 "title": mock.mock["document1"]["title"],
#                 "content": mock.mock["document1"]["content"],
#                 "document_type": mock.mock["document1"]["document_type"],
#             },
#         )
#         self.create_document_response = response

#     def send_put_or_post_request(self, endpoint, payload, method="post"):
#         """Simulates a request through the endpoint the frontend would use"""
#         request_data = {
#             "path": endpoint,
#             "data": json.dumps(payload),
#             "headers": self.headers,
#             "content_type": "application/json",
#         }

#         if method == "put":
#             response = self.client.put(**request_data).json()
#         else:
#             response = self.client.post(**request_data).json()
#         return response

#     def test_chat_knowledge_record_created(self):
#         """Test that a chat knowledge record is created when a Document is created"""

#         with connection.cursor() as cursor:
#             cursor.execute(
#                 """
#                 SELECT COUNT(*)
#                 FROM data_chatbot_knowledge
#                 WHERE metadata_->>'delvin_document_id' = %s
#                 """,
#                 [str(self.create_document_response["id"])],
#             )
#             count = cursor.fetchone()[0]
#             self.assertGreater(
#                 count, 0, "No matching record found in chat knowledge table"
#             )

#     def test_chat_knowledge_records_edited(self):
#         """Test that a chat knowledge record reflects the correct information
#         when its associated Document is updated"""
#         modified_title = "Modified First Document"
#         modified_content = "Modified Content of First Document"
#         self.send_put_or_post_request(
#             f"/api/documents/{self.create_document_response['id']}/update/",
#             {
#                 "id": self.create_document_response["id"],
#                 "title": modified_title,
#                 "content": modified_content,
#                 "project": self.project1_id,
#                 "document_type": mock.mock["document1"]["document_type"],
#                 "document": self.create_document_response["id"],
#             },
#             "put",
#         )

#         with connection.cursor() as cursor:
#             cursor.execute(
#                 """
#                 SELECT metadata_->>'document_title' as title, metadata_->>'document_content' as content
#                 FROM data_chatbot_knowledge
#                 WHERE metadata_->>'delvin_document_id' = %s
#                 """,
#                 [str(self.create_document_response["id"])],
#             )
#             result = cursor.fetchone()
#             self.assertIsNotNone(result, "No matching record found")
#             db_title, db_content = result
#             self.assertEqual(
#                 db_content, modified_content, "Content was not updated correctly"
#             )

#     def test_chat_knowledge_record_deleted(self):
#         """Test that a chat knowledge record is deleted when its associated Document is deleted"""

#         self.client.delete(
#             f"/api/documents/{self.create_document_response['id']}/delete/",
#             headers=self.headers,
#         )

#         with connection.cursor() as cursor:
#             cursor.execute(
#                 """
#                 SELECT COUNT(*)
#                 FROM data_chatbot_knowledge
#                 WHERE metadata_->>'delvin_document_id' = %s
#                 """,
#                 [str(self.create_document_response["id"])],
#             )
#             count = cursor.fetchone()[0]
#             self.assertEqual(count, 0, "One or more matching records found")

#     @classmethod
#     def tearDownClass(cls):
#         """
#         NOTE:
#         All database connections need to be closed for this test suit to pass
#         If the database can't be deleted during tear down, the tests will fail.
#         Two methods are used here to force connections to close.
#         """
#         for conn in connections.all():
#             conn.close()

#         # Forcefully remove all connections: https://stackoverflow.com/questions/5408156/how-to-drop-a-postgresql-database-if-there-are-active-connections-to-it
#         with connection.cursor() as cursor:
#             cursor.execute(
#                 """
#                 SELECT pg_terminate_backend(pid)
#                 FROM pg_stat_activity
#                 WHERE datname = %s
#                 AND pid <> pg_backend_pid();
#             """,
#                 [connections["default"].settings_dict["NAME"]],
#             )

#         cls.patcher.stop()
#         super().tearDownClass()

# def tearDown(self):
#     """Removes all data created during the test
#     Note that the RAG feature uses both a docstore and a vector store
#     """
#     try:
#         with connection.cursor() as cursor:
#             cursor.execute(
#                 """
#                 DELETE FROM data_chatbot_knowledge
#                 WHERE metadata_->>'delvin_project_id' = %s
#                 """,
#                 [str(self.project1_id)],
#             )
#             cursor.execute(
#                 """
#                 DELETE FROM data_docstore_chatbot_knowledge
#                 WHERE key LIKE %s
#                 """,
#                 [f"%{self.project1_id}%"],
#             )
#             connection.commit()
#     except:
#         pass
#     finally:
#         # Clean up Django models in reverse order of creation
#         Document.objects.filter(project_id=self.project1_id).delete()
#         Chatbot.objects.filter(project_id=self.project1_id).delete()
#         Project.objects.filter(id=self.project1_id).delete()
#         UserAccount.objects.all().delete()
#         # Attempt to force connections to close: https://stackoverflow.com/questions/45672334/django-how-to-get-a-fresh-connection-to-the-database
#         connection.close()

#         super().tearDown()
