import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogFooter,
  DialogClose,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { ScrollArea } from "@/components/ui/scroll-area";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Dataset } from "./Dataset";
import { createNewDatasetInDb } from "../utils/datasets_sidebar";
import { Dataset as DatasetType } from "@/typings/dataset";

type Props = {
  datasets: never[];
  fetchAndSetDatasets: () => Promise<void>;
  onActiveDatasetChange: React.Dispatch<
    React.SetStateAction<Partial<DatasetType>>
  >;
};

const DatasetsListSidebar = ({
  datasets,
  fetchAndSetDatasets,
  onActiveDatasetChange,
}: Props) => {
  const [newDatasetName, setNewDatasetName] = useState("");
  // const [newDatasetDescription, setNewDatasetDescription] = useState("");

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  useEffect(() => {
    fetchAndSetDatasets();
  }, []);

  const handleNewDatasetNameChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    const target = event.target as HTMLInputElement;
    setNewDatasetName(target.value);
  };

  // const handleNewDatasetDescriptionChange = (
  //   event: React.ChangeEvent<HTMLInputElement>
  // ) => {
  //   const target = event.target as HTMLInputElement;
  //   setNewDatasetDescription(target.value);
  // };

  const addNewDataset = async () => {
    await createNewDatasetInDb(
      projectId,
      newDatasetName,
      // newDatasetDescription
    );
    fetchAndSetDatasets();
    setNewDatasetName("");
    // setNewDatasetDescription("");
  };

  return (
    <div className="w-1/4 h-[75vh]">
      <ScrollArea className="flex flex-col justify-end flex-grow py-2.5 px-5 rounded-2xl bg-gray-100 p-4 max-h-full">
        {datasets &&
          datasets.map((d: DatasetType) => (
            <Dataset
              key={d.name}
              dataset={d}
              datasetSelected={onActiveDatasetChange}
            />
          ))}
      </ScrollArea>
      <Dialog>
        <DialogTrigger asChild>
          <Button className="mt-2">New Dataset</Button>
        </DialogTrigger>
        <DialogContent className="sm:max-w-[425px]">
          <DialogHeader>
            <DialogTitle>Create New Dataset</DialogTitle>
          </DialogHeader>
          <div className="grid gap-4 py-4">
            <div className="grid grid-cols-1 gap-4">
              <Label htmlFor="dataset_name">Dataset Name</Label>
              <Input
                id="dataset_name"
                value={newDatasetName}
                onChange={handleNewDatasetNameChange}
              />
            </div>
            {/* <div className="grid grid-cols-1 gap-4">
              <Label htmlFor="dataset_description">Description</Label>
              <Input
                id="dataset_description"
                value={newDatasetDescription}
                onChange={handleNewDatasetDescriptionChange}
              />
            </div> */}
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button type="submit" onClick={addNewDataset}>
                Save changes
              </Button>
            </DialogClose>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export { DatasetsListSidebar };
