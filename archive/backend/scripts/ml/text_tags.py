"""Module with get tags for text functionality"""

import numpy as np
import pandas as pd


def ssr_gradient(x, y, b):
    print(x)
    print(y)
    print(b)
    res = pd.DataFrame(b * x).sum() - y
    dcdw = np.sum(x * res, axis=0).reshape((self.num_dims, 1))
    print(res)
    return res.mean(), (res * x).mean()  # .mean() is a method of np.ndarray


def sgd(
    x,
    y,
    gradient=ssr_gradient,
    activation=logistic,
    n_vars=None,
    weights=None,
    learn_rate=0.1,
    decay_rate=0.0,
    batch_size=1,
    n_iter=50,
    tolerance=1e-06,
    dtype="float64",
    random_state=None,
):
    """
    NOTE: learn about sklearn linear regression
    x - batch of embedding vector (typically 16)
    y - predicted value
    gradient - function that computes the partial derivitive dy/dx1,dy/dx2,...dy/dxn
    Intercept is weights[0], x[0] = 1, x = [1] + list(embedding_vector)
    """
    # Checking if the gradient is callable
    if not callable(gradient):
        raise TypeError("'gradient' must be callable")

    n_vars = n_vars or x.shape[1]

    # Setting up the data type for NumPy arrays
    dtype_ = np.dtype(dtype)

    # Converting x and y to NumPy arrays
    x, y = np.array(x, dtype=dtype_), np.array(y, dtype=dtype_)
    n_obs = x.shape[0]
    if n_obs != y.shape[0]:
        raise ValueError("'x' and 'y' lengths do not match")
    xy = np.c_[x.reshape(n_obs, -1), y.reshape(n_obs, 1)]

    # Initializing the random number generator
    seed = None if random_state is None else int(random_state)
    rng = np.random.default_rng(seed=seed)

    # Initializing the values of the variables
    weights = (
        rng.normal(size=int(n_vars)).astype(dtype_)
        if weights is None
        else np.array(weights, dtype=dtype_)
    )

    # Setting up and checking the learning rate
    learn_rate = np.array(learn_rate, dtype=dtype_)
    if np.any(learn_rate <= 0):
        raise ValueError("'learn_rate' must be greater than zero")

    # Setting up and checking the decay rate
    decay_rate = np.array(decay_rate, dtype=dtype_)
    if np.any(decay_rate < 0) or np.any(decay_rate > 1):
        raise ValueError("'decay_rate' must be between zero and one")

    # Setting up and checking the size of minibatches
    batch_size = int(batch_size)
    if not 0 < batch_size <= n_obs:
        raise ValueError(
            "'batch_size' must be greater than zero and less than "
            "or equal to the number of observations"
        )

    # Setting up and checking the maximal number of iterations
    n_iter = int(n_iter)
    if n_iter <= 0:
        raise ValueError("'n_iter' must be greater than zero")

    # Setting up and checking the tolerance
    tolerance = np.array(tolerance, dtype=dtype_)
    if np.any(tolerance <= 0):
        raise ValueError("'tolerance' must be greater than zero")

    # Setting the difference to zero for the first iteration
    diff = 0
    grad = pd.DataFrame()

    # Performing the gradient descent loop
    for _ in range(n_iter):
        # Shuffle x and y
        rng.shuffle(xy)

        # Performing minibatch moves
        for start in range(0, n_obs, batch_size):
            stop = start + batch_size
            x_batch, y_batch = xy[start:stop, :-1], xy[start:stop, -1:]

            # Recalculating the difference
            print("BEFORE", grad.shape, x_batch.shape, y_batch.shape, weights.shape)
            grad = np.array(gradient(x_batch, y_batch, weights), dtype_)
            print("AFTER", grad.shape)
            print()
            diff = decay_rate * diff - learn_rate * grad

            # Checking if the absolute difference is small enough
            if np.all(np.abs(diff) <= tolerance):
                break

            # Updating the values of the variables
            weights += diff

    return weights if weights.shape else weights.item()
