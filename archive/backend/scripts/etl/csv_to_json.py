"""Converts csv representation of any model to json acceptable by `loaddata` command to populate/update"""

import csv
import json


def csv_to_json(csv_file, json_file):
    with open(csv_file, newline="", encoding="utf-8") as file:
        reader = csv.DictReader(file)
        json_data = []

        for row in reader:
            item = {"model": row["model"], "pk": row["pk"], "fields": {}}
            for key, value in row.items():
                if not key.startswith("fields."):
                    continue
                # Remove 'fields.' prefix and add to 'fields' dict
                field_key = key[7:]
                if field_key in ("embedding", "chat") and not value:
                    value = None
                item["fields"][field_key] = value
            json_data.append(item)

    with open(json_file, "w", encoding="utf-8") as file:
        json.dump(json_data, file, indent=4)


csv_to_json("message.csv", "output.json")
