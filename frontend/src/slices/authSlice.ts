import Cookies from "js-cookie";

import { User } from "@/typings/user";
import { AnyAction, PayloadAction, createSlice } from "@reduxjs/toolkit";
import { RootState } from "@/store";

interface AuthState {
  access: string | null;
  refresh: string | null;
  isAuthenticated: boolean | null;
  user: User | null;
  loading: boolean;
}

const initialState: AuthState = {
  access: Cookies.get("access") ?? null,
  refresh: Cookies.get("refresh") ?? null,
  isAuthenticated: null,
  user: null,
  loading: false,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    authenticatedSuccess(state) {
      state.isAuthenticated = true;
    },

    authenticatedFail(state) {
      state.isAuthenticated = false;
    },

    loginSuccess(
      state,
      action: PayloadAction<{ access: string; refresh: string }>
    ) {
      const { access, refresh } = action.payload;
      Cookies.set("access", access, { sameSite: "Strict", secure: true });
      Cookies.set("refresh", refresh, { sameSite: "Strict", secure: true });
      state.isAuthenticated = true;
      state.access = access;
      state.refresh = refresh;
      state.loading = false;
    },

    loginFail(state) {
      Cookies.remove("access");
      Cookies.remove("refresh");
      state.access = null;
      state.refresh = null;
      state.isAuthenticated = false;
      state.user = null;
      state.loading = false;
    },

    signupSuccess(state) {
      state.isAuthenticated = false;
      state.loading = false;
    },

    signupFail(state) {
      state.isAuthenticated = false;
      state.loading = false;
    },

    loadUserSuccess(state, action: PayloadAction<any>) {
      state.user = action.payload;
    },

    loadUserFail(state) {
      state.user = null;
    },

    logoutSuccess(state) {
      Cookies.remove("access");
      Cookies.remove("refresh");
      state.access = null;
      state.refresh = null;
      state.isAuthenticated = false;
      state.user = null;
    },

    loading(state) {
      state.loading = true;
    },

    passwordResetSuccess(state) {
      // Future implementation or side effect triggers
    },
    passwordResetFail(state) {
      // Future implementation or side effect triggers
    },
    passwordResetConfirmSuccess(state) {
      // Future implementation or side effect triggers
    },
    passwordResetConfirmFail(state) {
      // Future implementation or side effect triggers
    },
    activationSuccess(state) {
      // Future implementation or side effect triggers
    },
    activationFail(state) {
      // Future implementation or side effect triggers
    },
  },
});

export const selectCurrentUserEmail = (state: RootState) =>
  state.auth.user?.email;
export const selectCurrentUserId = (state: RootState) => state.auth.user?.id;

export const {
  authenticatedSuccess,
  authenticatedFail,
  loginSuccess,
  loginFail,
  signupSuccess,
  signupFail,
  loadUserSuccess,
  loadUserFail,
  logoutSuccess,
  loading,
  passwordResetSuccess,
  passwordResetFail,
  passwordResetConfirmSuccess,
  passwordResetConfirmFail,
  activationSuccess,
  activationFail,
} = authSlice.actions;

export default authSlice.reducer;
