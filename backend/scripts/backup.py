import os
from django.conf import settings
from django.core.management import call_command, CommandError


def backup_all_tables(backup_path="backups", apps=settings.INSTALLED_APPS):
    """Backup all tables from the Django app database into separate JSON files.
    Each file will be named for its corresponding model.
    :param backup_path: The directory where backup files will be stored.
    """
    os.makedirs(backup_path, exist_ok=True)

    for app in apps:
        try:
            # app's models module
            mod = __import__(f"{app}.models", fromlist=["*"])
            os.makedirs(f"{backup_path}/{app}", exist_ok=True)
            for model in dir(mod):
                model_class = getattr(mod, model)

                if not hasattr(model_class, "_meta"):
                    continue
                db_table_name = model_class._meta.db_table
                if not db_table_name.startswith(app):
                    continue
                filename = os.path.join(backup_path, app, f"{db_table_name}.json")
                print(f"Backing up {db_table_name} to {filename}")
                with open(filename, "w") as f:
                    try:
                        call_command("dumpdata", f"{app}.{model}", stdout=f)
                    except CommandError:
                        # ignore if model was imported from other app
                        pass
        except ImportError as e:
            print(f"Error importing models for app {app}: {e}")


if __name__ == "__main__":
    backup_all_tables(apps=["api", "accounts"])
