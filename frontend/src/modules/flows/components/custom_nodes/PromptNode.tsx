import { useCallback } from "react";
import { Handle, NodeProps, Position, useReactFlow } from "reactflow";
import { BsFillTrash3Fill } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import {
  setIsDeleteNodeDialogOpened,
  setActiveNode,
  setNodeSidebar,
} from "@/slices/flowSlice";
import { RootState } from "@/store";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";

type NodeData = {
  nodeId: string;
  nodeName: string;
  nodePrompt: string;
};

type Props = NodeProps<NodeData>;

const PromptNode = ({ data, isConnectable }: Props) => {
  const dispatch = useDispatch();
  const { getNodes, setNodes } = useReactFlow();

  const erroneousNodeIds: string[] = useSelector(
    (state: RootState) => state.flow.erroneousNodes
  ).map((node) => node.id);

  const onTrashIconClick = useCallback(() => {
    dispatch(setIsDeleteNodeDialogOpened(true));
    const currentNode = getNodes().find((node) => {
      return node.id === data.nodeId;
    });
    dispatch(setActiveNode(currentNode!));
  }, [data.nodeId]);

  const activeNode = useSelector((state: RootState) => state.flow.activeNode);

  const connectionInProgress = useSelector(
    (state: RootState) => state.flow.connectionInProgress
  );
  const connectionSourceNodeId = useSelector(
    (state: RootState) => state.flow.connectionSourceNodeId
  );

  const onNodeClicked = () => {
    const currentNode = getNodes().find((node) => {
      return node.id === data.nodeId;
    });

    setNodes((nodes) =>
      nodes.map((node) =>
        node.id === currentNode!.id
          ? node
          : {
              ...node,
              style: { width: node.style?.width, height: node.style?.height },
            }
      )
    );

    dispatch(setActiveNode(currentNode!));
    dispatch(
      setNodeSidebar({
        opened: true,
        nodeType: nodeNameToTypeMapping.PROMPT_NODE,
      })
    );
  };

  return (
    <>
      <Handle
        type="target"
        position={Position.Left}
        style={{
          background: "black",
          color: "black",
          backgroundColor: "primary",
          top: "8%",
        }}
        onConnect={(params) => console.log("handle onConnect", params)}
        isConnectable={isConnectable}
      />
      <Handle
        type="source"
        position={Position.Right}
        style={{
          top: "92%",
          background: "#6767E5",
          color: "#6767E5",
          right: `${
            connectionInProgress && connectionSourceNodeId === data.nodeId
              ? "-5%"
              : ""
          }`,
        }}
        isConnectable={isConnectable}
        className={`transition-all duration-500 ease-in-out ${
          connectionInProgress && connectionSourceNodeId === data.nodeId
            ? "w-4 h-4"
            : ""
        }`}
      />
      <div
        className={`h-full border rounded-xl ${
          erroneousNodeIds.includes(data.nodeId)
            ? "border-red-600 border-2"
            : activeNode.id === data.nodeId
            ? "border-primary border-2"
            : "border-slate-400"
        } bg-white text-xs`}
        onClick={onNodeClicked}
      >
        <div className="flex items-center justify-between w-full bg-muted px-2 rounded-t-xl">
          <span className="node-title">{data.nodeName}</span>
          <BsFillTrash3Fill
            className="text-xs text-destructive"
            onClick={onTrashIconClick}
          />
        </div>
        <div className="flex flex-col h-5/6 px-2 pt-1">
          <div className="flex-grow">
            <p className="text-xs font-bold">Prompt:</p>
            <p className={` text-[10px] ${data.nodePrompt === "" ? "text-red-500" : ""}`}>
              {data.nodePrompt === ""
                ? "No prompt yet. Click to add a prompt"
                : data.nodePrompt.slice(0,200)}
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export { PromptNode };
