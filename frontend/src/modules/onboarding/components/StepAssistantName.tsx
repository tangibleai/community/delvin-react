import { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { useForm, useWatch } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  Form,
  FormField,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { AppDispatch, RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { setOnboardingData } from "@/slices/onboardingSlice";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

import { useFetchProvidersQuery } from "@/slices/api/providerApiSlice";
import { useParams } from "react-router-dom";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";
import { useFetchGenerativeModelsQuery } from "@/slices/api/generativeModelsApiSlice";
import { GenerativeModel } from "@/typings/generative_model";
import { useListChatbotsQuery } from "@/slices/api/chatbotsApiSlice";

const StepAssistantName = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData
  );

  const [noProviderForWorkspace, setNoProviderForWorkspace] = useState(false);

  const [availableLLMs, setAvailableLLMs] = useState<GenerativeModel[]>([]);

  const { data: project } = useGetCurrentProjectDetailsQuery(projectId, {
    skip: !projectId,
  });
  const { data: providers = [] } = useFetchProvidersQuery(
    project?.workspace || "",
    {
      skip: !project || !project?.workspace,
    }
  );
  const { data: generativeModels = [] as GenerativeModel[] } =
    useFetchGenerativeModelsQuery();

  const { data: chatbots = [], isLoading: retrievingChatbots } =
    useListChatbotsQuery({ project_id: projectId });
  const formSchema = z.object({
    model_id: z.string().nonempty("Please, select a model."),
    llm_provider_id: noProviderForWorkspace
      ? z.string().nonempty("You didn't add any provider yet.")
      : z.string(),
    name: z.string().min(2, {
      message: "Name must be at least 2 characters.",
    }),
  });

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      model_id: onboardingData.stepAssistantName.model_id,
      llm_provider_id: onboardingData.stepAssistantName.llm_provider_id,
      name: "",
    },
  });

  const { handleSubmit, reset } = form;

  const llmProviderId = useWatch({
    control: form.control,
    name: "llm_provider_id",
  });

  useEffect(() => {
    if (!providers.length) return;

    setNoProviderForWorkspace(providers.length === 0);

    form.setValue("llm_provider_id", providers[0].id);
  }, [providers]);

  useEffect(() => {
    if (onboardingData) {
      reset({
        name: onboardingData.stepAssistantName.name || "",
      });
    }
  }, [onboardingData, reset]);

  useEffect(() => {
    const selectedProviderId = form.getValues("llm_provider_id");
    if (!selectedProviderId) return;

    const getAndSetAvailableLLMs = async () => {
      const selectedProvider = providers.find(
        (provider) => provider.id === selectedProviderId
      )!;
      const providerLLMs = generativeModels.filter(
        (model) => model.provider_type === selectedProvider.type
      );
      setAvailableLLMs(providerLLMs || []);

      if (providerLLMs && providerLLMs.length > 0) {
        form.setValue("model_id", String(providerLLMs[0].id));
      } else {
        form.setValue("model_id", "");
      }
    };
    getAndSetAvailableLLMs();
  }, [llmProviderId]);

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const { name, llm_provider_id, model_id } = values;

    if (chatbots.some((chatbot) => chatbot.origin === name)) {
      form.setError("name", {
        type: "manual",
        message: "This name is already taken. Please choose another name.",
      });
      return false;
    }

    dispatch(
      setOnboardingData({
        ...onboardingData,
        stepAssistantName: {
          name: name,
          llm_provider_id: llm_provider_id,
          model_id: model_id,
        },
      })
    );

    return true;
  };

  useImperativeHandle(ref, () => ({
    submitForm: () => {
      return new Promise<boolean>((resolve) => {
        handleSubmit(async (values) => {
          const result = await onSubmit(values);
          resolve(result);
        })();
      });
    },
    updateOnboardingDataWithDraft: () => {
      const { name, llm_provider_id, model_id } = form.getValues();
      dispatch(
        setOnboardingData({
          ...onboardingData,
          stepAssistantName: {
            name: name,
            llm_provider_id: llm_provider_id,
            model_id: model_id,
          },
        })
      );
    },
  }));

  return (
    <div className="w-fit mx-auto flex flex-col">
      <></>
      <Form {...form}>
        <form onSubmit={handleSubmit(onSubmit)} className="space-y-8">
          <p className="font-medium text-3xl">Let's start from the basics</p>
          <FormField
            control={form.control}
            name="name"
            render={({ field }) => (
              <FormItem className="flex flex-col gap-4">
                <FormLabel className="text-xl">
                  What's your assistant's name?
                </FormLabel>
                <FormControl>
                  <Input {...field} className="text-xl" />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="llm_provider_id"
            render={({ field }) => (
              <FormItem className="flex flex-col gap-4">
                <FormLabel className="text-xl">
                  Choose your LLM Provider
                </FormLabel>
                <Select onValueChange={field.onChange} value={field.value}>
                  <FormControl>
                    <SelectTrigger className="text-base">
                      <SelectValue placeholder="Select a provider" />
                    </SelectTrigger>
                  </FormControl>
                  <SelectContent>
                    {providers.map((provider) => (
                      <SelectItem key={provider.id} value={provider.id}>
                        {provider.label}
                      </SelectItem>
                    ))}
                  </SelectContent>
                </Select>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="model_id"
            render={({ field }) => (
              <FormItem className="flex flex-col gap-1">
                <FormLabel className="text-xl">
                  Which language model do you want to use?
                </FormLabel>
                <Select onValueChange={field.onChange} value={field.value}>
                  <FormControl>
                    <SelectTrigger className="text-base">
                      <SelectValue placeholder="Select a model to display" />
                    </SelectTrigger>
                  </FormControl>
                  <SelectContent>
                    {availableLLMs.map((model) => (
                      <SelectItem
                        key={model.id}
                        value={String(model.id)}
                        className="text-base"
                      >
                        {model.label}
                      </SelectItem>
                    ))}
                  </SelectContent>
                </Select>
                <FormMessage />
              </FormItem>
            )}
          />
        </form>
      </Form>
    </div>
  );
});

export default StepAssistantName;
