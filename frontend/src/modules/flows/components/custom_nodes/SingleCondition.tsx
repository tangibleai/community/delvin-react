import { Handle, NodeProps, Position, useReactFlow } from "reactflow";
import { useDispatch, useSelector } from "react-redux";
import { setActiveNode, setNodeSidebar } from "@/slices/flowSlice";
import { RootState } from "@/store";
import { Condition, ConditionTypes } from "../../typing/condition";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";

type NodeData = {
  nodeId: string;
  nodeName: string;
  nodePrompt: string;
  condition: Condition;
};

type Props = NodeProps<NodeData>;

const SingleCondition = ({ data, isConnectable }: Props) => {
  const dispatch = useDispatch();
  const { getNodes } = useReactFlow();

  const connectionInProgress = useSelector(
    (state: RootState) => state.flow.connectionInProgress
  );
  const connectionSourceNodeId = useSelector(
    (state: RootState) => state.flow.connectionSourceNodeId
  );

  const onNodeClicked = () => {
    const currentNode = getNodes().find((node) => {
      return node.id === data.nodeId;
    });

    const parentNode = getNodes().find(
      (node) => node.id === currentNode?.parentId
    );

    dispatch(setActiveNode(parentNode!));
    dispatch(
      setNodeSidebar({
        opened: true,
        nodeType: nodeNameToTypeMapping.CONDITION_BLOCK,
      })
    );
  };

  return (
    <>
      <Handle
        type="source"
        position={Position.Right}
        style={{
          background: "black",
          color: "black",
          right: `${
            connectionInProgress && connectionSourceNodeId === data.nodeId
              ? "-5%"
              : ""
          }`,
        }}
        isConnectable={isConnectable}
        className={`transition-all duration-500 ease-in-out ${
          connectionInProgress && connectionSourceNodeId === data.nodeId
            ? "w-4 h-4"
            : ""
        }`}
      />
      <div
        className={`h-full border rounded-xl border-slate-400 bg-white text-[0.5rem]`}
        onClick={onNodeClicked}
      >
        <div className="flex flex-col h-full px-2">
          <div className="flex h-full items-center">
            {data.condition.type === ConditionTypes.ELSE ? (
              <p>Else ...</p>
            ) : (
              <p
                className={` ${
                  data.condition.properties.tag_name === ""
                    ? "text-red-500"
                    : ""
                }`}
              >
                {data.condition.properties.tag_name === ""
                  ? "No condition yet. Click to add a condition"
                  : `Tag "${data.condition.properties.tag_name}" is detected`}
              </p>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export { SingleCondition };
