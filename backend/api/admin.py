from django.contrib import admin

from api.models import Project, Contact, ProjectCollaborator, Message, Document


class SampleTaskAdmin(admin.ModelAdmin):
    list_display = ["name", "created_on"]
    search_fields = ["name"]


admin.site.register(Project)
admin.site.register(Contact)
admin.site.register(ProjectCollaborator)
admin.site.register(Message)
admin.site.register(Document)
