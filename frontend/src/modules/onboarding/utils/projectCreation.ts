import { OnboardingData } from "@/typings/onboarding_data";
import { chatbotTypes } from "@/typings/chatbot_config";

export const composeChatbotDescription = (
  onboardingData: OnboardingData,
  chatbotType: string
) => {
  let descriptionPiecesObject = {};

  if (chatbotType === chatbotTypes.DISCUSSION) {
    descriptionPiecesObject = onboardingData.discussionBotSettings;
  } else if (chatbotType === chatbotTypes.QUESTION_ANSWER) {
    descriptionPiecesObject = onboardingData.questionAnswerBotSettings;
  }

  const description = Object.values(descriptionPiecesObject).join("");

  return description;
};

export const composeConfigObject = (
  onboardingData: OnboardingData,
  chatbotType: string
) => {
  let configObject = {} as any;

  if (chatbotType === chatbotTypes.DISCUSSION) {
    configObject = onboardingData.discussionBotSettings;
  } else if (chatbotType === chatbotTypes.QUESTION_ANSWER) {
    configObject = onboardingData.questionAnswerBotSettings;
  } else if (chatbotType === chatbotTypes.GUIDED_CONVERSATION) {
    configObject = onboardingData.guidedConversationsBotSettings;
  }

  return {
    ...configObject,
    bot_type: chatbotType,
  };
};
