import emptyApi from "../apiSlice";
import { Project } from "@/typings/project";

interface CreateProjectRequestBody {
  name: string;
  workspace_id: string;
}

interface UpdateProjectRequestBody {
  id: string;
  name: string;
  workspace_id: string;
}

export const projectsApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    getCurrentProjectDetails: builder.query<Project, string>({
      query: (projectId: string) => ({
        url: `project/${projectId}/details/`,
        method: "GET",
      }),
      providesTags: (result, error, arg) => [{ type: "Projects", id: arg }],
    }),

    listProjects: builder.query<Project[], string>({
      query: (workspaceId: string) => ({
        url: "projects/list/",
        params: { workspace_id: workspaceId },
      }),
      providesTags: (result = [], error, arg) => [
        "Projects",
        ...result.map(({ id }) => ({ type: "Projects", id }) as const),
      ],
    }),
    createProject: builder.mutation<Project, CreateProjectRequestBody>({
      query: (project) => ({
        url: "project/create/",
        method: "POST",
        data: project,
      }),
      invalidatesTags: ["Projects"],
    }),
    updateProject: builder.mutation<Project[], UpdateProjectRequestBody>({
      query: (project) => ({
        url: "projects/create/",
        method: "POST",
        data: project,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: "Projects", id: arg.id },
      ],
    }),
    deleteProject: builder.mutation<null, string>({
      query: (projectId: string) => ({
        url: `project/${projectId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: ["Projects"],
    }),
    resetProjectToken: builder.mutation<string, string>({
      query: (projectId) => ({
        url: `project/${projectId}/update-token/`,
        method: "PUT",
      }),
      invalidatesTags: (result, error, arg) => [{ type: "Projects", id: arg }],
    }),
  }),
});

export const {
  useGetCurrentProjectDetailsQuery,
  useLazyListProjectsQuery,
  useListProjectsQuery,
  useCreateProjectMutation,
  useUpdateProjectMutation,
  useDeleteProjectMutation,
  useResetProjectTokenMutation,
} = projectsApiSlice;
