import axiosInstance from "@/api/axios";
import { Dataset } from "@/typings/dataset";

const fetchTaggedMessages = async (activeDataset: Dataset) => {
  const response = await axiosInstance.get(
    `/api/project/get_dataset_tagged_messages/?dataset_id=${activeDataset.id}`,
  );

  const taggedMessages = response.data;
  return taggedMessages;
};

const removeTaggedMessage = async (taggedMessageId: string) => {
  const response = await axiosInstance.post(
    `/api/project/understanding_remove_tagged_message/?tagged_message_id=${taggedMessageId}`,
  );
  return response;
};

const retrainDataset = async (projectId: string) => {
  const body = JSON.stringify({
    project_id: projectId,
    // description: newDatasetDescription,
  });

  const response = await axiosInstance.post(
    `/api/project/retrain_model/`,
    body,
  );
  return response;
};

export { fetchTaggedMessages, removeTaggedMessage, retrainDataset };
