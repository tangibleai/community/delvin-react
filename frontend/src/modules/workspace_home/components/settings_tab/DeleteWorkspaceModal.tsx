import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";

import { IoReload } from "react-icons/io5";
import { useToast } from "@/components/ui/use-toast";
import { useNavigate, useParams } from "react-router-dom";
import { updateUser } from "@/lib/api";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/store";
import { loadUserSuccess } from "@/slices/authSlice";
import {
  useDeleteWorkspaceMutation,
  useGetWorkspaceQuery,
  useListWorkspacesQuery,
} from "@/slices/api/workspacesApiSlice";

function DeleteWorkspaceModal() {
  const navigate = useNavigate();
  const dispatch: AppDispatch = useDispatch();
  const { toast } = useToast();

  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;

  const user = useSelector((state: RootState) => state.auth.user);

  const { data: workspaces } = useListWorkspacesQuery(user!.id);
  const { data: activeWorkspace } = useGetWorkspaceQuery(workspaceId);
  const [deleteWorkspace] = useDeleteWorkspaceMutation();

  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const userId = useSelector((state: RootState) => state.auth.user?.id);

  useEffect(() => {
    // Handles resetting the form after submit or x
    // Can't directly access x in Dialog
    if (!isModalOpen) {
      form.reset();
    }
    setError("");
    setLoading(false);
  }, [isModalOpen]);

  // async function onWorkspaceDelete() {
  //   if (!workspaces) return;

  //   if (workspaces.length < 2) {
  //     setError(
  //       "User must have at least one workspace. Create a new one before deleting this."
  //     );
  //     return;
  //   }

  //   setLoading(true);
  //   setError("");

  //   try {
  //     await deleteWorkspace(activeWorkspace!);
  //     setIsModalOpen(false);
  //     toast({
  //       title: "Success!",
  //       description: "Workspace was deleted successfully.",
  //     });
  //     const newActiveWorkspace = workspaces.filter(
  //       (w) => w.id !== activeWorkspace!.id
  //     )[0];
  //     const updatedUser = await updateUser(userId!, {
  //       newCurrentWorkspaceId: newActiveWorkspace.id,
  //     });
  //     dispatch(loadUserSuccess(updatedUser));
  //     return navigate(`/workspace/${newActiveWorkspace.id}`);
  //   } catch (err) {
  //     setError(
  //       "An error occurred while deleting the workspace. Please try again."
  //     );
  //   }

  //   setLoading(false);
  // }

  async function onWorkspaceDelete() {
    if (!workspaces) return;

    if (workspaces.length < 2) {
      setError(
        "User must have at least one workspace. Create a new one before deleting this."
      );
      return;
    }

    setLoading(true);

    try {
      await deleteWorkspace(workspaceId).unwrap();
    } catch (error) {
      setError(
        "An error occurred while deleting the workspace. Please try again."
      );
      setLoading(false);
      return;
    }

    if (error !== "") setError("");

    setIsModalOpen(false);
    toast({
      title: "Success!",
      description: "Workspace was deleted successfully.",
    });
    const newActiveWorkspace = workspaces.filter(
      (w) => w.id !== activeWorkspace!.id
    )[0];
    const updatedUser = await updateUser(userId!, {
      newCurrentWorkspaceId: newActiveWorkspace.id,
    });
    dispatch(loadUserSuccess(updatedUser));
    setLoading(false);
    return navigate(`/workspace/${newActiveWorkspace.id}`);
  }

  const confirmationString = `delete ${activeWorkspace?.name}`;

  const formSchema = z.object({
    userDeleteConfirmation: z
      .string()
      .min(8, {
        message: "Please type the confirmation phrase",
      })
      .refine((val) => val.includes(`${confirmationString}`), {
        message: `The confirmation phrase must be "${confirmationString}"`,
      })
      .refine((val) => !/\s+$/.test(val), {
        message: "The confirmation phrase should not end with spaces.",
      }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      userDeleteConfirmation: "",
    },
  });

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button variant="destructive" className="w-fit">
          Delete workspace
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-md w-fit">
        <DialogHeader>
          <DialogTitle>Delete workspace</DialogTitle>
        </DialogHeader>
        {error && (
          <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
            Error: {error}
          </p>
        )}
        <p>This action cannot be undone.</p>

        <p>
          All data associated with this workspace will be permanently deleted
          and unrecoverable.
        </p>

        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onWorkspaceDelete)}
            className="space-y-8"
          >
            <FormField
              control={form.control}
              name="userDeleteConfirmation"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>
                    Type <pre className="inline">{confirmationString}</pre> to
                    confirm.
                  </FormLabel>
                  <FormControl>
                    <Input placeholder={confirmationString} {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <DialogFooter>
              <Button
                disabled={loading}
                type="submit"
                variant="destructive"
                className="order-2 sm:order-1"
              >
                {loading && (
                  <IoReload className={"mr-2 h-4 w-4 animate-spin"} />
                )}
                {loading ? "Deleting..." : "Delete workspace"}
              </Button>
              <Button
                type="button"
                variant="outline"
                className="order-1 sm:order-2"
                onClick={() => setIsModalOpen(false)}
              >
                Cancel
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}

export default DeleteWorkspaceModal;
