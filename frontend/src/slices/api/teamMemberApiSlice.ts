import emptyApi from "../apiSlice";
import { Member } from "@/typings/member";
import { Workspace } from "@/typings/workspace";

type GetTeamMembersRequestParams = {
  workspace_id: string;
  email?: string;
  role?: string;
};

type CreateTeamMemberRequestBody = {
  email: string;
  role: string;
  workspace_id: string | undefined;
};

type UpdateTeamMemberRoleRequestBody = {
  id: number;
  role: string;
};

const teamMemberApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    getTeamMembers: builder.query<Member[], GetTeamMembersRequestParams>({
      query: (requestParams) => ({
        url: `team-members/`,
        method: "GET",
        params: requestParams,
      }),
      providesTags: ["TeamMember"],
    }),

    createTeamMember: builder.mutation<Member, CreateTeamMemberRequestBody>({
      query: (requestBody) => ({
        url: "team-members/create/",
        method: "POST",
        data: requestBody,
      }),
      invalidatesTags: ["TeamMember"],
    }),
    updateTeamMemberRole: builder.mutation<
      Member,
      UpdateTeamMemberRoleRequestBody
    >({
      query: (requestBody) => ({
        url: `team-members/${requestBody.id}/update/`,
        method: "PUT",
        data: requestBody,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: "TeamMember", id: arg.id },
      ],
    }),
    retrieveTeamList: builder.query<Member[], Workspace>({
      async queryFn(workspace, _queryApi, _extraOptions, fetchWithBQ) {
        try {
          // Query backend for team members
          const membersResult = await fetchWithBQ({
            url: `team-members/?workspace_id=${workspace.id}`,
            method: "GET",
          });

          if (membersResult.error) throw membersResult.error;

          const ownerResult = await fetchWithBQ({
            url: `workspace/${workspace.id}/details/`,
            method: "GET",
          });

          if (ownerResult.error) throw ownerResult.error;

          const workspaceDetails: Workspace = ownerResult.data as Workspace;

          // Combine the results with the owner data at the beginning
          const ownerJson = {
            id: 0,
            delvin_user: workspaceDetails.owner,
            role: "Owner",
          } as Member;

          const allTeamMembers = membersResult.data as Member[];
          allTeamMembers.unshift(ownerJson);

          return { data: allTeamMembers };
        } catch (error) {
          return { error: { status: "CUSTOM_ERROR", error } };
        }
      },
      providesTags: ["TeamMember"],
    }),
    deleteTeamMember: builder.mutation<null, number>({
      query: (teamMemberId: number) => ({
        url: `team-members/${teamMemberId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: ["TeamMember"],
    }),
  }),
});

export const {
  useGetTeamMembersQuery,
  useCreateTeamMemberMutation,
  useUpdateTeamMemberRoleMutation,
  useRetrieveTeamListQuery,
  useDeleteTeamMemberMutation,
} = teamMemberApiSlice;
