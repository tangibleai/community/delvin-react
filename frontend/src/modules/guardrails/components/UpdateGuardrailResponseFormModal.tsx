import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Textarea } from "@/components/ui/textarea";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { processError } from "@/lib/utils";
import { GuardrailResponse } from "@/typings/guardrail_response";
import { useEffect, useState } from "react";
import { MdOutlineEdit } from "react-icons/md";
import { useUpdateGuardrailActionMutation } from "@/slices/api/guardrailResponsesApiSlice";
import { GuardrailAction } from "@/typings/guardrail_action";

type Props = {
  guardrailResponse: GuardrailResponse;
};

export default function UpdateGuardrailResponse({ guardrailResponse }: Props) {
  const [updateGuardrailAction, { isError, error }] =
    useUpdateGuardrailActionMutation();
  const [open, setOpen] = useState(false);
  const [responseTypeSetting, setResponseTypeSetting] = useState(
    guardrailResponse.guardrail_action.action_type
  );
  const [textAreaLabel, setTextAreaLabel] = useState("Message");

  const formSchema = z.object({
    responseType: z.string().min(1, {
      message: "Please select an action type",
    }),
    content: z.string().min(2, {
      message: "Please write a message that's at least 2 characters.",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      responseType: responseTypeSetting,
      content: guardrailResponse.guardrail_action.content,
    },
  });

  const onSubmit = async (data: z.infer<typeof formSchema>) => {
    // Updates guardrail action content and action type
    // Does not updates message tag definition.
    // Upon update, renews the guardrail responses.
    const updatedAction: GuardrailAction = {
      id: guardrailResponse.guardrail_action.id!,
      action_type: data.responseType,
      content: data.content,
    };
    await updateGuardrailAction(updatedAction).unwrap();
    if (isError) {
      processError(
        `There was an error updating guardrail responses. Details: ${error}`
      );
    }
    setOpen(false);
    form.reset();
  };

  function handleOpenChange(e: any) {
    // Handles the modal visibility
    setOpen((prev: boolean) => !prev);
    form.reset();
  }

  useEffect(() => {
    if (selectedOption == "scripted") {
      setTextAreaLabel("Message to send");
    } else if (selectedOption == "generative") {
      setTextAreaLabel("LLM instructions");
    } else {
      setTextAreaLabel("Message");
    }
  });

  const { watch, register } = form;
  const selectedOption = watch("responseType", responseTypeSetting);

  return (
    <Dialog open={open} onOpenChange={(e: any) => handleOpenChange(e)}>
      <DialogTrigger asChild>
        <Button variant="ghost">
          <MdOutlineEdit />
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px]">
        <DialogHeader>
          <DialogTitle>Update response</DialogTitle>
        </DialogHeader>
        <div className="space-y-1">
          <Label htmlFor="trigger">Trigger</Label>
          <Input
            id="trigger"
            value={guardrailResponse?.message_tag_definition?.name}
            disabled={true}
          />
        </div>
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onSubmit)}
            className="w-2/3 space-y-6"
          >
            <FormField
              control={form.control}
              name="responseType"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Response type</FormLabel>
                  <Select
                    defaultValue={field.value}
                    onValueChange={field.onChange}
                  >
                    <FormControl>
                      <SelectTrigger>
                        <SelectValue />
                      </SelectTrigger>
                    </FormControl>
                    <SelectContent>
                      <SelectItem value="scripted">
                        pre-defined message
                      </SelectItem>
                      {/* <SelectItem value="generative">
                        llm-generated message
                      </SelectItem> */}
                    </SelectContent>
                  </Select>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="content"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>{textAreaLabel}</FormLabel>
                  <FormControl>
                    <Textarea className="resize-none" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <Button type="submit">Update response</Button>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}
