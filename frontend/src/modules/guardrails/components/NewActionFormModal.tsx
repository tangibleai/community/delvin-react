import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Progress } from "@/components/ui/progress";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Textarea } from "@/components/ui/textarea";
import { useToast } from "@/components/ui/use-toast";
import { Response } from "@/slices/responseSlice";
import { GuardrailResponse, NewGuardrailResponse } from "@/typings/guardrail_response";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { z } from "zod";
import { useCreateGuardrailResponseMutation } from "@/slices/api/guardrailResponsesApiSlice";
import { MessageTagDefinition } from "@/typings/message_tag_definition";


interface Props {
  guardrailResponses: GuardrailResponse[]; // guardrail responses
  tagDefinitionOpen: boolean; // state to open tag definition modal
  guardrailActionOpen: boolean; // state to open guardrail action modal
  setTagDefinitionOpen: (isOpen: boolean) => void; // Function to set tagDefinitionOpen state
  setGuardrailActionOpen: (isOpen: boolean) => void; // Function to set guardrailActionOpen state
}

export default function AddAction(props: Props) {
  const routeParams = useParams();
  const { toast } = useToast();
  const projectId: any = routeParams.projectId;
  const responseData: Response = useSelector((state: any) => state.response);
  const [createGuardrailResponse, { isLoading }] = useCreateGuardrailResponseMutation();
  const [textAreaLabel, setTextAreaLabel] = useState("Message");
  const [textAreaDescription, setTextAreaDescription] = useState("");

  const formSchema = z.object({
    responseType: z
      .string({
        required_error: "Please select a response option",
      })
      .min(1, "Please select a response option"),
    content: z
      .string({
        required_error: "Please give directions for what the bot should say",
      })
      .min(1, "Please give directions for what the bot should say"),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      responseType: "scripted",
      content: "",
    },
  });

  const onSubmit = async (data: z.infer<typeof formSchema>) => {
    // closing the modals
    props.setTagDefinitionOpen(false);
    props.setGuardrailActionOpen(false);

    const { responseType, content } = data;
    const { tagDescription, examples, tagName, tagID } = responseData;
    const messageTagDefinition: MessageTagDefinition = (tagID === "") ?
                {
                  id: "",
                  name: tagName,
                  description: tagDescription,
                  project: projectId,
                  message_tag_group: "",
                }
                :
                {
                  id: tagID,
                  name: "",
                  description: "",
                  project: "",
                  message_tag_group: "",
                }
                ;


    try {
      // Creating message tag definition, guardrail response, and training the model.
      const newGuardrailResponse : NewGuardrailResponse = {
        guardrail_action: {
          action_type: responseType,
          content: content,
          },
        project: projectId,
        message_tag_definition: messageTagDefinition,
        examples: examples,
        }
        await createGuardrailResponse(newGuardrailResponse);
      toast({
        variant: "default",
        title: "Guardrail response",
        description: "The guardrail response has been created successfully!",
      });
    } catch (error: any) {
      toast({
        variant: "destructive",
        title: "Guardrail response",
        description: "There was an error while creating guardrail response!",
      });
      console.error(error);
      throw error;
    }
  };

  function resetForm() {
    form.reset();
  }

  useEffect(() => {
    resetForm();
  }, [props.guardrailActionOpen]);

  useEffect(() => {
    if (selectedOption == "scripted") {
      setTextAreaLabel("Message to send");
      setTextAreaDescription("Write the message the bot should send");
    } else if (selectedOption == "generative") {
      setTextAreaLabel("LLM instructions");
      setTextAreaDescription(
        "Tell the LLM what kind of response it should make",
      );
    } else {
      setTextAreaLabel("Message");
      setTextAreaDescription("");
    }
  });

  const { watch, register } = form;
  const selectedOption = watch("responseType");

  return (
    <Dialog
      open={props.guardrailActionOpen}
      onOpenChange={props.setGuardrailActionOpen}
    >
      <DialogContent className="sm:max-w-[900px]">
        <DialogHeader>
          <DialogTitle>Create new response</DialogTitle>
          <div className="flex flex-col items-center">
            <Progress value={100} className="w-[60%] my-4" />
            <h3>Step 2: Add action</h3>
          </div>
        </DialogHeader>
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onSubmit)}
            className="space-y-6 min-h-[540px]"
          >
            <div className="flex flex-col">
              <FormField
                control={form.control}
                name="responseType"
                render={({ field }) => (
                  <FormItem>
                    <div className="flex flex-row p-2">
                      <div className="flex flex-col w-1/4 p-2">
                        <FormLabel className="">Response type</FormLabel>
                        <FormDescription>
                          Select how the response will be made
                        </FormDescription>
                      </div>
                      <div className="flex flex-col w-3/4 p-2">
                        <Select
                          defaultValue={field.value}
                          onValueChange={field.onChange}
                        >
                          <FormControl>
                            <SelectTrigger>
                              <SelectValue placeholder="Select action type" />
                            </SelectTrigger>
                          </FormControl>
                          <SelectContent>
                            <SelectItem value="scripted">
                              pre-defined message
                            </SelectItem>
                            {/* <SelectItem value="generative">
                              llm-generated message
                            </SelectItem> */}
                          </SelectContent>
                        </Select>
                        <FormMessage />
                      </div>
                    </div>
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="content"
                render={({ field }) => (
                  <FormItem>
                    <div className="flex flex-row p-2">
                      <div className="flex flex-col w-1/4 p-2">
                        <FormLabel>{textAreaLabel}</FormLabel>
                        <FormDescription>{textAreaDescription}</FormDescription>
                      </div>
                      <div className="flex flex-col w-3/4 p-2">
                        <FormControl>
                          <Textarea
                            placeholder="Sorry, I am not able to respond to that."
                            className="resize-none"
                            {...field}
                          />
                        </FormControl>
                        <FormMessage />
                      </div>
                    </div>
                  </FormItem>
                )}
              />
              <div className="flex flex-row justify-evenly">
                <Button type="submit" className="w-32">
                  Create response
                </Button>
              </div>
            </div>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}
