export interface Project {
  id: string;
  name: string;
  token_key: string;
  activated: boolean;
  workspace: string;
  config: {
    guardrails: {
      builtin: {
        enabled: boolean;
        categories: Record<string, BuiltinCategory>;
      };
    };
  };
}

export interface BuiltinCategory {
  code: string;
  name: string;
  enabled: boolean;
  response: string;
}
