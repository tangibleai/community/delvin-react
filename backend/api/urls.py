from django.urls import path, include

urlpatterns = [
    # Following endpoints should be prefixed as internal/ and external.
    # But that change needs frontend update, will be safer to do as a second phase.
    # path("external/", include("api.external.urls")),
    # path("internal/", include("api.internal.urls")),
    path("", include("api.external.urls")),
    path("", include("api.internal.urls")),
]
