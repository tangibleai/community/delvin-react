import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "@/components/ui/button";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { RootState } from "@/store";
import { setNodeSidebar } from "@/slices/flowSlice";
import { Textarea } from "@/components/ui/textarea";
import { useReactFlow } from "reactflow";
import { cn } from "@/lib/utils";
import { BsArrowRightShort } from "react-icons/bs";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";

function PromptNodeSidebar() {
  const { setNodes } = useReactFlow();
  const [status, setStatus] = useState(false);

  const dispatch = useDispatch();

  const formSchema = z.object({
    nodename: z
      .string()
      .min(2, {
        message: "Node name must be at least 2 characters.",
      })
      .refine((val) => val !== "start", {
        message: "Name 'start' is reserved.",
      }),
    prompt: z.string().min(2, {
      message: "Prompt must be at least 2 characters.",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      nodename: "",
      prompt: "",
    },
  });

  const { reset } = form;

  const activeNode = useSelector((state: RootState) => state.flow.activeNode);

  useEffect(() => {
    if (activeNode.data) {
      reset({
        nodename: activeNode.data.nodeName || "",
        prompt: activeNode.data.nodePrompt || "",
      });
    }
  }, [activeNode, reset]);

  async function onSubmit(values: z.infer<typeof formSchema>) {
    const { nodename, prompt } = values;
    setNodes((nodes) =>
      nodes.map((node) =>
        node.id === activeNode.id
          ? {
            ...node,
            data: {
              ...node.data,
              nodeName: nodename,
              nodePrompt: prompt,
            },
          }
          : { ...node }
      )
    );
  }

  const nodeSidebar = useSelector((state: RootState) => state.flow.nodeSidebar);

  const handleToggle = () => {
    setStatus(true);
    dispatch(
      setNodeSidebar({
        nodeType: nodeSidebar.nodeType,
        opened: !nodeSidebar.opened,
      })
    );
    setTimeout(() => setStatus(false), 500);
  };

  const sidebarOpened =
    nodeSidebar.opened &&
    nodeSidebar.nodeType === nodeNameToTypeMapping.PROMPT_NODE;

  return (
    <>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="space-y-8 w-full px-4"
        >
          <FormField
            control={form.control}
            name="prompt"
            render={({ field }) => (
              <FormItem>
                <FormLabel className="text-3xl">Prompt</FormLabel>
                <FormControl>
                  <Textarea
                    placeholder="My New Node Prompt"
                    {...field}
                    rows={5}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <div className="flex justify-end gap-2">
            <Button type="submit">Update</Button>
            <Button variant="outline" onClick={handleToggle}>
              Cancel
            </Button>
          </div>
        </form>
      </Form>
    </>
  );
}

export default PromptNodeSidebar;
