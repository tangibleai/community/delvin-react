export const MemberRoles = {
  OWNER: "Owner",
  EDITOR: "Editor",
  VIEWER: "Viewer",
} as const;

type MemberRole = (typeof MemberRoles)[keyof typeof MemberRoles];

export interface Member {
  id: number;
  delvin_user: string;
  role: MemberRole;
  workspace: any;
  verification_status: string;
  properties: any;
}
