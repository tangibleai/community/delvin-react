import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import dotenv from "dotenv";
import tsconfigPaths from "vite-tsconfig-paths";

dotenv.config({ path: "../.env" });

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), tsconfigPaths()],
  publicDir: "./public",
  base: process.env.DEBUG === "True" ? "/" : "/static/",

  build: { manifest: true },
  server: {
    host: "127.0.0.1",
    strictPort: true,
    port: 3000,
  },
});
