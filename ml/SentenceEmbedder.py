import logging
import os
from sentence_transformers import SentenceTransformer

logger = logging.getLogger(__name__)
logger.setLevel(level=os.environ.get("LOG_LEVEL", "WARNING"))
handler = logging.StreamHandler()
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)


class SentenceEmbedder:
    """
    Model template. You can load your model parameters in __init__ from a location accessible at runtime
    """

    def __init__(self):
        """
        Add any initialization parameters. These will be passed at runtime from the graph definition parameters defined in your seldondeployment kubernetes resource manifest.
        """
        self.model = SentenceTransformer("all-MiniLM-L6-v2")

    def predict(self, X, feature_names):
        """
        Return a prediction.

        Parameters
        ----------
        X : array-like
        feature_names : array of feature names (optional)
        """
        logger.debug(X)
        logger.debug(feature_names)
        embeddings = self.model.encode(X)
        logger.debug(embeddings)

        return embeddings

    def send_feedback(self,features,feature_names,reward,truth,routing):
        """
        Handle feedback

        Parameters
        ----------
        features : array - the features sent in the original predict request
        feature_names : array of feature names. May be None if not available.
        reward : float - the reward
        truth : array with correct value (optional)
        """
        logger.debug("Send feedback called")
        return []
