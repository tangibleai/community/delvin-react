## Cetin
* [X] Complete the merge !87 and pull from staging
* [X] Tests for Guardrail Response backend 
* [X] Split views.py, serializers.py and models.py according to hierarchy
* [X] Check if the combined URL's work with the generic views.
    - It does not work. generics.ListCreateAPIView might work. This can combine list and create views.

## Vlad
* [ ] Finish the Queue tab in understanding
* [ ] Make the dataset copying faster 
* [ ] Tests for understanding functionality and for Chatbot Response

## Maria
* [ ] Send hierarachy of folders to Cetin
* [ ] Complete the chatbot engine 
* [ ] Designs for next sprint plan
