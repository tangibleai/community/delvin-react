// import { AiOutlineDelete } from "react-icons/ai";
import { Badge } from "@/components/ui/badge";
import { TaggedMessage } from "@/typings/tagged_message";

type Props = {
  taggedMessage: TaggedMessage;
  // onTaggedMessageDeleted: (taggedMessageId: string) => Promise<void>;
};

const DatasetTaggedMessage = ({
  taggedMessage, // onTaggedMessageDeleted,
}: Props) => {
  return (
    <div className="flex flex-col p-2 mb-2 bg-white rounded shadow cursor-pointer">
      <div className="flex justify-between items-center mb-2">
        <p>{taggedMessage.message.text}</p>
        {/* <AiOutlineDelete
          size={"1.5rem"}
          onClick={() => {
            onTagDeleted(taggedMessage.id);
          }}
        /> */}
      </div>
      <div className="flex flex-wrap">
        {taggedMessage.message_tags.length > 0 &&
          taggedMessage.message_tags.map((tag, index) => (
            <Badge key={index}>{tag.name}</Badge>
          ))}
      </div>
    </div>
  );
};

export { DatasetTaggedMessage };
