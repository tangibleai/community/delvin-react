from api.models import Contact
from api.etl import utils


def process(df_or_records_list, *args, **kwargs):
    contacts = df_or_records_list
    if not isinstance(contacts, list):
        contacts = df_or_records_list.to_dict("records")
    required_fields = {"origin", "project__name", "project__owner__email"}
    utils.check_missing_fields(contacts, required_fields)

    contacts = utils.resolve_user_for_each_record(
        contacts, user_email_field_name="project__owner__email"
    )
    contacts = utils.resolve_project_for_each_record(contacts)
    check_duplicated_contacts(contacts)
    db_records = save_contacts_to_db(contacts)

    return db_records


def check_duplicated_contacts(contacts):
    for contact in contacts:
        origin_name = contact["origin"]
        project_rec = contact["project_rec"]

        duplicated_contacts = Contact.objects.filter(
            origin=origin_name, project=project_rec
        )
        if duplicated_contacts:
            project_name = contact["project__name"]
            raise ValueError(
                f'Contact with "{origin_name}" origin already exists for "{project_name}" project'
            )

    return []


def save_contacts_to_db(contacts):
    initialized_records = [
        Contact(origin=c["origin"], project=c["project_rec"], internal=True)
        for c in contacts
    ]
    saved_records = Contact.objects.bulk_create(initialized_records)

    return saved_records
