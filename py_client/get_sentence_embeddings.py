from pprint import pprint

import numpy as np
import requests

url = "https://ml.delvin.to/api/v1.0/predictions"
# url = "http://ml.delvin.to/api/v1.0/doc"
# url = "http://ml.delvin.to/seldon/default/delvin-sentence-embedder<model-name>/api/v1.0/doc/"
# http://<ingress_url>/seldon/<namespace>/<model-name>/api/v1.0/doc/

sentences = ["The sky is blue."]

data = {"data": {"ndarray": sentences}}

r = requests.post(url=url, json=data)

print("status code: ", r.status_code)

try:
    embedding = r.json()["data"]["ndarray"]
    pprint(embedding)
    print("shape: ", np.array(embedding).shape)
    print("dim: ", np.array(embedding).ndim)
except Exception as err:
    print(err)
