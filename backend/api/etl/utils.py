import os

import pandas as pd

from api import models
from core.constant import CONSTANTS


def get_df(csv_path="normalized_utterances_labels.csv"):
    """Returns a dataframe converted from csv Returns dataframe got from csv file

    Args:
        csv_path (Path): Name of file inside CONSTANTS.DATA_DIR directory which should be converted to Dataframe
    Returns:
        `pandas.DataFrame`: Dataframe
    Raises:
        FileNotFound: If filename or path is invalid
    """

    csv_path = CONSTANTS.DATA_DIR / csv_path

    if not os.path.exists(csv_path):
        raise FileNotFoundError(
            f"csv file not found. Please, verify the path {csv_path} or file name."
        )

    df = pd.read_csv(csv_path)

    return df


def check_missing_fields(records, required_fields):
    for rec in records:
        missing_fields = set(required_fields) - set(rec)
        if missing_fields:
            raise ValueError(
                f"Missing {[f for f in missing_fields]} columns for record: {rec}"
            )

    return []


def resolve_user_for_each_record(
    records_as_dicts, user_email_field_name="owner__email"
):
    for r in records_as_dicts:
        user_email = r[user_email_field_name]
        try:
            r["owner_rec"] = models.UserAccount.objects.get(email=user_email)
        except models.UserAccount.DoesNotExist:
            raise ValueError(f'User with "{user_email}" email not found')

    return records_as_dicts


def resolve_project_for_each_record(
    records_as_dicts,
    project_name_field_name="project__name",
    user_email_field_name="project__owner__email",
):
    for rec in records_as_dicts:
        project_name = rec[project_name_field_name]
        user_rec = rec["owner_rec"]
        try:
            rec["project_rec"] = models.Project.objects.get(
                name=project_name, workspace__owner__email=user_rec
            )
        except models.Project.DoesNotExist:
            user_email = rec[user_email_field_name]
            raise ValueError(
                f'Project with "{project_name}" name and "{user_email}" user not found'
            )

    return records_as_dicts
