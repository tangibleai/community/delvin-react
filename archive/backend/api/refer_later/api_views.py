class ProjectAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        serializer = ProjectApiSerializer(data=request.data, many=True)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        try:
            db_records = project.process(serializer.validated_data)
        except ValueError as exc:
            return Response(str(exc), status=status.HTTP_400_BAD_REQUEST)

        return Response(
            db_records,
            status=status.HTTP_201_CREATED,
        )


project_api_view = ProjectAPIView.as_view()


class UserAccountAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        response = process_request(
            request,
            UserAccountApiSerializer,
            UserAccount,
            UserCreateSerializer,
            user_account,
        )
        return response


user_account_api_view = UserAccountAPIView.as_view()


class ContactAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        response = process_request(
            request, ContactApiSerializer, Contact, ContactSerializer, contact
        )
        return response


contact_api_view = ContactAPIView.as_view()


class ChatAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        response = process_request(
            request, ChatApiSerializer, Chat, ChatSerializer, chat
        )
        return response


chat_api_view = ChatAPIView.as_view()


class MessageTagGroupAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        response = process_request(
            request,
            MessageTagGroupApiSerializer,
            MessageTagGroup,
            MessageTagGroupSerializer,
            message_tag_group,
        )
        return response


message_tag_group_api_view = MessageTagGroupAPIView.as_view()


class DatasetAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        response = process_request(
            request, DatasetApiSerializer, Dataset, DatasetSerializer, dataset
        )
        return response


dataset_api_view = DatasetAPIView.as_view()


class MLModelAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        response = process_request(
            request, MLModelApiSerializer, MLModel, MLModelSerializer, ml_model
        )
        return response


ml_model_api_view = MLModelAPIView.as_view()


class MessageTagDefinitionAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        response = process_request(
            request,
            MessageTagDefinitionApiSerializer,
            MessageTagDefinitionSerializer,
            MessageTagDefinition,
            message_tag_definition,
        )
        return response


message_tag_definition_api_view = MessageTagDefinitionAPIView.as_view()


def process_request(
    request,
    api_serializer_class,
    model_class,
    db_record_serializer_class,
    processor_module_name,
):
    serializer = api_serializer_class(data=request.data, many=True)
    if not serializer.is_valid():
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    try:
        db_records = processor_module_name.process(serializer.validated_data)
    except ValueError as exc:
        return Response(str(exc), status=status.HTTP_400_BAD_REQUEST)

    pk_list = [rec.pk for rec in db_records]
    queryset = model_class.objects.filter(pk__in=pk_list)

    db_records_serializer = db_record_serializer_class(queryset, many=True)
    return Response(
        db_records_serializer.data,
        status=status.HTTP_201_CREATED,
    )
