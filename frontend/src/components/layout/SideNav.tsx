import { Link, useLocation, useNavigate } from "react-router-dom";

import { cn } from "@/lib/utils";
import { buttonVariants } from "@/components/ui/button";

import { useEffect, useState } from "react";
import { NavItem } from "@/typings/nav_item";

interface SideNavProps {
  items: NavItem[];
  isOpen: boolean;
  setOpen?: (open: boolean) => void;
  className?: string;
}

export function SideNav({ items, isOpen, setOpen, className }: SideNavProps) {
  const location = useLocation();
  const pathParts = location.pathname.split("/");
  const lastPart = pathParts[pathParts.length - 1];
  const [openItem, setOpenItem] = useState("");
  const [lastOpenItem, setLastOpenItem] = useState("");

  useEffect(() => {
    if (isOpen) {
      setOpenItem(lastOpenItem);
    } else {
      setLastOpenItem(openItem);
      setOpenItem("");
    }
  }, [isOpen]);

  return (
    <nav className="space-y-2">
      {items.map((item) => (
        <>
          {isOpen ? (
            <div key={item.title}>
              <Link
                to={item.href}
                className={cn(
                  buttonVariants({ variant: "ghost" }),
                  "group relative flex h-12 justify-start",
                  lastPart === item.href && "bg-muted font-bold hover:bg-muted",
                )}
              >
                <item.icon
                  className={cn(
                    "h-5 w-5",
                    isOpen ? "fill-primary" : "fill-gray-500",
                  )}
                />
                <span className="absolute left-12 text-base duration-200">
                  {item.title}
                </span>
              </Link>
            </div>
          ) : (
            <div
              key={item.title}
              className={cn(
                buttonVariants({ variant: "ghost" }),
                "group relative flex h-12 justify-start",
                lastPart === item.href && "bg-muted font-bold hover:bg-muted",
              )}
            >
              <Link to={item.href}>
                <item.icon className="h-5 w-5 fill-gray-500" />
              </Link>
              <span
                className={cn(
                  "absolute left-12 text-base duration-200",
                  !isOpen && className,
                )}
              >
                {item.title}
              </span>
            </div>
          )}
        </>
      ))}
    </nav>
  );
}
