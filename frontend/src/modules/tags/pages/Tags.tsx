import { Button, Title } from "@tremor/react";
import React, { useState } from "react";
import CreateNewRuleModal from "../components/CreateNewRuleModal";

function Tags() {
  const [isNewRuleModalOpen, setIsNewRuleModalOpen] = useState(false);

  return (
    <div>
      <div className="flex flex-row justify-between mx-auto mt-20">
        <Title> Message Tag Rules </Title>
        <Button
          className="font-bold"
          onClick={() => setIsNewRuleModalOpen(true)}
        >
          + New Rule
        </Button>
      </div>

      <CreateNewRuleModal
        isNewRuleModalOpen={isNewRuleModalOpen}
        setIsNewRuleModalOpen={setIsNewRuleModalOpen}
      />
    </div>
  );
}

export default Tags;
