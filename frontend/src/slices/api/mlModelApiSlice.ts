import emptyApi from "../apiSlice";
import { MLModel } from "@/typings/ml_model";

const mlModelApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    getMLModel: builder.query<MLModel, string>({
      query: (projectId: string) => ({
        url: `ml-models/`,
        method: "GET",
        params: { project_id: projectId },
      }),
      providesTags: (result, error, arg) => [{ type: "MLModel", id: arg }],
    }),

    retrainMLModel: builder.mutation<MLModel, string>({
      query: (projectId) => ({
        url: "project/retrain_model/",
        method: "POST",
        data: {
          project_id: projectId,
        },
      }),
      invalidatesTags: ["MLModel"],
    }),
  }),
});

export const { useGetMLModelQuery, useRetrainMLModelMutation } =
  mlModelApiSlice;
