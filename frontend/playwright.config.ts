import { defineConfig } from "@playwright/test";

export default defineConfig({
  timeout: 30000,
  globalTimeout: 600000,
  reporter: "list",
  testDir: "./src/__tests__",

  webServer: {
    command: "npm run start:test",
    url: "http://127.0.0.1:3000",
  },
  use: {
    baseURL: "http://app.delvin.to",
  },
});
