import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@tremor/react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { MdEdit } from "react-icons/md";

import { Switch } from "@/components/ui/switch";
import { Label } from "@/components/ui/label";
import { RootState } from "@/store";
import EditModerationCategoryModal from "./EditModerationCategoryModal";
import { BuiltinCategory, Project } from "@/typings/project";
import { useToast } from "@/components/ui/use-toast";
import {
  useToggleBuiltinModerationMutation,
  useUpdateModerationCategoryMutation,
} from "@/slices/api/moderationApiSlice";
import { useParams } from "react-router-dom";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";

export default function BuiltinModeration() {
  const { toast } = useToast();
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const [dialogOpened, setDialogOpened] = useState(false);
  const [selectedModeration, setSelectedModeration] = useState<BuiltinCategory>(
    {
      code: "",
      name: "",
      enabled: false,
      response: "",
    }
  );

  const { data: project = {} as Project } =
    useGetCurrentProjectDetailsQuery(projectId);

  const [toggleBuiltinModeration] = useToggleBuiltinModerationMutation();
  const [updateModerationCategory] = useUpdateModerationCategoryMutation();

  const onEditButtonClicked = (moderation: BuiltinCategory) => {
    setDialogOpened(true);
    setSelectedModeration(moderation);
  };

  const onToggleBuiltinModeration = async (checked: boolean) => {
    try {
      await toggleBuiltinModeration({
        project_id: projectId,
        enabled: checked,
      }).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Couldn't toggle modaration. Try again later.",
      });
    }
  };

  const onToggleCategoryModeration = async (details: BuiltinCategory) => {
    try {
      await updateModerationCategory({
        project_id: projectId,
        details,
      }).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Couldn't toggle moderation category. Try again later.",
      });
      return;
    }

    toast({
      title: "Success!",
      description: "Moderation category was toggled!",
    });
  };

  const moderationEnabled =
    Object.keys(project).length > 0 &&
    Object.keys(project?.config).includes("guardrails")
      ? project?.config?.guardrails?.builtin?.enabled
      : false;

  return (
    <>
      <EditModerationCategoryModal
        dialogOpened={dialogOpened}
        setDialogOpened={setDialogOpened}
        moderation={selectedModeration}
      />
      <div className="flex flex-col flex-grow">
        <div className="flex items-center gap-4 mt-2">
          <Switch
            id="builtin moderation"
            checked={moderationEnabled}
            onCheckedChange={onToggleBuiltinModeration}
          />
          <Label htmlFor="builtin moderation" className="text-base">
            Built-in Moderation Enabled
          </Label>
        </div>

        {moderationEnabled ? (
          <Table className="mt-4 max-w-full">
            <TableHead>
              <TableRow>
                <TableCell>Enabled</TableCell>
                <TableCell>Category</TableCell>
                <TableCell>Response</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.entries(
                project?.config?.guardrails?.builtin?.categories ?? {}
              ).map(([category, details]) => (
                <TableRow className="hover:bg-slate-100">
                  <TableCell>
                    <Switch
                      checked={details.enabled}
                      onCheckedChange={() =>
                        onToggleCategoryModeration({
                          ...details,
                          enabled: !details.enabled,
                        })
                      }
                    />
                  </TableCell>
                  <TableCell>{details.name}</TableCell>
                  <TableCell className="max-w-xl">
                    <p className="text-wrap">{`${details.response.slice(
                      0,
                      60
                    )}...`}</p>
                  </TableCell>
                  <TableCell className="relative overflow-hidden">
                    <div>
                      <MdEdit
                        size="1.5rem"
                        className="cursor-pointer transition-all duration-200 hover:scale-125"
                        onClick={() => onEditButtonClicked(details)}
                      />
                    </div>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        ) : (
          <div className="w-full h-full pt-40 flex flex-grow justify-center items-center text-lg font-medium">
            Enable Built-in Moderation to configure responses to individual
            categories
          </div>
        )}
      </div>
    </>
  );
}
