import os
from uuid import uuid4

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from api.models import *
from api.tests import utils as test_utils  # since "utils" is reserved name in python
from api.tests import mock


class MessageTagDefinitionsAPITests(TestCase):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    @classmethod
    def setUpTestData(cls):
        set_up_data = test_utils.user_and_project_set_up(cls.client)
        cls.headers = set_up_data["headers"]
        cls.project = set_up_data["project"]

    def test_get_message_tag_definitions_unauthorized(self):
        url = reverse("message_tag_definitions_view")
        response = self.client.get(url, {"project_id": self.project.id})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_message_tag_definitions(self):
        url = reverse("message_tag_definitions_view")
        response = self.client.get(
            url, {"project_id": self.project.id}, headers=self.headers
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serialized_tags_definitions = response.json()
        self.assertGreater(
            len(serialized_tags_definitions),
            0,
            "Failed to fetch message tag definitions",
        )

    def test_post_message_tag_definitions_unauthorized(self):
        url = reverse("message_tag_definitions_view")
        data = {
            "project_id": self.project.id,
            "name": mock.mock["user1"]["message_tag_definition_name"],
            "description": "Description for new tag",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_message_tag_definitions_valid(self):
        url = reverse("message_tag_definitions_view")
        data = {
            "project_id": self.project.id,
            "name": mock.mock["user1"]["message_tag_definition_name"],
            "description": "Description for new tag",
        }
        response = self.client.post(url, data, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(
            MessageTagDefinition.objects.filter(
                name=mock.mock["user1"]["message_tag_definition_name"],
                project=self.project,
            ).exists()
        )

    def test_post_message_tag_definitions_invalid_group(self):
        url = reverse("message_tag_definitions_view")
        data = {
            "project_id": self.project.id,
            "name": mock.mock["user1"]["message_tag_definition_name"],
            "description": "Description for new tag",
            "message_tag_group_id": uuid4(),  # invalid UUID for group
        }
        response = self.client.post(url, data, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_message_tag_definitions_invalid_group_1(self):
        url = reverse("message_tag_definitions_view")
        data = {
            "project_id": self.project.id,
            "name": mock.mock["user1"]["message_tag_definition_name"],
            "description": "Description for new tag",
        }
        response = self.client.post(url, data, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        MessageTagDefinition.objects.filter(project=self.project).delete()

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()


class AlterMessageTagDefinitionsAPITests(TestCase):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    @classmethod
    def setUpTestData(cls):
        set_up_data = test_utils.user_and_project_set_up(cls.client)
        cls.headers = set_up_data["headers"]
        cls.project = set_up_data["project"]

        cls.tag_definition = MessageTagDefinition.objects.create(
            project=cls.project,
            name=mock.mock["user1"]["message_tag_definition_name"],
            description="A test tag",
        )

    def test_delete_message_tag_definition_unauthorized(self):
        url = reverse(
            "alter_message_tag_definition_view", kwargs={"pk": self.tag_definition.id}
        )
        response = self.client.delete(url)  # No auth headers passed
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_message_tag_definition_valid(self):
        tag_definition_id = self.tag_definition.id
        url = reverse(
            "alter_message_tag_definition_view", kwargs={"pk": tag_definition_id}
        )
        response = self.client.delete(url, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            MessageTagDefinition.objects.filter(id=tag_definition_id).exists()
        )

    def test_delete_message_tag_definition_invalid_pk(self):
        url = reverse("alter_message_tag_definition_view", kwargs={"pk": uuid4()})
        response = self.client.delete(url, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def tearDown(self):
        MessageTagDefinition.objects.filter(project=self.project).delete()

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()
