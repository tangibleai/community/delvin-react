#!/bin/bash
# TODO: Use Pytest to discover all tests
#       Configure pytest to run doctests with ELIPPSIS and NORMALIZE_WHITESPACE

/opt/venv/bin/python manage.py collectstatic --noinput
/opt/venv/bin/python manage.py migrate && echo || exit 1
#/opt/venv/bin/black . --check && echo || exit 1
/opt/venv/bin/coverage run manage.py test --exclude-tag=exclude_me -v 2 && echo || exit 1
/opt/venv/bin/coverage report
/opt/venv/bin/coverage xml -o coverage.xml
/opt/venv/bin/codecov --token "$CODECOV_TOKEN" --root "$CI_REPOSITORY_URL" --commit "$CI_COMMIT_SHA" --file coverage.xml
