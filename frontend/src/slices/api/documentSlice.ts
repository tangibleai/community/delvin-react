import { url } from "inspector";
import emptyApi from "../apiSlice";
import { Document } from "@/typings/document";

// Define our single API slice object
const documentApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    // GET documents
    getDocuments: builder.query<Document[], { chatbotId: string }>({
      query: ({ chatbotId }) => ({
        url: `documents/?chatbot_id=${chatbotId}`,
        method: "GET",
      }),
      providesTags: ["Document"],
    }),

    // CREATE documents
    createDocument: builder.mutation({
      query: (body) => {
        const headers =
          body.document_type == undefined || body.document_type == "file"
            ? { "Content-Type": "multipart/form-data" }
            : {};

        return {
          url: `documents/create/`,
          method: "POST",
          data: body,
          headers: headers,
        };
      },
      invalidatesTags: ["Document"],
    }),

    // UPDATE documents
    updateDocument: builder.mutation({
      query: (body) => {
        const headers =
          body.document_type == undefined || body.document_type == "file"
            ? { "Content-Type": "multipart/form-data" }
            : {};

        return {
          url: `documents/${body.document}/update/`,
          method: "PUT",
          data: body,
          headers: headers,
        };
      },
      invalidatesTags: ["Document"],
    }),

    // DELETE documents
    deleteDocument: builder.mutation({
      query: (id) => ({
        url: `documents/${id}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: ["Document"],
    }),
  }),
});

// Export the auto-generated hook for the `getPosts` query endpoint
export const {
  useGetDocumentsQuery,
  useCreateDocumentMutation,
  useUpdateDocumentMutation,
  useDeleteDocumentMutation,
} = documentApiSlice;
