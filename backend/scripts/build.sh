#!/usr/bin/env bash

echo "Starting migrations!"
python manage.py migrate

echo "Creating api schema"
python manage.py spectacular --file schema.yml

echo "Collecting static files!"
python manage.py collectstatic --noinput -v 2

# Create default user
echo "Creating super user!"
python -m scripts.create_super_user
