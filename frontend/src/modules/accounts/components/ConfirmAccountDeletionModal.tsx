import { Button } from "@/components/ui/button";
import { useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/store";
import { useToast } from "@/components/ui/use-toast";
import { deleteUser } from "@/lib/api";
import { useNavigate } from "react-router-dom";
import { logout } from "@/actions/auth";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";

const ConfirmAccountDeletionModal = () => {
  const navigate = useNavigate();
  const dispatch: AppDispatch = useDispatch();

  const [open, setOpen] = useState(false);

  const { toast } = useToast();

  const user = useSelector((state: RootState) => state.auth.user);

  const onAccountDelete = async () => {
    try {
      await deleteUser(user!.id);
      toast({
        title: "Success!",
        description: "Account was successfully deleted.",
      });
      setOpen(false);
      dispatch(logout());
      navigate(AUTH_PAGE_URLS.login);
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Couldn't delete account. Please, try again later.",
      });
      setOpen(false);
    }
  };

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button variant="destructive">Delete account</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Delete account</DialogTitle>
          <p className="text-sm text-gray-500">
            Deleting your account will remove your user account and all
            assistants that you own. Please make sure to add a new owner to any
            project you do not want deleted.
          </p>
        </DialogHeader>

        <DialogFooter>
          <Button
            variant="destructive"
            className="order-2 sm:order-1"
            onClick={onAccountDelete}
          >
            Delete
          </Button>
          <Button
            variant="outline"
            className="order-1 sm:order-2"
            onClick={() => setOpen(false)}
          >
            Cancel
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default ConfirmAccountDeletionModal;
