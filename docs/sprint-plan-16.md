## Cetin 

* [X] Investigate staging crash
* [X] Research set up uptime monitoring for staging
  * On GCP a uptime monitoring has been created. It is sending emails on crash.
* [X] Record video of checking Kubernetes node log
* [X] Research Prometheus and alert config
  * prometheus-community/kube-prometheus-stack has been installed
  * Alerts are not set.
* [X] Guardrail creation dialog - Maria will create issue and assign it. 

## Vlad 

* [ ] Improved onboarding 


## Maria 

*  [ ] Flows backend architecture design 
*  [ ] Reach out to Giuseppe and set up an email connection. 
