import { MessageTagDefinition } from "./message_tag_definition";

export interface UnderstandingQueueTag {
  tag: MessageTagDefinition;
  score?: number; // optional since manually selected tags won't have score until saved in db and predicted by api endpoint
  isConfirmed: boolean;
}
