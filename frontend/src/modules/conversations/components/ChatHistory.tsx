import React, { useState, useEffect, useRef, useCallback } from "react";
import { fetchMessages } from "@/lib/conversations";
import { MessageSeparator, Message } from "@chatscope/chat-ui-kit-react";
import moment from "moment";
import { Oval } from "react-loader-spinner";
import { MessageDirections } from "@/typings/message";
import { Message as MessageType } from "@/typings/message";

interface ChatHistoryProps {
  chatId: string;
}

const ChatHistory = ({ chatId }: ChatHistoryProps) => {
  const [messages, setMessages] = useState<MessageType[]>([]);
  const [loading, setLoading] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [initialLoadDone, setInitialLoadDone] = useState(false);
  const scrollContainerRef = useRef<HTMLDivElement>(null);

  const loadMessages = useCallback(
    async (initialLoad = false) => {
      if (loading) {
        return;
      }

      setLoading(true);

      try {
        const offset = initialLoad ? 0 : messages.length;
        const newMessages = await fetchMessages(chatId, offset, 20);
        console.log("messages", messages);

        if (newMessages.length < 20) {
          setHasMore(false);
        }

        if (!Array.isArray(newMessages)) {
          setLoading(false);
          console.error("Fetched messages is not an array:", newMessages);
          return;
        }

        // Ensure no duplicate messages are added
        setMessages((prevMessages) =>
          initialLoad ? newMessages : [...newMessages, ...prevMessages]
        );

        if (initialLoad && scrollContainerRef.current) {
          scrollContainerRef.current.scrollTop =
            scrollContainerRef.current.scrollHeight;
        }

        setLoading(false);
      } catch (error) {
        setLoading(false);
      }
    },
    [chatId, loading, messages]
  );

  useEffect(() => {
    if (!initialLoadDone) {
      loadMessages(true);
      setInitialLoadDone(true);
    }
  }, [loadMessages, initialLoadDone]);

  const handleScroll = () => {
    if (scrollContainerRef.current) {
      if (scrollContainerRef.current.scrollTop === 0 && hasMore && !loading) {
        loadMessages();
      }
    }
  };

  let lastMessageDate = "";

  return (
    <div
      ref={scrollContainerRef}
      onScroll={handleScroll}
      className="px-10 h-5/6 overflow-y-scroll"
    >
      {loading && (
        <div className="flex items-center justify-center">
          <Oval
            visible={true}
            height="30"
            color="#a29bfe"
            secondaryColor="#f6f5fe"
            ariaLabel="oval-loading"
            wrapperStyle={{}}
            wrapperClass=""
          />
        </div>
      )}
      {Array.isArray(messages) &&
        messages.map((message) => {
          const messageDate = moment(message.timestamp).format(
            "dddd, MMMM Do YYYY"
          );
          const showSeparator = messageDate !== lastMessageDate;
          lastMessageDate = messageDate;

          return (
            <div key={`${message.id}${message.timestamp}`}>
              {showSeparator && (
                <MessageSeparator
                  content={messageDate}
                  className="text-slate-500 text-base font-semibold"
                  key={message.timestamp}
                />
              )}
              <Message
                model={{
                  message: message.text,
                  direction:
                    message.direction == MessageDirections.OUTBOUND
                      ? "incoming"
                      : "outgoing",
                  position: 0,
                }}
                key={message.id}
              >
                <Message.Footer
                  sentTime={moment(message.timestamp).format("HH:mm")}
                />
              </Message>
            </div>
          );
        })}
    </div>
  );
};

export default ChatHistory;
