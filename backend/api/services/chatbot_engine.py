import os
from api.models import Chat, Chatbot, Flow, Message, ResponseLog, Provider
from api.services import flow_manager
from api.services.builtin_moderation_manager import BuiltinModerationManager
from api.services.generative_conversation_manager import GenerativeConversationManager
from api.services.guardrail_response_manager import GuardrailResponseManager
from api.services.rag.chat_knowledge_manager import (
    KnowledgeDocumentIndexManager,
    KnowledgeDocumentQueryResponder,
)
from api.utils.prompt_configuration import BotTypeEnum, ChatbotConfig, ProjectConfig


def engine_response(message: Message, chatbot: Chatbot, chat: Chat):
    context = None

    if not chatbot.provider.api_key:
        workspace = chatbot.project.workspace
        if not workspace.has_default_provider():
            return {
                "response_text": "We are sorry, but you didn't specify any provider for chatbot.",
                "source": "",
                "context": context,
            }

        chatbot.provider = Provider.get_default_provider()
        chatbot.save()
        chatbot.provider.api_key = os.environ["OPENROUTER_API_KEY"]

    # Custom Guardrails
    guardrail_response = GuardrailResponseManager(
        message.project, chatbot
    ).get_response(message)
    if guardrail_response:
        return {
            "response_text": guardrail_response,
            "source": ResponseLog.Source.GUARDRAILS,
            "context": context,
        }

    project_config = ProjectConfig.parse_obj(message.project.config)

    # Builtin Guardrails
    moderation_response = BuiltinModerationManager(
        message.project, project_config, chatbot
    ).get_response(message)
    if moderation_response:
        return {
            "response_text": moderation_response,
            "source": ResponseLog.Source.MODERATION,
            "context": context,
        }

    chatbot_config = ChatbotConfig.parse_obj(chatbot.config)
    # NOTE: backwards-compatibility for transitioning to new bot types
    if chatbot_config.bot_type == BotTypeEnum.GuidedConversations:
        flow_id = chatbot_config.flow
        flow = Flow.objects.get(id=flow_id)

        LLM_response = flow_manager.FlowManager(
            chat, chatbot_config, flow, message.text, chatbot
        ).get_response()

        return {
            "response_text": LLM_response,
            "source": ResponseLog.Source.FLOW,
            "context": context,
        }
    elif (
        chatbot_config.bot_type == BotTypeEnum.QuestionAnswer
        or chatbot_config.bot_type == BotTypeEnum.KnowledgeBased
    ):
        response, confidence_score = KnowledgeDocumentQueryResponder(
            message.project.id, chatbot.id
        ).answer_user_query(message.text, chatbot_config)
        return {
            "response_text": response,
            "source": ResponseLog.Source.KNOWLEDGE,
            "context": context,
        }

    else:
        if chatbot_config.knowledge.enabled:
            context, confidence = KnowledgeDocumentIndexManager(
                message.project.id
            ).provide_knowledge_context(message.text)
        else:
            context = None
        response = GenerativeConversationManager(
            message.project, chatbot_config, chatbot
        ).get_response(message, context)
        return {
            "response_text": response,
            "source": ResponseLog.Source.PROMPT,
            "context": context,
        }
