# Cetin 

* [X] Set up the Django Project + DRF
* [X] Make progress on the tutorial (JWT-based authentication: https://www.youtube.com/watch?v=QFDyXWRYQjY&list=PLJRGQoqpRwdfoa9591BcUS6NmMpZcvFsM Session-based authentication: https://www.youtube.com/watch?v=Bsf9WaU9wZ4&list=PLJRGQoqpRwddrFlVEQV16D1jdol40ETyx&index=3) on the backend side

# Maria 
* [x] Set up the React project with Vite
* [x] Make progress on the tutorial on the FrontEnd side.
