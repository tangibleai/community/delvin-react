export interface GuardrailAction {
  id?: string;
  action_type: string;
  content: string;
}
