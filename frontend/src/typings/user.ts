export interface User {
  id: string;
  email: string;
  name: string;
  current_workspace: string;
}
