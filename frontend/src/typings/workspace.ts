import { Plan } from "./plan";

export type WorkspaceFeatureFlagsField = {
  flows: boolean;
};

export interface Workspace {
  id: string;
  name: string;
  owner: string;
  properties: object;
  plan: Plan;
  feature_flags: WorkspaceFeatureFlagsField;
}

export type NewWorkspace = Pick<Workspace, "name" | "owner">;