1. Push all the code to staging
2. Create merge request
3. Make sure all tests are passing (unit, integratin and human tests). If possible run tests through staging data
4. Write release notes called changelog (list what did you do in merge request)
5. Create an annotated tag with `git tag` command with version number in it and push it to gitlab
