import os
from pathlib import Path

import requests
from dotenv import load_dotenv

ROOT = Path(__file__).resolve().parent.parent

load_dotenv(dotenv_path=os.path.join(ROOT / "py_client" / ".env"))

endpoint = "http://localhost:8000/auth/users/"

email = os.environ["USER"]
password = os.environ["PASSWORD"]

data = {
    "email": email,
    "name": email.split("@")[0],
    "password": password,
    "re_password": password,
}


response = requests.post(
    endpoint,
    json=data,
)

if response.status_code == 201:
    print(response.json())
else:
    print(response.status_code)
    print(response.content)
