import Cookies from "js-cookie";
import { processError } from "@/lib/utils";
import axios from "axios";
// axios.defaults.xsrfHeaderName = 'x-csrftoken'
// axios.defaults.xsrfCookieName = 'csrftoken'
// axios.defaults.withCredentials = true

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_REACT_APP_API_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

// Add a request interceptor
axiosInstance.interceptors.request.use(
  (config) => {
    const token = Cookies.get("access");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

axiosInstance.interceptors.response.use(
  (response) => response,
  async (error) => {
    const originalRequest = error.config;

    // If the error status is 401 and there is no originalRequest._retry flag,
    // it means the token has expired and we need to refresh it
    if (
      400 <= error.response.status &&
      error.response.status < 600 &&
      !originalRequest._retry
    ) {
      originalRequest._retry = true;

      try {
        const refreshToken = Cookies.get("refresh");
        const response = await axios.post(
          `${import.meta.env.VITE_REACT_APP_API_URL}/jwt/refresh`,
          { refresh: refreshToken }
        );
        if (response.status !== 200) {
          // Handle refresh token error or redirect to login
          console.log("Error while getting refresh token", response);
          return Promise.reject(error);
        }

        const { token } = response.data.access;

        // Store the new access token in a SameSite cookie
        Cookies.set("access", token, { sameSite: "Strict", secure: true });

        // Retry the original request with the new token
        originalRequest.headers.Authorization = `Bearer ${token}`;
        return axios(originalRequest);
      } catch (error) {
        processError(`Error while getting refresh token. Details: ${error}`);
      }
    }

    return Promise.reject(error);
  }
);

export default axiosInstance;
