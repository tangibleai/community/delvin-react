export interface MessageTagDefinition {
  id: string;
  name: string;
  description: string;
  project: string;
  message_tag_group: string;
}
