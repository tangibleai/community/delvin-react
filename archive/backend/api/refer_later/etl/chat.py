from api.models import Chat, Contact
from api.etl import utils


def process(df_or_records_list, *args, **kwargs):
    chats = df_or_records_list
    if not isinstance(chats, list):
        chats = df_or_records_list.to_dict("records")
    required_fields = (
        "title",
        "origin_id",
        "contact_id__origin",
        "contact_id__project__name",
        "contact_id__project__owner__email",
    )
    utils.check_missing_fields(chats, required_fields)

    chats = resolve_dependent_records(chats)
    check_duplicated_chats(chats)
    db_records = save_records_to_db(chats)

    return db_records


def resolve_dependent_records(chats):
    chats = utils.resolve_user_for_each_record(
        chats, user_email_field_name="contact_id__project__owner__email"
    )
    chats = utils.resolve_project_for_each_record(
        chats,
        project_name_field_name="contact_id__project__name",
        user_email_field_name="contact_id__project__owner__email",
    )
    chats = resolve_contact_for_each_record(chats)

    return chats


def resolve_contact_for_each_record(chats):
    for c in chats:
        project_rec = c["project_rec"]
        contact_origin_name = c["contact_id__origin"]
        try:
            c["contact_rec"] = Contact.objects.get(
                origin=contact_origin_name, project=project_rec
            )
        except Contact.DoesNotExist:
            user_email = c["contact_id__project__owner__email"]
            project_name = c["contact_id__project__name"]
            raise ValueError(
                f'Contact with "{contact_origin_name}" origin of "{project_name}" project with "{user_email}" owner doesn\'t exist'
            )

    return chats


def check_duplicated_chats(chats):
    optional_col = "is_default"

    for chat in chats:
        origin_name = chat["origin_id"]
        contact_rec = chat["contact_rec"]

        filter_by = dict(contact_id=contact_rec)

        filter_by[optional_col] = chat.get(optional_col)
        check_default_chat_duplication(chat, filter_by, optional_col)
        del filter_by[optional_col]

        filter_by["origin_id"] = origin_name

        duplicated_chats = Chat.objects.filter(**filter_by)
        if duplicated_chats:
            contact_origin_name = chat["contact_id__origin"]
            raise ValueError(
                f'Chat with "{origin_name}" origin already exists for "{contact_origin_name}" contact'
            )

    return []


def check_default_chat_duplication(chat, filter_by, optional_col):
    non_default_chat = not chat.get(optional_col)
    if non_default_chat:
        return False

    default_chat_exists = Chat.objects.filter(**filter_by)
    if default_chat_exists:
        project_name = chat["contact_id__project__name"]
        raise ValueError(f'Default chat already exists for "{project_name}" project')

    return False


def save_records_to_db(chats):
    initialized_records = [
        Chat(origin_id=c["origin_id"], title=c["title"], contact_id=c["contact_rec"])
        for c in chats
    ]
    saved_records = Chat.objects.bulk_create(initialized_records)

    return saved_records
