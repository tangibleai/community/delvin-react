import Cookies from "js-cookie";
import axios from "axios";

import {
  authenticatedSuccess,
  authenticatedFail,
  loginSuccess,
  loginFail,
  signupSuccess,
  signupFail,
  loadUserSuccess,
  loadUserFail,
  logoutSuccess,
  loading,
  passwordResetSuccess,
  passwordResetFail,
  passwordResetConfirmSuccess,
  passwordResetConfirmFail,
  activationSuccess,
  activationFail,
} from "../slices/authSlice";
import { processError } from "@/lib/utils";

export const checkAuthenticated = () => async (dispatch: any) => {
  if (Cookies.get("access")) {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const body = JSON.stringify({ token: Cookies.get("access") });
    try {
      const res = await axios.post(
        `${import.meta.env.VITE_REACT_APP_API_URL}/auth/jwt/verify`,
        body,
        config
      );
      if (res.data.code !== "token_not_valid") {
        dispatch(authenticatedSuccess());
        return true;
      }
    } catch (err) {
      dispatch(authenticatedFail());
      processError(`Failed to check if user is authenticated. Details: ${err}`);
      return false;
    }
  } else {
    dispatch(authenticatedFail());
    processError(`Failed to check if user is authenticated.`);
    return false;
  }
};

export const load_user = () => async (dispatch: any) => {
  if (Cookies.get("access")) {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${Cookies.get("access")}`,
        Accept: "application/json",
      },
    };

    try {
      const res = await axios.get(
        `${import.meta.env.VITE_REACT_APP_API_URL}/auth/users/me/`,
        config
      );

      const response = await axios.get(
        `${import.meta.env.VITE_REACT_APP_API_URL}/api/user/${
          res.data.id
        }/details/`,
        config
      );

      const user = response.data;

      // const user = await retrieveUser(res.data.id);
      dispatch(loadUserSuccess(user));
      return true;
    } catch (err) {
      dispatch(loadUserFail());
      processError(`Failed to load user info. Details: ${err}`);
      return false;
    }
  } else {
    dispatch(loadUserFail());
    processError("Failed to load user info.");
    return false;
  }
};

export const login =
  (email: string, password: string) => async (dispatch: any) => {
    dispatch(loading());
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const body = JSON.stringify({ email, password });
    try {
      const res = await axios.post(
        `${import.meta.env.VITE_REACT_APP_API_URL}/auth/jwt/create/`,
        body,
        config
      );
      dispatch(loginSuccess(res.data));
      return { isSuccessful: true, message: "" };
    } catch (err: any) {
      dispatch(loginFail());
      const errorMessage = err?.request?.response;
      processError(`Failed to log in user. Details: ${errorMessage}`);
      return { isSuccessful: false, message: errorMessage };
    }
  };

export const signup =
  (name: string, email: string, password: string, re_password: string) =>
  async (dispatch: any) => {
    dispatch(loading());
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const body = JSON.stringify({ name, email, password, re_password });

    try {
      const res = await axios.post(
        `${import.meta.env.VITE_REACT_APP_API_URL}/auth/users/`,
        body,
        config
      );
      dispatch(signupSuccess());
      return { isSuccessful: true, message: "" };
    } catch (err: any) {
      dispatch(signupFail());
      let errorMessage = err?.request?.response;
      processError(`Failed to sign up user. Details: ${errorMessage}`);
      return { isSuccessful: false, message: errorMessage };
    }
  };

export const verifyTeamMember =
  (
    uid: string,
    token: string,
    name: string,
    password: string,
    re_password: string
  ) =>
  async (dispatch: any) => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${Cookies.get("access")}`,
      },
    };

    const body = JSON.stringify({ uid, token, name, password, re_password });

    try {
      const res = await axios.post(
        `${
          import.meta.env.VITE_REACT_APP_API_URL
        }/api/team-members/${uid}/confirm-invitation/`,
        body,
        config
      );
      await dispatch(loadUserSuccess(res.data));
      await dispatch(login(res.data.email, password));
      return res;
    } catch (err) {
      dispatch(signupFail());
      processError(`Failed to verify team member. Details: ${err}`);
    }
  };

export const verify = (uid: string, token: string) => async (dispatch: any) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ uid, token });

  try {
    await axios.post(
      `${import.meta.env.VITE_REACT_APP_API_URL}/auth/users/activation/`,
      body,
      config
    );

    dispatch(activationSuccess());
  } catch (err) {
    dispatch(activationFail());
    processError(`Failed to activate user. Details: ${err}`);
  }
};

export const reset_password = async (email: string) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ email });
  let statusCode = 400;

  try {
    const response = await axios.post(
      `${import.meta.env.VITE_REACT_APP_API_URL}/auth/users/reset_password/`,
      body,
      config
    );

    statusCode = response.status;
  } catch (err) {
    processError(`Failed to reset password. Details: ${err}`);
  }

  return statusCode;
};

export const reset_password_confirm = async (
  uid: string,
  token: string,
  new_password: string,
  re_new_password: string
) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ uid, token, new_password, re_new_password });
  let statusCode = 400;

  try {
    const response = await axios.post(
      `${
        import.meta.env.VITE_REACT_APP_API_URL
      }/auth/users/reset_password_confirm/`,
      body,
      config
    );
    statusCode = response.status;
  } catch (err) {
    processError(`Failed to confirm password reset. Details: ${err}`);
  }

  return statusCode;
};

export const logout = () => (dispatch: any) => {
  dispatch(logoutSuccess());
};
