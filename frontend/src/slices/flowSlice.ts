import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Node } from "reactflow";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";

interface Flow {
  flowName: string;
  isDeleteNodeDialogOpened: boolean;
  activeNode: Node;
  connectionInProgress: boolean; // true when use started stretching new edge
  connectionSourceNodeId: string; // used in pair with `connectionInProgress`
  erroneousNodes: Node[];
  nodeSidebar: {
    opened: boolean;
    nodeType:
      | (typeof nodeNameToTypeMapping)[keyof typeof nodeNameToTypeMapping]
      | null;
  };
}

const initialState: Flow = {
  flowName: "",
  isDeleteNodeDialogOpened: false,
  activeNode: {} as Node,
  connectionInProgress: false,
  connectionSourceNodeId: "",
  erroneousNodes: [],
  nodeSidebar: {
    opened: false,
    nodeType: null,
  },
};

const flowSlice = createSlice({
  name: "flow",
  initialState,
  reducers: {
    setFlowName: (state, action: PayloadAction<string>) => {
      state.flowName = action.payload;
    },
    setIsDeleteNodeDialogOpened: (state, action: PayloadAction<boolean>) => {
      state.isDeleteNodeDialogOpened = action.payload;
    },
    setActiveNode: (state, action: PayloadAction<Node>) => {
      state.activeNode = action.payload;
    },
    setConnectionInProgress: (state, action: PayloadAction<boolean>) => {
      state.connectionInProgress = action.payload;
    },
    setConnectionSourceNodeId: (state, action: PayloadAction<string>) => {
      state.connectionSourceNodeId = action.payload;
    },
    setErroneousNodes: (state, action: PayloadAction<Node[]>) => {
      state.erroneousNodes = action.payload;
    },
    setNodeSidebar: (
      state,
      action: PayloadAction<{
        opened: boolean;
        nodeType:
          | (typeof nodeNameToTypeMapping)[keyof typeof nodeNameToTypeMapping]
          | null;
      }>
    ) => {
      state.nodeSidebar = action.payload;
    },
    closeNodeSidebar: (
      state
    ) => {
      state.nodeSidebar.opened = false;
      state.nodeSidebar.nodeType = null;
    }

  },
});

export const {
  setFlowName,
  setIsDeleteNodeDialogOpened,
  setActiveNode,
  setConnectionInProgress,
  setConnectionSourceNodeId,
  setErroneousNodes,
  setNodeSidebar,
  closeNodeSidebar,
} = flowSlice.actions;
export const updateCurrentFlow = (state: { flow: Flow | null }) => state.flow;
export default flowSlice.reducer;
