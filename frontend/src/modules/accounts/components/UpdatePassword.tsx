import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { passwordStringValidator } from "../utils/util";
import { resetUserPassword } from "../actions/action";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { useToast } from "@/components/ui/use-toast";
import { Navigate } from "react-router-dom";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";

// Define validation schema
const passwordSchema = z
  .object({
    currentPassword: z
      .string()
      .min(
        1,
        "You did not enter your existing password correctly. Please retype the password"
      ),
    newPassword: z
      .string()
      .min(8, { message: "The password should be at least 8 characters." })
      .refine(passwordStringValidator, {
        message: "String should not be all numbers",
      }),
    confirmPassword: z.string(),
  })
  .refine((data) => data.newPassword === data.confirmPassword, {
    message:
      "The passwords you entered don't match. Please confirm both passwords before continuing",
    path: ["confirmPassword"],
  });

const UpdatePasswordModal = () => {
  const { toast } = useToast();
  const [open, setOpen] = useState(false);
  const [error, setError] = useState<string>("");

  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated
  );

  if (!isAuthenticated) {
    return <Navigate to={AUTH_PAGE_URLS.login} />;
  }

  const userId = useSelector((state: RootState) => state.auth.user?.id);

  console.log("user id", userId);

  const form = useForm<z.infer<typeof passwordSchema>>({
    resolver: zodResolver(passwordSchema),
    defaultValues: {
      currentPassword: "",
      newPassword: "",
      confirmPassword: "",
    },
  });

  const onSubmit = async (values: z.infer<typeof passwordSchema>) => {
    const { currentPassword, newPassword } = values;

    try {
      await resetUserPassword(userId!, currentPassword, newPassword);
      toast({
        title: "Success!",
        description: "Password was successfully reset.",
      });
      setError("");
      form.reset();
      setOpen(false);
    } catch (error: any) {
      setError(error?.response?.data);
      return;
    }
  };

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button>Reset Passowrd</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Update password</DialogTitle>
          <p className="text-sm text-gray-500">
            To keep your account secure, enter your current password. Then, the
            new password you wish to use.
          </p>
          {error && (
            <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
              Error: {error}
            </p>
          )}
        </DialogHeader>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
            <FormField
              control={form.control}
              name="currentPassword"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Current password</FormLabel>
                  <FormControl>
                    <Input
                      type="password"
                      placeholder="Current password"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="newPassword"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>New password</FormLabel>
                  <FormControl>
                    <Input
                      type="password"
                      placeholder="New password"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="confirmPassword"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Confirm new password</FormLabel>
                  <FormControl>
                    <Input
                      type="password"
                      placeholder="Confirm new password"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <Button type="submit">Update password</Button>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default UpdatePasswordModal;
