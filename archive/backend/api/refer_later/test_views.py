USER_ACCOUNTS = [
    {
        "email": "vlad@tangibleai.com",
        "name": "Vlad",
        "password": "globalteamwork",
        "is_active": "TRUE",
    },
    {
        "email": "hobson@totalgood.com",
        "name": "Hobson",
        "password": "globalteamwork",
        "is_active": "TRUE",
    },
]

PROJECTS = [
    {
        "name": "Vlad's Test Project",
        "owner__email": USER_ACCOUNTS[0]["email"],
    },
    {
        "name": "Hobson's Test Project",
        "owner__email": USER_ACCOUNTS[0]["email"],
    },
]

CONTACTS = [
    {
        "origin": "Vlad's Test Contact",
        "project__name": PROJECTS[0]["name"],
        "project__owner__email": PROJECTS[0]["owner__email"],
    },
    {
        "origin": "Hobson's Test Contact",
        "project__name": PROJECTS[1]["name"],
        "project__owner__email": PROJECTS[1]["owner__email"],
    },
]


CHATS = [
    {
        "origin_id": "Vlad's Test Chat",
        "title": "First",
        "contact_id__origin": CONTACTS[0]["origin"],
        "contact_id__project__name": CONTACTS[0]["project__name"],
        "contact_id__project__owner__email": CONTACTS[0]["project__owner__email"],
    },
    {
        "origin_id": "Hobson's Test Chat",
        "title": "Second",
        "contact_id__origin": CONTACTS[1]["origin"],
        "contact_id__project__name": CONTACTS[1]["project__name"],
        "contact_id__project__owner__email": CONTACTS[1]["project__owner__email"],
    },
]


MESSAGE_TAG_GROUPS = [
    {
        "name": "First tag group",
        "project__name": PROJECTS[0]["name"],
        "project__owner__email": PROJECTS[0]["owner__email"],
    },
    {
        "name": "Second tag group",
        "project__name": PROJECTS[0]["name"],
        "project__owner__email": PROJECTS[0]["owner__email"],
    },
]

DATASETS = [
    {
        "name": "First",
        "description": "1",
        "project__name": PROJECTS[0]["name"],
        "project__owner__email": PROJECTS[0]["owner__email"],
    },
    {
        "name": "Second",
        "description": "2",
        "project__name": PROJECTS[0]["name"],
        "project__owner__email": PROJECTS[0]["owner__email"],
    },
]

ML_MODELS = [
    {
        "description": "First ML model",
        "project__name": PROJECTS[0]["name"],
        "project__owner__email": PROJECTS[0]["owner__email"],
        "dataset__name": "First",
    },
    {
        "description": "Second ML model",
        "project__name": PROJECTS[1]["name"],
        "project__owner__email": PROJECTS[1]["owner__email"],
        "dataset__name": "Second",
    },
]

MESSAGE_TAG_DEFINITIONS = [
    {
        "name": "change_language",
        "description": "Implicit or explicit intent to change language",
        "message_tag_group__name": MESSAGE_TAG_GROUPS[0]["name"],
        "project__name": PROJECTS[0]["name"],
        "project__owner__email": PROJECTS[0]["owner__email"],
        "dataset__name": DATASETS[0]["name"],
    },
    {
        "name": "goodbye",
        "description": "Stop conversation",
        "message_tag_group__name": MESSAGE_TAG_GROUPS[1]["name"],
        "project__name": PROJECTS[1]["name"],
        "project__owner__email": PROJECTS[1]["owner__email"],
        "dataset__name": DATASETS[1]["name"],
    },
]


class TestUserAccountApiView(TestCase, SerializeMixin):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        UserAccount.objects.all().delete()

    def test_valid_data(self):
        response = self.client.post(
            "/api/user_account/",
            data=json.dumps(USER_ACCOUNTS),
            content_type="application/json",
        )
        self.assertTrue(200 <= response.status_code <= 299)

    def test_missing_field(self):
        user_accounts = copy.deepcopy(USER_ACCOUNTS)
        del user_accounts[0]["name"]
        response = self.client.post(
            "/api/user_account/",
            data=json.dumps(user_accounts),
            content_type="application/json",
        )
        self.assertTrue(400 <= response.status_code <= 499)

    def tearDown(self):
        UserAccount.objects.all().delete()

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()


class TestProjectApiView(TestCase, SerializeMixin):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        Project.objects.all().delete()
        cls.client.post(
            "/api/user_account/",
            data=json.dumps(USER_ACCOUNTS),
            content_type="application/json",
        )

    def test_valid_data(self):
        response = self.client.post(
            "/api/project/", data=json.dumps(PROJECTS), content_type="application/json"
        )
        self.assertTrue(200 <= response.status_code <= 299)

    def test_missing_field(self):
        projects = copy.deepcopy(PROJECTS)
        del projects[0]["name"]
        response = self.client.post(
            "/api/project/", data=json.dumps(projects), content_type="application/json"
        )
        self.assertTrue(400 <= response.status_code <= 499)

    def tearDown(self):
        Project.objects.all().delete()

    @classmethod
    def tearDownClass(cls):
        Project.objects.all().delete()
        UserAccount.objects.all().delete()
        super().tearDownClass()


class TestContactApiView(TestCase, SerializeMixin):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        Contact.objects.all().delete()
        cls.client.post(
            "/api/user_account/",
            data=json.dumps(USER_ACCOUNTS),
            content_type="application/json",
        )
        cls.client.post(
            "/api/project/", data=json.dumps(PROJECTS), content_type="application/json"
        )

    def test_valid_data(self):
        response = self.client.post(
            "/api/project/contact/",
            data=json.dumps(CONTACTS),
            content_type="application/json",
        )
        self.assertTrue(200 <= response.status_code <= 299)

    def test_missing_field(self):
        contacts = copy.deepcopy(CONTACTS)
        del contacts[0]["origin"]
        response = self.client.post(
            "/api/project/contact/",
            data=json.dumps(contacts),
            content_type="application/json",
        )
        self.assertTrue(400 <= response.status_code <= 499)

    def tearDown(self):
        Contact.objects.all().delete()

    @classmethod
    def tearDownClass(cls):
        Contact.objects.all().delete()
        Project.objects.all().delete()
        UserAccount.objects.all().delete()
        super().tearDownClass()


class TestMessageTagGroupApiView(TestCase, SerializeMixin):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        MessageTagGroup.objects.all().delete()
        cls.client.post(
            "/api/user_account/",
            data=json.dumps(USER_ACCOUNTS),
            content_type="application/json",
        )
        cls.client.post(
            "/api/project/", data=json.dumps(PROJECTS), content_type="application/json"
        )

    def test_valid_data(self):
        response = self.client.post(
            "/api/project/message_tag_group/",
            data=json.dumps(MESSAGE_TAG_GROUPS),
            content_type="application/json",
        )
        self.assertTrue(200 <= response.status_code <= 299)

    def test_missing_field(self):
        tag_groups = copy.deepcopy(MESSAGE_TAG_GROUPS)
        del tag_groups[0]["name"]
        response = self.client.post(
            "/api/project/message_tag_group/",
            data=json.dumps(tag_groups),
            content_type="application/json",
        )
        self.assertTrue(400 <= response.status_code <= 499)

    def tearDown(self):
        MessageTagGroup.objects.all().delete()

    @classmethod
    def tearDownClass(cls):
        MessageTagGroup.objects.all().delete()
        Project.objects.all().delete()
        UserAccount.objects.all().delete()
        super().tearDownClass()


class TestDatasetApiView(TestCase, SerializeMixin):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        Dataset.objects.all().delete()
        cls.client.post(
            "/api/user_account/",
            data=json.dumps(USER_ACCOUNTS),
            content_type="application/json",
        )
        cls.client.post(
            "/api/project/", data=json.dumps(PROJECTS), content_type="application/json"
        )

    def test_valid_data(self):
        response = self.client.post(
            "/api/project/dataset/",
            data=json.dumps(DATASETS),
            content_type="application/json",
        )
        self.assertTrue(200 <= response.status_code <= 299)

    def test_missing_field(self):
        datasets = copy.deepcopy(DATASETS)
        del datasets[0]["name"]
        response = self.client.post(
            "/api/project/dataset/",
            data=json.dumps(datasets),
            content_type="application/json",
        )
        self.assertTrue(400 <= response.status_code <= 499)

    def tearDown(self):
        Dataset.objects.all().delete()

    @classmethod
    def tearDownClass(cls):
        Dataset.objects.all().delete()
        Project.objects.all().delete()
        UserAccount.objects.all().delete()
        super().tearDownClass()
