from django.urls import reverse
from django.test import TestCase
from rest_framework import status


class TestUrls(TestCase):
    """
    Test urls
    """

    def test_home_url(self):
        response = self.client.get(reverse("home"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sentry_debug(self):
        with self.assertRaises(ZeroDivisionError):
            self.client.get(reverse("sentry_debug"))

    def test_schema(self):
        response = self.client.get(reverse("schema"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_swagger_ui(self):
        response = self.client.get(reverse("swagger-ui"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
