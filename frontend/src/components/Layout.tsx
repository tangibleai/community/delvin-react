import React, { useEffect } from "react";
import { checkAuthenticated, load_user } from "../actions/auth";
import { AppDispatch } from "../store";
import { useDispatch } from "react-redux";

function Layout({ children }: { children: React.ReactNode }) {
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    dispatch(checkAuthenticated());
    dispatch(load_user());
  }, []);

  return <div className="dark:bg-gray-800">{children}</div>;
}

export default Layout;
