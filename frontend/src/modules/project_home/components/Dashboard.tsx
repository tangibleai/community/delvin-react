import { useQuery } from "react-query";
import { useParams } from "react-router-dom";

import {
  Title,
  Card,
  Metric,
  LineChart,
  AreaChart,
  Grid,
  Col,
} from "@tremor/react";

import { getProjectHomeData } from "@/actions/project/project_home";

const Dashboard = () => {
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const { data: projectHomeData, isLoading: isHomeDataLoading } = useQuery(
    ["chartData", projectId],
    () => getProjectHomeData(projectId),
  );

  return (
    <div className="flex-1 flex-col">
      <div className="text-left my-5 mx-2">
        <h1 className="text-3xl"> Project Stats </h1>
      </div>

      <Grid numItems={3}>
        <Col className="flex-1 p-2">
          <Card>
            <Title> Active Users </Title>
            <Metric> {projectHomeData?.total.active_users} </Metric>
            <LineChart
              className="mt-6"
              data={projectHomeData?.chart_data}
              index="date"
              categories={["active_users"]}
              showLegend={false}
              yAxisWidth={40}
            />
          </Card>
        </Col>

        <Col className="flex-1 p-2">
          <Card>
            <Title> New Users </Title>
            <Metric> {projectHomeData?.total.new_users}</Metric>
            <AreaChart
              className="mt-6"
              data={projectHomeData?.chart_data}
              index="date"
              categories={["total_users"]}
              colors={["violet"]}
              showLegend={false}
              yAxisWidth={40}
            />
          </Card>
        </Col>

        <Col className="flex-1 p-2">
          <Card>
            <Title> Messages </Title>
            <Metric> {projectHomeData?.total.messages}</Metric>
            <AreaChart
              className="mt-6"
              data={projectHomeData?.chart_data}
              index="date"
              categories={["inbound", "outbound"]}
              colors={["yellow", "emerald"]}
              showLegend={false}
              stack={true}
              yAxisWidth={40}
            />
          </Card>
        </Col>
      </Grid>
    </div>
  );
};

export default Dashboard;
