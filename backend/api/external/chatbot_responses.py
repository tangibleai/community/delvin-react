from django.http import JsonResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.auth import ProjectTokenAuthentication
from api.models import Chatbot, Plan, Project
from api.serializers import ChatbotResponseViewRequestSerializer
from api.services.chatbot_engine import engine_response
from api.tasks import create_response_logs, evaluate_plan_type_and_properties
from core.constant import CONSTANTS
from utils.chatbot_utils import (
    create_message_from_request,
    save_response_message,
)
from django.core.cache import cache

DEFAULT_CACHE_TIMEOUT = CONSTANTS.DEFAULT_CACHE_TIMEOUT


class ChatbotResponse(APIView):
    authentication_classes = [ProjectTokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ChatbotResponseViewRequestSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        validated_data = serializer.validated_data

        project_id = request.auth
        # Get the cached project
        project = Project.objects.get_cached(project_id)

        # Plan has to be up to date always, will not be cached.
        plan = project.workspace.plan

        # Cache the contact and chat
        message, errors = create_message_from_request(
            validated_data, project, internal=False
        )

        if not message:
            return Response({"errors": errors})

        chatbot_origin = validated_data.get("assistant", "default_assistant")
        # Get the cached chatbot
        chatbot = Chatbot.objects.get_cached(project, chatbot_origin)

        # When expired allows free plan to continue with free models.
        # Paid plan is blocked if status is not ACTIVE.
        if plan.status != Plan.Status.ACTIVE:
            response = "Thank you for using our service. To send more messages, please consider upgrading your plan."
            response_dict = {"response_text": response, "source": "P", "context": None}
        else:
            response_dict = engine_response(message, chatbot, message.chat)

        chatbot_uses_default_llm_provider = not chatbot.provider
        # Evaluate plan type for generative messages and calculate quota
        evaluate_plan_type_and_properties.delay(
            project.id, chatbot_uses_default_llm_provider
        )

        # Creates a message object for the response
        response_message_object = save_response_message(
            response_dict["response_text"], message
        )

        # Creates a response log for the user message and the response.
        # User message is the incoming message from the user and response is the generated response.
        # Both are message objects.
        create_response_logs.delay(
            user_message_id=message.id,
            response_id=response_message_object.id,
            source=response_dict["source"],
            ai_usage={"usage_records": [], "cost": 0.1},
            metadata={},
            context=response_dict["context"],
        )

        if not project.activated:
            project.activated = True
            project.save()

        return Response({"chatbot_response": response_dict["response_text"]})


get_chatbot_response = ChatbotResponse.as_view()
