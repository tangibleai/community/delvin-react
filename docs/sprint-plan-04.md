## Maria 
* [ ] Create "Set up project" page where user can see and copy the token for the first time 
* [ ] Finish the "Project Home" characters
* [ ] (aspirational) Create a "Project Settings" page where user can reset the project token


## Cetin

* Small Fixes in the token-authenticated endpoint: 
    *  [X] Shorten the token to 32 characters. 
    *  [X] Change the url endpoint to "api/public/message/"
* [X] Create an endpoint to reset the project token
* [X] Endpoint to delete the project  
