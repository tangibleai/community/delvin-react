from rest_framework import generics, serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import ProviderChoices


class ProviderChoiceSerializer(serializers.Serializer):
    name = serializers.CharField()


class ProviderTypeListView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProviderChoiceSerializer

    def get_queryset(self):
        return [{"name": choice.label} for choice in ProviderChoices]

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
