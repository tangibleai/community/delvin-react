import Dashboard from "../components/Dashboard";
import NewProjectPage from "../components/NewProjectPage";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";
import { useParams } from "react-router-dom";

function ProjectHome() {
  const routeParams = useParams();
  const projectId = routeParams.projectId!;
  const { data: project } = useGetCurrentProjectDetailsQuery(projectId);

  return <>{project?.activated ? <Dashboard /> : <NewProjectPage />}</>;
}

export default ProjectHome;
