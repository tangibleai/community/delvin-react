import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { ScrollArea } from "@/components/ui/scroll-area";
import { useEffect, useState } from "react";
import { Header } from "./Header";
import { MessageAssociatedWithTag } from "./MessageAssociatedWithTag";
import { useParams } from "react-router-dom";
import { useToast } from "@/components/ui/use-toast";
import { TaggedMessage } from "@/typings/tagged_message";
import {
  useDeleteMessageTagMutation,
  useGetMessagesAssociatedWithTagQuery,
  useTagTextMutation,
} from "@/slices/api/messageTagApiSlice";
import { MessageTagDefinition } from "@/typings/message_tag_definition";
import { Skeleton } from "@/components/ui/skeleton";
import {
  useDeleteMessageTagDefinitionMutation,
  useListMessageTagDefinitionsQuery,
} from "@/slices/api/messageTagDefinition";
import { IoReload } from "react-icons/io5";

type Props = {
  activeTagDefinition: MessageTagDefinition;
  setActiveTagDefinition: React.Dispatch<React.SetStateAction<object>>;
};

const TagDefinitionContent = ({
  activeTagDefinition,
  setActiveTagDefinition,
}: Props) => {
  const { toast } = useToast();
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const [newExample, setNewExample] = useState("");

  const { data: tags = [], isFetching: retrievingTags } =
    useListMessageTagDefinitionsQuery(projectId);
  const [deleteMessageTag, { isLoading: deletingMessageTag }] =
    useDeleteMessageTagMutation();
  const [
    deleteMessageTagDefinition,
    { isLoading: deletingMessageTagDefinition },
  ] = useDeleteMessageTagDefinitionMutation();

  const [tagText, { isLoading: taggingUserPrompt }] = useTagTextMutation();

  const {
    data: messagesAssociatedWithTag = [],
    isFetching: retrievingMessagesAssociatedWithTag,
    isError: errorRetrievingMessagesAssiciatedWithTag,
  } = useGetMessagesAssociatedWithTagQuery(activeTagDefinition.id, {
    skip: !activeTagDefinition || Object.keys(activeTagDefinition).length === 0,
  });

  const handleNewExampleChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const target = event.target as HTMLInputElement;
    setNewExample(target.value);
  };

  const handleCreateNewExample = async () => {
    try {
      await tagText({
        project_id: projectId,
        message_text: newExample,
        tag_ids: [activeTagDefinition.id],
      }).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Could not create a new tagged message.",
      });
    }

    setNewExample("");

    toast({
      title: "Success!",
      description: "Message was successfully tagged.",
    });
  };

  const addingNewExamplesEnabled =
    !retrievingTags &&
    !retrievingMessagesAssociatedWithTag &&
    Object.keys(activeTagDefinition).length !== 0 &&
    !deletingMessageTagDefinition;

  return (
    <div className="w-3/4 p-7">
      {!tags.length ? (
        <div className="w-full h-full flex items-center justify-center text-center text-xl rounded-2xl bg-muted">
          Create first tag
        </div>
      ) : (
        <>
          <Header
            activeTagDefinition={activeTagDefinition}
            setActiveTagDefinition={setActiveTagDefinition}
            deleteMessageTagDefinition={deleteMessageTagDefinition}
            deletingMessageTagDefinition={deletingMessageTagDefinition}
          />
          <p className="text-3xl font-bold mt-6">Examples</p>
          <div className="mt-3 flex justify-between items-center">
            <Input
              id="tag_name"
              className="w-4/5"
              disabled={
                !addingNewExamplesEnabled ||
                taggingUserPrompt ||
                deletingMessageTag
              }
              value={newExample}
              onChange={handleNewExampleChange}
            />
            <Button
              className="bg-green-500 hover:bg-green-600"
              disabled={
                !addingNewExamplesEnabled ||
                taggingUserPrompt ||
                deletingMessageTag
              }
              onClick={handleCreateNewExample}
            >
              {taggingUserPrompt && (
                <IoReload className="mr-2 h-4 w-4 animate-spin" />
              )}
              New Example
            </Button>
          </div>

          <ScrollArea className="flex flex-col justify-end flex-grow py-2.5 px-5 rounded-2xl bg-gray-100 p-4 h-[55vh] mt-4">
            {retrievingMessagesAssociatedWithTag ||
            deletingMessageTagDefinition ||
            taggingUserPrompt ||
            deletingMessageTag ? (
              <div className="space-y-2">
                {Array.from({ length: 10 }).map((_, index) => (
                  <Skeleton key={index} className="h-16 mx-auto bg-gray-300" />
                ))}
              </div>
            ) : (
              messagesAssociatedWithTag &&
              messagesAssociatedWithTag.map((taggedMessage, index) => (
                <MessageAssociatedWithTag
                  key={index}
                  taggedMessage={taggedMessage}
                  deleteMessageTag={deleteMessageTag}
                />
              ))
            )}
            {errorRetrievingMessagesAssiciatedWithTag && <></>}
          </ScrollArea>
        </>
      )}
    </div>
  );
};

export { TagDefinitionContent };
