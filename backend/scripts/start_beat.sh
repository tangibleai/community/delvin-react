#!/usr/bin/env bash
python manage.py migrate

echo "Starting celery beat"
exec celery --app core.celery beat -l INFO
