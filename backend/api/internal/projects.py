from django.db import transaction
from django.utils.translation import gettext_lazy as _
from drf_spectacular.extensions import OpenApiAuthenticationExtension
from rest_framework import generics, serializers, status
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.views import APIView

from api.models import Project, Workspace, TeamMember
from api.tasks import delete_project_document_metadata
from api.utils import dataset_copy


class ProjectTokenAuthenticationScheme(OpenApiAuthenticationExtension):
    target_class = (
        "api.auth.ProjectTokenAuthentication"  # full import path OR class ref
    )
    name = "TokenAuthentication"  # name used in the schema

    def get_security_definition(self, auto_schema):
        return {
            "type": "apiKey",
            "in": "header",
            "name": "Authorization",
            "description": 'Token-based authentication with required prefix "Bearer"',
        }


class ProjectDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        exclude = ["digest"]


class ProjectCreateSerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    workspace_id = serializers.UUIDField(required=True)


class ProjectUpdateSerializer(serializers.ModelSerializer):
    """
    Prevents token, token_key and owner values sent by API requests.
    name field is optional. More field could be added in the future.
    """

    class Meta:
        model = Project
        exclude = ["digest"]

    name = serializers.CharField(required=False)
    token = serializers.CharField(read_only=True)
    token_key = serializers.CharField(read_only=True)
    workspace = serializers.CharField(read_only=True)


class ProjectDetailViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ("id", "name", "activated", "workspace", "config")


class ProjectListAPIView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectDetailSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned projects to a given user,
        by filtering against a `owner_id` query parameter in the URL.
        """
        user = self.request.user
        queryset = Project.objects.all().filter(workspace__owner=user)
        return queryset


project_list_view = ProjectListAPIView.as_view()


class ProjectCreateAPIView(generics.CreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectCreateSerializer
    queryset = Project.objects.all()

    @transaction.atomic()
    def post(self, request, *args, **kwargs):
        data = request.data.copy()

        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)

        validated_data = serializer.validated_data

        name = validated_data.get("name")
        workspace_id = validated_data.get("workspace_id")

        workspace = Workspace.objects.get(id=workspace_id)
        owner = workspace.owner

        instance, token = Project.objects.create(name=name, workspace=workspace)

        user_project_id = instance.id

        dataset_copy.copy_dataset_and_dependencies(user_project_id, owner.email)

        # creating chatbot
        # chatbot = chatbots.create_chatbot(payload=data, project=instance)

        # serialized_records = {
        #     "project": {**ProjectDetailSerializer(instance).data, "token": token},
        #     "chatbot": chatbots.ChatbotSerializer(chatbot).data,
        # }

        return Response(
            ProjectDetailSerializer(instance).data, status=status.HTTP_201_CREATED
        )


project_create_view = ProjectCreateAPIView.as_view()


class ProjectUpdateAPIView(generics.UpdateAPIView):
    """
    PUT method is available for the owner.
    PATCH method is forbidden.
    """

    authentication_classe = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectUpdateSerializer

    def update_project(self):
        return Project.objects.update_project(
            instance=self.get_object(),
            name=self.request.data.get("name", None),
        )

    def get_queryset(self):
        """
        The get_queryset function is used to get the queryset of the user data corresponding to the logged in user.
        It is called when the view is instantiated, and it returns a list containing this user only.
        """
        user = self.request.user
        queryset = Project.objects.all().filter(workspace__owner=user)
        return queryset

    def put(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = ProjectUpdateSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            instance = self.update_project()
            data["name"] = instance.name
            return Response(data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)


project_update_view = ProjectUpdateAPIView.as_view()


class ProjectDetailAPIView(generics.RetrieveAPIView):
    """Returns the detail of the Project with a given ID"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectDetailViewSerializer
    queryset = Project.objects.all()
    lookup_field = "id"


project_detail_view = ProjectDetailAPIView.as_view()


class ProjectDeleteAPIView(generics.DestroyAPIView):
    """Deletes the Project associated with the PK passed via the URL"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Project.objects.all()
    serializer_class = ProjectDetailSerializer

    def delete(self, request, *args, **kwargs):
        project_id = self.kwargs.get("pk")
        project = Project.objects.filter(id=project_id).first()
        project.delete()

        delete_project_document_metadata.delay(project_id)

        return Response(status=status.HTTP_204_NO_CONTENT)


project_delete_view = ProjectDeleteAPIView.as_view()


class ProjectUpdateTokenAPIView(generics.UpdateAPIView):
    """
    PUT method is available for the owner.
    PATCH method is forbidden.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectUpdateSerializer

    def update_token(self):
        return Project.objects.update_token(
            instance=self.get_object(),
        )

    def get_queryset(self):
        """
        The get_queryset function is used to get the queryset of the user data corresponding to the logged in user.
        It is called when the view is instantiated, and it returns a list containing this user only.
        #TODO: prevent users that don't have access to the workspace to update the token
        """
        queryset = Project.objects.all()
        return queryset

    def put(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = self.serializer_class(data=data)

        if serializer.is_valid(raise_exception=True):
            instance, token = self.update_token()
            data["id"] = instance.pk
            data["name"] = instance.name
            data["token"] = token
            data["token_key"] = instance.token_key
            data["workspace"] = instance.workspace.pk
            return Response(data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)


project_update_token_view = ProjectUpdateTokenAPIView.as_view()


class ProjectsListView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectDetailSerializer

    def get_queryset(self):
        """
        Returns a list of projects inside the user's workspace

        """

        workspace_id = self.request.query_params.get("workspace_id", None)

        if workspace_id:
            workspace = Workspace.objects.get(id=workspace_id)

            return Project.objects.filter(workspace=workspace)

        return Response(
            {"error": "Missing workspace id"}, status=status.HTTP_400_BAD_REQUEST
        )


projects_list_view = ProjectsListView.as_view()


class ToggleBuiltinModerationRequestSerializer(serializers.Serializer):
    enabled = serializers.BooleanField(required=True)


class ToggleBuiltinModerationView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    request_serializer = ToggleBuiltinModerationRequestSerializer
    serializer_class = ProjectDetailSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.request_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        enabled = validated_data.get("enabled", False)
        project_id = self.kwargs.get("pk")

        project = Project.objects.filter(id=project_id).first()

        project.update_moderation_enabled_field(enabled)

        serializer = self.serializer_class(project)

        return Response(serializer.data)


toggle_builtin_moderation_view = ToggleBuiltinModerationView.as_view()


class BuiltinCategorySerializer(serializers.Serializer):
    code = serializers.CharField(max_length=255)
    name = serializers.CharField(max_length=255)
    enabled = serializers.BooleanField()
    response = serializers.CharField(max_length=255)


class UpdateBuiltinModerationCategoryView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectDetailSerializer
    request_serializer = BuiltinCategorySerializer

    def post(self, request, *args, **kwargs):
        serializer = self.request_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        project_id = self.kwargs.get("pk")
        project = Project.objects.filter(id=project_id).first()

        project.update_category(**validated_data)

        serializer = self.serializer_class(project)

        return Response(serializer.data)


update_builtin_moderation_category_view = UpdateBuiltinModerationCategoryView.as_view()
