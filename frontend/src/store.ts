import { configureStore } from "@reduxjs/toolkit";

import authReducer from "./slices/authSlice";
import projectReducer from "./slices/projectSlice";
import flowReducer from "./slices/flowSlice";
import responseReducer from "./slices/responseSlice";
import onboardingReducer from "./slices/onboardingSlice";
import { emptyApi } from "./slices/apiSlice";
import chatbotReducer from "./slices/chatbotSlice";

const store = configureStore({
  reducer: {
    auth: authReducer,
    project: projectReducer,
    flow: flowReducer,
    response: responseReducer,
    onboarding: onboardingReducer,
    [emptyApi.reducerPath]: emptyApi.reducer,
    chatbot: chatbotReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      emptyApi.middleware,
    ),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
