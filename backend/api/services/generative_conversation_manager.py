from core.constant import CONSTANTS
from utils.text_generator import LLMResponseGeneration

from utils.chatbot_utils import previous_messages_gen_format
from api.utils.build_llm_prompt import build_llm_prompt
from api.utils.prompt_configuration import ChatbotConfig
from api.models import Chatbot


class GenerativeConversationManager:
    def __init__(self, project, config: ChatbotConfig, chatbot: Chatbot):
        self.project = project
        self.config = config
        self.chatbot = chatbot
        self.llm_api_key = chatbot.provider.api_key
        self.model_string = self.chatbot.generative_model.model_string

    def add_context_knowledge(self, prompt, context):
        return (
            prompt
            + f"""   
This is what you know:
-------------
{context}
-------------
If the user asked a question and you can't answer it based on provided information, {self.config.knowledge.fallback}
"""
        )

    def get_response(self, message, context=None):
        prompt = self.config.get_general_prompt_str()
        if context:
            prompt = self.add_context_knowledge(prompt, context)
        initial_prompt = [{"role": "system", "content": prompt}]
        message_history = previous_messages_gen_format(message)

        messages = initial_prompt + message_history

        generator = LLMResponseGeneration(
            model_name=self.model_string, api_key=self.llm_api_key
        )
        llm_response = generator.generate_text(messages)
        return llm_response
