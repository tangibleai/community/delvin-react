import { XMarkIcon } from "@heroicons/react/24/outline";
import { cn } from "@/lib/utils"; // Adjust import as necessary
import NodeSidebarHeader from "./NodeSidebarHeader";

interface SidebarProps {
    isOpen: boolean;
    onClose: () => void;
    width?: string;
    children: React.ReactNode;
    className?: string;
}

export default function NodeSidebar({
    isOpen,
    onClose,
    width = "30%", // Default width
    children,
    className,
}: SidebarProps) {
    return (
        isOpen && (
        <div
            className={cn(
                "fixed inset-y-0 z-50 right-0 transition-transform transform",
                isOpen
                    ? "translate-x-0"

                    : "translate-x-full"
            )}
            style={{ width }}
        >
            <div
                className={cn(
                    "h-full mt-20 bg-white shadow-lg flex flex-col border-t",
                    className
                )}
            >
                <div className="p-4 flex items-center justify-between border-b">
                    <NodeSidebarHeader/>
                    <button
                        onClick={onClose}
                        className="rounded-md p-2 hover:bg-gray-100"
                    >
                        <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                    </button>
                </div>
                <div className="flex-1 overflow-auto p-4">{children}</div>
            </div>
        </div>
        )
    );
}