from api.models import Project, Workspace
from api.etl import utils


def process(df_or_records_list, *args, **kwargs):
    projects = df_or_records_list
    if not isinstance(projects, list):
        projects = df_or_records_list.to_dict("records")
    required_fields = {"name", "owner__email"}
    utils.check_missing_fields(projects, required_fields)

    projects = utils.resolve_user_for_each_record(projects)
    check_duplicated_projects(projects)
    db_records = save_projects_to_db(projects)

    return db_records


def check_duplicated_projects(projects):
    for project in projects:
        name = project["name"]
        owner_email = project["owner__email"]

        duplicated_projects = Project.objects.filter(
            name=name, workspace__owner__email=owner_email
        )
        if duplicated_projects:
            raise ValueError(
                f'Project with "{name}" name already exists for {owner_email} user'
            )

    return []


def save_projects_to_db(projects):
    db_records = []
    for p in projects:
        name = p["name"]
        user_rec = p["owner_rec"]
        workspace = Workspace.objects.filter(owner=user_rec).first()
        rec, token = Project.objects.create(name=name, workspace=workspace)
        db_records.append(rec)

    return db_records
