"""Runs this data migration after api/migrations/0018... migration file"""

from accounts.models import UserAccount
from api.models import (
    MLModel,
    Message,
    MessageTag,
    MessageTagDefinition,
    MessageTagGroup,
    Project,
)
from core.constant import CONSTANTS


class TagsAppender:
    def __init__(self):
        self.user = UserAccount.objects.filter(
            email=CONSTANTS.DEFAULT_USER_EMAIL
        ).first()
        self.project = Project.objects.get(workspace__owner=self.user)

        self.message_tag_group = MessageTagGroup.objects.get(project=self.project)

        self.yes_tag_rec = MessageTagDefinition.objects.create(
            project=self.project,
            message_tag_group=self.message_tag_group,
            name="yes",
            description="Agreement in response",
        )
        self.no_tag_rec = MessageTagDefinition.objects.create(
            project=self.project,
            message_tag_group=self.message_tag_group,
            name="no",
            description="Disagreement in response",
        )

        first_message = Message.objects.filter(project=self.project).first()

        self.new_message_record_create_params = {
            "direction": first_message.direction,
            "sender_type": first_message.sender_type,
            "channel_type": first_message.channel_type,
            "project": self.project,
            "contact": first_message.contact,
            "chat": first_message.chat,
        }

        self.ml_model = MLModel.objects.get(project=self.project)

    def create_yes_tag(self):
        yes_tag_statements = [
            "yes",
            "yeah",
            "sure",
            "sounds good",
            "okay, let's do that",
            "right",
            "yep",
            "I'd love that",
            "I agree",
            "y",
            "all right",
            "I'm in",
            "that works",
            "fine",
        ]
        for text in yes_tag_statements:
            message = Message.objects.create(
                **self.new_message_record_create_params, text=text
            )
            MessageTag.objects.create(
                message=message,
                tag=self.yes_tag_rec,
                ml_model=self.ml_model,
                labeler=self.user,
            )

        remained_text_statements_with_shared_tags = [
            "yes, add me",
            "yeah, that's cool",
            "Yes, let's do that.  That sounds interesting",
            "not really, but okay",
            "sure, that sounds fun",
            "I guess, but is there anything else?",
        ]
        for text in remained_text_statements_with_shared_tags:
            message, _ = Message.objects.get_or_create(
                **{
                    **self.new_message_record_create_params,
                    "project": self.project,
                    "text": text,
                }
            )
            MessageTag.objects.create(
                message=message,
                tag=self.yes_tag_rec,
                ml_model=self.ml_model,
                labeler=self.user,
            )

    def create_no_tag(self):
        no_tag_statements = [
            "no",
            "nah",
            "nope",
            "not at all",
            "not really",
            "I don't want to",
            "I don't think so",
            "I disagree",
            "maybe later",
            "not interested",
            "no way",
            "no thanks",
            "not interested",
            "n",
            "x",
        ]
        for text in no_tag_statements:
            message = Message.objects.create(
                **self.new_message_record_create_params, text=text
            )
            MessageTag.objects.create(
                message=message,
                tag=self.no_tag_rec,
                ml_model=self.ml_model,
                labeler=self.user,
            )

        remained_text_statements_with_shared_tags = [
            "not a chance in hell",
            "no let's do something else",
            "I'd rather pick another topic",
            "No, this sucks.  Stop messaging",
            "not really, but okay",
        ]
        for text in remained_text_statements_with_shared_tags:
            message, _ = Message.objects.get_or_create(
                **{
                    **self.new_message_record_create_params,
                    "project": self.project,
                    "text": text,
                }
            )
            MessageTag.objects.create(
                message=message,
                tag=self.no_tag_rec,
                ml_model=self.ml_model,
                labeler=self.user,
            )

    def run(self):
        self.create_yes_tag()
        self.create_no_tag()


if __name__ == "__main__":
    TagsAppender().run()
