import emptyApi from "../apiSlice";
import { TaggedMessage } from "@/typings/tagged_message";

type TagTextRequestBody = {
  project_id: string;
  message_text: string;
  tag_ids: string[];
};

const chatbotsApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    deleteMessageTag: builder.mutation<null, string>({
      query: (messageTagId: string) => ({
        url: `message-tag/${messageTagId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: ["MessageTag", "MLModel"],
    }),
    // additional non-CRUD hooks
    tagText: builder.mutation<[], TagTextRequestBody>({
      query: (requestBody) => ({
        url: "message-tags/",
        method: "POST",
        data: requestBody,
      }),
      invalidatesTags: ["MessageTag", "MLModel"],
    }),
    getMessagesAssociatedWithTag: builder.query<TaggedMessage[], string>({
      query: (messageTagDefinitionId: string) => ({
        url: "message-tags/with-tag/",
        method: "GET",
        params: { message_tag_definition_id: messageTagDefinitionId },
      }),
      providesTags: ["MessageTag"],
    }),
  }),
});

export const {
  useDeleteMessageTagMutation,
  useTagTextMutation,
  useGetMessagesAssociatedWithTagQuery,
} = chatbotsApiSlice;
