import os
import re

from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
from djoser.utils import decode_uid, encode_uid
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field, OpenApiParameter, extend_schema
from rest_framework import serializers, generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from accounts.models import UserAccount
from api.internal.projects import ProjectCreateSerializer
from api.models import Project, ProjectCollaborator, Workspace, TeamMember
from core.constant import CONSTANTS
from core.settings import DEFAULT_FROM_EMAIL


class ProjectCollaboratorListViewSerializer(serializers.ModelSerializer):
    delvin_user = serializers.CharField(source="delvin_user__email")
    role = serializers.SerializerMethodField()

    class Meta:
        model = ProjectCollaborator
        fields = ("id", "delvin_user", "role", "verification_status", "properties")

    @extend_schema_field(OpenApiTypes.STR)
    def get_role(self, obj):
        """Converts the role from a single character to a readable name

        V > Viewer
        """
        roles_dict = dict(ProjectCollaborator.ProjectCollaboratorRoles.choices)
        role = roles_dict.get(obj["role"])

        return role


class ProjectCollaboratorSerializer(serializers.ModelSerializer):
    delvin_user = serializers.CharField(source="delvin_user__email")
    project = serializers.UUIDField()
    role = serializers.CharField()

    class Meta:
        model = ProjectCollaborator
        fields = ("delvin_user", "project", "role")


class ProjectCollaboratorsByProjectAPIView(generics.ListAPIView):
    """Returns a list of Team Members associated with a project

    Currently, only Editor and Viewer team members are displayed in the frontend.

    Project.owners are retrieve from a separate query

    TODO: Check that someone has clicked their invitation within a certain time (ie., one week)
        If not, remove their name from list and instance in ProjectCollaborators
    """

    serializer_class = ProjectCollaboratorListViewSerializer

    def get_queryset(self):
        project_id = self.request.GET.get("project_id")
        project_collaborators = ProjectCollaborator.objects.filter(
            project__id=project_id
        ).values(
            "id", "delvin_user__email", "role", "verification_status", "properties"
        )

        return project_collaborators


project_collaborator_by_project_view = ProjectCollaboratorsByProjectAPIView.as_view()


class ProjectCollaboratorCreateAPIView(generics.CreateAPIView):
    """Handles creating a ProjectCollaborator instance for a Project"""

    queryset = Project.objects.all()
    serializer_class = ProjectCollaboratorSerializer

    def validate_and_retrieve_user(self, email):
        """Checks if there is an existing Delvin user for an email"""
        user = UserAccount.objects.filter(email=email)
        if user.count() > 0:
            return user.first()

    def retrieve_project(self, project):
        """Retrieves the project passed through a request"""
        project = Project.objects.filter(id=project).first()
        return project

    def is_duplicate_project_member(self, email, project):
        """Checks whether a requested member to a project is already a member"""
        owner_check = Project.objects.filter(id=project, workspace__owner__email=email)
        member_check = ProjectCollaborator.objects.filter(
            project__id=project, delvin_user__email=email
        )
        return owner_check or member_check

    def send_invitation_email(self, email, project):
        """Sends an email to an invited project team member without a Delvin account"""
        user = UserAccount.objects.create(email=email)
        user.is_active = False
        user.save()

        domain = os.environ.get("VITE_REACT_APP_API_URL")
        uid = encode_uid(user.pk)
        token = default_token_generator.make_token(user)
        invitation_link = f"{domain}/verify/team-member/{uid}/{token}/"

        if not re.search(CONSTANTS.TEST_USER_EMAIL_PATTERN, email):
            send_mail(
                "Delvin Invitation",
                f"You have been invited to join the {project} project on Delvin.  Click the following link to set up your account: {invitation_link}",
                DEFAULT_FROM_EMAIL,
                [email],
                fail_silently=False,
            )

    def send_notification_email(self, email, project):
        """Sends an email to an invited project team member with a Delvin account"""

        if not re.search(CONSTANTS.TEST_USER_EMAIL_PATTERN, email):
            send_mail(
                "Delvin Notice",
                f"You have been added to the {project} project on Delvin.  Log in to check it out!",
                DEFAULT_FROM_EMAIL,
                [email],
                fail_silently=False,
            )

    def create_project_collaborator(self, user_instance=None, project=None, email=None):
        """Creates a ProjectCollaborator instance for new team members

        Those with an existing Delvin account will be linked to a UserAccount
        Those without will need to undergo verification, so no UserAccount is linked
        """
        role = ProjectCollaborator.get_role_abbreviation(
            full_role=self.request.data.get("role")
        )

        collaborator_context = {
            "project": project,
            "role": role,
        }

        if user_instance:
            collaborator_context.update(
                {
                    "delvin_user": user_instance,
                    "verification_status": ProjectCollaborator.ProjectCollaboratorStatus.VERIFIED,
                }
            )
        else:
            collaborator_context.update(
                {
                    "verification_status": ProjectCollaborator.ProjectCollaboratorStatus.UNVERIFIED,
                    "properties": {"email": email},
                }
            )

        return ProjectCollaborator.objects.create(**collaborator_context)

    def post(self, request, *args, **kwargs):
        """Handles a request from the NewMemberModal on the frontend to add a new team member"""
        data = request.data.copy()
        email = data.get("delvin_user")
        serializer = self.serializer_class(data=data)

        if serializer.is_valid(raise_exception=True):
            user = self.validate_and_retrieve_user(data.get("delvin_user"))
            project = self.retrieve_project(data.get("project"))

            # Prospective team member doesn't have a UserAccount instance
            if not user:
                project_collaborator = self.create_project_collaborator(
                    email=email, project=project
                )
                # Necessary to update user context from frontend
                # because unverified users are missing information
                data = {
                    "id": project_collaborator.id,
                    "project": data.get("project"),
                    "role": data.get("role"),
                    "delvin_user": None,
                    "verification_status": project_collaborator.verification_status,
                    "properties": project_collaborator.properties,
                }
                self.send_invitation_email(email, project_collaborator.project.name)
                return Response(
                    data,
                    status=status.HTTP_202_ACCEPTED,
                )

            # Prospective team member is already a ProjectCollaborator for Project
            is_duplicate_project_member = self.is_duplicate_project_member(
                data.get("delvin_user"), data.get("project")
            )
            if is_duplicate_project_member:
                return Response(
                    {"detail": "This user is already a team member."},
                    status=status.HTTP_409_CONFLICT,
                )

            # New team member for Project and has UserAccount
            project_collaborator = self.create_project_collaborator(
                user_instance=user, project=project
            )
            data.update(
                {
                    "id": project_collaborator.id,
                    "verification_status": project_collaborator.verification_status,
                    "properties": project_collaborator.properties,
                }
            )
            self.send_notification_email(email, project_collaborator.project.name)
            return Response(data, status=status.HTTP_201_CREATED)


project_collaborator_add_view = ProjectCollaboratorCreateAPIView.as_view()


class ProjectCollaboratorUpdateAPIView(generics.UpdateAPIView):
    """Updates a ProjectCollaborator's role within a particular Project"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectCollaboratorSerializer

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        email = data.get("delvin_user")
        project = data.get("project")
        full_role = data.get("role")
        role = ProjectCollaborator.get_role_abbreviation(full_role=full_role)

        serializer = ProjectCollaboratorSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            team_member = ProjectCollaborator.objects.filter(
                delvin_user__email=email, project_id=project
            ).first()
            team_member.role = role
            team_member.save()
            return Response(data, status=status.HTTP_200_OK)


project_collaborator_update_view = ProjectCollaboratorUpdateAPIView.as_view()


class ProjectCollaboratorDeleteAPIView(generics.DestroyAPIView):
    """Deletes a ProjectCollaborator entry"""

    # TODO: This will not work on the Project.owner.  Need to address this issue.

    queryset = ProjectCollaborator.objects.all()

    @extend_schema(
        tags=["api"],
        description="Project collaborator delete view",
        parameters=[
            OpenApiParameter(
                name="id",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="Unique identifier of the project collaborator",
            ),
        ],
        responses={
            204: None,
        },
    )
    def delete(self, request, *args, **kwargs):
        id_ = self.kwargs.get("id")
        record_to_delete = self.queryset.filter(id=id_)
        record_to_delete.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


project_collaborator_delete_view = ProjectCollaboratorDeleteAPIView.as_view()


class InvitationConfirmationView(generics.CreateAPIView):
    authentication_classes = []
    permission_classes = []

    @extend_schema(
        tags=["api"],
        description="Project collaborator invitation confirmation",
        request=None,  # No specific request serializer needed
        parameters=[
            OpenApiParameter(
                name="uid",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="Unique identifier of the invitation",
            ),
            OpenApiParameter(
                name="token",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="Token",
            ),
            OpenApiParameter(
                name="password",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="password",
            ),
        ],
        responses={
            200: {"description": "User successfully activated"},
        },
    )
    def post(self, request, *args, **kwargs):
        data = request.data
        uid = data["uid"]
        token = data["token"]
        password = data["password"]

        user_pk = decode_uid(uid)
        user = UserAccount.objects.filter(pk=user_pk).first()

        if not user.is_active:
            # Activate the team member's UserAccount
            user.is_active = True
            user.set_password(password)
            user.save()

            # Update ProjectCollaborators
            project_collaborators = ProjectCollaborator.objects.all()
            for project_collaborator in project_collaborators:
                if project_collaborator.properties.get("email", "") == user.email:
                    project_collaborator.delvin_user = user
                    project_collaborator.save()
            return Response(
                {"id": user.id, "email": user.email}, status=status.HTTP_200_OK
            )
        else:
            return Response(
                {"id": user.id, "email": user.email}, status=status.HTTP_200_OK
            )


confirm_invitation = InvitationConfirmationView.as_view()


class ProjectListForMemberAPIView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectCreateSerializer

    def get_queryset(self):
        """
        Returns a list of projects that a user is
        either a ProjectOwner or ProjectCollaborator

        """
        queryset = Project.objects.all()

        owned_projects = []

        workspace_id = self.request.query_params.get("workspace_id", None)

        user_id = self.request.query_params.get("owner_id", None)

        if workspace_id:
            workspace = Workspace.objects.get(id=workspace_id)

            return Project.objects.filter(workspace=workspace)

        if user_id is not None:
            # Returns projects a user owns
            owned_projects = queryset.filter(owner__id=user_id)

            # Returns projects shared with a user
            project_collaborator_projects = ProjectCollaborator.objects.filter(
                delvin_user__id=user_id
            ).values_list("project_id", flat=True)

            shared_projects = queryset.filter(id__in=project_collaborator_projects)

            # Combines the two results
            queryset = owned_projects.union(shared_projects)
            return queryset
        return Response(
            {"error": "Missing user id"}, status=status.HTTP_400_BAD_REQUEST
        )


project_list_for_member_view = ProjectListForMemberAPIView.as_view()
