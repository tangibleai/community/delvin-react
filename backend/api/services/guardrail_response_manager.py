from api.models import (
    MessageTagDefinition,
    GuardrailResponse,
    GuardrailAction,
    Message,
    Project,
    Chatbot,
)

from api.utils.tags_prediction import predict_tags
from utils.chatbot_utils import previous_messages_gen_format
from utils.text_generator import LLMResponseGeneration
from api.utils.build_llm_prompt import build_llm_prompt


class GuardrailResponseManager:
    def __init__(self, project: Project, chatbot: Chatbot):
        self.project = project
        self.chatbot = chatbot
        self.llm_api_key = chatbot.provider.api_key
        self.model_string = self.chatbot.generative_model.model_string

    def find_response_action(self, message):
        tags = predict_tags(message.text, self.project)
        for record in tags:
            tag = record["tag"]
            response = GuardrailResponse.objects.filter(
                message_tag_definition=tag
            ).first()
            if response:
                return response.guardrail_action

        return None

    def execute_action(self, message, action: GuardrailAction) -> str:
        """
        Returns the response to a given action as a string.
        """
        if action.action_type == GuardrailAction.ActionType.SCRIPTED:
            return action.content
        elif action.action_type == GuardrailAction.ActionType.GENERATIVE:
            message_history = previous_messages_gen_format(message)
            prompt = build_llm_prompt(self.chatbot.config)
            initial_prompt = [{"role": "system", "content": prompt}]
            new_prompt = [{"role": "system", "content": action.content}]
            messages = initial_prompt + message_history + new_prompt
            llm_generate_text = LLMResponseGeneration(
                model_name=self.model_string,
                api_key=self.llm_api_key,
            )
            llm_response = llm_generate_text(messages)
            return llm_response

    def get_response(self, message):
        action = self.find_response_action(message)

        if action:
            return self.execute_action(message, action)

        return None
