export const AUTH_PAGE_URLS = {
  home: "/",
  signup: "/signup",
  login: "/login",
  reset_password: "/reset_password",
  reset_password_confirm: "/password/reset/confirm/:uid/:token",
  verify_team_member: "/verify/team-member/:uid/:token",
  activate: "/activate/:uid/:token",
  resend_email: "/resend_email",
  not_found: "*",
  account_settings: "/account_settings",
};

export const WORKSPACE_PAGE_URLS = {
  workspace: "/workspace/:workspaceId",
};

export const PROJECT_PAGE_URLS = {
  project: {
    path: "/project/:projectId/",
    children: {
      home: "home",
      conversations: "conversations",
      chat_with_assistant: "chat_with_assistant",
      tags: "tags",
      chat: "chat/:chatId",
      settings: "settings",
      chatbot_config: "chatbot_config",
      understanding: "understanding",
      guardrail_responses: "guardrail_responses",
      flow_playground: "flow/:flowId",
    },
  },
  init_assistant: "/project/:projectId/init_assistant", // not included to previous item to exclude menu sidebar
};
