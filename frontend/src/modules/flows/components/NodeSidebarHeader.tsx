import { setActiveNode } from "@/slices/flowSlice";
import { AppDispatch, RootState } from "@/store";
import { useSelector, useDispatch } from "react-redux";
import { useReactFlow } from "reactflow";

export default function NodeSidebarHeader() {
    const dispatch: AppDispatch = useDispatch();
    const { setNodes } = useReactFlow();
  
    const activeNode = useSelector((state: RootState) => state.flow.activeNode);

    const onBlockNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newName = e.target.value;
        dispatch(
          setActiveNode({
            ...activeNode,
            data: { ...activeNode.data, nodeName: newName },
          })
        );
        setNodes((nodes) =>
          nodes.map((node) =>
            node.id === activeNode.id
              ? {
                  ...node,
                  data: { ...node.data, nodeName: newName },
                }
              : node
          )
        );
      };
    
    if (!activeNode) {
        return null; 
    }
      
    return (
        <div className="flex p-2 items-center justify-between ">
        <input
          type="text"
          value={activeNode?.data?.nodeName}
          onChange={onBlockNameChange}
          className="p-2 border rounded-md w-full"
        />
      </div>
    )

}