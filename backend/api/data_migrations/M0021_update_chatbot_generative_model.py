"Apply after api/migrations/0020_provider.py migration"

import logging

from api.models import GenerativeModel, Chatbot

logger = logging.getLogger(__name__)


def migrate_chatbot_generative_model():
    try:
        generative_model = GenerativeModel.objects.get(
            model_string="openrouter/meta-llama/llama-3-70b-instruct"
        )

        updated_count = Chatbot.objects.update(generative_model=generative_model)

        print(
            f"Successfully updated {updated_count} Chatbot records to use the 'Llama 3 70B Instruct' model."
        )
    except GenerativeModel.DoesNotExist:
        print(
            "Error: The 'Llama 3 70B Instruct' model was not found in GenerativeModel."
        )
    except Exception as e:
        print(f"An error occurred: {e}")


if __name__ == "__main__":
    migrate_chatbot_generative_model()
