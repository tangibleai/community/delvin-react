from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.internal.serializers import MessageSerializer
from api.models import Message, Project, Chat


class MessageListAPIView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = MessageSerializer

    def list(self, request, *args, **kwargs):
        project_id = request.query_params.get("project_id")
        chat_origin = request.query_params.get("chat_origin")

        if not project_id or not chat_origin:
            return Response(
                {'detail': 'Missing required query parameters: project_id and chat_origin.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            project = Project.objects.get(id=project_id)
        except Project.DoesNotExist:
            return Response(
                {'detail': 'Project not found.'},
                status=status.HTTP_404_NOT_FOUND
            )
        
        chat = Chat.objects.filter(contact_id__project=project, origin_id=chat_origin).first()

        if not chat:
            queryset = Message.objects.none()
        else:
            queryset =  Message.objects.filter(project=project, chat=chat).order_by("-timestamp")

        response =  {"results": self.get_serializer(queryset, many=True).data}
        return Response(response)


message_list_view = MessageListAPIView.as_view()
