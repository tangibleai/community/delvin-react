import { useNavigate, useParams } from "react-router-dom";
import { useState } from "react";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { RxCross2 } from "react-icons/rx";

import { AppDispatch } from "@/store";
import { useDispatch } from "react-redux";
import { resetOnboarding } from "@/slices/onboardingSlice";

function StopOnboardingModal() {
  const dispatch: AppDispatch = useDispatch();
  const navigate = useNavigate();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const [isModalOpen, setIsModalOpen] = useState(false);

  function onConfirmButtonClick() {
    dispatch(resetOnboarding());
    navigate(`/project/${projectId}/chatbot_config`);
  }

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button variant="ghost">
          <RxCross2 size="3rem" />
        </Button>
      </DialogTrigger>
      <DialogContent className="flex flex-col gap-7 sm:max-w-md text-lg">
        <DialogHeader>
          <DialogTitle className="text-xl">Stop onboarding</DialogTitle>
        </DialogHeader>
        <p>
          Are you sure you want to leave the onboarding? Current progress won't
          be saved.
        </p>
        <DialogFooter>
          <Button
            variant="destructive"
            className="order-2 sm:order-1 text-base"
            onClick={onConfirmButtonClick}
          >
            Yes, let me out
          </Button>
          <Button
            variant="outline"
            className="order-1 sm:order-2 text-base"
            onClick={() => setIsModalOpen(false)}
          >
            Cancel
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}

export default StopOnboardingModal;
