import {
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeaderCell,
  TableRow,
} from "@tremor/react";
import { FaCheck } from "react-icons/fa6";

import { HiTrash } from "react-icons/hi2";
import moment from "moment";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setFlowName } from "@/slices/flowSlice";
import { Flow } from "@/typings/flow";

// uncomment when want to enable copy button again
// function CopyIcon() {
//   return (
//     <svg
//       width="24"
//       height="24"
//       viewBox="0 0 24 24"
//       fill="none"
//       xmlns="http://www.w3.org/2000/svg"
//     >
//       <rect x="3" y="3" width="12" height="12" fill="black" />
//       <rect x="9" y="9" width="12" height="12" fill="white" stroke="black" />
//     </svg>
//   );
// }

type Props = {
  flows: Flow[];
  handleDeleteFlow: (flowId: string) => Promise<any>;
};

const FlowsTable = ({ flows, handleDeleteFlow }: Props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const onFlowClick = (flow: Flow) => {
    // console.log("flow", flow);
    dispatch(setFlowName(flow.name));
    navigate(`/project/${projectId}/flow/${flow.id}`);
  };

  return (
    <Card>
      <Table>
        <TableHead className="text-xl">
          <TableRow>
            <TableHeaderCell>Flows</TableHeaderCell>
            <TableHeaderCell>Status</TableHeaderCell>
            <TableHeaderCell>Updated</TableHeaderCell>
            <TableHeaderCell></TableHeaderCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {flows &&
            flows.map((flow, index) => (
              <TableRow
                className="hover:bg-slate-100"
                key={index}
                onClick={() => onFlowClick(flow)}
              >
                <TableCell className="font-bold">{flow.name}</TableCell>
                <TableCell className="text-3xl">
                  <FaCheck />
                  {/* <FaXmark /> */}
                </TableCell>
                <TableCell className="italic">
                  {moment(flow.updated_at).format("MMMM D, YYYY, h:mm A")}
                </TableCell>
                <TableCell className="flex items-center gap-5 text-3xl">
                  <HiTrash
                    onClick={() => {
                      handleDeleteFlow(flow?.id);
                    }}
                  />
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </Card>
  );
};

export { FlowsTable };
