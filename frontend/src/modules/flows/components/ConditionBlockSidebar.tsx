import { useEffect, useState } from "react";
import { Card, Select, SelectItem } from "@tremor/react";
import { Button } from "@/components/ui/button";
import ConditionDropdown from "./ConditionDropdown";
import { useReactFlow } from "reactflow";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { deleteSingleConditionNodes } from "../utils/flow_playground";
import { ScrollArea } from "@/components/ui/scroll-area";
import { RxCross1 } from "react-icons/rx";
import { Node } from "reactflow";
import { addSingleCondition } from "../utils/add_single_condition";
import { Condition, ConditionTypes } from "../typing/condition";
import { useParams } from "react-router-dom";
import { useListMessageTagDefinitionsQuery } from "@/slices/api/messageTagDefinition";

function ConditionBlockSidebar() {
  const { getNodes, setNodes } = useReactFlow();

  const activeNode = useSelector((state: RootState) => state.flow.activeNode);

  const [conditions, setConditions] = useState<Condition[]>([]);

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const { data: tags } = useListMessageTagDefinitionsQuery(projectId);

  useEffect(() => {
    retrieveAndSetConditions();
  }, [activeNode]);

  const retrieveAndSetConditions = () => {
    const childNodes = getNodes().filter(
      (node) => node.parentId === activeNode.id
    );
    const conditions = childNodes
      .filter((node) => node.data?.condition.type !== ConditionTypes.ELSE)
      .map((node) => node.data.condition);

    setConditions(conditions);
  };

  const handleConditionChange = (nodeId: string, newTagId: string) => {
    const tag = tags!.find((tag) => tag.id === newTagId);
    const tagName = tag!.name;
    const updatedConditions = conditions.map((condition) => {
      return condition.attachedNodeId === nodeId
        ? {
            ...condition,
            properties: { tag_name: tagName, tag_id: newTagId },
          }
        : condition;
    });
    setConditions(updatedConditions);
    setNodes((nodes) =>
      nodes.map((node) =>
        node.id === nodeId
          ? {
              ...node,
              data: {
                ...node.data,
                condition: {
                  ...node.data.condition,
                  type: ConditionTypes.TAG_DETECTED,
                  properties: { tag_name: tagName, tag_id: newTagId },
                } as Partial<Condition>,
              },
            }
          : node
      )
    );
  };

  const deleteNode = (nodeToDelete: Node) => {
    const nodesWithoutRemoved = deleteSingleConditionNodes(
      [nodeToDelete],
      getNodes()
    );
    setNodes(nodesWithoutRemoved);
  };

  const onConditionDelete = (nodeId: string) => {
    const conditionsWithoutRemoved = conditions.filter(
      (cond) => cond.attachedNodeId !== nodeId
    );
    setConditions(conditionsWithoutRemoved);
    const nodeToDelete = getNodes().find((node) => node.id === nodeId)!;
    deleteNode(nodeToDelete);
  };

  const onUpdate = () => {};
  const onConditionAdd = (conditionType: string) => {
    const result = addSingleCondition(
      getNodes(),
      conditions,
      activeNode,
      conditionType
    );
    const nodes = result.nodes;
    const updatedConditions = result.conditions;

    setNodes(nodes);
    setConditions(updatedConditions);
  };

  return (
    <>
      <div className="flex flex-col gap-5 h-full">
        <ScrollArea
          key={"projects scroll area"}
          className="flex flex-col h-[50%]"
        >
          {conditions.map((condition, index) => (
            <div key={index} className="mb-4">
              <p className="font-bold">
                {index === 0 ? "If..." : "Else if..."}
              </p>
              <Card className="flex gap-4">
                <div>
                  <div className="flex items-center gap-4">
                    Tag
                    {tags?.length && (
                      <Select
                        value={condition.properties.tag_id}
                        onValueChange={(newTagId) =>
                          handleConditionChange(
                            condition.attachedNodeId,
                            newTagId
                          )
                        }
                      >
                        {tags.map((tag) => (
                          <SelectItem value={tag.id}>{tag.name}</SelectItem>
                        ))}
                      </Select>
                    )}
                  </div>

                  <p>
                    {condition.type === ConditionTypes.TAG_DETECTED
                      ? "is detected..."
                      : "isn't detected..."}
                  </p>
                </div>
                <div className="flex justify-end">
                  <RxCross1
                    size={"1rem"}
                    onClick={() => onConditionDelete(condition.attachedNodeId)}
                  />
                </div>
              </Card>
            </div>
          ))}
        </ScrollArea>

        <ConditionDropdown addCondition={onConditionAdd} />

        <Card className="flex">
          <p className="font-bold">Else ...</p>
        </Card>
      </div>

      <div className="flex justify-end mt-4">
        <Button className="mr-2" onClick={() => onUpdate()}>
          Update
        </Button>
        <Button variant="outline">Cancel</Button>
      </div>
    </>
  );
}

export default ConditionBlockSidebar;
