import { useParams } from "react-router-dom";
import { useState } from "react";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";

import { Member } from "@/typings/member";
import { useDeleteTeamMemberMutation } from "@/slices/api/teamMemberApiSlice";

type Props = {
  userToDelete: any;
  onDeleteMember: (deleteMember: Member) => void;
};

function DeleteMemberModal({ userToDelete, onDeleteMember }: Props) {
  const [error, setError] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [deleteTeamMember] = useDeleteTeamMemberMutation();

  async function handleClickDelete(userToDelete: Member) {
    try {
      await deleteTeamMember(userToDelete.id).unwrap();
    } catch (error: any) {
      return setError(String(error));
    }

    onDeleteMember(userToDelete);
    setIsModalOpen(false);
  }

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button variant="ghost" className="w-full">
          Remove
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-md">
        <DialogHeader>
          <DialogTitle>Delete member</DialogTitle>
        </DialogHeader>
        <p>Are you sure you want to delete this user?</p>
        <DialogFooter>
          <Button
            variant="destructive"
            onClick={() => handleClickDelete(userToDelete)}
          >
            Delete
          </Button>
          <Button variant="outline" onClick={() => setIsModalOpen(false)}>
            Cancel
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}

export default DeleteMemberModal;
