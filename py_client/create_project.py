import os
from json import JSONDecodeError
from pathlib import Path
from pprint import pprint

import requests
from dotenv import load_dotenv

ROOT = Path(__file__).resolve().parent.parent

load_dotenv(dotenv_path=os.path.join(ROOT / "py_client" / ".env"))

endpoint = "http://localhost:8000/api/project/create/"

token = os.environ["AUTH_TOKEN"]

headers = {"Authorization": f"Bearer {token}"}


data = {
    "name": "1rd project user1",
}


response = requests.post(
    endpoint,
    headers=headers,
    json=data,
)

if response.status_code == 200:
    try:
        pprint(response.json())
    except JSONDecodeError:
        print(response.text)
else:
    print(response.status_code)
    print(response.text)
