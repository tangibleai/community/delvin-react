from api.models import Chatbot, Flow


# forward migration function
def create_chatbot_for_each_flow():
    for flow in Flow.objects.all():
        name = flow.name
        project = flow.project

        chatbot = Chatbot.objects.create(project=project, origin=name)
        chatbot.config["flow"] = flow.id
        chatbot.save()


if __name__ == "__main__":
    create_chatbot_for_each_flow()
