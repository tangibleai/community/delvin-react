from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from api.models import (
    Chatbot,
    Project,
    Workspace,
    Provider,
    ProviderChoices,
    GenerativeModel,
)
from accounts.models import UserAccount
from rest_framework_simplejwt.tokens import RefreshToken
from api.utils.prompt_configuration import create_chatbot_config_object, BotTypeEnum


class ChatbotAPITests(TestCase):
    fixtures = ["default_account_data.json"]

    def setUp(self):
        super().setUp()
        self.client = APIClient()

        self.user = UserAccount.objects.create_user(
            email="test@example.com", name="Test", password="password"
        )
        refresh = RefreshToken.for_user(self.user)
        self.token = str(refresh.access_token)

        self.headers = {"HTTP_AUTHORIZATION": "Bearer " + self.token}
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + self.token)

        self.workspace = Workspace.objects.create(
            name="Test Workspace", owner=self.user
        )
        self.project, _ = Project.objects.create(
            name="Test Project", workspace=self.workspace
        )

        self.provider = Provider.objects.create(
            label="Test",
            type=ProviderChoices.OPENROUTER.value,
            workspace=self.workspace,
            api_key="abcd",
        )
        self.generative_model, _ = GenerativeModel.objects.get_or_create(
            label="Test Generative model",
            model_string="openrouter/meta-llama/llama-3-70b-instruct",
            priority=1,
            provider_type=ProviderChoices.OPENROUTER.value,
        )

        self.chatbot = Chatbot.objects.create(
            project=self.project,
            generative_model=self.generative_model,
            config=create_chatbot_config_object(),
            origin="default_assistant",
        )

    def test_get_chatbots(self):
        url = f"{reverse('chatbots_list_view')}?project_id={str(self.project.id)}"

        response = self.client.get(url, headers=self.headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["id"], self.chatbot.id)
        self.assertEqual(response.data[0]["origin"], "default_assistant")

    def test_create_chatbot(self):
        url = reverse("create_chatbot_view")
        data = {
            "project_id": str(self.project.id),
            "model": Chatbot.GenerativeModel.OPENAI_GPT_TURBO,
            "config": create_chatbot_config_object(
                prompt_settings={
                    "bot_type": BotTypeEnum.Discussion.value,
                    "chatbotPersona": "You are a helpful assistant.",
                    "instructions": "Assist the user with information.",
                    "constraints": "Do not provide personal opinions.",
                }
            ),
            "origin": "test_assistant",
            "llm_provider_id": self.provider.id,
            "model_id": self.generative_model.id,
        }

        response = self.client.post(url, data, format="json", headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(Chatbot.objects.count(), 2)
        self.assertEqual(response.data["origin"], "test_assistant")
        self.assertEqual(
            response.data["provider"],
            self.provider.id,
        )
        self.assertEqual(response.data["generative_model"], self.generative_model.id)

    def test_update_chatbot(self):
        url = reverse("update_chatbot_view", kwargs={"pk": self.chatbot.id})
        self.new_generative_model, _ = GenerativeModel.objects.get_or_create(
            label="New Generative model",
            model_string="openrouter/openai/gpt-3.5-turbo",
            priority=2,
            provider_type=ProviderChoices.OPENAI.value,
        )
        data = {
            "project": str(self.project.id),
            "generative_model": self.new_generative_model.id,
            "config": create_chatbot_config_object(
                prompt_settings={
                    "bot_type": BotTypeEnum.QuestionAnswer.value,
                    "chatbotPersona": "Answer precisely.",
                    "instructions": "Provide concise answers.",
                    "constraints": "Avoid unnecessary information.",
                    "fallbackResponses": "I dont know",
                }
            ),
            "origin": "updated_assistant",
        }

        response = self.client.put(url, data, format="json", headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.chatbot.refresh_from_db()

        self.assertEqual(self.chatbot.origin, "updated_assistant")
        self.assertEqual(self.chatbot.generative_model, self.new_generative_model)
        self.assertEqual(
            self.chatbot.config["bot_type"], BotTypeEnum.QuestionAnswer.value
        )

    def test_delete_chatbot(self):
        url = reverse("delete_chatbot_view", kwargs={"pk": self.chatbot.id})

        response = self.client.delete(url, headers=self.headers)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        self.assertFalse(Chatbot.objects.filter(id=self.chatbot.id).exists())

    def tearDown(self):
        UserAccount.objects.all().delete()
        Workspace.objects.all().delete()
        Project.objects.all().delete()
        Chatbot.objects.all().delete()
        Provider.objects.all().delete()
        GenerativeModel.objects.all().delete()
