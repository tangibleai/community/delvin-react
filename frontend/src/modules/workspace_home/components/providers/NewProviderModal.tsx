import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Input } from "@/components/ui/input";
import { Provider } from "@/typings/provider";
import { useCreateProviderMutation } from "@/slices/api/providerApiSlice";
import { IoReload } from "react-icons/io5";
import { useToast } from "@/components/ui/use-toast";
import { Workspace } from "@/typings/workspace";

const providerSchema = z.object({
  label: z.string().min(1, { message: "Provider label is required" }),
  type: z.string().min(1, { message: "Please select a provider type" }),
  api_key: z.string().min(1, { message: "API Token is required" }),
});

type Props = {
  activeWorkspace: Workspace;
  providerTypes: { name: string }[];
};

function NewProviderModal({ activeWorkspace, providerTypes }: Props) {
  const { toast } = useToast();

  const [isModalOpen, setIsModalOpen] = useState(false);

  const [createProvider, { isLoading: creatingProvider }] =
    useCreateProviderMutation();

  const form = useForm({
    resolver: zodResolver(providerSchema),
    defaultValues: {
      label: "",
      type: "",
      api_key: "",
    },
  });

  const { register, handleSubmit, setValue, watch, formState } = form;
  const { errors } = formState;
  const selectedType = watch("type");

  useEffect(() => {
    if (providerTypes.length > 0) {
      setValue("type", providerTypes[0].name);
    }
  }, [providerTypes, setValue]);

  const onSubmit = async (formValues: Omit<Provider, "id" | "workspace">) => {
    try {
      const createdProvider = await createProviderDbRecord(formValues);
      showProviderCreatedToastMessage(createdProvider);
      closeDialog();
      resetForm();
    } catch (error: any) {
      processProviderCreationFailure(error);
    }
  };

  const createProviderDbRecord = async (
    formValues: Omit<Provider, "id" | "workspace">
  ) => {
    const newProvider = {
      ...formValues,
      workspace: activeWorkspace.id,
    };
    const createdProvider = await createProvider(newProvider).unwrap();

    return createdProvider;
  };

  const showProviderCreatedToastMessage = (createdProvider: Provider) => {
    toast({
      title: "Success!",
      description: `Successfully added provider: ${createdProvider.label}`,
    });
  };

  const closeDialog = () => {
    setIsModalOpen(false);
  };

  const resetForm = () => {
    form.reset();
  };

  const processProviderCreationFailure = (error: any) => {
    const errorMessage = error?.data?.detail || error.message;

    const wrongApiKeyForProvider =
      error.status === 400 &&
      errorMessage === "Invalid API key for the specified provider.";
    if (wrongApiKeyForProvider) {
      setWrongApiKeyFormFieldErrorMessage();
    } else {
      showProviderCreationErrorToastMessage(errorMessage);
    }
  };

  const setWrongApiKeyFormFieldErrorMessage = () => {
    form.setError("api_key", {
      type: "manual",
      message: "The provided API key is not valid for the selected provider.",
    });
  };

  const showProviderCreationErrorToastMessage = (errorMessage: any) => {
    toast({
      title: "Error creating provider",
      description: errorMessage,
    });
  };

  return (
    <Dialog
      open={isModalOpen || creatingProvider}
      onOpenChange={setIsModalOpen}
    >
      <DialogTrigger asChild>
        <Button>+ New Provider</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Create a New Provider</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={handleSubmit(onSubmit)} className="space-y-4">
            <FormItem>
              <FormLabel>Provider Label</FormLabel>
              <FormControl>
                <Input placeholder="Provider label" {...register("label")} />
              </FormControl>
              {errors.label && (
                <FormMessage>{errors.label.message}</FormMessage>
              )}
            </FormItem>
            <FormItem>
              <FormLabel>Provider Type</FormLabel>
              <FormControl>
                <Select
                  onValueChange={(value) => setValue("type", value)}
                  value={selectedType}
                >
                  <SelectTrigger>
                    <SelectValue placeholder="Select provider type" />
                  </SelectTrigger>
                  <SelectContent>
                    <SelectGroup>
                      {providerTypes.map((type) => (
                        <SelectItem key={type.name} value={type.name}>
                          {type.name}
                        </SelectItem>
                      ))}
                    </SelectGroup>
                  </SelectContent>
                </Select>
              </FormControl>
              {errors.type && <FormMessage>{errors.type.message}</FormMessage>}
            </FormItem>
            <FormItem>
              <FormLabel>API Token</FormLabel>
              <FormControl>
                <Input
                  placeholder="API token"
                  type="password"
                  {...register("api_key")}
                />
              </FormControl>
              {errors.api_key && (
                <FormMessage>{errors.api_key.message}</FormMessage>
              )}
            </FormItem>
            <DialogFooter>
              <Button type="submit" disabled={creatingProvider}>
                {creatingProvider && (
                  <IoReload className="mr-2 h-4 w-4 animate-spin" />
                )}
                Create Provider
              </Button>
              <Button
                variant="outline"
                onClick={() => setIsModalOpen(false)}
                disabled={creatingProvider}
              >
                {creatingProvider && (
                  <IoReload className="mr-2 h-4 w-4 animate-spin" />
                )}
                Cancel
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}

export default NewProviderModal;
