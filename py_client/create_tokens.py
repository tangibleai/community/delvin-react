import os
from pathlib import Path

import requests
from dotenv import load_dotenv

ROOT = Path(__file__).resolve().parent.parent

load_dotenv(dotenv_path=os.path.join(ROOT / "py_client" / ".env"))

auth_endpoint = "http://localhost:8000/auth/jwt/create/"
# auth_endpoint = "http://app.delvin.to/auth/jwt/create/"

email = os.environ["USER1"]
password = os.environ["PASSWORD1"]


def get_access_tokens():
    auth_response = requests.post(
        auth_endpoint,
        json={"email": email, "password": password},
    )

    if auth_response.status_code == 200:
        refresh, access = auth_response.json().get("refresh"), auth_response.json().get(
            "access"
        )
        return refresh, access
