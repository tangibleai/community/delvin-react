from rest_framework import serializers


class ChatbotResponseViewRequestSerializer(serializers.Serializer):
    user_message = serializers.CharField()
    assistant = serializers.CharField(required=False)
    contact = serializers.CharField()
    chat = serializers.CharField(required=False)

class ChatbotInternalResponseViewRequestSerializer(
    ChatbotResponseViewRequestSerializer
):
    project_id = serializers.UUIDField()

