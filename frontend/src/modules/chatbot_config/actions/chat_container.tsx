import axiosInstance from "@/api/axios";
import { processError } from "@/lib/utils";

async function getResponsefromLLM(
  userPrompt: string,
  currentChatOrigin: string,
  projectId: string,
  chatbotOrigin: string,
  contactOrigin: string,
) {
  const body = JSON.stringify({
    project_id: projectId,
    chat: currentChatOrigin,
    user_message: userPrompt,
    assistant: chatbotOrigin,
    contact: contactOrigin
  });
  try {
    const response = await axiosInstance.post(
      `/api/project/internal_usage/chatbot_response/`,
      body
    );
    const LLMResponse = response.data;

    return LLMResponse;
  } catch (error: any) {
    processError(`Failed to get response from LLM. Details: ${error}`);
  }
}

export { getResponsefromLLM };
