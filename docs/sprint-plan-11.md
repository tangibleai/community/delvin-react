## Cetin

- [X] Create an action and a response in a single API call
- [X] Implement the delete response button
- [X] Stretch: Implement edit response button

## Vlad 

 * [x] Update the csv's using for data migration according to the new [Default Data version](https://docs.google.com/spreadsheets/d/1OcpX-YCeGW0piFOHRJeJuxvblJZ_cmKzdhSAhP1Hkq0/edit?usp=sharing)
  - [x] fix masmatch between names in CSVs and download locally
  - [x] exclude chat creation from data migration
 * [x] Prepare MR !92 to be merged to staging. 
  - [x] remove retrain model endpoint and view introduced in next MR
  - [x] remove unused imports
 * [x] Run data migration on staging
 * [x] Remove message retagging in Retrain Model View and make it receive project ID
 * [x] Hide the dataset tab from the Understanding screen and move the Retrain Model button to the Tags tab.
 * [x] Sanity check on the retraining functionality: 
       - Create a new project
       - Check if greeting phrase (i.e. hello) does not have any tags 
       - Add an intent "greeting" and 5-6 examples of greetings. 
       - Check if "hello" is tagged as "greeting"
 * [x] Implement a scikit-learn version of SGD in sgd.py file.


## Maria

* [x] Merge MRs !90 !91
* [x] Merge MR !92
