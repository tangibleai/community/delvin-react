import uuid

from api import models
from api.tests import mock
from api.utils import dataset_copy


def is_uuid(string):
    """Checks if a string is a valid UUID."""
    try:
        uuid.UUID(string)
        return True
    except ValueError:
        return False


def filter_by_fields(input_dict, filter_fields):
    filtered_data = {
        key: value for key, value in input_dict.items() if key in filter_fields
    }
    return filtered_data


def user_and_project_set_up(client):
    """
    Creates user and project. Uses `dataset_copy.copy_dataset_and_dependencies` function which should be tested before any of currect function call

    Args:
        client [rest_framework.test.APIClient]: Client that will instantiate request call
    Returns:
        dict: User record, access token, headers and project record
    """
    user_init_params = filter_by_fields(
        mock.mock["user1"], ["email", "name", "password"]
    )
    user = models.UserAccount.objects.create_user(**user_init_params)
    response = client.post("/auth/jwt/create/", user_init_params)
    access_token = response.json()["access"]

    headers = {"Authorization": "JWT " + access_token}

    workspace = models.Workspace.objects.create(
        name=f"{user.name}'s Workspace", owner=user, properties={}
    )

    project_name = "Sample Project"
    project, token = models.Project.objects.create(
        name=project_name, workspace=workspace
    )

    dataset_copy.copy_dataset_and_dependencies(project.id, mock.mock["user1"]["email"])

    return {
        "user": user,
        "access_token": access_token,
        "project": project,
        "headers": headers,
        "workspace": workspace,
    }
