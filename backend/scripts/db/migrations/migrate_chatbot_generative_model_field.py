"""
Currently `Chatbot.generative_model` field is defined as:
```
class Chatbot(models.Model):
    ...
    generative_model = models.CharField(
        max_length=99,
        choices=GenerativeModel.choices,
        default=GenerativeModel.MISTRAL_7b_INSTRUCT,
        help_text="The generative model used by the chatbot to generate responses",
    )
    ...
```

We want to change its type to reference `GenerativeModel` model:
```
class Chatbot(models.Model):
    ...
    generative_model = models.ForeignKey(
        "GenerativeModel", null=True, on_delete=models.SET_NULL, related_name="chatbots"
    )
    ...


class ProviderChoices(models.TextChoices):
    OPENAI = "OpenAI", "OpenAI"
    ANTHROPIC = "Anthropic", "Anthropic"
    OPENROUTER = "OpenRouter", "OpenRouter"


class GenerativeModel(models.Model):
    class PriorityChoices(models.IntegerChoices):
        DEFAULT = 1, "Default"
        STANDARD = 2, "Standard"
        CUSTOM = 3, "Custom"

    label = models.CharField(
        max_length=100, help_text="User-friendly name of the model"
    )
    model_string = models.CharField(
        max_length=255, help_text="Model identifier for API calls"
    )
    priority = models.IntegerField(
        choices=PriorityChoices.choices, default=PriorityChoices.DEFAULT
    )
    provider_type = models.CharField(max_length=50, choices=ProviderChoices.choices)
```

All Chatbots will be switched to `ProviderChoices.OPENROUTER` provider
"""

import json

from api import models

OLD_TO_NEW_MODEL_STRING = {
    "openai/gpt-3.5-turbo": "openrouter/openai/gpt-3.5-turbo",
    "anthropic/claude-3.5-sonnet": "openrouter/anthropic/claude-3.5-sonnet",
    "mistralai/mistral-7b-instruct": "openrouter/openai/gpt-3.5-turbo",
    "meta-llama/llama-3-70b": "openrouter/meta-llama/llama-3-70b-instruct",
    "google/gemini-flash-1.5": "openrouter/google/gemini-pro-1.5",
    "openai/gpt-4o": "openrouter/openai/gpt-4o",
    "meta-llama/llama-3.1-8b-instruct:free": "openrouter/openai/gpt-3.5-turbo",
}


def figure_new_model_string_for_chatbots():
    new_chatbot_model_string = {}

    for c in models.Chatbot.objects.all():
        new_model_string = get_new_model_string_for_chatbot(c)
        new_chatbot_model_string[c.id] = new_model_string

    return new_chatbot_model_string


def get_new_model_string_for_chatbot(chatbot):
    old_model_string = chatbot.generative_model
    new_model_string = get_new_model_string_by_old(old_model_string)

    return new_model_string


def get_new_model_string_by_old(old):
    new_model_string = OLD_TO_NEW_MODEL_STRING.get(
        old, "openrouter/openai/gpt-3.5-turbo"
    )

    return new_model_string


def write_to_json_file(new_chatbot_model_string):
    with open("new_chatbot_model_string.json", "w") as json_file:
        json.dump(new_chatbot_model_string, json_file, indent=4)


def read_from_json():
    with open("new_chatbot_model_string.json", "r") as json_file:
        data = json.load(json_file)

        return data

    return {}


def change_generative_model_field_to_new_strings(new_chatbot_model_string):
    for c in models.Chatbot.objects.all():
        model_string = new_chatbot_model_string.get(str(c.id))
        generative_model = models.GenerativeModel.objects.get(
            provider_type=models.ProviderChoices.OPENROUTER.value,
            model_string=model_string,
        )
        c.generative_model = generative_model
        c.save()
