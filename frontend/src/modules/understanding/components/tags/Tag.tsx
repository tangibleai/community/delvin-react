import { MessageTagDefinition } from "@/typings/message_tag_definition";

type Props = {
  tagDefinition: MessageTagDefinition;
  activeTagDefinition: MessageTagDefinition;
  setActiveTagDefinition: React.Dispatch<React.SetStateAction<object>>;
};

const Tag = ({
  tagDefinition,
  activeTagDefinition,
  setActiveTagDefinition,
}: Props) => {
  return (
    <div
      onClick={() => {
        setActiveTagDefinition(tagDefinition);
      }}
      className={`${
        tagDefinition.id === activeTagDefinition.id ? "bg-gray-100" : "bg-white"
      } hover:bg-gray-200 p-2 mb-2 rounded shadow cursor-pointer`}
    >
      {tagDefinition.name}
    </div>
  );
};

export { Tag };
