import binascii
from hmac import compare_digest

from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions
from rest_framework.authentication import (
    BaseAuthentication,
    get_authorization_header,
)

from api.crypto import hash_token
from api.models import Project
from core.constant import CONSTANTS


class ProjectTokenAuthentication(BaseAuthentication):
    """
    Project model authentication
    """

    keyword = "Bearer"

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None
        if len(auth) == 1:
            msg = _("Invalid token header. No credentials provided.")
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _("Invalid token header. " "Token string should not contain spaces.")
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _(
                "Invalid token header. Token string should not contain invalid characters."
            )
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, key):
        model = Project

        msg = _("Invalid token.")
        for project in model.objects.filter(
            token_key=key[: CONSTANTS.TOKEN_KEY_LENGTH]
        ):
            try:
                digest = hash_token(key)
            except (TypeError, binascii.Error):
                raise exceptions.AuthenticationFailed(msg)
            if compare_digest(digest, project.digest):
                if not project.workspace.owner.is_active:
                    raise exceptions.AuthenticationFailed(
                        _("User inactive or deleted.")
                    )
                # Returning project id instead of the token
                return project.workspace.owner, project.pk
        raise exceptions.AuthenticationFailed(msg)

    def authenticate_header(self, request):
        return self.keyword
