export interface Provider {
  id: string;
  label: string;
  type: string;
  workspace: string | null;
  api_key: string;
}
