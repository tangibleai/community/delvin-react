class DatasetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dataset
        exclude = ["tagged_messages"]


class MLModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = MLModel
        exclude = ["sklearn_pipeline"]


class MLWeightsSerializer(serializers.ModelSerializer):
    class Meta:
        model = MLWeights
        fields = "__all__"


class UserAccountApiSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=200)
    name = serializers.CharField(max_length=200)
    password = serializers.CharField(max_length=200)
    is_active = serializers.CharField(max_length=200)


class ProjectApiSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    owner__email = serializers.CharField(max_length=200)


class ContactApiSerializer(serializers.Serializer):
    origin = serializers.CharField(max_length=200)
    project__name = serializers.CharField(max_length=200)
    project__owner__email = serializers.CharField(max_length=200)


class ChatApiSerializer(serializers.Serializer):
    origin_id = serializers.CharField(max_length=200)
    title = serializers.CharField(max_length=200)
    contact_id__origin = serializers.CharField(max_length=200)
    contact_id__project__name = serializers.CharField(max_length=200)
    contact_id__project__owner__email = serializers.CharField(max_length=200)


class MessageTagGroupApiSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    project__name = serializers.CharField(max_length=200)
    project__owner__email = serializers.CharField(max_length=200)


class DatasetApiSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    description = serializers.CharField(max_length=200)
    project__name = serializers.CharField(max_length=200)
    project__owner__email = serializers.CharField(max_length=200)


class MLModelApiSerializer(serializers.Serializer):
    description = serializers.CharField(max_length=200)
    project__name = serializers.CharField(max_length=200)
    project__owner__email = serializers.CharField(max_length=200)
    dataset__name = serializers.CharField(max_length=200)


class MessageTagDefinitionApiSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    description = serializers.CharField(max_length=200)
    message_tag_group__name = serializers.CharField(max_length=200)
    project__name = serializers.CharField(max_length=200)
    project__owner__email = serializers.CharField(max_length=200)
    dataset__name = serializers.CharField(max_length=200)
