function getViewportHeight(
  scrollContainerRef: React.RefObject<HTMLDivElement>,
) {
  const scrollArea = getScrollArea(scrollContainerRef);
  let height;
  if (scrollArea) {
    const childDiv = scrollArea.querySelector("div");
    height = childDiv?.offsetHeight;
  }
  return height;
}

function getScrollArea(scrollContainerRef: React.RefObject<HTMLDivElement>) {
  const scrollContainer = scrollContainerRef.current;
  let scrollArea;
  if (scrollContainer) {
    // since scrollContainer is not the scrollable div itself we need to query one
    scrollArea = scrollContainer.querySelector(
      "[data-radix-scroll-area-viewport]",
    );
  }
  return scrollArea;
}

export { getViewportHeight, getScrollArea };
