"""
Django settings for core project.

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
https://www.django-rest-framework.org/api-guide/settings/

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

import logging
import os
import warnings
from datetime import timedelta
from pathlib import Path

import dj_database_url
import sentry_sdk
from dotenv import load_dotenv
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.logging import LoggingIntegration
from corsheaders.defaults import default_headers

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
PROJECT_ROOT = BASE_DIR.parent

logger = logging.getLogger(__name__)

load_dotenv(dotenv_path=PROJECT_ROOT / ".env")

ENV = os.environ

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ENV["SECRET_KEY"]

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = ENV["DEBUG"] in ["True", "true"]

ALLOWED_HOSTS = ENV["ALLOWED_HOSTS"].split(",")
CSRF_TRUSTED_ORIGINS = ENV["CSRF_TRUSTED_ORIGINS"].split(",")
CORS_ALLOWED_ORIGINS = ENV["CORS_ALLOWED_ORIGINS"].split(",")
# Needed to allow sentry headers from front-end
CORS_ALLOW_HEADERS = (
    *default_headers,
    "sentry-trace",
    "baggage",
)

# Needed for debug available inside templates
INTERNAL_IPS = ENV["INTERNAL_IPS"].split(",")
# CORS_ALLOW_ALL_ORIGINS = True

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
    "django_extensions",
    "djoser",
    "corsheaders",
    "celery",
    "celery_progress",
    "accounts",
    "api",
    "django_celery_results",
    "django_celery_beat",
    "drf_spectacular",
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "core.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "core.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {"default": dj_database_url.config(default=ENV.get("DJANGO_DATABASE_URL"))}


# Sendgrid
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = ENV["SENDGRID_EMAIL_HOST"]
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = ENV["SENDGRID_EMAIL_HOST_USER"]
EMAIL_HOST_PASSWORD = ENV["SENDGRID_API_KEY"]
SENDGRID_API_KEY = ENV["SENDGRID_API_KEY"]
DEFAULT_FROM_EMAIL = ENV["SENDGRID_EMAIL_ADDRESS"]

# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = "/static/"

STATICFILES_DIRS = [
    PROJECT_ROOT / "frontend" / "dist",
]

STATIC_ROOT = BASE_DIR / "staticfiles"

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.IsAuthenticated",
    ],
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework_simplejwt.authentication.JWTAuthentication",
        "api.auth.ProjectTokenAuthentication",
    ],
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
}

SPECTACULAR_SETTINGS = {
    "ENUM_NAME_OVERRIDES": {
        "MessageDirectionEnum": "api.models.Message.Direction",
        "MessageTagRuleDirectionEnum": "api.models.MessageTagRule.Direction",
    },
    "DISABLE_ERRORS_AND_WARNINGS": ENV.get("SPECTACULAR_DISABLE_ERRORS_AND_WARNINGS")
    in ["True", "true"],
}

SIMPLE_JWT = {
    "AUTH_HEADER_TYPES": ["JWT", "Bearer", "Token"],
    "ACCESS_TOKEN_LIFETIME": timedelta(days=1),
    "REFRESH_TOKEN_LIFETIME": timedelta(days=30),
}

DJOSER = {
    "LOGIN_FIELD": "email",
    "USER_CREATE_PASSWORD_RETYPE": True,
    "SEND_ACTIVATION_EMAIL": True,
    "SEND_CONFIRMATION_EMAIL": True,
    "USERNAME_CHANGED_EMAIL_CONFIRMATION": True,
    "PASSWORD_CHANGED_EMAIL_CONFIRMATION": True,
    "SET_USERNAME_RETYPE": True,
    "SET_PASSWORD_RETYPE": True,
    "PASSWORD_RESET_CONFIRM_URL": "password/reset/confirm/{uid}/{token}",
    "USERNAME_RESET_CONFIRM_URL": "email/reset/confirm/{uid}/{token}",
    "ACTIVATION_URL": "activate/{uid}/{token}",
    "SERIALIZERS": {
        "user_create": "accounts.serializers.SpecialUserCreateSerializer",
        "user": "accounts.serializers.SpecialUserCreateSerializer",
        "user_delete": "djoser.serializers.UserDeleteSerializer",
    },
}

CELERY_BROKER_URL = ENV.get("CELERY_BROKER_URL")
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_RESULT_SERIALIZER = "json"
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_BACKEND = ENV.get("CELERY_RESULT_BACKEND")
CELERY_TIMEZONE = "UTC"
CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"

# REDIS CACHE

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": ENV.get("CELERY_BROKER_URL"),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
    }
}

AUTH_USER_MODEL = "accounts.UserAccount"

CSRF_COOKIE_HTTPONLY = False

# Sentry logging settings
SENTRY_DSN = ENV.get("SENTRY_DSN")
if SENTRY_DSN:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        environment=ENV.get("VITE_SENTRY_ENVIRONMENT"),
        integrations=[
            DjangoIntegration(
                transaction_style="url",
                middleware_spans=True,
                signals_spans=False,
                cache_spans=False,
            ),
            LoggingIntegration(
                level=logging.INFO,  # Capture info and above as breadcrumbs
                event_level=logging.ERROR,  # Send records as events
            ),
        ],
        traces_sample_rate=float(ENV.get("SENTRY_TRACES_SAMPLE_RATE", 1)),
        send_default_pii=True,
    )
    logger.info(f"{SENTRY_DSN}, starting up {__file__}")

# Configuration to log to the console during development
# Set by the `DJANGO_LOG_LEVEL` environment
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "detailed": {
            "format": "{asctime} - {levelname} - {pathname}:{lineno} - {message}",
            "style": "{",
        },
    },
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "detailed"},
    },
    "root": {
        "handlers": ["console"],
        "level": ENV.get("DJANGO_LOG_LEVEL", "WARNING"),
    },
}


FIXTURE_DIRS = [BASE_DIR / "fixtures"]


# disable FutureWarning warning
warnings.simplefilter(action="ignore", category=FutureWarning)
