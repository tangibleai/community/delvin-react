import { ROUTES as AUTH_ROUTES } from "./AuthRoutes";
import { ROUTES as PROJECT_ROUTES } from "./ProjectRoutes";
import { ROUTES as WORKSPACE_ROUTES } from "./WorkspaceRoutes";
import { createBrowserRouter } from "react-router-dom";

const routes = AUTH_ROUTES.concat(WORKSPACE_ROUTES).concat(PROJECT_ROUTES);

export const PAGE_URLS = createBrowserRouter(routes);
