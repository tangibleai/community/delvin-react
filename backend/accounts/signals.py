from django.db.models.signals import post_delete
from django.dispatch import receiver

from api.models import Workspace


@receiver(post_delete, sender=Workspace)
def update_current_workspace(sender, instance, **kwargs):
    user = instance.owner

    if user.current_workspace == instance:
        new_workspace = Workspace.objects.filter(owner=user).first()

        user.current_workspace = new_workspace
        user.save()
