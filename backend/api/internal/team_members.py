import os
import re

from django.utils.translation import gettext_lazy as _
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
from djoser.utils import decode_uid, encode_uid
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field, OpenApiParameter, extend_schema
from rest_framework import serializers, generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.exceptions import MethodNotAllowed

from accounts.models import UserAccount
from api.models import ProjectCollaborator, Workspace, TeamMember
from core.constant import CONSTANTS
from core.settings import DEFAULT_FROM_EMAIL

from api.internal import users


class TeamMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamMember
        fields = "__all__"


class TeamMembersListRequestSerializer(serializers.Serializer):
    ROLE_CHOICES = (
        TeamMember.TeamMemberRoles.OWNER.label,
        TeamMember.TeamMemberRoles.EDITOR.label,
        TeamMember.TeamMemberRoles.VIEWER.label,
    )

    workspace_id = serializers.UUIDField(required=True)
    email = serializers.EmailField(required=False)
    role = serializers.ChoiceField(required=False, choices=ROLE_CHOICES)


class TeamMembersListResponseSerializer(serializers.ModelSerializer):
    delvin_user = serializers.CharField(source="delvin_user__email")
    role = serializers.SerializerMethodField()

    class Meta:
        model = TeamMember
        fields = ("id", "delvin_user", "role", "verification_status", "properties")

    @extend_schema_field(OpenApiTypes.STR)
    def get_role(self, obj):
        """Converts the role from a single character to a readable name

        V > Viewer
        """
        roles_dict = dict(TeamMember.TeamMemberRoles.choices)
        role = roles_dict.get(obj["role"])

        return role


class TeamMembersListView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = TeamMembersListResponseSerializer

    def get_queryset(self):
        serializer = TeamMembersListRequestSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)  # raise exception if invalid
        validated_data = serializer.validated_data

        workspace_id = validated_data.get("workspace_id")
        query_by = {"workspace_id": workspace_id}
        email = validated_data.get("email")
        if email:
            query_by["delvin_user__email"] = email
        role = validated_data.get("role")
        if role:
            query_by["role"] = role

        team_members = TeamMember.objects.filter(**query_by).values(
            "id", "delvin_user__email", "role", "verification_status", "properties"
        )

        return team_members


team_members_view = TeamMembersListView.as_view()


class TeamMemberCreateSerializer(serializers.Serializer):
    ROLE_CHOICES = (
        TeamMember.TeamMemberRoles.OWNER.label,
        TeamMember.TeamMemberRoles.EDITOR.label,
        TeamMember.TeamMemberRoles.VIEWER.label,
    )
    email = serializers.EmailField(required=True)
    role = serializers.ChoiceField(required=True, choices=ROLE_CHOICES)
    workspace_id = serializers.UUIDField(required=True)


class TeamMemberCreateView(generics.CreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = TeamMember.objects.all()
    serializer_class = TeamMemberSerializer

    def validate_and_retrieve_user(self, email):
        """Checks if there is an existing Delvin user for an email"""
        user = UserAccount.objects.filter(email=email)
        if user.count() > 0:
            return user.first()

    def retrieve_workspace(self, workspace_id):
        """Retrieves the project passed through a request"""
        project = Workspace.objects.filter(id=workspace_id).first()
        return project

    def is_duplicate_workspace_member(self, email, workspace_id):
        owner_check = Workspace.objects.filter(id=workspace_id, owner__email=email)
        member_check = TeamMember.objects.filter(
            workspace__id=workspace_id, delvin_user__email=email
        )
        return owner_check or member_check

    def send_invitation_email(self, email, workspace_name):
        """Sends an email to an invited workspace team member without a Delvin account"""
        user = UserAccount.objects.create(email=email)
        user.is_active = False
        user.save()

        domain = os.environ.get("VITE_REACT_APP_API_URL")
        uid = encode_uid(user.pk)
        token = default_token_generator.make_token(user)
        invitation_link = f"{domain}/verify/team-member/{uid}/{token}/"

        if not re.search(CONSTANTS.TEST_USER_EMAIL_PATTERN, email):
            send_mail(
                "Delvin Invitation",
                f"You have been invited to join the {workspace_name} workspace on Delvin.  Click the following link to set up your account: {invitation_link}",
                DEFAULT_FROM_EMAIL,
                [email],
                fail_silently=False,
            )

    def send_notification_email(self, email, workspace):
        """Sends an email to an invited workspace team member with a Delvin account"""

        if not re.search(CONSTANTS.TEST_USER_EMAIL_PATTERN, email):
            send_mail(
                "Delvin Notice",
                f"You have been added to the {workspace} workspace on Delvin.  Log in to check it out!",
                DEFAULT_FROM_EMAIL,
                [email],
                fail_silently=False,
            )

    def create_team_member(self, user_instance=None, workspace=None, email=None):
        role = TeamMember.get_role_abbreviation(full_role=self.request.data.get("role"))

        team_member_context = {
            "workspace": workspace,
            "role": role,
        }

        if user_instance:
            team_member_context.update(
                {
                    "delvin_user": user_instance,
                    "verification_status": TeamMember.TeamMemberStatus.VERIFIED,
                }
            )
        else:
            team_member_context.update(
                {
                    "verification_status": TeamMember.TeamMemberStatus.UNVERIFIED,
                    "properties": {"email": email},
                }
            )

        return TeamMember.objects.create(**team_member_context)

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = TeamMemberCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)  # raise exception if invalid
        validated_data = serializer.validated_data

        email = validated_data.get("email")
        workspace_id = validated_data.get("workspace_id")

        user = self.validate_and_retrieve_user(email)
        workspace = Workspace.objects.filter(id=workspace_id).first()

        # Prospective team member doesn't have a UserAccount instance
        if not user:
            team_member = self.create_team_member(email=email, workspace=workspace)
            # Necessary to update user context from frontend
            # because unverified users are missing information
            data = {
                "id": team_member.id,
                "workspace_id": validated_data.get("workspace_id"),
                "role": validated_data.get("role"),
                "delvin_user": None,
                "verification_status": team_member.verification_status,
                "properties": team_member.properties,
            }
            self.send_invitation_email(email, team_member.workspace.name)
            return Response(
                data,
                status=status.HTTP_202_ACCEPTED,
            )

        # Prospective team member is already a TeamMember for Workspace
        is_duplicate_workspace_member = self.is_duplicate_workspace_member(
            email, workspace_id
        )
        if is_duplicate_workspace_member:
            return Response(
                {"detail": "This user is already a team member."},
                status=status.HTTP_409_CONFLICT,
            )

        # New team member for Workspace and has UserAccount
        team_member = self.create_team_member(user_instance=user, workspace=workspace)
        data.update(
            {
                "id": team_member.id,
                "verification_status": team_member.verification_status,
                "properties": team_member.properties,
            }
        )
        self.send_notification_email(email, team_member.workspace.name)
        return Response(data, status=status.HTTP_201_CREATED)


team_member_create_view = TeamMemberCreateView.as_view()


class TeamMemberDeleteView(generics.DestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    """Deletes a TeamMember entry"""

    # TODO: This will not work on the Project.owner.  Need to address this issue.

    queryset = TeamMember.objects.all()

    @extend_schema(
        tags=["api"],
        description="Team member delete view",
        parameters=[
            OpenApiParameter(
                name="id",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="Unique identifier of the workspace team member",
            ),
        ],
        responses={
            204: None,
        },
    )
    def delete(self, request, *args, **kwargs):
        id_ = self.kwargs.get("id")
        record_to_delete = self.queryset.filter(id=id_)
        record_to_delete.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


team_member_delete_view = TeamMemberDeleteView.as_view()


class InvitationConfirmationView(generics.CreateAPIView):
    authentication_classes = []
    permission_classes = []

    @extend_schema(
        tags=["api"],
        description="Team member invitation confirmation",
        request=None,  # No specific request serializer needed
        parameters=[
            OpenApiParameter(
                name="uid",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="Unique identifier of the invitation",
            ),
            OpenApiParameter(
                name="token",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="Token",
            ),
            OpenApiParameter(
                name="password",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="password",
            ),
        ],
        responses={
            200: {"description": "User successfully activated"},
        },
    )
    def post(self, request, *args, **kwargs):
        data = request.data
        uid = data["uid"]
        token = data["token"]
        name = data["name"]
        password = data["password"]

        user_pk = decode_uid(uid)
        user = UserAccount.objects.filter(pk=user_pk).first()

        if not user.is_active:
            # Activate the team member's UserAccount
            user.is_active = True
            user.name = name
            user.set_password(password)
            user.save()

            # Update TeamMembers
            team_members = TeamMember.objects.all()
            for team_member in team_members:
                if team_member.properties.get("email", "") == user.email:
                    team_member.delvin_user = user
                    team_member.save()
            return Response(
                {"id": user.id, "email": user.email}, status=status.HTTP_200_OK
            )
        else:
            return Response(
                {"id": user.id, "email": user.email}, status=status.HTTP_200_OK
            )


confirm_invitation = InvitationConfirmationView.as_view()


class TeamMemberUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamMember
        fields = ("role",)


class TeamMemberUpdateView(generics.UpdateAPIView):
    """
    PATCH method is forbidden.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = TeamMemberSerializer

    def get_queryset(self):
        queryset = TeamMember.objects.filter(id=self.kwargs.get("pk"))
        return queryset

    def make_team_member_workspace_owner(self):
        team_member = self.get_object()

        workspace = team_member.workspace
        workspace_owner = workspace.owner

        new_team_member = TeamMember.objects.create(
            delvin_user=workspace_owner,
            workspace=workspace,
            verification_status=team_member.verification_status,
            role=TeamMember.TeamMemberRoles.EDITOR,
            properties=team_member.properties,
        )

        new_owner = team_member.delvin_user
        workspace.owner = new_owner
        workspace.save()

        team_member.delete()

        serializer = self.serializer_class(new_team_member)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = TeamMemberUpdateSerializer(data=data)
        serializer.is_valid(raise_exception=False)

        validated_data = serializer.validated_data
        team_member = self.get_object()

        role = validated_data.get("role") or team_member.role

        if role == TeamMember.TeamMemberRoles.OWNER:
            response = self.make_team_member_workspace_owner()
            return response

        TeamMember.objects.filter(id=self.get_object().id).update(role=role)

        team_member = self.get_object()

        serializer = self.serializer_class(team_member)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)


team_member_update_view = TeamMemberUpdateView.as_view()
