## Maria 

* [ ] Create a model for BigQueryConnection 
* [ ] Supply code for the task that pull data from BigQuery to Delvin
* [ ] (Stretch) Create user interface to create a BigQuery connection


## Cetin

* [X] Set up celery and celery-beat
* [X] Update the k8s cluster to include celery and redis
* [X] Separate endpoints to update project and unpdate project token  
