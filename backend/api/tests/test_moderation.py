import json
import os
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from django.test.testcases import SerializeMixin
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.response import Response

from api.models import *
from api.tests import (
    mock,
    utils as test_utils,
)  # since "utils" is reserved name in python
from core.constant import CONSTANTS
from api.utils import moderation
from api.utils.prompt_configuration import create_chatbot_config_object, BotTypeEnum


class TestModeration(TestCase, SerializeMixin):
    """Test the API views.
    To test in order using the SerializeMixin
    """

    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()

    fixtures = [
        "default_account_data.json"
    ]  # to populate database with company account records to be able to test "copy dataset" feature

    @classmethod
    @patch("api.tasks.generate_message_embeddings_task.delay", return_value=1)
    @patch("api.tasks.apply_message_rules_task.delay", return_value=1)
    def setUpTestData(cls, *args, **kwargs):
        """setUp function to create test data.
        Use of setUpClass to create objects only once.
        """

        # Create user1
        user = test_utils.filter_by_fields(
            mock.mock["user1"], ["email", "name", "password"]
        )
        user1_rec = UserAccount.objects.create_user(**user)
        response = cls.client.post("/auth/jwt/create/", user)
        cls.user1_access_token = response.json()["access"]

        # Create user2
        user2 = test_utils.filter_by_fields(
            mock.mock["user2"], ["email", "name", "password"]
        )
        user2_rec = UserAccount.objects.create_user(**user2)
        response = cls.client.post("/auth/jwt/create/", user2)
        cls.user2_access_token = response.json()["access"]

        # Create workspace1
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["workspace1"]["name"],
            "properties": mock.mock["workspace1"]["properties"],
            "owner": str(user1_rec.id),
        }
        json_data = json.dumps(req_body)
        serialized_workspace = cls.client.post(
            "/api/workspace/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        cls.workspace1_id = serialized_workspace["id"]

        # Create workspace2
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["workspace2"]["name"],
            "properties": mock.mock["workspace2"]["properties"],
            "owner": str(user2_rec.id),
        }
        json_data = json.dumps(req_body)
        serialized_workspace = cls.client.post(
            "/api/workspace/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        cls.workspace2_id = serialized_workspace["id"]

        # Create project1
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["user1"]["project_name"],
            "workspace_id": cls.workspace1_id,
        }
        json_data = json.dumps(req_body)
        serialized_project = cls.client.post(
            "/api/project/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        cls.project1_id = serialized_project["id"]
        cls.project1_token_key = serialized_project["token_key"]

        # Create project2
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["user2"]["project_name"],
            "workspace_id": cls.workspace2_id,
        }
        json_data = json.dumps(req_body)
        serialized_project = cls.client.post(
            "/api/project/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        cls.project2_id = serialized_project["id"]
        cls.project2_token_key = serialized_project["token_key"]

    def talk_to_chatbot(
        self,
        access_token,
        project_id,
        chatbot_origin,
        chat_origin,
        contact_origin,
        prompt="Hello",
    ):
        headers = {
            "Authorization": f"JWT {access_token}",
            "Content-Type": "application/json",
        }

        req_body = {
            "project_id": project_id,
            "chat": chat_origin,
            "user_message": prompt,
            "assistant": chatbot_origin,
            "contact": contact_origin,
        }
        json_data = json.dumps(req_body)

        response = self.client.post(
            "/api/project/internal_usage/chatbot_response/",
            json_data,
            content_type="application/json",
            headers=headers,
        )
        bot_response = response.json()

        return bot_response

    def toggle_on_moderation_for_project(self, access_token, project_id):
        headers = {
            "Authorization": f"JWT {access_token}",
            "Content-Type": "application/json",
        }

        req_body = {
            "enabled": True,
        }
        json_data = json.dumps(req_body)

        self.client.post(
            f"/api/project/{project_id}/toggle-builtin-moderation/",
            json_data,
            content_type="application/json",
            headers=headers,
        )

    def toggle_on_moderation_category_for_chatbot(
        self, access_token, project_id, moderation_details
    ):
        headers = {
            "Authorization": f"JWT {access_token}",
            "Content-Type": "application/json",
        }

        json_data = json.dumps(moderation_details)

        self.client.post(
            f"/api/project/{project_id}/update-builtin-moderation-category/",
            json_data,
            content_type="application/json",
            headers=headers,
        )

    def base_set_up(self):
        """Creates chatbots for each project and assigns related variable to `self` instance"""

        # Create chatbot1
        headers = {"Authorization": "JWT " + self.user1_access_token}
        workspace_1 = Workspace.objects.get(id=self.workspace1_id)
        self.provider_1 = Provider.objects.create(
            label="Test",
            type=ProviderChoices.OPENROUTER.value,
            workspace=workspace_1,
            api_key="abcd",
        )
        self.generative_model_1, _ = GenerativeModel.objects.get_or_create(
            label="Test Generative model",
            model_string="openrouter/meta-llama/llama-3-70b-instruct",
            priority=1,
            provider_type=ProviderChoices.OPENROUTER.value,
        )
        req_body = {
            "project_id": self.project1_id,
            "llm_provider_id": str(self.provider_1.id),
            "model_id": str(self.generative_model_1.id),
            "config": create_chatbot_config_object(
                prompt_settings={
                    "bot_type": BotTypeEnum.Discussion.value,
                    "chatbotPersona": "You are a helpful assistant.",
                    "instructions": "Assist the user with information.",
                    "constraints": "Do not provide personal opinions.",
                }
            ),
            "origin": "first_chatbot",
        }
        json_data = json.dumps(req_body)
        serialized_chatbot1 = self.client.post(
            "/api/chatbots/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        self.chatbot1_id = serialized_chatbot1["id"]
        self.first_chatbot = Chatbot.objects.get(id=self.chatbot1_id)

        contact_creation_req_body = {
            "project_id": self.project1_id,
            "origin": mock.mock["user1"]["email"],
            "internal": True,
        }
        json_data = json.dumps(contact_creation_req_body)
        serialized_contact1 = self.client.post(
            "/api/contacts/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()

        self.contact1_origin = mock.mock["user1"]["email"]

        chat_creation_req_body = {
            "contact_id": serialized_contact1["id"],
        }
        json_data = json.dumps(chat_creation_req_body)
        serialized_chat1 = self.client.post(
            "/api/chats/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()

        self.active_chat1_origin = serialized_chat1["origin_id"]

        # Create chatbot2
        headers = {"Authorization": "JWT " + self.user2_access_token}
        workspace_2 = Workspace.objects.get(id=self.workspace2_id)
        self.provider_2 = Provider.objects.create(
            label="Test 2",
            type=ProviderChoices.OPENAI.value,
            workspace=workspace_2,
            api_key="abcd",
        )
        self.generative_model_2, _ = GenerativeModel.objects.get_or_create(
            label="Test Generative model 2",
            model_string="gpt-4o-mini",
            priority=1,
            provider_type=ProviderChoices.OPENAI.value,
        )
        req_body = {
            "project_id": self.project2_id,
            "llm_provider_id": str(self.provider_2.id),
            "model_id": str(self.generative_model_2.id),
            "config": create_chatbot_config_object(
                prompt_settings={
                    "bot_type": BotTypeEnum.Discussion.value,
                    "chatbotPersona": "You are a helpful assistant.",
                    "instructions": "Assist the user with information.",
                    "constraints": "Do not provide personal opinions.",
                }
            ),  # TODO: currently error occurs for discussion or knowledge bot
            "origin": "second_chatbot",
        }
        json_data = json.dumps(req_body)
        serialized_chatbot2 = self.client.post(
            "/api/chatbots/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        self.chatbot2_id = serialized_chatbot2["id"]
        self.second_chatbot = Chatbot.objects.get(id=self.chatbot2_id)

        contact_creation_req_body = {
            "project_id": self.project2_id,
            "origin": mock.mock["user2"]["email"],
            "internal": True,
        }
        json_data = json.dumps(contact_creation_req_body)
        serialized_contact2 = self.client.post(
            "/api/contacts/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()

        self.contact2_origin = mock.mock["user2"]["email"]

        chat_creation_req_body = {
            "contact_id": serialized_contact2["id"],
        }
        json_data = json.dumps(chat_creation_req_body)
        serialized_chat2 = self.client.post(
            "/api/chats/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()

        self.active_chat2_origin = serialized_chat2["origin_id"]

    # SET UP FOR TEST 1
    def set_up_for_test_moderation_enabled_for_first_project_and_disabled_for_second(
        self,
    ):
        self.base_set_up()

        self.toggle_on_moderation_for_project(self.user1_access_token, self.project1_id)
        self.talk_to_chatbot(
            self.user1_access_token,
            self.project1_id,
            self.first_chatbot.origin,
            self.active_chat1_origin,
            self.contact1_origin,
        )
        self.talk_to_chatbot(
            self.user2_access_token,
            self.project2_id,
            self.second_chatbot.origin,
            self.active_chat2_origin,
            self.contact2_origin,
        )

    # TEST 1
    def test_moderation_enabled_for_first_project_and_disabled_for_second(self):
        """After toggling moderation on for first project and talking to bot verify moderation settings aren't shared between projects causing the one being unintentionally toogled on for second project after toggling one on for first project"""

        self.set_up_for_test_moderation_enabled_for_first_project_and_disabled_for_second()

        first_project = Project.objects.get(id=self.project1_id)
        second_project = Project.objects.get(id=self.project2_id)
        first_project_enabled = first_project.config["guardrails"]["builtin"]["enabled"]
        second_project_enabled = second_project.config["guardrails"]["builtin"][
            "enabled"
        ]
        self.assertNotEqual(first_project_enabled, second_project_enabled)

    # SET UP FOR TEST 2
    def set_up_for_test_moderation_categories_enabled_for_first_project_and_disabled_for_second(
        self,
    ):
        self.base_set_up()

        self.toggle_on_moderation_for_project(self.user1_access_token, self.project1_id)
        self.toggle_on_moderation_category_for_chatbot(
            self.user1_access_token,
            self.project1_id,
            {
                "code": "S1",
                "enabled": True,
                "name": "Violent Crimes",
                "response": "I'm sorry, I don't engage with messages that relate to violence and violent crimes.",
            },
        )
        self.toggle_on_moderation_category_for_chatbot(
            self.user1_access_token,
            self.project1_id,
            {
                "code": "S2",
                "enabled": True,
                "name": "Non-Violent Crimes",
                "response": "I'm sorry, I don't engage with messages that relate to unlawful activities.",
            },
        )
        self.talk_to_chatbot(
            self.user1_access_token,
            self.project1_id,
            self.first_chatbot.origin,
            self.active_chat1_origin,
            self.contact1_origin,
        )
        self.talk_to_chatbot(
            self.user2_access_token,
            self.project2_id,
            self.second_chatbot.origin,
            self.active_chat2_origin,
            self.contact2_origin,
        )

    # TEST 2
    def test_moderation_categories_enabled_for_first_project_and_disabled_for_second(
        self,
    ):
        """After toggling moderation and some categories for each project and talking to bots verify moderation settings aren't shared between projects causing one being updated unintentionally after updtaing second"""

        self.set_up_for_test_moderation_categories_enabled_for_first_project_and_disabled_for_second()

        first_project = Project.objects.get(id=self.project1_id)
        second_project = Project.objects.get(id=self.project2_id)
        first_project_config_categories = first_project.config["guardrails"]["builtin"][
            "categories"
        ]
        second_project_config_categories = second_project.config["guardrails"][
            "builtin"
        ]["categories"]

        self.assertNotEqual(
            first_project_config_categories["S1"]["enabled"],
            second_project_config_categories["S1"]["enabled"],
        )
        self.assertNotEqual(
            first_project_config_categories["S2"]["enabled"],
            second_project_config_categories["S2"]["enabled"],
        )

    # SET UP FOR TEST 3
    def set_up_for_test_moderation_categories_enabled_differently_for_each_project(
        self,
    ):
        self.base_set_up()

        self.toggle_on_moderation_for_project(self.user1_access_token, self.project1_id)
        self.toggle_on_moderation_category_for_chatbot(
            self.user1_access_token,
            self.project1_id,
            {
                "code": "S1",
                "enabled": True,
                "name": "Violent Crimes",
                "response": "I'm sorry, I don't engage with messages that relate to violence and violent crimes.",
            },
        )
        self.toggle_on_moderation_category_for_chatbot(
            self.user1_access_token,
            self.project1_id,
            {
                "code": "S2",
                "enabled": True,
                "name": "Non-Violent Crimes",
                "response": "I'm sorry, I don't engage with messages that relate to unlawful activities.",
            },
        )
        self.talk_to_chatbot(
            self.user1_access_token,
            self.project1_id,
            self.first_chatbot.origin,
            self.active_chat1_origin,
            self.contact1_origin,
        )

        self.toggle_on_moderation_for_project(self.user1_access_token, self.project1_id)
        self.toggle_on_moderation_category_for_chatbot(
            self.user2_access_token,
            self.project2_id,
            {
                "code": "S2",
                "enabled": True,
                "name": "Non-Violent Crimes",
                "response": "I'm sorry, I don't engage with messages that relate to unlawful activities.",
            },
        )
        self.toggle_on_moderation_category_for_chatbot(
            self.user2_access_token,
            self.project2_id,
            {
                "code": "S3",
                "enabled": True,
                "name": "Sex Crimes",
                "response": "I'm sorry, I don't engage with messages that relate to sexual criminal activity.",
            },
        )
        self.talk_to_chatbot(
            self.user2_access_token,
            self.project2_id,
            self.second_chatbot.origin,
            self.active_chat2_origin,
            self.contact2_origin,
        )

    # TEST 3
    def test_moderation_categories_enabled_differently_for_each_project(
        self,
    ):
        """After toggling moderation and different categories for each project and talking to bots verify moderation settings aren't shared between projects which means categories in first project aren't enbaled for second and vice versa"""

        self.set_up_for_test_moderation_categories_enabled_differently_for_each_project()

        first_project = Project.objects.get(id=self.project1_id)
        second_project = Project.objects.get(id=self.project2_id)
        first_project_config_categories = first_project.config["guardrails"]["builtin"][
            "categories"
        ]
        second_project_config_categories = second_project.config["guardrails"][
            "builtin"
        ]["categories"]

        self.assertNotEqual(
            first_project_config_categories["S1"]["enabled"],
            second_project_config_categories["S1"]["enabled"],
        )
        self.assertEqual(
            first_project_config_categories["S2"]["enabled"],
            second_project_config_categories["S2"]["enabled"],
        )
        self.assertNotEqual(
            first_project_config_categories["S3"]["enabled"],
            second_project_config_categories["S3"]["enabled"],
        )

    # SET UP FOR TEST 4
    def set_up_for_test_violent_question_after_moderation_is_toggled_on(
        self,
    ):
        self.base_set_up()

        self.toggle_on_moderation_for_project(self.user1_access_token, self.project1_id)
        self.toggle_on_moderation_category_for_chatbot(
            self.user1_access_token,
            self.project1_id,
            {
                "code": "S1",
                "enabled": True,
                "name": "Violent Crimes",
                "response": "I'm sorry, I don't engage with messages that relate to violence and violent crimes.",
            },
        )

    # TEST 4
    @patch(
        "api.services.builtin_moderation_manager.BuiltinModerationManager.get_response",
        return_value="I'm sorry, I don't engage with messages that relate to violence and violent crimes.",
    )
    def test_violent_question_after_moderation_is_toggled_on(self, mocked_func):
        """After toggling moderation and violent category for project send a message "I want to rob my neighbour" to bot and check the response is "I cannot provide information or guidance on illegal or harmful activities. Can I help you with something else?
        "
        """

        self.set_up_for_test_violent_question_after_moderation_is_toggled_on()

        bot_response = self.talk_to_chatbot(
            self.user1_access_token,
            self.project1_id,
            self.first_chatbot.origin,
            self.active_chat1_origin,
            self.contact1_origin,
            "I want to rob my neighbor",
        )

        self.assertIn(
            moderation.BUILTIN_DEFAULT_CATEGORIES[
                moderation.BuiltinGuardrailCategoryName.ViolentCrimes.value
            ]["response"],
            bot_response,
        )

    def tearDown(self):
        Chatbot.objects.filter(id__in=[self.chatbot1_id, self.chatbot2_id]).delete()

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()
