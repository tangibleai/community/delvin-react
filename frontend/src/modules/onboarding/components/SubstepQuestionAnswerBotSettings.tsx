import { useForm } from "react-hook-form";
import * as z from "zod";
import {
  Form,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
  FormField,
  FormDescription,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { zodResolver } from "@hookform/resolvers/zod";
import { forwardRef, useEffect, useImperativeHandle } from "react";
import { AppDispatch, RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { setOnboardingData } from "@/slices/onboardingSlice";
import { Button } from "@/components/ui/button";
import { questionAnswerBotSampleInput } from "../utils/onboardingSampleInput";

const SubstepQuestionAnswerBotSettings = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();

  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData,
  );

  const formSchema = z.object({
    chatbotPersona: z.string().min(2, {
      message: "Chatbot persona must be at least 2 characters.",
    }),

    constraints: z.string().min(2, {
      message: "Constraints must be at least 2 characters.",
    }),
    fallbackResponses: z.string().min(2, {
      message: "Fallback responses must be at least 2 characters.",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      chatbotPersona:
        onboardingData.questionAnswerBotSettings.chatbotPersona || "",
      constraints: onboardingData.questionAnswerBotSettings.constraints || "",
      fallbackResponses:
        onboardingData.questionAnswerBotSettings.fallbackResponses || "",
    },
  });

  const { handleSubmit, reset } = form;

  useEffect(() => {
    if (onboardingData) {
      reset({
        chatbotPersona:
          onboardingData.questionAnswerBotSettings.chatbotPersona || "",
        constraints: onboardingData.questionAnswerBotSettings.constraints || "",
        fallbackResponses:
          onboardingData.questionAnswerBotSettings.fallbackResponses || "",
      });
    }
  }, [onboardingData, reset]);

  useImperativeHandle(ref, () => ({
    submitForm: () => {
      return new Promise<boolean>((resolve) => {
        handleSubmit(async (values) => {
          const result = await onSubmit(values);
          resolve(result);
        })();
      });
    },
    updateOnboardingDataWithDraft: () => {
      const { chatbotPersona, fallbackResponses, constraints } =
        form.getValues();
      dispatch(
        setOnboardingData({
          ...onboardingData,
          questionAnswerBotSettings: {
            chatbotPersona: chatbotPersona,
            constraints: constraints,
            fallbackResponses: fallbackResponses,
          },
        }),
      );
    },
  }));

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const { chatbotPersona, fallbackResponses, constraints } = values;
    dispatch(
      setOnboardingData({
        ...onboardingData,
        questionAnswerBotSettings: {
          chatbotPersona: chatbotPersona,
          constraints: constraints,
          fallbackResponses: fallbackResponses,
        },
      }),
    );

    return true;
  };

  const fillFieldsWithTemplate = () => {
    form.setValue(
      "chatbotPersona",
      questionAnswerBotSampleInput["chatbotPersona"],
    );
    form.setValue("constraints", questionAnswerBotSampleInput["constraints"]);
    form.setValue(
      "fallbackResponses",
      questionAnswerBotSampleInput["fallbackResponses"],
    );
  };

  return (
    <div className="flex justify-center gap-5">
      <div className="w-[6/7] mx-auto flex flex-col">
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
            <div className="flex flex-col gap-2 items-center w-full text-base">
              <p className="font-medium text-3xl">
                Create a prompt for your assistant
              </p>
              <span className="leading-none">
                The prompt contains all the instructions for your chatbot. You
                can change these later.
              </span>
            </div>
            <div className="flex flex-col items-center">
              <Button onClick={fillFieldsWithTemplate} className="text-lg">
                Use sample prompt
              </Button>
            </div>
            <div className="flex gap-2">
              <div className="flex flex-col gap-3 text-lg">
                <FormField
                  control={form.control}
                  name="chatbotPersona"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Chatbot persona</FormLabel>
                      <FormDescription>
                        Describe the assistant's personality, target audience,
                        and tone of voice.
                      </FormDescription>
                      <FormControl>
                        <Input {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="constraints"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Constraints</FormLabel>
                      <FormDescription>
                        What topics your bot should not respond to? How long
                        should the answer be? What else the bot should not do?
                        etc.
                      </FormDescription>
                      <FormControl>
                        <Textarea {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="fallbackResponses"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Fallback responses</FormLabel>
                      <FormDescription>
                        The response the chatbot should use if it can't find the
                        answer in the knowledge documents you provide.
                      </FormDescription>
                      <FormControl>
                        <Textarea {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
          </form>
        </Form>
      </div>
    </div>
  );
});

export default SubstepQuestionAnswerBotSettings;
