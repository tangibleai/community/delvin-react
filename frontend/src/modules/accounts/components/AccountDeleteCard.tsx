import { Card } from "@tremor/react";
import ConfirmAccountDeletionModal from "./ConfirmAccountDeletionModal";

const AccountDeleteCard = () => {
  return (
    <Card className="border-2 border-black space-y-4">
      <p className="font-semibold text-xl">Delete account</p>
      <p className="text-base">
        Deleting your account will remove your user account and all assistants
        that you own. Please make sure to add a new owner to any project you do
        not want deleted.
      </p>
      <ConfirmAccountDeletionModal />
    </Card>
  );
};

export default AccountDeleteCard;
