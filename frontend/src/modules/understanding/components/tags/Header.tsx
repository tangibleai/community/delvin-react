import { Button } from "@/components/ui/button";
import { Skeleton } from "@/components/ui/skeleton";
import { useToast } from "@/components/ui/use-toast";
import { useDeleteMessageTagDefinitionMutation } from "@/slices/api/messageTagDefinition";
import { MessageTagDefinition } from "@/typings/message_tag_definition";
import { MutationTrigger } from "@reduxjs/toolkit/dist/query/react/buildHooks";
import { BaseQueryFn, MutationDefinition } from "@reduxjs/toolkit/query";
import { AxiosRequestConfig } from "axios";
import { IoReload } from "react-icons/io5";

type Props = {
  activeTagDefinition: MessageTagDefinition;
  setActiveTagDefinition: React.Dispatch<React.SetStateAction<object>>;
  deleteMessageTagDefinition: MutationTrigger<
    MutationDefinition<
      string,
      BaseQueryFn<
        {
          url: string;
          method?: AxiosRequestConfig["method"];
          data?: AxiosRequestConfig["data"];
          params?: AxiosRequestConfig["params"];
          headers?: AxiosRequestConfig["headers"];
        },
        unknown,
        unknown
      >,
      "MessageTagDefinition",
      null,
      "api"
    >
  >;
  deletingMessageTagDefinition: boolean;
};

const Header = ({
  activeTagDefinition,
  setActiveTagDefinition,
  deleteMessageTagDefinition,
  deletingMessageTagDefinition,
}: Props) => {
  const { toast } = useToast();

  const onTagDeleted = async (tagDefinitionId: string) => {
    try {
      await deleteMessageTagDefinition(tagDefinitionId).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Could not remove a tag. Please, try again.",
      });
      return;
    }

    setActiveTagDefinition({});
    toast({
      title: "Success!",
      description: "Tag was successfully removed.",
    });
  };

  return (
    <div className="flex justify-between items-center">
      {Object.keys(activeTagDefinition).length ? (
        <div>
          <h1 className="text-xl font-bold">{activeTagDefinition?.name}</h1>
          <p className="text-gray-600">{activeTagDefinition?.description}</p>
        </div>
      ) : (
        <div className="w-36 space-y-2">
          <Skeleton className="h-10 bg-gray-300" />
          <Skeleton className="h-3 bg-gray-300" />
        </div>
      )}
      <Button
        variant="destructive"
        disabled={
          deletingMessageTagDefinition ||
          !Object.keys(activeTagDefinition).length
        }
        onClick={() => {
          onTagDeleted(activeTagDefinition?.id);
        }}
      >
        {deletingMessageTagDefinition && (
          <IoReload className="mr-2 h-4 w-4 animate-spin" />
        )}
        Delete tag
      </Button>
    </div>
  );
};

export { Header };
