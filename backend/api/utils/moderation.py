from enum import Enum
from pydantic import BaseModel


class BuiltinGuardrailCategoryName(str, Enum):
    ViolentCrimes = "S1"
    NonViolentCrimes = "S2"
    SexCrimes = "S3"
    ChildExploitation = "S4"
    SpecializedAdvice = "S5"
    Privacy = "S6"
    IntellectualProperty = "S7"
    IndiscriminateWeapons = "S8"
    Hate = "S9"
    SelfHarm = "S10"
    SexualContent = "S11"


class BuiltinCategory(BaseModel):
    code: str
    name: str
    enabled: bool
    response: str


#
# LMGUARD_DEFAULT_RESPONSES = {
#     BuiltinGuardrailCategoryName.ViolentCrimes: ,
#     BuiltinGuardrailCategoryName.NonViolentCrimes: "I'm sorry, I don't engage with messages that relate to unlawful activities.",
#     BuiltinGuardrailCategoryName.SexCrimes: "I'm sorry, I don't engage with messages that relate to sexual criminal activity. ",
#     BuiltinGuardrailCategoryName.ChildExploitation: "I'm sorry, I don't engage with messages that relate to exploitation of children",
#     BuiltinGuardrailCategoryName.SpecializedAdvice: "I'm sorry, I can't give specialized medical, financial, or legal advice.",
#     BuiltinGuardrailCategoryName.Privacy: "I'm sorry, I can't reveal personal information of individuals.",
#     BuiltinGuardrailCategoryName.IntellectualProperty: "I'm sorry, I can't violate intellectual property rights",
# }

BUILTIN_DEFAULT_CATEGORIES = {
    BuiltinGuardrailCategoryName.SexualContent.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.SexualContent.value,
            name="Sexual Content",
            enabled=False,
            response="I don't engage with these kind of comments. ⛔",
        )
    ),
    BuiltinGuardrailCategoryName.SelfHarm.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.SelfHarm.value,
            name="Self Harm",
            enabled=False,
            response="I'm really sorry to hear you're feeling this way. It's important to talk to someone who can offer support; please consider calling a hotline or speaking to a healthcare professional who can provide you with the guidance and help you need.",
        )
    ),
    BuiltinGuardrailCategoryName.Privacy.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.Privacy.value,
            name="Privacy",
            enabled=False,
            response="I cannot reveal any personal information about individuals.",
        )
    ),
    BuiltinGuardrailCategoryName.Hate.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.Hate.value,
            name="Hate",
            enabled=False,
            response="I don't appreciate hate speech of any kind.",
        )
    ),
    BuiltinGuardrailCategoryName.ViolentCrimes.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.ViolentCrimes.value,
            name="Violent Crimes",
            enabled=False,
            response="I don't engage with messages that relate to violence and violent crimes.",
        )
    ),
    BuiltinGuardrailCategoryName.NonViolentCrimes.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.NonViolentCrimes.value,
            name="Non-Violent Crimes",
            enabled=False,
            response="I don't engage with messages that relate to unlawful activities.",
        )
    ),
    BuiltinGuardrailCategoryName.SexCrimes.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.SexCrimes.value,
            name="Sex Crimes",
            enabled=False,
            response="I don't engage with messages that relate to sexual criminal activity.",
        )
    ),
    BuiltinGuardrailCategoryName.ChildExploitation.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.ChildExploitation.value,
            name="Child Exploitation",
            enabled=False,
            response="I don't engage with messages that relate to exploitation and abuse of children",
        )
    ),
    BuiltinGuardrailCategoryName.SpecializedAdvice.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.SpecializedAdvice.value,
            name="Specialized Advice",
            enabled=False,
            response="I'm sorry, I can't give specialized medical, financial, or legal advice.",
        )
    ),
    BuiltinGuardrailCategoryName.IntellectualProperty.value: dict(
        BuiltinCategory(
            code=BuiltinGuardrailCategoryName.IntellectualProperty.value,
            name="Intellectual Property",
            enabled=False,
            response="I can't engage in discussions that may violate intellectual property rights",
        )
    ),
}
