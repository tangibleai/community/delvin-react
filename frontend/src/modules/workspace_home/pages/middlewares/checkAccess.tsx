import { RootState } from "@/store";
import { ComponentType, ReactElement } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Loading from "@/components/Loading.tsx";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { useGetTeamMembersQuery } from "@/slices/api/teamMemberApiSlice";
import UserHasNoAccessToWorkspace from "../UserHasNoAccessToWorkspace";

function proceedToWorkspaceHomeOrShowErrorPage<P extends object>(
  Component: ComponentType<P>
): (props: P) => ReactElement {
  return function WithAuthentication(props: P): ReactElement {
    const user = useSelector((state: RootState) => state.auth.user);

    const routeParams = useParams();
    const workspaceId = routeParams.workspaceId!;

    const { data: workspace, isFetching: fetchingWorkspace } =
      useGetWorkspaceQuery(workspaceId);
    const { data: teamMembers = [], isFetching: fetchingTeamMembers } =
      useGetTeamMembersQuery({
        workspace_id: workspaceId,
      });

    const userHasAccessToWorkspace =
      workspace?.owner == user?.email ||
      teamMembers.some((member) => member.delvin_user == user?.email);

    if (fetchingWorkspace || fetchingTeamMembers) {
      return <Loading />;
    }

    if (user && userHasAccessToWorkspace) {
      return <Component {...(props as P)} />;
    }

    return <UserHasNoAccessToWorkspace />;
  };
}

export default proceedToWorkspaceHomeOrShowErrorPage;
