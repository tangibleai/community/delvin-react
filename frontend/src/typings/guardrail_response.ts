import { GuardrailAction } from "./guardrail_action";
import { MessageTagDefinition } from "./message_tag_definition";

export interface GuardrailResponse {
  id: string;
  guardrail_action: GuardrailAction;
  project: string;
  message_tag_definition: MessageTagDefinition;
}

export interface NewGuardrailResponse {
  guardrail_action: GuardrailAction;
  project: string;
  message_tag_definition: MessageTagDefinition;
  examples: string [];
}