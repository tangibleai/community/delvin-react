import json
from fixtures.flow_samples import SAMPLE_UI_EDGES, SAMPLE_UI_NODES
from flows.flow_graph import (
    ConditionBlockNode,
    ConditionEdge,
    Edge,
    FlowGraph,
    Node,
    NodeTypeEnum,
    PromptNode,
    UiNodeTypeEnum,
)


def build_flow_graph(ui_nodes: list[dict], ui_edges: list[dict]) -> FlowGraph:
    first_node, start_node_edge = find_first_node_and_edge(ui_nodes, ui_edges)
    if not first_node:
        raise ValueError("First node not found")

    ui_edges = [edge for edge in ui_edges if edge["id"] != start_node_edge["id"]]
    nodes = create_nodes_from_ui_nodes(ui_nodes, ui_edges)
    nodes = create_edges_from_ui_edges(ui_nodes, ui_edges, nodes)

    flow_graph = FlowGraph(nodes=nodes, source_node_id=first_node["id"])

    return flow_graph


def find_first_node_and_edge(
    ui_nodes: list[dict], ui_edges: list[dict]
) -> (dict, dict):
    start_node = next(
        (node for node in ui_nodes if node["type"] == NodeTypeEnum.StartNode), None
    )
    if start_node:
        first_edge = next(
            (edge for edge in ui_edges if edge["source"] == start_node["id"]), None
        )
        first_node_id = first_edge["target"]
        return (
            next((node for node in ui_nodes if node["id"] == first_node_id), None),
            first_edge,
        )


def create_nodes_from_ui_nodes(
    ui_nodes: list[dict], ui_edges: list[dict]
) -> dict[str, Node]:
    nodes_without_start_node = gather_nodes_without_start_node(ui_nodes, ui_edges)
    filtered_nodes = exclude_single_condition_nodes(nodes_without_start_node)
    typed_nodes = {
        node["id"]: create_node_with_type(node)
        for node in filtered_nodes
        if node is not None
    }

    return typed_nodes


def gather_nodes_without_start_node(
    ui_nodes: list[dict], ui_edges: list[dict]
) -> list[dict]:
    node_ids_in_edges = set(edge["source"] for edge in ui_edges) | set(
        edge["target"] for edge in ui_edges
    )
    nodes_without_start_node = [
        node
        for node in ui_nodes
        if node["id"] in node_ids_in_edges and node["type"] != NodeTypeEnum.StartNode
    ]

    return nodes_without_start_node


def exclude_single_condition_nodes(nodes: list[dict]) -> list[dict]:
    nodes = [
        node
        for node in nodes
        if node["type"] != UiNodeTypeEnum.SingleConditionBlock.value
    ]

    return nodes


def create_node_with_type(node: dict) -> Node:
    node_type = node["type"]
    node_class = Node
    if node_type == NodeTypeEnum.MessageNode:
        node_class = PromptNode
    elif node_type == NodeTypeEnum.ConditionBlock:
        node_class = ConditionBlockNode

    node = node_class.from_ui_node(node)

    return node


def create_edges_from_ui_edges(
    ui_nodes: list[dict], ui_edges: list[dict], nodes: dict[str, Node]
) -> dict:
    for edge in ui_edges:
        source_node_id = edge["source"]
        target_node_id = edge["target"]

        source_node = nodes.get(source_node_id)

        ui_source_node = next(
            (node for node in ui_nodes if node["id"] == source_node_id)
        )
        if ui_source_node["type"] == UiNodeTypeEnum.SingleConditionBlock.value:
            condition_block_node_id = ui_source_node["parentId"]
            edge_obj = ConditionEdge(
                id=ui_source_node["id"],
                source_node_id=condition_block_node_id,
                target_node_id=target_node_id,
                condition=ui_source_node["data"]["condition"],
            )
            _, condition_block_node_obj = next(
                (id, node)
                for id, node in nodes.items()
                if id == condition_block_node_id
            )
            source_node = condition_block_node_obj
            condition_block_node_obj.edges.append(edge_obj)
        else:
            edge_obj = Edge(
                source_node_id=source_node_id,
                target_node_id=target_node_id,
            )

        source_node.edges.append(edge_obj)

    return nodes


if __name__ == "__main__":
    flow_graph = build_flow_graph(SAMPLE_UI_NODES, SAMPLE_UI_EDGES)
    print(json.dumps(flow_graph.json(), indent=2))
