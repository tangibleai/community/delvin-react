import { Dataset as DatasetType } from "@/typings/dataset";

type Props = {
  dataset: DatasetType;
  datasetSelected: React.Dispatch<React.SetStateAction<object>>;
};

const Dataset = ({ dataset, datasetSelected }: Props) => {
  return (
    <div
      onClick={() => {
        datasetSelected(dataset);
      }}
      className="bg-white hover:bg-gray-100 p-2 mb-2 rounded shadow cursor-pointer "
    >
      {dataset.name}
    </div>
  );
};

export { Dataset };
