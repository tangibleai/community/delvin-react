import { PromptNode } from "../components/custom_nodes/PromptNode";
import { StartNode } from "../components/custom_nodes/StartNode";
import { ConditionBlock } from "../components/custom_nodes/ConditionBlock";
import { SingleCondition } from "../components/custom_nodes/SingleCondition";

// we define the nodeTypes outside of the component to prevent re-renderings
// you could also use useMemo inside the component
export const nodeTypes = {
  promptNode: PromptNode,
  startNode: StartNode,
  conditionBlock: ConditionBlock,
  singleCondition: SingleCondition,
};
