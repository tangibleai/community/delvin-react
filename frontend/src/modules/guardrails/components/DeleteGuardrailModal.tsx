import { useState } from "react";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { HiTrash } from "react-icons/hi2";

type Props = {
  guardRailID: string;
  onDelete: any;
};

function DeleteGuardrailModal({ guardRailID, onDelete }: Props) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  function handleClickDelete() {
    try {
      onDelete(guardRailID);
      setIsModalOpen(false);
    } catch (error) {
      console.error("Failed to delete guardrail:", error);
    }
  }

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        <Button variant="ghost">
          <HiTrash />
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-md">
        <DialogHeader>
          <DialogTitle>Delete guardrail</DialogTitle>
        </DialogHeader>
        <p>Are you sure you want to delete this guardrail?</p>
        <DialogFooter>
          <Button variant="destructive" onClick={() => handleClickDelete()}>
            Delete
          </Button>
          <Button variant="outline" onClick={() => setIsModalOpen(false)}>
            Cancel
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}

export default DeleteGuardrailModal;
