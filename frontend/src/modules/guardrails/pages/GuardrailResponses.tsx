import { Tab, TabList, TabPanel, TabPanels, TabGroup } from "@tremor/react";

import BuiltinModeration from "../components/BuiltinModeration";
import CustomGuardrails from "../components/CustomGuardrails";

export default function GuardrailResponses() {
  return (
    <div className="h-full flex flex-col">
      <div className="head-section">
        <h1 className="header">Guardrails</h1>
      </div>
      <TabGroup>
        <TabList className="mt-2">
          <Tab>Custom guardrails</Tab>
          <Tab>Built-in moderation</Tab>
        </TabList>
        <TabPanels>
          <TabPanel className="text-left">
            <CustomGuardrails />
          </TabPanel>
          <TabPanel>
            <BuiltinModeration />
          </TabPanel>
        </TabPanels>
      </TabGroup>
    </div>
  );
}
