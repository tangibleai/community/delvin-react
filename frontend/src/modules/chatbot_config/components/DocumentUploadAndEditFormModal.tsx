import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Label } from "@/components/ui/label";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Separator } from "@/components/ui/separator";
import { zodResolver } from "@hookform/resolvers/zod";

import { ChatbotConfig } from "@/typings/chatbot_config";
import { Document } from "@/typings/document";
import DocumentFormFile from "./DocumentFormFile";
import DocumentFormGoogle from "./DocumentFormGoogle";
import DocumentFormText from "./DocumentFormText";

type Props = {
  document: Document;
  selectedChatbot: ChatbotConfig;
  handleAddNewDocument: (newDocument: Document) => void;
  submit_action: string;
};

function DocumentUploadAndEditFormModal({
  document,
  selectedChatbot,
  handleAddNewDocument,
  submit_action,
}: Props) {
  const [error, setError] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedUploadOption, setSelectedUploadOption] = useState(
    "properties" in document ? document.properties.doc_type : "text"
  );

  // Form Text
  const modal_trigger_button_text =
    submit_action == "add" ? "Add document" : document.title;
  const modal_text =
    submit_action == "add" ? "Add document" : "Update document";

  const routeParams = useParams();
  const projectId = routeParams.projectId;

  const formSchema = z.object({
    title: z.string().min(1, {
      message: "Please enter a title",
    }),
    content: z.string().min(4, {
      message: "Please add information your bot should know",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      title: document.title,
      content: document.content,
    },
  });

  useEffect(() => {
    // Handles resetting the form after submit or x
    // Can't directly access x in Dialog
    if (!isModalOpen) {
      form.reset();
    }
  }, [isModalOpen]);

  async function handleModalAfterSubmit(result: any) {
    handleAddNewDocument(result);
    setError(null);
    setIsModalOpen(false);
    if (submit_action == "update") {
    } else {
      setSelectedUploadOption("file");
    }
  }

  async function handleCancel() {
    setError(null);
    setIsModalOpen(false);
    if (submit_action == "update") {
    } else {
      setSelectedUploadOption("file");
    }
  }

  const handleOptionChange = (file_type: string) => {
    setSelectedUploadOption(file_type);
  };

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger asChild>
        {submit_action == "add" ? (
          <Button variant="default">{modal_trigger_button_text}</Button>
        ) : (
          <p className="hover:underline">{modal_trigger_button_text}</p>
        )}
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{modal_text}</DialogTitle>
          <DialogDescription>
            Add information that your bot should use when responding to users.
          </DialogDescription>
        </DialogHeader>

        {submit_action == "add" ? (
          <>
            <RadioGroup defaultValue={selectedUploadOption}>
              <Label>File location</Label>
              <p>Select how you want to add a document.</p>
              <div className="flex items-center space-x-2">
                <RadioGroupItem
                  value="text"
                  id="option-one"
                  onClick={() => handleOptionChange("text")}
                />
                <Label htmlFor="option-one">Form</Label>
              </div>
              <div className="flex items-center space-x-2">
                <RadioGroupItem
                  value="file"
                  id="option-two"
                  onClick={() => handleOptionChange("file")}
                />
                <Label htmlFor="option-two">Select file</Label>
              </div>
              <div className="flex items-center space-x-2">
                <RadioGroupItem
                  value="google"
                  id="option-three"
                  onClick={() => handleOptionChange("google")}
                />
                <Label htmlFor="option-three">Google Doc</Label>
              </div>
            </RadioGroup>
            <Separator />
          </>
        ) : (
          <></>
        )}

        {selectedUploadOption == "file" ? (
          <DocumentFormFile
            document={document as Document}
            selectedChatbot={selectedChatbot}
            onSubmit={handleModalAfterSubmit}
            onCancel={handleCancel}
            submit_action={submit_action}
          />
        ) : selectedUploadOption == "google" ? (
          <DocumentFormGoogle
            document={document as Document}
            selectedChatbot={selectedChatbot}
            onSubmit={handleModalAfterSubmit}
            onCancel={handleCancel}
            submit_action={submit_action}
          />
        ) : (
          <DocumentFormText
            document={document as Document}
            selectedChatbot={selectedChatbot}
            onSubmit={handleModalAfterSubmit}
            onCancel={handleCancel}
            submit_action={submit_action}
          />
        )}
      </DialogContent>
    </Dialog>
  );
}

export default DocumentUploadAndEditFormModal;
