## Maria 
* [ ] Write specs for NLU model interface 
* [ ] Write specs for v1 of RAG analysis

## Cetin

* [X] Create a message embedding every time the message is received
* [X] Do the above in a Celery task
* [X] Create Django models for MessageTagGroup, MessageTagDefinition, MessageTag, MessageTagRule
* [X] Create Endpoint to create MessageTagDefinition and MessageTagRule

## Hobs 
* [ ] Implement the NLU api according to spec
