## Maria
* [ ] Create API endpoint for creating the BigQuery connection
* [ ] Write the initial sync task that pulls data from BigQuery to Delvin and runs once
* [ ] Write the periodic sync task that pulls data every hour


## Cetin
* [X] Study Kubernetes!
* [X] Stretch: Check if djoser allows authentication with httpOnly cookie
  * Alternative way [blacklist](https://medium.com/django-rest/logout-django-rest-framework-eb1b53ac6d35)
