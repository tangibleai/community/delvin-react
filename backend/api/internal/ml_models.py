from rest_framework import serializers, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import Project, MLModel
from api.utils import views


class MLModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = MLModel
        exclude = ["sklearn_pipeline"]


class RetrainModelView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        data = request.data
        project_id = data.get("project_id")
        project = Project.objects.get(id=project_id)
        ml_model = MLModel.objects.filter(project=project).first()

        views.retrain_model(project, ml_model)

        ml_model.status = MLModel.Status.UP_TO_DATE
        ml_model.save()

        return Response(
            "Model retrained",
            status=status.HTTP_201_CREATED,
        )


retrain_model_view = RetrainModelView.as_view()


class MLModelViewGETRequestSerializer(serializers.Serializer):
    project_id = serializers.UUIDField()


class MLModelView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    get_request_serializer = MLModelViewGETRequestSerializer
    get_response_serializer = MLModelSerializer

    def get(self, request):
        serializer = self.get_request_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        project_id = validated_data.get("project_id")
        project = Project.objects.get(id=project_id)

        ml_model = MLModel.objects.filter(project=project).first()

        response_serializer = self.get_response_serializer(ml_model)
        serialized_ml_model = response_serializer.data

        return Response(serialized_ml_model)


ml_model_view = MLModelView.as_view()
