import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Tag } from "./Tag";
import { useToast } from "@/components/ui/use-toast";
import { IoReload } from "react-icons/io5";
import { UnderstandingQueueTag } from "@/typings/understanding_queue_tag";
import { FaPlus } from "react-icons/fa6";
import {
  useListMessageTagDefinitionsQuery,
  usePredictTagsQuery,
} from "@/slices/api/messageTagDefinition";
import { MessageTagDefinition } from "@/typings/message_tag_definition";
import { useTagTextMutation } from "@/slices/api/messageTagApiSlice";

const QueueTabContent = () => {
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const [tagsForPrompt, setTagsForPrompt] = useState<UnderstandingQueueTag[]>(
    []
  );

  const [availableTags, setAvailableTags] = useState<MessageTagDefinition[]>(
    []
  ); // tags listed in a dropdown to tag user text

  const [selectedTagId, setSelectedTagId] = useState<string>();
  const [userPrompt, setUserPrompt] = useState("");
  const [taggingPrompt, setTaggingPrompt] = useState(false); // true in the middle of wait time between request to tag message accepted by server and receiving a response

  const [predictingTags, setPredictingTags] = useState(false);
  const [triedToPredictTags, setTriedToPredictTags] = useState(false);

  const { data: tags = [] } = useListMessageTagDefinitionsQuery(projectId);

  const { data: predictedTags = [] } = usePredictTagsQuery(
    {
      text: userPrompt,
      project_id: projectId,
    },
    {
      skip: userPrompt === "", // Skip the query if there's no workspace ID
    }
  );
  const [tagText, { isLoading }] = useTagTextMutation();

  useEffect(() => {
    fetchAndSetTags();
  }, []);

  useEffect(() => {
    if (availableTags.length > 0) setSelectedTagId(availableTags[0].id);
  }, [availableTags]);

  const fetchAndSetTags = async () => {
    setAvailableTags(tags);
  };

  const handleUserTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target as HTMLInputElement;
    setUserPrompt(target.value);
  };

  const handlePredictTagsButtonPressed = async () => {
    setAvailableTags([]);
    setTagsForPrompt([]);
    setPredictingTags(true);
    setTriedToPredictTags(true);

    const convertedPredictedTags = predictedTags.map(
      (item: UnderstandingQueueTag) => ({
        ...item,
        score: Math.floor(item.score! * 100),
      })
    );

    if (convertedPredictedTags.length === 0) {
      toast({
        title: "No tags predicted for text.",
      });
      setAvailableTags(tags);
      setPredictingTags(false);
      return;
    }
    const availableTagsAfterPrediction = tags.filter((tag) => {
      const tagAmongPredited = convertedPredictedTags.some(
        (predictedTag: UnderstandingQueueTag) => predictedTag.tag.id === tag.id
      );
      return !tagAmongPredited;
    });

    setTagsForPrompt(convertedPredictedTags);
    setAvailableTags(availableTagsAfterPrediction);
    setPredictingTags(false);
  };

  const handleTagConfirmationToggled = (
    confirmedTag: UnderstandingQueueTag
  ) => {
    setTagsForPrompt((tags) =>
      tags.map((tag) => ({
        ...tag,
        isConfirmed:
          tag.tag.id === confirmedTag.tag.id
            ? !tag.isConfirmed
            : tag.isConfirmed,
      }))
    );
  };

  const handleAddTagButtonPressed = () => {
    const selectedTag = availableTags.find((tag) => tag.id === selectedTagId);
    const availableTagsRemained = availableTags.filter(
      (tag) => tag.id !== selectedTagId
    );
    setAvailableTags(availableTagsRemained);
    const addedTag = {
      tag: selectedTag,
      isConfirmed: true,
    } as UnderstandingQueueTag;
    setTagsForPrompt([...tagsForPrompt, addedTag]);
  };

  const { toast } = useToast();

  const handleSaveTaggedMessageInDb = async () => {
    setTaggingPrompt(true);
    setUserPrompt("");
    setTagsForPrompt([]);
    fetchAndSetTags();

    const confirmedTags = tagsForPrompt.filter((tag) => tag.isConfirmed);

    try {
      await tagText({
        project_id: projectId,
        message_text: userPrompt,
        tag_ids: confirmedTags.map((tag) => tag.tag.id),
      }).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong. Please, try again.",
      });
    }

    toast({
      title: "Success!",
      description: "Prompt was successfully tagged.",
    });

    setTaggingPrompt(false);
    setTriedToPredictTags(false);

    return;
  };

  return (
    <div className="mt-10 w-full">
      <div className="w-1/2 mx-auto">
        <p className="text-3xl font-semibold">Classify a message</p>

        <div className="my-10">
          <p>Enter a message to classify using your existing tags.</p>
          <div className="my-5 flex justify-between items-center gap-8">
            <Input
              id="tag_name"
              className="text-lg border border-black"
              value={userPrompt}
              onChange={handleUserTextChange}
              placeholder="Thanks for talking today!"
            />
            {predictingTags ? (
              <Button disabled className="text-lg">
                <IoReload className="mr-2 h-4 w-4 animate-spin" />
                Predict tags
              </Button>
            ) : (
              <Button
                className="text-lg"
                onClick={handlePredictTagsButtonPressed}
              >
                Predict tags
              </Button>
            )}
          </div>
        </div>
        {triedToPredictTags && !predictingTags && (
          <>
            <p>
              A list of potential tag matches is below. Click "Confirm" if you
              agree with a tag suggestion, and you can add other relevant tags.
            </p>
            {tagsForPrompt.length > 0 && (
              <div className="my-5 flex flex-col gap-3 mb-10">
                {tagsForPrompt.map((tag) => (
                  <Tag
                    tag={tag}
                    handleTagConfirmationToggled={handleTagConfirmationToggled}
                  />
                ))}
              </div>
            )}
            {availableTags.length > 0 && (
              <div className="flex gap-5">
                <Select onValueChange={setSelectedTagId} value={selectedTagId}>
                  <SelectTrigger className="w-1/5 text-base">
                    <SelectValue placeholder="Select tag" />
                  </SelectTrigger>
                  <SelectContent>
                    {availableTags.length > 0 ? (
                      availableTags.map((tag) => (
                        <SelectItem
                          key={tag.id}
                          value={tag.id}
                          className="text-base"
                        >
                          {tag.name}
                        </SelectItem>
                      ))
                    ) : (
                      <></>
                    )}
                  </SelectContent>
                </Select>
                {predictingTags ? (
                  <Button disabled className="text-lg text-primary gap-1">
                    <FaPlus />
                    Add tag
                  </Button>
                ) : (
                  <Button
                    onClick={handleAddTagButtonPressed}
                    className="text-lg bg-muted text-primary gap-1 hover:text-white"
                  >
                    <FaPlus />
                    Add tag
                  </Button>
                )}
              </div>
            )}

            {tagsForPrompt.some((tag) => {
              return tag.isConfirmed === true;
            }) ? (
              <Button
                className="mt-10 text-lg"
                onClick={handleSaveTaggedMessageInDb}
              >
                Save
              </Button>
            ) : taggingPrompt ? (
              <Button disabled className="mt-10 text-lg">
                <IoReload className="mr-2 h-4 w-4 animate-spin" />
                Tagging message
              </Button>
            ) : (
              <Button disabled className="mt-10 text-lg">
                Save
              </Button>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export { QueueTabContent };
