import { useState } from "react";

type Props = {
  document_status: string;
};

export default function StatusComponent({ document_status }: Props) {
  const [showTooltip, setShowTooltip] = useState(false);

  let status = "";
  let status_message = "";

  if (document_status == "C") {
    status = "Created, in processing";
    status_message =
      "The new document has been saved and is being processed.  It might take a few minutes.";
  } else if (document_status == "U") {
    status = "Updated, in processing";
    status_message =
      "Your changes have been saved, and we a still processing the changes.  It may take a few minutes.";
  } else if (document_status == "P") {
    status = "Processed";
    status_message =
      "All processing is finished.  Your document is live for your bot to use.";
  } else {
    status = "Error";
    status_message = "There was an error processing your document.";
  }

  return (
    <>
      <div className="flex justify-between">
        <p
          onMouseEnter={() => setShowTooltip(true)}
          onMouseLeave={() => setShowTooltip(false)}
        >
          {status}
        </p>
      </div>
      {showTooltip && (
        <div
          data-tooltip="tooltip"
          className="bg-gray-800 text-white p-2 rounded-md absolute "
        >
          {status_message}
        </div>
      )}
    </>
  );
}
