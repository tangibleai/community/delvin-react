class RetrainModelView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        data = request.data
        project_id = data.get("project_id")
        project = Project.objects.get(id=project_id)
        ml_model = MLModel.objects.filter(project=project).first()

        views.retrain_model(project, ml_model)

        # ================================================================================
        messages = Message.objects.filter(project=project)
        for m in messages:
            print("text", m.text)
            for t in MessageTag.objects.filter(message=m, ml_model=ml_model):
                print(f"    {t.name}", end="")
            MessageTag.objects.filter(message=m, ml_model=ml_model).delete()
            new_message_tags = text_tags.predict_tags(m, project)
            print("new tags", new_message_tags)
            for t_name in new_message_tags:
                tag = MessageTagDefinition.objects.get(name=t_name, project=project)
                MessageTag.objects.create(message=m, tag=tag, ml_model=ml_model)
            print()
        # ================================================================================

        print("retrained")

        return Response(
            "Model retrained",
            status=status.HTTP_201_CREATED,
        )


class CopyFromProjectDefaultDatasetView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = DatasetSerializer

    def post(self, request):
        data = self.request.data
        serializer = self.serializer_class(data=data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        validated_data = serializer.validated_data
        name = validated_data.get("name")
        project = validated_data.get("project")
        project_name = project.name
        project_owner_email = project.owner.email
        default_dataset_name = Dataset.objects.get(
            project=project, name=CONSTANTS.DEFAULT_USER_DATASET_NAME
        ).name

        project = Project.objects.get(name=project_name, owner=project_owner_email)

        dataset_copy.copy_dataset_and_dependencies(
            t_project_id=project.id
            t_owner_email=project_owner_email,
            s_project_name=project_name,
            s_owner_email=project_owner_email,
            s_dataset_name=default_dataset_name,
            t_dataset_name=name,
        )

        return Response("Dataset created")


copy_from_project_default_dataset_view = CopyFromProjectDefaultDatasetView.as_view()


class GetDatasetsView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = DatasetSerializer

    def get_queryset(self):
        data = self.request.data
        project_id = self.request.query_params.get("project_id", None) or data.get(
            "project_id", None
        )

        if project_id is None:
            return Response(
                {"error": "Missing project id"}, status=status.HTTP_400_BAD_REQUEST
            )

        project = Project.objects.get(id=project_id)
        datasets = Dataset.objects.filter(
            project=project
        )  # .exclude(name=CONSTANTS.DEFAULT_DATASET_NAME)

        return datasets


get_datasets_view = GetDatasetsView.as_view()


class DatasetDeleteAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = DatasetSerializer

    def post(self, request):
        data = self.request.data
        project_id = self.request.query_params.get("project_id", None) or data.get(
            "project_id", None
        )

        if not project_id:
            return Response(
                {"error": "Missing project id or record not found"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        dataset_id = self.request.query_params.get("dataset_id", None) or data.get(
            "dataset_id", None
        )

        dataset = Dataset.objects.filter(id=dataset_id).first()
        if not dataset:
            return Response(
                {"error": "Missing dataset id or record not found"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        tagged_messages = dataset.tagged_messages

        dataset.delete()

        for t in tagged_messages:
            if t.dataset_set.all():
                t.delete()

        serializer = self.serializer_class(dataset)

        return Response(serializer.data)


dataset_delete_view = DatasetDeleteAPIView.as_view()


class GetDatasetTaggedMesssagesView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = MessageTagSerializer

    def get(self, request):
        data = self.request.data

        dataset_id = self.request.query_params.get("dataset_id", None) or data.get(
            "dataset_id", None
        )

        dataset = Dataset.objects.filter(id=dataset_id).first()
        if not dataset:
            return Response(
                {"error": "Missing dataset id or record not found"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        ml_model = MLModel.objects.get(dataset=dataset)
        message_tag_recs = MessageTag.objects.filter(ml_model=ml_model)

        project = dataset.project
        default_dataset = Dataset.objects.filter(
            project=project, name=CONSTANTS.DEFAULT_USER_DATASET_NAME
        ).first()
        ml_model_for_default_dataset = MLModel.objects.get(dataset=default_dataset)
        default_dataset_message_tag_recs = MessageTag.objects.filter(
            ml_model=ml_model_for_default_dataset
        )

        # ================
        message_tag_group = MessageTag.objects.filter(ml_model=ml_model)
        for mt in message_tag_group:
            print(mt.message.text, "    ", mt.tag.name)

        message_tags = message_tag_group.select_related("message").prefetch_related(
            Prefetch(
                "message__messagetag_set",
                queryset=MessageTag.objects.filter(ml_model=ml_model).select_related(
                    "tag"
                ),
            )
        )

        result = []
        for mt in message_tags:
            # message_tag_id_to_delete = None
            # for message_tag in mt.message.messagetag_set.all():
            #     if message_tag.tag.id == tag_definition_id:
            #         message_tag_id_to_delete = message_tag.id
            #         break

            message_dict = {
                "message": mt.message.text,
                "tags": [tag.tag.name for tag in mt.message.messagetag_set.all()],
            }
            result.append(message_dict)

        for item in result:
            print(item["message"], item["tags"])
        # ============

        # transformed_tagged_messages = []
        # for rec in message_tag_recs:
        #     message = rec.message
        #     tag_definitions = []
        #     tags_for_message = message_tag_recs.filter(message=message)
        #     for rec in tags_for_message:
        #         tag = rec.tag
        #         tagged_mesages_from_default_dataset = (
        #             default_dataset_message_tag_recs.filter(
        #                 message__text=message.text, tag__name=tag.name
        #             )
        #         )
        #         is_from_default_dataset = len(tagged_mesages_from_default_dataset) > 0
        #         tag_definitions.append(
        #             {
        #                 "name": tag.name,
        #                 "is_from_default_dataset": is_from_default_dataset,
        #             }
        #         )
        #     transformed_tagged_messages.append(
        #         {"message": {"text": message.text}, "tag_definitions": tag_definitions}
        #     )

        return Response(result)


get_dataset_tagged_messages_view = GetDatasetTaggedMesssagesView.as_view()


class UnderstandingRemoveTaggedMessageView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = self.request.data

        tagged_message_id = self.request.query_params.get(
            "tagged_message_id", None
        ) or data.get("tagged_message_id", None)

        tagged_message = TaggedMessage.objects.filter(id=tagged_message_id).first()
        if not tagged_message:
            return Response(
                {"error": "Missing tagged message id or record not found"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        dataset_id = self.request.query_params.get("dataset_id", None) or data.get(
            "dataset_id", None
        )

        dataset = Dataset.objects.filter(id=dataset_id).first()
        if not dataset:
            return Response(
                {"error": "Missing dataset id or record not found"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        message = tagged_message.message
        tagged_message.delete()
        message.delete()

        return Response(
            "Tagged message was successfully remove", status=status.HTTP_201_CREATED
        )


understanding_remove_tagged_message_view = (
    UnderstandingRemoveTaggedMessageView.as_view()
)
