import { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import {
  Form,
  FormField,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
  FormDescription,
} from "@/components/ui/form";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import discussionChatbotImg from "@/assets/discussion-assistant.png";
import questionAnswerChatbotImg from "@/assets/question-answer-assistant.png";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/store";
import { setOnboardingData } from "@/slices/onboardingSlice";
import { chatbotTypes, ChatbotType } from "@/typings/chatbot_config";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";
import { useParams } from "react-router-dom";
import { Project } from "@/typings/project";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { Workspace } from "@/typings/workspace";

const formSchema = z.object({
  chatbotType: z.enum(
    [
      chatbotTypes.PROMPT_BASED,
      chatbotTypes.KNOWLEDGE_BASED,
      chatbotTypes.HYBRID,
      chatbotTypes.EVERYTHING,
      chatbotTypes.DISCUSSION,
      chatbotTypes.QUESTION_ANSWER,
      chatbotTypes.GUIDED_CONVERSATION,
    ],
    {
      required_error: "You need to select an assistant type.",
    }
  ),
});

const StepSetPrompt = forwardRef((_, ref) => {
  const dispatch: AppDispatch = useDispatch();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const { data: project = { workspace: "" } as Project } =
    useGetCurrentProjectDetailsQuery(projectId);
  const { data: workspace = {} as Workspace } = useGetWorkspaceQuery(
    project.workspace,
    {
      skip: !project.workspace,
    }
  );

  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData
  );

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      chatbotType:
        (onboardingData.stepSetPrompt?.chatbotType as ChatbotType) || "",
    },
  });

  const { handleSubmit, reset } = form;

  const [selectedType, setSelectedType] = useState(
    (onboardingData.stepSetPrompt?.chatbotType as ChatbotType) || ""
  );

  useEffect(() => {
    if (onboardingData) {
      const chatbotType =
        (onboardingData.stepSetPrompt?.chatbotType as ChatbotType) || "";
      setSelectedType(chatbotType);
      reset({ chatbotType });
    }
  }, [onboardingData, reset]);

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const { chatbotType } = values;

    await dispatch(
      setOnboardingData({
        ...onboardingData,
        stepSetPrompt: { chatbotType: chatbotType },
      })
    );

    return true;
  };

  useImperativeHandle(ref, () => ({
    submitForm: () => {
      return new Promise<boolean>((resolve) => {
        handleSubmit(async (values) => {
          const result = await onSubmit(values);
          resolve(result);
        })();
      });
    },
    updateOnboardingDataWithDraft: () => {
      const { chatbotType } = form.getValues();
      dispatch(
        setOnboardingData({
          ...onboardingData,
          stepSetPrompt: { chatbotType: chatbotType },
        })
      );
    },
  }));

  const handleTypeChange = (chatbotType: ChatbotType) => {
    form.setValue("chatbotType", chatbotType);
    setSelectedType(chatbotType);
  };

  return (
    <div className="flex gap-20 w-5/6 h-full">
      <div className="w-fit mx-auto flex flex-col pt-[5%]">
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
            <FormField
              control={form.control}
              name="chatbotType"
              render={({ field }) => (
                <FormItem className="flex flex-col gap-4">
                  <FormLabel className="flex flex-col gap-4 mb-7">
                    <p className="text-3xl">
                      What kind of assistant would you like to build?
                    </p>
                    <p className="text-lg w-4/5">
                      Choose your conversation mode. You can adjust this later
                      in the settings page.
                    </p>
                  </FormLabel>
                  <FormControl>
                    <RadioGroup
                      onValueChange={(value) =>
                        handleTypeChange(value as ChatbotType)
                      }
                      value={field.value}
                      className="flex flex-col space-y-5"
                    >
                      <FormItem className="flex flex-col items-left align-middle space-x-3 space-y-0 text-lg">
                        <div className="flex flex-row align-middle">
                          <FormControl className="mt-1.5 mx-2">
                            <RadioGroupItem value={chatbotTypes.DISCUSSION} />
                          </FormControl>
                          <FormLabel className="font-normal text-lg">
                            Discussion
                          </FormLabel>
                        </div>
                        <FormDescription>
                          A prompt-based assistant has flexible, multi-step
                          conversations with users that are shaped by the role
                          and task you define in a text prompt.
                        </FormDescription>
                      </FormItem>
                      <FormItem className="flex flex-col items-left space-x-3 space-y-0">
                        <div className="flex flex-row align-middle">
                          <FormControl className="mt-1.5 mx-2">
                            <RadioGroupItem
                              value={chatbotTypes.QUESTION_ANSWER}
                            />
                          </FormControl>
                          <FormLabel className="font-normal text-lg">
                            Question and answer
                          </FormLabel>
                        </div>
                        <FormDescription>
                          An assistant attempts to answer each user's question
                          separately using documents that you provide.
                        </FormDescription>
                      </FormItem>
                      {workspace?.feature_flags?.flows && (
                        <FormItem className="flex flex-col items-left space-x-3 space-y-0">
                          <div className="flex flex-row align-middle">
                            <FormControl className="mt-1.5 mx-2">
                              <RadioGroupItem
                                value={chatbotTypes.GUIDED_CONVERSATION}
                              />
                            </FormControl>
                            <FormLabel className="font-normal text-lg">
                              Guided conversations
                            </FormLabel>
                          </div>
                          <FormDescription>
                            Create multi-step branched conversations for
                            controlled and nuanced AI dialog.
                          </FormDescription>
                        </FormItem>
                      )}
                    </RadioGroup>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </form>
        </Form>
      </div>

      <div className="h-full">
        {selectedType === chatbotTypes.DISCUSSION && (
          <img
            src={discussionChatbotImg}
            alt="Discussion-based Assistant"
            className="w-[600px] mt-12"
          />
        )}
        {selectedType === chatbotTypes.QUESTION_ANSWER && (
          <img
            src={questionAnswerChatbotImg}
            alt="Question-and-Answer Assistant"
            className="w-[600px] mt-12"
          />
        )}
      </div>
    </div>
  );
});

export default StepSetPrompt;
