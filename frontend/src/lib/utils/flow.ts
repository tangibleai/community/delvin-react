export const nodeNameToTypeMapping = {
  PROMPT_NODE: "promptNode",
  CONDITION_BLOCK: "conditionBlock",
  SINGLE_CONDITION: "singleCondition",
  START_NODE: "startNode",
};
