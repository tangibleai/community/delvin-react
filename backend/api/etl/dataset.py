from api import models
from api.etl import utils


def process(df_or_records_list, *args, **kwargs):
    datasets = df_or_records_list
    if not isinstance(df_or_records_list, list):
        datasets = df_or_records_list.to_dict("records")
    required_fields = {"name", "description", "project__name", "project__owner__email"}
    utils.check_missing_fields(datasets, required_fields)

    datasets = resolve_dependent_records(datasets)
    check_duplicated_datasets(datasets)
    db_records = save_datasets_to_db(datasets)

    return db_records


def resolve_dependent_records(tag_groups):
    tag_groups = utils.resolve_user_for_each_record(
        tag_groups, user_email_field_name="project__owner__email"
    )
    tag_groups = utils.resolve_project_for_each_record(tag_groups)

    return tag_groups


def check_duplicated_datasets(datasets):
    for d in datasets:
        name = d["name"]
        project_rec = d["project_rec"]

        duplicated_tag_groups = models.Dataset.objects.filter(
            name=name, project=project_rec
        )
        if duplicated_tag_groups:
            project_name = d["project__name"]
            raise ValueError(
                f'Dataset with "{name}" name already exists for "{project_name}" project'
            )

    return []


def save_datasets_to_db(datasets):
    initialized_records = [
        models.Dataset(name=d["name"], project=d["project_rec"]) for d in datasets
    ]
    saved_records = models.Dataset.objects.bulk_create(initialized_records)

    return saved_records
