from rest_framework import serializers, generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import (
    Contact,
    Message,
    Project,
)


class ContactSerializer(serializers.ModelSerializer):
    project_id = serializers.UUIDField(required=True)

    class Meta:
        model = Contact
        fields = "__all__"


class ContactDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ["id", "origin", "inserted_at"]


class ContactListRequestSerializer(serializers.Serializer):
    project_id = serializers.UUIDField(required=False)
    origin = serializers.CharField(required=False)
    internal=serializers.BooleanField(required=False)


class ContactListAPIView(generics.ListAPIView):
    """Return contact list by project"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ContactSerializer

    def get_queryset(self):
        data = self.request.query_params

        serializer = ContactListRequestSerializer(data=data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        validated_data = serializer.validated_data

        project_id = validated_data.get("project_id")
        project = Project.objects.get(id=project_id)
        internal = validated_data.get("internal")

        search_params = {"project__id": project_id}
        origin = validated_data.get("origin")
        if origin:
            search_params["origin"] = origin

        queryset = Contact.objects.filter(**search_params)

        if not queryset:
            contact = Contact.objects.create(project=project, origin=origin, internal=internal)
            queryset = Contact.objects.filter(id=contact.id)

        return queryset


contact_list_view = ContactListAPIView.as_view()


class CreateContactSerializer(serializers.Serializer):
    project_id = serializers.UUIDField()
    origin = serializers.CharField()
    internal = serializers.BooleanField()


class CreateContactView(generics.CreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CreateContactSerializer

    def post(self, request, *args, **kwargs):
        data = request.data.copy()

        serializer = self.serializer_class(data=data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        validated_data = serializer.validated_data

        project_id = validated_data.get("project_id")
        origin = validated_data.get("origin")
        internal = validated_data.get("internal")

        project = Project.objects.get(id=project_id)

        contact = Contact.objects.create(
            project=project, origin=origin, internal=internal
        )

        return Response(ContactSerializer(contact).data, status=status.HTTP_201_CREATED)


create_contact_view = CreateContactView.as_view()
