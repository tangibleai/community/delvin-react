import logging
import os

from drf_spectacular.utils import OpenApiParameter, extend_schema, OpenApiTypes
from enum import Enum
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from pydantic import BaseModel
from rest_framework import serializers, generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import (
    Chatbot,
    Document,
    Project,
)
from api.services.object_storage_file_manager import (
    ObjectStorageDocumentManager,
)
from api.tasks import (
    update_document_metadata,
    extract_document_file_metadata,
    delete_document_file_and_metadata,
    extract_google_doc_metadata,
    update_google_doc_metadata,
)

logger = logging.getLogger(__name__)


class DocumentCategory(str, Enum):
    File = "file"
    Url = "url"
    Text = "text"
    Google = "google"


def get_google_credentials():
    """Get valid user credentials from storage.

    The file token.json stores the user's access and refresh tokens, and is
    created automatically when the authorization flow completes for the first
    time.

    Returns:
        Credentials, the obtained credential.

    * NOTE: This function is from the GoogleDocReader class from llama-index.
    Calling this early helps handle exceptions
    """
    SCOPES = ["https://www.googleapis.com/auth/documents.readonly"]

    creds = None
    if os.path.exists("token.json"):
        creds = Credentials.from_authorized_user_file("token.json", SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file("credentials.json", SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open("token.json", "w") as token:
            token.write(creds.to_json())

    return creds


def extract_google_doc_id(url):
    try:
        start_index = url.index("/d/") + 3
        end_index = url.index("/", start_index)
        file_id = url[start_index:end_index]
        return file_id
    except ValueError:
        return None


def update_old_document_instance_properties(project_id):
    """Fix a document instance by adding the doc_type property

    Applied before a retrieved document is returned to the frontend
    Ensures frontend displays the correct form

    NOTE: Temporary function for backwards compatibility
    """
    documents = Document.objects.filter(project__id=project_id)
    for doc in documents:
        if ".txt" in doc.title or ".doc" in doc.title or ".pdf" in doc.title:
            doc.properties = {"doc_type": "file"}
        else:
            doc.properties = {"doc_type": "text"}
        doc.save()
    return documents


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = "__all__"


class DocumentGetAPIView(generics.ListAPIView):
    """Returns a list of Documents uploaded to a project"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Document.objects.all()
    serializer_class = DocumentSerializer

    def get(self, request, *args, **kwargs):
        chatbot_id = request.GET.get("chatbot_id")
        # NOTE: Temporary function for backwards compatibility - updates database with missing key
        # update_old_document_instance_properties(chatbot_id)
        chatbot_documents = Document.objects.filter(chatbot__id=chatbot_id).values(
            "id", "title", "content", "status", "properties"
        )
        return Response(chatbot_documents, status=status.HTTP_200_OK)


document_get_api_view = DocumentGetAPIView.as_view()


class DocumentDeleteAPIView(generics.DestroyAPIView):
    """Deletes a Document entry and the associated metadata records in data_chatbot_knowledge table"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Document.objects.all()
    serializer_class = DocumentSerializer

    @extend_schema(
        tags=["api"],
        description="Document delete view",
        parameters=[
            OpenApiParameter(
                name="id",
                required=True,
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                description="Unique identifier of the document",
            ),
        ],
        responses={
            204: None,
        },
    )
    def delete(self, request, *args, **kwargs):
        id_ = self.kwargs.get("id")

        document = Document.objects.filter(id=id_).first()

        document_dict = {
            "id": document.pk,
            "title": document.title,
            "content": document.content,
            "status": document.status,
        }
        update_document_metadata.delay(
            document.project.id, document.chatbot.id, document_dict, "delete"
        )

        # TODO: Should add a file_type field to Document
        if (
            ".pdf" in document_dict["title"]
            or ".txt" in document_dict["title"]
            or ".docx" in document_dict["title"]
        ):
            object_storage_document_manager = ObjectStorageDocumentManager(
                document.project_id, document_dict
            )
            object_storage_document_manager.delete_object_from_object_storage()

        document.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


document_delete_api_view = DocumentDeleteAPIView.as_view()


class KnowledgeDocumentUploadView(generics.CreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Document.objects.all()
    serializer_class = DocumentSerializer

    def document_instance_to_dict(self, document):
        document_dict = {
            "id": document.pk,
            "title": document.title,
            "content": document.content,
            "status": document.status,
            "properties": document.properties,
        }
        return document_dict

    def process_form_content(self, document_data, project_id, chatbot_id):
        chatbot = Chatbot.objects.filter(id=chatbot_id).first()
        document = Document.objects.create(
            title=document_data.get("title"),
            content=document_data.get("content"),
            project=chatbot.project,
            chatbot=chatbot,
            properties={"doc_type": DocumentCategory.Text},
        )
        document_dict = self.document_instance_to_dict(document)

        task = update_document_metadata.delay(
            project_id, chatbot_id, document_dict, "create"
        )
        task_id = task.task_id
        document_dict = document_dict | {"celery_task_id": task_id}
        return document_dict

    def process_file(self, document_data, project_id, chatbot_id):
        data_dict = document_data.dict()
        document_file = data_dict.get("content")

        # TODO: Adjust to handle multiple document uploads (5 PDFs)
        # TODO: Adjust to handle multiple document uploads of different types (1 PDF, 1 DOC)
        chatbot = Chatbot.objects.filter(id=chatbot_id).first()
        document = Document.objects.create(
            title=document_file.name,
            content="",
            project=chatbot.project,
            chatbot=chatbot,
            properties={"doc_type": DocumentCategory.File},
        )

        document_dict = self.document_instance_to_dict(document)

        object_storage_document_manager = ObjectStorageDocumentManager(
            project_id, document_dict
        )
        file_name = object_storage_document_manager.upload_file_to_object_storage(
            document_file
        )

        task = extract_document_file_metadata.delay(
            project_id, chatbot_id, file_name, document_dict
        )

        task_id = task.task_id
        document_dict = document_dict | {"celery_task_id": task_id}

        return document_dict

    def process_google_doc(self, document_data, project_id, chatbot_id):
        google_doc_id = extract_google_doc_id(document_data["content"])
        chatbot = Chatbot.objects.filter(id=chatbot_id).first()

        # Try to get document info
        google_doc_title = ""
        try:
            credentials = get_google_credentials()
            docs_service = build("docs", "v1", credentials=credentials)
            google_doc = (
                docs_service.documents().get(documentId=google_doc_id).execute()
            )
            google_doc_title = google_doc.get("title")
        except Exception as e:
            logger.warning(
                f"process_google_doc: Failed to retrieve Google Doc information: {e}"
            )
            return {"error": "Please use a valid URL for a public Google Doc."}

        document = Document.objects.create(
            title=google_doc_title,
            content=document_data["content"],
            project=chatbot.project,
            chatbot=chatbot,
            properties={"doc_type": DocumentCategory.Google},
        )

        document_dict = self.document_instance_to_dict(document)

        task = extract_google_doc_metadata.delay(
            project_id, chatbot_id, document_dict, google_doc_id
        )

        task_id = task.task_id
        document_dict = document_dict | {"celery_task_id": task_id}

        return document_dict

    def post(self, request, *args, **kwargs):
        try:
            document_data = request.data.copy()
            document_type = document_data.get("document_type", "")

            project_id = document_data["project"]
            chatbot_id = document_data["chatbot"]
        except Exception as e:
            logger.error(f"Error with payload for uploading a knowledge document: {e}")
            return Response(document_dict, status=status.HTTP_400_BAD_REQUEST)

        try:
            if document_type == "text":
                document_dict = self.process_form_content(
                    document_data, project_id, chatbot_id
                )
            elif document_type == "file":
                document_dict = self.process_file(document_data, project_id, chatbot_id)
            elif document_type == "google":
                document_dict = self.process_google_doc(
                    document_data, project_id, chatbot_id
                )
            else:
                return Response(
                    {"error": "Invalid document type"},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY,
                )
        except Exception as e:
            logger.error(f"Error uploading a new knowledge document: {e}")
            return Response(
                {"error": "Error processing the document"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        if document_dict.get("error"):
            return Response(document_dict, status=status.HTTP_400_BAD_REQUEST)

        return Response(document_dict, status=status.HTTP_201_CREATED)


create_knowledge_document_api_view = KnowledgeDocumentUploadView.as_view()


class KnowledgeDocumentUpdateView(generics.UpdateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Document.objects.all()
    serializer_class = DocumentSerializer

    def document_instance_to_dict(self, document):
        document_dict = {
            "id": document.pk,
            "title": document.title,
            "content": document.content,
            "status": document.status,
            "properties": document.properties,
        }
        return document_dict

    def update_text_document(self, document_data, project_id, chatbot_id):
        document = Document.objects.filter(id=document_data["document"]).first()

        document.title = document_data["title"]
        document.content = document_data["content"]
        document.status = document.DocumentStatus.UPDATED
        document.save()

        document_dict = self.document_instance_to_dict(document)

        task = update_document_metadata.delay(
            project_id, chatbot_id, document_dict, "update"
        )

        task_id = task.task_id
        document_dict = document_dict | {"celery_task_id": task_id}
        return document_dict

    def update_file_document(self, document_data, project_id, chatbot_id):
        document_data = document_data.dict()
        document_id = document_data["document"]
        document_file = document_data.get("content")

        # TODO: Adjust to handle multiple document uploads (5 PDFs)
        # TODO: Adjust to handle multiple document uploads of different types (1 PDF, 1 DOC)
        chatbot = Chatbot.objects.filter(id=chatbot_id).first()

        # Create a new version
        document = Document.objects.create(
            title=document_file.name,
            content="",
            project=chatbot.project,
            chatbot=chatbot,
            properties={"doc_type": DocumentCategory.File},
        )
        document_dict = self.document_instance_to_dict(document)

        # Save file to object storage
        # NOTE: This needs to happen before metadata extraction can happen
        object_storage_document_manager = ObjectStorageDocumentManager(
            project_id, document_dict
        )
        file_name = object_storage_document_manager.upload_file_to_object_storage(
            document_file
        )

        # Call metadata extraction
        extract_document_file_metadata.delay(
            project_id, chatbot_id, file_name, document_dict
        )

        # Delete references to old version
        document_to_delete = Document.objects.filter(id=document_id).first()
        delete_document_dict = self.document_instance_to_dict(document_to_delete)
        task = delete_document_file_and_metadata.delay(
            project_id, chatbot_id, delete_document_dict
        )
        document_to_delete.delete()

        task_id = task.task_id
        document_dict = document_dict | {"celery_task_id": task_id}

        return document_dict

    def update_google_doc_document(self, document_data, project_id, chatbot_id):
        google_doc_id = extract_google_doc_id(document_data["content"])

        # Try to get document info
        google_doc_title = ""
        try:
            credentials = get_google_credentials()
            docs_service = build("docs", "v1", credentials=credentials)
            google_doc = (
                docs_service.documents().get(documentId=google_doc_id).execute()
            )
            google_doc_title = google_doc.get("title")
        except Exception as e:
            logger.warning(
                f"update_google_doc_document: Failed to retrieve Google Doc information: {e}"
            )
            return {"error": "Please use a valid URL for a public Google Doc."}
        document = Document.objects.filter(id=document_data["document"]).first()
        document.status = document.DocumentStatus.UPDATED
        document.save()
        document_dict = self.document_instance_to_dict(document)

        task = update_google_doc_metadata.delay(
            project_id, chatbot_id, document_dict, google_doc_id
        )

        task_id = task.task_id
        document_dict = document_dict | {"celery_task_id": task_id}

        return document_dict

    def put(self, request, *args, **kwargs):
        try:
            document_data = request.data.copy()
            document_type = document_data["document_type"]

            document = Document.objects.filter(id=document_data["document"]).first()
            if not document:
                logger.error(f"Error requested document to update does not exist: {e}")
                return Response({}, status=status.HTTP_404_NOT_FOUND)

            project_id = document_data["project"]
            chatbot_id = document.chatbot.id
        except Exception as e:
            logger.error(f"Error with payload for updating a knowledge document: {e}")
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        try:
            if document_type == "text":
                document_dict = self.update_text_document(
                    document_data, project_id, chatbot_id
                )
            elif document_type == "file":
                document_dict = self.update_file_document(
                    document_data, project_id, chatbot_id
                )
            elif document_type == "google":
                document_dict = self.update_google_doc_document(
                    document_data, project_id, chatbot_id
                )
            else:
                return Response(
                    {"error": "Invalid document type"},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY,
                )
        except Exception as e:
            logger.error(f"Error updating a knowledge document: {e}")
            return Response(
                {"error": "Error processing the document"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        if document_dict.get("error"):
            return Response(document_dict, status=status.HTTP_400_BAD_REQUEST)

        return Response(document_dict, status=status.HTTP_200_OK)


update_knowledge_document_api_view = KnowledgeDocumentUpdateView.as_view()
