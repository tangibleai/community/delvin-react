import apiIntegration from "@/assets/project_home/api-integration.png";
import platforms from "@/assets/project_home/platforms.png";
import turnIO from "@/assets/project_home/turn-io.png";
import { Callout } from "@/components/ui/callout";
import { PROJECT_PAGE_URLS } from "@/routes/pageUrls";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";
import { resetOnboarding } from "@/slices/onboardingSlice";
import { setWelcomeMessageShown } from "@/slices/projectSlice";
import { AppDispatch, RootState } from "@/store";
import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

const NewProjectPage = () => {
  const dispatch: AppDispatch = useDispatch();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;
  const { data: project } = useGetCurrentProjectDetailsQuery(projectId);

  const welcomeMessageShown = useSelector(
    (state: RootState) => state.project.welcomeMessageShown
  );
  const onboardingData = useSelector(
    (state: RootState) => state.onboarding.onboardingData
  );
  // used to prevent useEffect execute twice in debug mode
  const initialLoadDone = useRef(false);

  useEffect(() => {
    if (initialLoadDone.current) return;
    initialLoadDone.current = true;

    if (onboardingData.stepAssistantName.name !== "") {
      dispatch(resetOnboarding());
      return;
    }

    if (onboardingData.stepAssistantName.name === "" && !welcomeMessageShown) {
      dispatch(setWelcomeMessageShown(true));
    }
  }, [dispatch]);

  return (
    <div className="flex flex-col w-full items-center justify-center p-4 mt-5 gap-9">
      {!welcomeMessageShown && (
        <Callout
          variant="default"
          title="Your chatbot has been created"
          className="w-full"
          buttonText={`Chat with ${project?.name}`}
          buttonLink={`/project/${projectId}/${PROJECT_PAGE_URLS.project.children.chat_with_assistant}`}
        >
          Happy chatting!
        </Callout>
      )}

      <div className="flex flex-col w-full gap-10 rounded-xl border-dashed border-2 border-gray-300 p-10">
        <div className="flex flex-col gap-4 text-lg items-start">
          <p className="text-2xl font-bold">Waiting for your first user!</p>
          <p>
            When we receive your first request, we'll show some analytics here.
          </p>
          <p>Learn how to integrate with your favorite platforms:</p>
        </div>

        <div className="grid gap-4 md:grid-cols-2 md:gap-8 lg:grid-cols-3">
          <a
            className="w-full h-full flex flex-col justify-center items-center gap-6 borger-primary border-2 border-tremor-brand rounded-xl text-center p-6 hover:bg-muted"
            href="https://docs.delvin.to/docs/delvin-turnio"
            target="_blank"
          >
            <img src={turnIO} alt="Turn.io" className="w-[70%] rounded-xl" />
            <span className="font-semibold text-gray-500 text-xl w-[70%]">
              Turn.io
            </span>
          </a>

          <a
            className="w-full h-full flex flex-col justify-center items-center gap-6 borger-primary border-2 border-tremor-brand rounded-xl text-center p-6 hover:bg-muted"
            href="https://docs.delvin.to/docs/delvin-textit"
            target="_blank"
          >
            <img src={platforms} alt="TextIt" className="w-[80%] rounded-xl" />
            <span className="font-semibold text-gray-500 text-xl w-[80%] text-center">
              TextIt, Glific, Weni, Rapidpro
            </span>
          </a>

          <a
            className="w-full h-full flex flex-col justify-center items-center gap-6 borger-primary border-2 border-tremor-brand rounded-xl text-center p-6 hover:bg-muted"
            href="#"
            target="_blank"
          >
            <img
              src={apiIntegration}
              alt="Custom API integration"
              className=" h-[60%] rounded-xl"
            />
            <span className="font-semibold text-gray-500 text-xl w-[80%] text-center">
              Custom API integration
            </span>
          </a>
        </div>
      </div>
    </div>
  );
};

export default NewProjectPage;
