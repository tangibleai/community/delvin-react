SAMPLE_UI_NODES = [
  {
    "id": "0",
    "type": "startNode",
    "data": {
      "title": 1
    },
    "style": {
      "width": 100,
      "height": 100
    },
    "position": {
      "x": -256.74453470555085,
      "y": 23.219789960450328
    },
    "width": 100,
    "height": 100,
    "selected": False,
    "positionAbsolute": {
      "x": -256.74453470555085,
      "y": 23.219789960450328
    },
    "dragging": False
  },
  {
    "id": "1",
    "type": "customNode",
    "data": {
      "nodeId": "1",
      "nodeName": "message-1",
      "nodePrompt": "Greet the user"
    },
    "style": {
      "width": 120.06,
      "height": 160.07999999999998,
      "borderStyle": "solid",
      "borderWidth": "0.5vh",
      "borderRadius": "1rem",
      "borderColor": "hsl(240 92% 61%)"
    },
    "position": {
      "x": -43.606655746882154,
      "y": -47.10300249119925
    },
    "width": 120,
    "height": 160,
    "selected": True,
    "positionAbsolute": {
      "x": -43.606655746882154,
      "y": -47.10300249119925
    },
    "dragging": False
  },
  {
    "id": "2",
    "type": "customNode",
    "data": {
      "nodeId": "2",
      "nodeName": "message-2",
      "nodePrompt": "Ask the user if they ever experienced impostor syndrome"
    },
    "style": {
      "width": 132.12,
      "height": 176.16
    },
    "position": {
      "x": 185.35128777064574,
      "y": -50.939392323588635
    },
    "width": 132,
    "height": 176,
    "selected": False,
    "positionAbsolute": {
      "x": 185.35128777064574,
      "y": -50.939392323588635
    },
    "dragging": False
  }
]

SAMPLE_UI_EDGES = [
  {
    "source": "1",
    "sourceHandle": None,
    "target": "2",
    "targetHandle": None,
    "id": "reactflow__edge-1-2"
  },
  {
    "source": "0",
    "sourceHandle": None,
    "target": "1",
    "targetHandle": None,
    "id": "reactflow__edge-0-1"
  }
]