#### Tutorials

[Youtube-Ingress-Nginx](https://www.youtube.com/watch?v=MpovOI5eK58)
[GitHub-Repo](https://github.com/digitalocean/Kubernetes-Starter-Kit-Developers/blob/main/03-setup-ingress-controller/nginx.md)
[GitHub-Repo-Wildcard-Certificates](https://github.com/digitalocean/Kubernetes-Starter-Kit-Developers/blob/main/03-setup-ingress-controller/guides/wildcard_certificates.md)

[Digitalocean-Documentation-Ingress-Nginx](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-on-digitalocean-kubernetes-using-helm)


#### Helm Charts
[Ingress](https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx/4.8.3)
[Cert-manager](https://artifacthub.io/packages/helm/cert-manager/cert-manager)

#### Install and Deploy Ingress
```commandline
chmod g-r .cube/kubeconfig.yaml 
chmod o-r .cube/kubeconfig.yaml 
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.publishService.enabled=true
# Watch services
kubectl --namespace default get services -o wide -w nginx-ingress-ingress-nginx-controller
# Create the ingress deployment to split traffic
kubectl apply -f k8s/ingress/ingress-deployment.yaml
```

#### Apply TLS
```commandline
kubectl create namespace cert-manager
# Add the Jetstack Helm repository
helm repo add jetstack https://charts.jetstack.io
helm repo update
## Install the cert-manager helm chart
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.13.3 --values k8s/ingress-https-main/cert-manager-values.yaml
```

- Create the issuer `production_issuer.yaml`
- Apply the issuer `kubectl apply -f production_issuer.yaml`
- Update the ingress to reflect changes related to the TLS 
- Apply ingress deployment `kubectl apply -f k8s/ingress-deployment.yaml`


#### Debugging
[Solving problems](https://stackoverflow.com/questions/63346728/issuing-certificate-as-secret-does-not-exist)
```commandline
kubectl get cert
kubectl describe service cert-manager-webhook -n cert-manager
kubectl describe cert delvin-kubernetes-tls
```
