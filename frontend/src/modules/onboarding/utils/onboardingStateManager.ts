import { chatbotTypes } from "@/typings/chatbot_config";
import { StateName } from "../types/state_names";

export type OnboardingState = {
  name: StateName;
  nextStateName: StateName | null;
  previousStateName: StateName | null;
  progress: number;
};

export const ONBOARDING_STATES: { [name: string]: OnboardingState } = {
  [StateName.StepAssistantName]: {
    name: StateName.StepAssistantName,
    nextStateName: StateName.StepSetPrompt,
    previousStateName: null,
    progress: 100 / 3,
  },
  [StateName.StepSetPrompt]: {
    name: StateName.StepSetPrompt,
    nextStateName: null,
    previousStateName: StateName.StepAssistantName,
    progress: (100 / 3) * 2,
  },
  [StateName.SubstepDiscussionBotSettings]: {
    name: StateName.SubstepDiscussionBotSettings,
    nextStateName: StateName.StepProjectCreation,
    previousStateName: StateName.StepSetPrompt,
    progress: 100,
  },
  [StateName.SubstepQuestionAnswerBotSettings]: {
    name: StateName.SubstepQuestionAnswerBotSettings,
    nextStateName: StateName.StepDocumentAdding,
    previousStateName: StateName.StepSetPrompt,
    progress: 75,
  },
  [StateName.SubstepGuidedConversationsBotSettings]: {
    name: StateName.SubstepGuidedConversationsBotSettings,
    nextStateName: StateName.StepProjectCreation,
    previousStateName: StateName.StepSetPrompt,
    progress: 75,
  },
  [StateName.StepDocumentAdding]: {
    name: StateName.StepDocumentAdding,
    nextStateName: StateName.StepProjectCreation,
    previousStateName: StateName.SubstepQuestionAnswerBotSettings,
    progress: 100,
  },
  [StateName.StepProjectCreation]: {
    name: StateName.StepProjectCreation,
    nextStateName: null,
    previousStateName: null,
    progress: 100, // doesn't matter in here
  },
};

export const getNextOnboardingState = (
  currentOnboardingState: OnboardingState,
  chatbotType: string
) => {
  if (currentOnboardingState.name == StateName.StepSetPrompt) {
    if (chatbotType === chatbotTypes.DISCUSSION) {
      return ONBOARDING_STATES[StateName.SubstepDiscussionBotSettings];
    } else if (chatbotType === chatbotTypes.QUESTION_ANSWER) {
      return ONBOARDING_STATES[StateName.SubstepQuestionAnswerBotSettings];
    } else {
      return ONBOARDING_STATES[StateName.SubstepGuidedConversationsBotSettings];
    }
  } else {
    return ONBOARDING_STATES[currentOnboardingState.nextStateName!];
  }
};

export const getPreviousOnboardingState = (
  currentOnboardingState: OnboardingState
) => {
  return ONBOARDING_STATES[currentOnboardingState.previousStateName!];
};
