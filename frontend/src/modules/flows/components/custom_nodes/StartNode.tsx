import { RootState } from "@/store";
import { useSelector } from "react-redux";
import { Handle, NodeProps, Position } from "reactflow";

type NodeData = {
  nodeId: string;
  nodePrompt: string;
  title: number;
};

type Props = NodeProps<NodeData>;

const StartNode = ({ data, isConnectable }: Props) => {
  const erroneousNodeIds: string[] = useSelector(
    (state: RootState) => state.flow.erroneousNodes
  ).map((node) => node.id);

  const connectionInProgress = useSelector(
    (state: RootState) => state.flow.connectionInProgress
  );
  const connectionSourceNodeId = useSelector(
    (state: RootState) => state.flow.connectionSourceNodeId
  );

  return (
    <>
      <Handle
        type="source"
        position={Position.Right}
        style={{
          background: "black",
          right: `${
            connectionInProgress && connectionSourceNodeId === data.nodeId
              ? "-7%"
              : ""
          }`,
        }}
        isConnectable={isConnectable}
        className={`transition-all duration-500 ease-in-out ${
          connectionInProgress && connectionSourceNodeId === data.nodeId
            ? "w-4 h-4"
            : ""
        }`}
      />
      <div
        className={`flex items-center justify-center text-center bg-primary h-full w-full rounded-full border ${
          erroneousNodeIds.includes(data.nodeId)
            ? "border-red-500 border-[0.5vh]"
            : "border-slate-400"
        }`}
      >
        <span className="text-center text-white">Start</span>
      </div>
    </>
  );
};

export { StartNode };
