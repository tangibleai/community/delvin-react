import logging
import os

from rest_framework import generics, serializers, status
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from accounts import models as accounts_models
from api import models as api_models
from api.internal.plans import PlanSerializer

logger = logging.getLogger(__name__)


class WorkspaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = api_models.Workspace
        fields = "__all__"


class WorkspacesListRequestSerializer(serializers.Serializer):
    user_id = serializers.UUIDField(required=True)


class WorkspacesListView(generics.ListAPIView):
    serializer_class = WorkspaceSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        serializer = WorkspacesListRequestSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)  # raise exception if invalid
        validated_data = serializer.validated_data

        user_id = validated_data.get("user_id")
        user = accounts_models.UserAccount.objects.get(id=user_id)
        active_workspace = user.current_workspace

        team_member = api_models.TeamMember.objects.filter(
            delvin_user__id=user_id
        ).first()

        remaining_workspaces = api_models.Workspace.objects.filter(
            owner=user,
        ).exclude(id=None if active_workspace is None else active_workspace.id)
        if team_member:
            remaining_workspaces = remaining_workspaces.union(
                api_models.Workspace.objects.filter(
                    teammember__delvin_user__id=user_id
                ).exclude(id=None if active_workspace is None else active_workspace.id)
            )

        workspaces = list(remaining_workspaces)
        if active_workspace:
            workspaces.insert(0, active_workspace)

        return workspaces

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


workspaces_list_view = WorkspacesListView.as_view()


class WorkspaceDetailsSerializer(serializers.ModelSerializer):
    owner = serializers.CharField(source="owner.email")
    plan = PlanSerializer(read_only=True)

    class Meta:
        model = api_models.Workspace
        fields = "__all__"


class WorkspaceDetailsView(generics.RetrieveAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = WorkspaceDetailsSerializer
    queryset = api_models.Workspace.objects.all()
    lookup_field = "id"


workspace_details_view = WorkspaceDetailsView.as_view()


class WorkspaceDeleteView(generics.DestroyAPIView):
    """Deletes the Workspace associated with the PK passed via the URL"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = api_models.Workspace.objects.all()
    serializer_class = WorkspaceDetailsSerializer

    def delete(self, request, *args, **kwargs):
        workspace_id = self.kwargs.get("pk")
        workspace = api_models.Workspace.objects.filter(id=workspace_id).first()
        workspace.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


workspace_delete_view = WorkspaceDeleteView.as_view()


class WorkspaceCreateView(generics.CreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = WorkspaceSerializer
    queryset = api_models.Workspace.objects.all()

    def post(self, request, *args, **kwargs):
        data = request.data.copy()

        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)

        validated_data = serializer.validated_data

        validated_data["feature_flags"] = {
            "flows": os.environ.get("FEATURE_FLAG_FLOWS", False)
            in (True, "True", "true")
        }

        workspace = api_models.Workspace.objects.create(**validated_data)

        serializer = self.serializer_class(workspace)

        return Response(serializer.data, status=status.HTTP_200_OK)


workspace_create_view = WorkspaceCreateView.as_view()


class WorkspaceUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = api_models.Workspace
        fields = ("name",)


class WorkspaceUpdateView(generics.UpdateAPIView):
    """
    PATCH method is forbidden.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = WorkspaceSerializer

    def get_queryset(self):
        queryset = api_models.Workspace.objects.filter(id=self.kwargs.get("pk"))
        return queryset

    def put(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = WorkspaceUpdateSerializer(data=data)
        serializer.is_valid(raise_exception=False)

        validated_data = serializer.validated_data
        instance = self.get_object()

        name = validated_data.get("name") or instance.name

        api_models.Workspace.objects.filter(id=self.get_object().id).update(name=name)

        instance = self.get_object()

        serializer = self.serializer_class(instance)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        message = _("PATCH")
        raise MethodNotAllowed(message)


workspace_update_view = WorkspaceUpdateView.as_view()
