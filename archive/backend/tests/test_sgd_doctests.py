from scripts.ml import sgd
from django.test import TestCase
import doctest


class TestSGDDoctests(TestCase):
    """Test sgd.fit() doctests for LogisticRegression"""

    def test_sgd_doctests(self):
        test_results = doctest.testmod(sgd)
        self.assertGreater(test_results.attempted, 10)
        self.assertEqual(test_results.failed, 0)
