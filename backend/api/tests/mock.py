import datetime

mock = {
    "uuid": "d3f6e231-4f0c-4e77-995e-d82c897347ae",
    "origin": "f0b619c4-467f-47fd-9f8e-9baa429e7e27",
    "chat": "04603472-28e8-45b7-b29d-7e62190635fb",
    "user1": {
        "email": "user1@example.com",
        "name": "user1",
        "password": "Pwd5544**",
        "project_name": "Project1",
        "message_tag_group_name": "MessageTagGroup1",
        "message_tag_definition_name": "MessageTagDefinition1",
        "guardrail_action": {"action_type": "scripted", "content": "__start__"},
        "guardrail_action2": {"action_type": "generative", "content": "__stop__"},
        "chatbot": {"model": "openai/gpt-3.5-turbo", "description": "test chatbot"},
    },
    "user2": {
        "email": "user2@example.com",
        "name": "user2",
        "password": "Pwd6655**",
        "project_name": "Project2",
        "role": "Editor",
        "properties": {},
        "chatbot": {
            "model": "mistralai/mistral-7b-instruct",
            "description": "test chatbot for second user",
        },
    },
    "user3": {
        "email": "user3@example.com",
        "name": "user3",
        "password": "Pwd6655**",
        "project_name": "Project3",
        "role": "Viewer",
        "properties": {},
    },
    "document1": {
        "title": "My First Document",
        "content": "This is my first document.",
        "document_type": "text",
    },
    "contact1": {"origin": "test@testuser.com", "internal": True},
    "chat1": {"origin_id": "1", "title": "default", "is_default": True},
    "chat2": {"origin_id": "1", "title": "Chat2", "is_default": False},
    "message1": {
        "text": "Hello World1",
        "direction": "inbound",
        "sender_type": "user",
        "channel_type": "web",
        "timestamp": datetime.datetime(2024, 1, 1),
    },
    "message2": {
        "text": "Hello World2",
        "direction": "outbound",
        "sender_type": "bot",
        "channel_type": "web",
        "timestamp": datetime.datetime(2024, 1, 2),
    },
    "message3": {
        "text": "Hello World3",
        "direction": "outbound",
        "sender_type": "bot",
        "channel_type": "web",
        "timestamp": datetime.datetime(2024, 1, 3),
    },
    "workspace1": {"name": "First workspace", "properties": {}},
    "workspace2": {"name": "Second workspace", "properties": {}},
}
