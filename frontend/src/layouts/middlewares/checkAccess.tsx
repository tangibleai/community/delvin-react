import { RootState } from "@/store";
import { ComponentType, ReactElement } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Loading from "@/components/Loading.tsx";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { useGetTeamMembersQuery } from "@/slices/api/teamMemberApiSlice";
import { useGetCurrentProjectDetailsQuery } from "@/slices/api/projectsApiSlice";
import { Project } from "@/typings/project";
import { Workspace } from "@/typings/workspace";
import UserHasNoAccessToProject from "../UserHasNoAccessToProject";

function proceedToProjectOrShowErrorPage<P extends object>(
  Component: ComponentType<P>
): (props: P) => ReactElement {
  return function CheckAccess(props: P): ReactElement {
    const user = useSelector((state: RootState) => state.auth.user);

    const routeParams = useParams();
    const projectId = routeParams.projectId!;

    const { data: project = {} as Project, isLoading: initialProjectFetching } =
      useGetCurrentProjectDetailsQuery(projectId);

    const { data: workspace = {} as Workspace, isFetching: fetchingWorkspace } =
      useGetWorkspaceQuery(project?.workspace, { skip: !project.workspace });
    const { data: teamMembers = [], isFetching: fetchingTeamMembers } =
      useGetTeamMembersQuery(
        {
          workspace_id: project!.workspace,
        },
        { skip: !project.workspace }
      );
    const userHasAccessToProject =
      workspace?.owner == user?.email ||
      teamMembers.some((member) => member.delvin_user == user?.email);

    if (initialProjectFetching || !project.workspace) return <Loading />;

    if (fetchingWorkspace || fetchingTeamMembers) {
      return <Loading />;
    }

    if (user && userHasAccessToProject) {
      return <Component {...(props as P)} />;
    }

    return <UserHasNoAccessToProject />;
  };
}

export default proceedToProjectOrShowErrorPage;
