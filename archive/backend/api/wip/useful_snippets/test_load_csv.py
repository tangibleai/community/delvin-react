from pathlib import Path
import tempfile

from django.test import TestCase
import pandas as pd

from accounts.models import UserAccount
from api.models import Contact, Project
from api.spreadsheet_to_db_records.user_creator import (
    load_users_csv,
)
from django.conf import settings


class TestLoadUsersCsv(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_data = [
            {
                "email": "user1@example.com",
                "name": "User One",
                "is_active": True,
                "is_admin": False,
                "project_name": "Project 1",
                "contact_origin_name": "origin1",
            },
            {
                "email": "user2@example.com",
                "name": "User Two",
                "is_active": True,
                "is_admin": True,
                "project_name": "Project 2",
                "contact_origin_name": "origin2",
            },
        ]

    def test_load_users_from_dataframe(self):
        df = pd.DataFrame(self.user_data)
        load_users_csv(df)
        self.verify_records_update_reflected_in_db()

    def test_load_users_from_csv_file(self):
        with tempfile.NamedTemporaryFile(mode="w", suffix=".csv") as tmp:
            pd.DataFrame(self.user_data).to_csv(tmp.name, index=False)
            load_users_csv(Path(tmp.name))
            self.verify_records_update_reflected_in_db()

    def test_load_users_from_csv_string(self):
        csv_string = pd.DataFrame(self.user_data).to_csv(index=False)
        with tempfile.NamedTemporaryFile(mode="w", suffix=".csv") as tmp:
            tmp.write(csv_string)
            tmp.seek(0)
            load_users_csv(tmp.name)
            self.verify_records_update_reflected_in_db()

    def verify_records_update_reflected_in_db(self):
        for user in self.user_data:
            # Verify UserAccount
            self.assertTrue(UserAccount.objects.filter(email=user["email"]).exists())
            user_account = UserAccount.objects.get(email=user["email"])
            self.assertEqual(user_account.name, user["name"])
            self.assertTrue(
                user_account.check_password(settings.ENV["TEST_USER_PASSWORD"])
            )
            self.assertEqual(user_account.is_active, user["is_active"])
            self.assertEqual(user_account.is_admin, user["is_admin"])

            # Verify associated Project
            self.assertTrue(
                Project.objects.filter(
                    name=user["project_name"], owner=user_account
                ).exists()
            )
            project = Project.objects.get(name=user["project_name"], owner=user_account)

            # Verify associated Contact
            self.assertTrue(
                Contact.objects.filter(
                    origin=user["contact_origin_name"], project=project
                ).exists()
            )

    def tearDown(self):
        Contact.objects.all().delete()
        Project.objects.all().delete()
        UserAccount.objects.all().delete()
