import { useCallback, useLayoutEffect, useRef, useState } from "react";
import { Handle, NodeProps, Position, useReactFlow } from "reactflow";
import { BsFillTrash3Fill } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import {
  setIsDeleteNodeDialogOpened,
  setActiveNode,
  setNodeSidebar,
} from "@/slices/flowSlice";
import { RootState } from "@/store";
import { addSingleCondition } from "../../utils/add_single_condition";
import { ConditionTypes } from "../../typing/condition";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";

type NodeData = {
  nodeId: string;
  nodeName: string;
  nodePrompt: string;
};

type Props = NodeProps<NodeData>;

const ConditionBlock = ({ data, isConnectable }: Props) => {
  const dispatch = useDispatch();
  const { getNodes, setNodes } = useReactFlow();
  const [nodeDeleted, setNodeDeleted] = useState(false);

  const erroneousNodeIds: string[] = useSelector(
    (state: RootState) => state.flow.erroneousNodes
  ).map((node) => node.id);

  const activeNode = useSelector((state: RootState) => state.flow.activeNode);

  const parentRef = useRef<HTMLDivElement>(null);
  const headerRef = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    if (headerRef.current) {
      const headerElement = headerRef.current;

      const headerHeight = parseInt(
        window.getComputedStyle(headerElement).height
      );

      const componentElement = parentRef.current!;

      const borderWidth = parseInt(
        window.getComputedStyle(componentElement).borderBlockWidth
      );

      let currentNode = getNodes().find((node) => node.id === data.nodeId)!;

      currentNode = {
        ...currentNode,
        data: {
          ...currentNode.data,
          headerHeight: headerHeight,
          borderWidth: borderWidth,
        },
      };

      const prefetchedNodes = getNodes();

      let nodes = prefetchedNodes.map((node) =>
        node.id === currentNode!.id ? currentNode : node
      );

      setNodes(nodes);

      dispatch(setActiveNode(currentNode!));

      const elseConditionAlreadyExists = prefetchedNodes.find(
        (node) =>
          node?.parentId === currentNode.id &&
          node.data.condition.type === ConditionTypes.ELSE
      );

      if (elseConditionAlreadyExists) return;

      const result = addSingleCondition(
        nodes,
        [],
        currentNode,
        ConditionTypes.ELSE
      );
      nodes = result.nodes;

      setNodes(nodes);
    }
  }, [parentRef, headerRef]);

  const onTrashIconClick = useCallback(() => {
    setNodeDeleted(true);

    dispatch(setIsDeleteNodeDialogOpened(true));

    const nodesAfterExcludingCurrentAndChildren = getNodes().filter(
      (node) => node.id !== data.nodeId && node.parentId !== data.nodeId
    );

    setNodes(nodesAfterExcludingCurrentAndChildren);
  }, [data.nodeId]);

  const onNodeClicked = () => {
    if (nodeDeleted) return;

    const currentNode = getNodes().find((node) => node.id === data.nodeId);

    dispatch(setActiveNode(currentNode!));
    dispatch(
      setNodeSidebar({
        opened: true,
        nodeType: nodeNameToTypeMapping.CONDITION_BLOCK,
      })
    );
  };

  return (
    <>
      <Handle
        type="target"
        position={Position.Left}
        style={{
          background: "black",
          color: "black",
          backgroundColor: "primary",
        }}
        onConnect={(params) => console.log("handle onConnect", params)}
        isConnectable={isConnectable}
      />
      <div
        ref={parentRef}
        className={`h-full border rounded-xl ${
          erroneousNodeIds.includes(data.nodeId)
            ? "border-red-600 border-2"
            : activeNode.id === data.nodeId
            ? "border-primary border-2"
            : "border-slate-400"
        } bg-white text-xs`}
        onClick={onNodeClicked}
      >
        <div
          ref={headerRef}
          className="flex items-center justify-between w-full bg-muted px-2 rounded-t-xl"
        >
          <span className="node-title">{data.nodeName}</span>
          <BsFillTrash3Fill
            className="text-xs text-destructive"
            onClick={onTrashIconClick}
          />
        </div>
      </div>
    </>
  );
};

export { ConditionBlock };
