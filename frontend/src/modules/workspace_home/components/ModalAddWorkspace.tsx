import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { Form, FormControl, FormField, FormItem } from "@/components/ui/form";
import { useToast } from "@/components/ui/use-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { z } from "zod";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { RootState } from "@/store";
import { FaPlusCircle } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import Loading from "@/components/Loading";
import { useCreateWorkspaceMutation } from "@/slices/api/workspacesApiSlice";
import { Workspace } from "@/typings/workspace";

interface Props {
  setDropdownOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

export default function ModalAddWorkspace({ setDropdownOpen }: Props) {
  const navigate = useNavigate();
  const { toast } = useToast();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [creatingWorkspace, setCreatingWorkspace] = useState(false);

  const [createWorkspace] = useCreateWorkspaceMutation();

  const formSchema = z.object({
    name: z.string().min(2, "Please enter a workspace name"),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
  });

  const userId = useSelector((state: RootState) => state.auth.user?.id) ?? "";

  const onSubmit = async (data: z.infer<typeof formSchema>) => {
    setCreatingWorkspace(true);
    setDropdownOpen(false);
    const { name } = data;

    let newWorkspace = { id: "1" } as Workspace;

    try {
      newWorkspace = await createWorkspace({
        owner: userId,
        name,
      }).unwrap();
    } catch (error: any) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Couldn't create new workspace.",
      });
      setIsModalOpen(false);
      setCreatingWorkspace(false);
      return;
    }
    setIsModalOpen(false);

    toast({
      title: "Success!",
      description: "New workspace was successfully created.",
    });
    navigate(`/workspace/${newWorkspace.id}`);
  };

  if (creatingWorkspace) {
    return <Loading />;
  }

  return (
    <Dialog open={isModalOpen} onOpenChange={setIsModalOpen}>
      <DialogTrigger>
        <div className="flex flex-row items-center h-10 px-4 py-2 justify-center gap-2 bg-primary text-primary-foreground whitespace-nowrap rounded-md text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50">
          <FaPlusCircle size={"1rem"} />
          <span>New Workspace</span>
        </div>
      </DialogTrigger>
      <DialogContent className="flex flex-col gap-7">
        <DialogHeader>
          <DialogTitle className="text-xl">New Workspace</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="flex flex-col gap-6">
              <FormField
                control={form.control}
                name="name"
                render={({ field }) => (
                  <FormItem className="flex items-center text-center gap-3">
                    <Label className="text-base">Name</Label>
                    <FormControl>
                      <Input {...field} className="border-2 border-black" />
                    </FormControl>
                  </FormItem>
                )}
              />

              <div className="flex flex-row justify-evenly">
                <Button type="submit" className="w-32 text-base">
                  Save
                </Button>
                <Button
                  type="button"
                  variant="outline"
                  className="w-32 text-base"
                  onClick={() => setIsModalOpen(false)}
                >
                  Cancel
                </Button>
              </div>
            </div>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}
