import numpy as np
import pandas as pd

from api.models import Message, MessageTagDefinition, MLWeights, MessageTag
from scripts.ml import embedding
from core.constant import CONSTANTS


def predict_tags(text_or_record, project, threashold=0.5, message_tag_group=None):
    """Returns a set of tags among known for utterance

    Args:
        text_or_record (:obj: `str`, `Message`): utterance or `Message` record with populated `Message.text` field
        threashold (float): Precision from 0 to 1 which tag should be great or equal to to be appended to resulted set
    Returns:
        set: Tags for utterance
    """

    filter_by = dict(project=project)
    if message_tag_group:
        filter_by["message_tag_group"] = message_tag_group

    tag_definitions = MessageTagDefinition.objects.filter(**filter_by)

    embedding_vector = (
        text_or_record.embedding
        if isinstance(text_or_record, Message)
        else embedding.embed_text(text_or_record)
    )  # sequence of floats

    probabilities = compute_probabilities(tag_definitions, embedding_vector)

    tags = []
    for tag, probability in probabilities.items():
        if probability >= threashold:
            tags.append({"tag": tag, "score": probability})

    return tags


def logistic(x):
    return 1.0 / (1.0 + np.exp(-np.array(x)))


def compute_probabilities(tag_definitions, embedding_vector):
    """Computes probability of tag being related to text encoded in embedding vector

    Args:
        tag_definitions (QuerySet<MessageTagDefinition>): Queryset of MessageTagDefinition
            records
        embedding_vector (:obj: `numpy.ndarray[float]`, `list[float]`):
            384 float numbers vector representing text
    Returns:
        pd.Series: label probability matching pandas series
    """

    df_labels = []
    coefs = []
    intercepts = []

    for t in tag_definitions:
        try:
            ml_weights = MLWeights.objects.get(tag_definition=t)
        except MLWeights.DoesNotExist:
            # MLWeights is a subset of MessageTagDefinition
            continue
        df_labels.append(t)
        coef = list(ml_weights.coef)
        coefs.append(coef)
        intercept = ml_weights.intercept
        intercepts.append(intercept)

    coefs = np.array(coefs)
    dot_product = logistic(
        coefs.dot(embedding_vector) + intercepts
    )  # ranges from 0 to 1
    probabilities = pd.Series(dot_product, index=df_labels)

    return probabilities


def detect_knowledge_question(message):
    try:
        knowledge_tag_def = MessageTagDefinition.objects.filter(
            name="knowledge question", project=message.project
        )

        embedding_vector = message.embedding

        prob = compute_probabilities(knowledge_tag_def, embedding_vector).iloc[0]

        if prob > CONSTANTS.KNOWLEDGE_TAG_THRESHOLD:
            return True
    except Exception:
        return False

    return False
