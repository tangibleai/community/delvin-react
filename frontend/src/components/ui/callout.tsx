// Tremor Raw Callout [v0.0.0]

import React from "react";
import { tv, type VariantProps } from "tailwind-variants";
import { Link } from "react-router-dom";
import { Button } from "@/components/ui/button"; // Adjust this import path as needed

import { cn } from "@/lib/utils";

const calloutVariants = tv({
  base: "flex overflow-hidden rounded-md p-4 text-sm",
  variants: {
    variant: {
      default: ["text-primary dark:text-primary", "bg-muted dark:bg-muted/70 "],
      success: [
        "text-emerald-900 dark:text-emerald-500",
        "bg-emerald-50 dark:bg-emerald-950/70 ",
      ],
      error: [
        " text-red-900 dark:text-red-500",
        "bg-red-50 dark:bg-red-950/70",
      ],
      warning: [
        " text-yellow-900 dark:text-yellow-500",
        "bg-yellow-50 dark:bg-yellow-950/70",
      ],
      neutral: [
        "text-gray-900 dark:text-gray-400",
        "bg-gray-100 dark:bg-gray-800/70",
      ],
    },
  },
  defaultVariants: {
    variant: "default",
  },
});

interface CalloutProps
  extends React.ComponentPropsWithoutRef<"div">,
    VariantProps<typeof calloutVariants> {
  title: string;
  icon?: React.ElementType | React.ReactElement;
  buttonText?: string;
  buttonLink?: string;
}
const Callout = React.forwardRef<HTMLDivElement, CalloutProps>(
  (
    {
      title,
      icon: Icon,
      buttonText,
      buttonLink,
      className,
      variant,
      children,
      ...props
    }: CalloutProps,
    forwardedRef,
  ) => {
    return (
      <div
        ref={forwardedRef}
        className={cn(calloutVariants({ variant }), className)}
        {...props}
      >
        <div className="flex-grow">
          <div className="flex items-start">
            {Icon && typeof Icon === "function" ? (
              <Icon
                className={cn("mr-1.5 h-5 w-5 shrink-0")}
                aria-hidden="true"
              />
            ) : (
              Icon
            )}
            <span className={cn("font-semibold")}>{title}</span>
          </div>
          <div className={cn("overflow-y-auto", children ? "mt-2" : "")}>
            {children}
          </div>
        </div>
        {buttonText && buttonLink && (
          <div className="flex items-center ml-4">
            <Link to={buttonLink}>
              <Button size="sm">{buttonText}</Button>
            </Link>
          </div>
        )}
      </div>
    );
  },
);

Callout.displayName = "Callout";

export { Callout, calloutVariants, type CalloutProps };
