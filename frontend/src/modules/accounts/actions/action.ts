import { processError } from "@/lib/utils";
import axios from "axios";

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_REACT_APP_API_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

const resendActivationEmail = async (email: string) => {
  let statusCode = 400;

  try {
    const body = JSON.stringify({
      email: email,
    });
    const response = await axiosInstance.post(
      `/auth/users/resend_activation/`,
      body,
    );
    statusCode = response.status;
  } catch (err) {
    processError(`Failed to resend activation email. Details: ${err}`);
  }

  return statusCode;
};

const retrieveUserByEmail = async (email: string) => {
  const response: { users?: []; error?: string } = {};
  try {
    const params = new URLSearchParams({
      email,
    });

    const apiResponse = await axiosInstance.get(`/api/users/?${params}`);
    response.users = apiResponse.data;
  } catch (err) {
    const errorMessage = "Rate limit exceeded. Please try again in a minute.";
    processError(errorMessage);

    response.error = errorMessage;
  }

  return response;
};

const resetUserPassword = async (
  userId: string,
  currentPassword: string,
  newPassword: string
) => {
  try {
    const data = {
      current_password: currentPassword,
      new_password: newPassword,
    };
    const body = JSON.stringify(data);
    const response = await axiosInstance.put(
      `/api/user/${userId}/reset_password/`,
      body
    );
    return response;
  } catch (error: any) {
    processError(`Failed to update user password. Details: ${error}`);
    throw error;
  }
};

export { resendActivationEmail, retrieveUserByEmail, resetUserPassword };
