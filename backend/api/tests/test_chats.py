import os
from uuid import uuid4

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from api.models import *
from api.tests import utils as test_utils
from api.tests import mock


class ChatsAPITests(TestCase):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    def setUp(self):
        set_up_data = test_utils.user_and_project_set_up(self.client)
        self.headers = set_up_data["headers"]
        self.project = set_up_data["project"]

        self.contact = Contact.objects.create(
            project=self.project, **mock.mock["contact1"]
        )

        self.chat1 = Chat.objects.create(contact_id=self.contact, **mock.mock["chat1"])

        self.chat2 = Chat.objects.create(contact_id=self.contact, **mock.mock["chat2"])

        self.message1 = Message.objects.create(
            project=self.project,
            contact=self.contact,
            chat=self.chat1,
            **mock.mock["message1"],
        )

        self.message2 = Message.objects.create(
            project=self.project,
            contact=self.contact,
            chat=self.chat1,
            **mock.mock["message2"],
        )

        self.message3 = Message.objects.create(
            project=self.project,
            contact=self.contact,
            chat=self.chat2,
            **mock.mock["message3"],
        )

    def test_get_chats_from_project(self):
        # Define the URL with query parameter for project_id
        url = f"{reverse('chats_list')}?project_id={str(self.project.id)}&internal=true"

        # Make the GET request
        response = self.client.get(url, headers=self.headers)

        # Check if the status code is OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check the response data (expect chats from project1 only, ordered by latest_message_date)
        # Should return two chats
        self.assertEqual(len(response.data), 2)
        # chat2 should be first in list because it's more recent
        self.assertEqual(response.data[0]["title"], mock.mock["chat2"]["title"])
        self.assertEqual(response.data[1]["title"], mock.mock["chat1"]["title"])

    def test_get_chats_order_ascending(self):
        # Test with ascending order
        url = f"{reverse('chats_list')}?project_id={str(self.project.id)}&ascending=true&internal=true"

        response = self.client.get(url, headers=self.headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Check if the first chat is the oldest (ascending order)
        self.assertEqual(response.data[0]["title"], mock.mock["chat1"]["title"])

    def tearDown(self):
        UserAccount.objects.all().delete()
        Project.objects.all().delete()
        Contact.objects.all().delete()
        Chat.objects.all().delete()
        Message.objects.all().delete()
