import { processError } from "@/lib/utils";
import axiosInstance from "../../api/axios";

export const getProjectHomeData = async (projectId: string) => {
  try {
    const response = await axiosInstance.get(
      `/api/charts/project-home/?project_id=${projectId}`,
    );

    if (response.status == 204) {
      return {
        total: {
          new_users: 0,
          active_users: 0,
          messages: 0,
        },
        chart_data: {
          date: new Date(),
          active_users: 0,
          total_users: 0,
          inbound: 0,
          outbound: 0,
        },
      };
    }

    const total = response.data.total;
    const chart_data = response.data.chart;

    return { total, chart_data };
  } catch (error: any) {
    processError(`Failed to fetch project home charts. Details: ${error}`);
  }
};
