import requests

endpoint = "http://localhost:8000/api/project/chatbot_response/"
token = 'your token'

headers = {"Authorization": f"Bearer {token}", "Content-Type": "application/json"}

data = {
    # assistant origin
    "assistant": "your assistant",
    # origin field of the contact,
    "contact": "contact email",
    # optional, origin field of the chat
    "chat": "chat origin",
    "user_message": "Hello!",
}

response = requests.post(
    endpoint,
    headers=headers,
    json=data,
)

print("status code:", response.status_code)
print("response text:", response.text)
