export interface Flow {
  id: string;
  project: string;
  name: string;
  description: string;
  assistant_role: string;
  metrics: Record<string, any>;
  ui: Record<string, any>;
  flow_graph: Record<string, any>;
  published: boolean;
  created_at: string;
  updated_at: string;
}
