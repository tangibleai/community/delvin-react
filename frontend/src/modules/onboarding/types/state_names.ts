export enum StateName {
  StepAssistantName = "StepAssistantName",
  StepSetPrompt = "StepSetPrompt",
  SubstepDiscussionBotSettings = "SubstepDiscussionBotSettings",
  SubstepQuestionAnswerBotSettings = "SubstepQuestionAnswerBotSettings",
  SubstepGuidedConversationsBotSettings = "SubstepGuidedConversationsBotSettings",
  StepDocumentAdding = "StepDocumentAdding",
  StepProjectCreation = "StepProjectCreation",
}
