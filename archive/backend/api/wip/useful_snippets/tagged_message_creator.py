from accounts.models import UserAccount
from api.models import Contact, Project, Message, MessageTagDefinition, TaggedMessage

from django.conf import settings
from scripts.ml.embedding import embed_text


def fill_nulls_in_df(df, message_direction=None, message_sender_type=None):
    """Ensures "direction" and "sender_type" columns are in dataframe, substitudes nulls in these columns with corresponding values

    Args:
        message_direction (:obj: `str`, optional):
            Value to put in place of null in "direction" column
        message_sender_type (:obj: `str`, optional):
            Value to put in place of null in "sender_type" column
    Returns:
        `pandas.DataFrame`: Dataframe with completely filled "direction" and "sender_type" columns
    Raises:
        KeyError: If either "direction" or "sender_type" not in DataFrame
    """

    message_direction_col = "direction"
    sender_type_col = "sender_type"
    required_columns = [message_direction_col, sender_type_col]

    for column in required_columns:
        if column not in df.columns:
            raise KeyError(f"Required column '{column}' is missing.")

    df[message_direction_col].fillna(message_direction, inplace=True)
    df[sender_type_col].fillna(message_sender_type, inplace=True)

    return df


def create_records(df):
    """Loads a dataframe of `TaggedMesssage` records into appropriate tables in the database

    Related records created:
    - `MessageTagDefinition`
    - `Message`
    - `TaggedMessage`

    >>> from api.spreadsheet_to_db_records.tagged_message.tagged_message import get_df, create_records
    >>> create_records(get_df())
    [...]
    >>> TaggedMessage.objects.all()
    <QuerySet [<TaggedMessage: TaggedMessage object...]>
    >>> TaggedMessage.objects.filter(message__text="Do you speak French").first()
    <TaggedMessage: TaggedMessage object ...>
    >>> TaggedMessage.objects.filter(message__text="Do you speak French").first().tag_definitions
    <django.db.models.fields.related_descriptors.create_forward_many_to_many_manager.<locals>.ManyRelatedManager object ...>
    >>> TaggedMessage.objects.filter(message__text="Do you speak French").first().tag_definitions.count()
    2
    """

    user_account = UserAccount.objects.filter(
        email=settings.ENV["TEST_AUTH_EMAIL"]
    ).first()
    project = Project.objects.filter(owner=user_account).first()
    contact = Contact.objects.filter(project=project).first()

    records = []
    for _, row in df.iterrows():
        utterance_col_num = 0
        label_col_num = 1
        utterance = row[utterance_col_num]
        label = row[label_col_num]

        direction = row[2]
        sender_type = row[3]

        embedding = embed_text(utterance)

        message_rec = Message.objects.get_or_create(
            direction=direction,
            sender_type=sender_type,
            channel_type=Message.ChannelType.WEB,
            text=utterance,
            project=project,
            contact=contact,
            embedding=embedding,
        )[0]
        tag_definition_rec = MessageTagDefinition.objects.get_or_create(
            name=label, project=project
        )[0]
        tagged_message_rec = TaggedMessage.objects.get_or_create(message=message_rec)[0]

        if not tagged_message_rec.tag_definitions.filter(
            id=tag_definition_rec.id
        ).exists():
            tagged_message_rec.tag_definitions.add(tag_definition_rec)
            tagged_message_rec.save()

        related_records = {
            "message": message_rec,
            "message_tag_definition": tag_definition_rec,
            "tagged_message": tagged_message_rec,
        }

        records.append(related_records)
    return records
