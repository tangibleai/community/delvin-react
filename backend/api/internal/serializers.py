"""The serializers below causing circular import!"""

from rest_framework import serializers

from api.models import Message, MessageTag, MessageTagDefinition


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"

    project = serializers.UUIDField(read_only=True)
    contact = serializers.SerializerMethodField()
    chat = serializers.SerializerMethodField(required=False)
    embedding = serializers.ListField(child=serializers.FloatField(), required=False)

    def get_contact(self, obj):
        return str(obj.contact.id)

    def get_chat(self, obj):
        if obj.chat:
            return str(obj.chat.id)
        return None


class MessageTagDefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageTagDefinition
        fields = "__all__"


class MessageTagSerializer(serializers.ModelSerializer):
    message = MessageSerializer(read_only=True)
    tag = MessageTagDefinitionSerializer(read_only=True)

    class Meta:
        model = MessageTag
        fields = "__all__"
