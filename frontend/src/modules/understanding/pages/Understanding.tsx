import { useParams } from "react-router-dom";

import { Button } from "@/components/ui/button";
import { IoReload } from "react-icons/io5";
import { Tab, TabList, TabPanel, TabPanels, TabGroup } from "@tremor/react";
import { useToast } from "@/components/ui/use-toast";

import { MLModelStatuses } from "@/typings/ml_model";
import { QueueTabContent } from "../components/queue/QueueTabContent";
import { TagsTabContent } from "../components/tags/TagsTabContent";
import {
  useGetMLModelQuery,
  useRetrainMLModelMutation,
} from "@/slices/api/mlModelApiSlice";

const Understanding = () => {
  const { toast } = useToast();
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const {
    data: mlModel = {
      id: "",
      project: "",
      name: "",
      description: "",
      dataset: "",
      status: MLModelStatuses.UP_TO_DATE,
      created_by: "",
    },
  } = useGetMLModelQuery(projectId);
  const [retrainMLModel, { isLoading: retrainingMLModel }] =
    useRetrainMLModelMutation();

  const handleRetrainButtonClicked = async () => {
    try {
      await retrainMLModel(projectId).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Could not retrain a model. Please, try again.",
      });
      return;
    }

    toast({
      title: "Success!",
      description: "Model was successfully retrained.",
    });
  };

  return (
    <>
      <div className="head-section">
        <h1 className="header">AI Training</h1>
        {retrainingMLModel || mlModel.status == MLModelStatuses.TRAINING ? (
          <Button disabled>
            <IoReload className="mr-2 h-4 w-4 animate-spin" />
            Training
          </Button>
        ) : mlModel.status == MLModelStatuses.UP_TO_DATE ? (
          <Button disabled onClick={handleRetrainButtonClicked}>
            Retrain model
          </Button>
        ) : mlModel.status == MLModelStatuses.NEEDS_RETRAINING ? (
          <Button onClick={handleRetrainButtonClicked}>Retrain model</Button>
        ) : (
          <></>
        )}
      </div>
      <TabGroup>
        <TabList className="mt-8">
          <Tab>Tags</Tab>
          <Tab>Add examples</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <TagsTabContent />
          </TabPanel>
          <TabPanel className="text-left">
            <QueueTabContent />
          </TabPanel>
        </TabPanels>
      </TabGroup>
    </>
  );
};

export default Understanding;
