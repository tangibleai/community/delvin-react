export interface Chat {
  contact_id: string;
  id: string;
  is_default: string;
  origin_id: string;
  title: string;
  lastMessage: string;
  lastMessageSender: string;
}
