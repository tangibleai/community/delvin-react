import { Button } from "@/components/ui/button";
import { useReactFlow } from "reactflow";

import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogFooter,
  DialogClose,
} from "@/components/ui/dialog";

import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/store";
import {
  setIsDeleteNodeDialogOpened,
  setNodeSidebar,
} from "@/slices/flowSlice";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";

const DeleteNodeDialog = () => {
  const { deleteElements, getNodes } = useReactFlow();

  const dispatch = useDispatch();

  const isDeleteNodeDialogOpened = useSelector(
    (state: RootState) => state.flow.isDeleteNodeDialogOpened
  );
  const nodeToDelete = useSelector((state: RootState) => state.flow.activeNode);

  const handleSubmit = useCallback(() => {
    dispatch(
      setNodeSidebar({
        opened: false,
        nodeType: null,
      })
    ); // required here
    let nodesToDelete;
    if (nodeToDelete.type === nodeNameToTypeMapping.CONDITION_BLOCK) {
      const nodes = getNodes();
      const singleConditions = nodes.filter(
        (node) => node.parentId === nodeToDelete.id
      );
      nodesToDelete = singleConditions.map((node) => ({
        id: node.id,
      }));
      nodesToDelete.push({ id: nodeToDelete.id });
    } else if (nodeToDelete.type === nodeNameToTypeMapping.PROMPT_NODE) {
      nodesToDelete = [{ id: nodeToDelete.id }];
    }
    deleteElements({ nodes: nodesToDelete });
    dispatch(setIsDeleteNodeDialogOpened(false));
  }, [nodeToDelete, deleteElements]);

  const onDialogClose = () => {
    dispatch(
      setNodeSidebar({
        opened: false,
        nodeType: null,
      })
    ); // required here
    dispatch(setIsDeleteNodeDialogOpened(false));
  };

  return (
    <Dialog open={isDeleteNodeDialogOpened} onOpenChange={onDialogClose}>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Delete?</DialogTitle>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <span>
            Deleting this node is irreversible. Are you sure you want to delete{" "}
            {nodeToDelete?.data?.title}?
          </span>
        </div>
        <DialogFooter>
          <Button type="submit" onClick={handleSubmit}>
            Yes, delete
          </Button>
          <DialogClose asChild>
            <Button type="button" onClick={onDialogClose}>
              Cancel
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export { DeleteNodeDialog };
