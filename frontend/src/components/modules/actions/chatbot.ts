import { ChatbotToChatMapping } from "@/slices/chatbotSlice";
import { ChatbotConfig } from "@/typings/chatbot_config";
import { nanoid } from "nanoid";

export const resolveChatForChatbotMapping = async (
  chatbot: ChatbotConfig,
  chatbotToChatMapping: ChatbotToChatMapping[]
) => {
  const mapping = chatbotToChatMapping.find(
    (mapping) => mapping.chatbotId == chatbot.id
  );

  const chatOrigin = mapping ? mapping.chatOrigin : nanoid();

  return { chatbotId: chatbot.id, chatOrigin: chatOrigin! };
};
