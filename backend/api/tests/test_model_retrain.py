import json
import os
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from django.test.testcases import SerializeMixin
from rest_framework.test import APIClient

import api.tasks
from api.models import *
from api.etl.runner import create_records
from api.utils.tags_prediction import predict_tags
from api.tests import mock


def is_uuid(string):
    """Checks if a string is a valid UUID."""
    try:
        uuid.UUID(string)
        return True
    except ValueError:
        return False


def filter_by_fields(input_dict, filter_fields):
    filtered_data = {
        key: value for key, value in input_dict.items() if key in filter_fields
    }
    return filtered_data


class TestViews(TestCase, SerializeMixin):
    """Test the API views.
    To test in order using the SerializeMixin
    """

    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()

    fixtures = [
        "default_account_data.json"
    ]  # to populate database with default account records to be able to test "copy dataset" feature

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        user = filter_by_fields(mock.mock["user1"], ["email", "name", "password"])
        user_rec = UserAccount.objects.create_user(**user)
        response = cls.client.post("/auth/jwt/create/", user)
        cls.user1_access_token = response.json()["access"]

        # Create workspace
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["workspace1"]["name"],
            "properties": mock.mock["workspace1"]["properties"],
            "owner": str(user_rec.id),
        }
        json_data = json.dumps(req_body)
        serialized_workspace = cls.client.post(
            "/api/workspace/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()

        workspace_id = serialized_workspace["id"]

        # Create project1
        headers = {"Authorization": "JWT " + cls.user1_access_token}
        req_body = {
            "name": mock.mock["user1"]["project_name"],
            "workspace_id": workspace_id,
        }
        json_data = json.dumps(req_body)
        serialized_project = cls.client.post(
            "/api/project/create/",
            json_data,
            content_type="application/json",
            headers=headers,
        ).json()
        cls.project1_id = serialized_project["id"]
        cls.project = Project.objects.get(id=cls.project1_id)
        cls.project1_token_key = serialized_project["token_key"]

    def test_retrain_model(self):
        tags = predict_tags("hello", self.project)
        self.assertTrue(len(tags) == 0)

        greeting_tag_name = "greeting"

        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.post(
            "/api/message-tag-definitions/",
            json.dumps(
                {
                    "project_id": str(self.project.id),
                    "name": greeting_tag_name,
                    "description": "User greeted",
                }
            ),
            headers=headers,
            content_type="application/json",
        ).json()

        hello_tag_id = response["id"]

        message_examples = [
            "hello",
            "hi",
            "good morning",
            "good evening",
            "nice to meet you",
            "glad to see you",
        ]

        for m in message_examples:
            self.tag_message_with_hello_tag(m, hello_tag_id)

        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.post(
            "/api/project/retrain_model/",
            json.dumps({"project_id": str(self.project.id)}),
            headers=headers,
            content_type="application/json",
        ).json()

        tags_with_stat = predict_tags("hello", self.project)
        self.assertTrue(len(tags_with_stat) > 0)
        self.assertTrue(
            greeting_tag_name in [tag_stat["tag"].name for tag_stat in tags_with_stat]
        )

    def tag_message_with_hello_tag(self, message_text, hello_tag_id):
        headers = {"Authorization": "JWT " + self.user1_access_token}
        response = self.client.post(
            "/api/message-tags/",
            json.dumps(
                {
                    "project_id": str(self.project.id),
                    "tag_ids": [hello_tag_id],
                    "message_text": message_text,
                }
            ),
            headers=headers,
            content_type="application/json",
        ).json()

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()
