import { createApi } from "@reduxjs/toolkit/query/react";
import axiosBaseQuery from "@/api/axiosBaseQuery";

export const emptyApi = createApi({
  reducerPath: "api",
  baseQuery: axiosBaseQuery({
    baseUrl: `${import.meta.env.VITE_REACT_APP_API_URL}/api/`,
  }),
  endpoints: () => ({}),
  tagTypes: [
    "Chat",
    "Chatbot",
    "Contact",
    "Document",
    "Flow",
    "GuardrailResponse",
    "Message",
    "MessageTag",
    "MessageTagDefinition",
    "MLModel",
    "Moderation",
    "Projects",
    "TeamMember",
    "Workspaces",
    "Provider",
    "GenerativeModel",
    "Plan",
  ],
});

// export const apiService = createApi({
//   reducerPath: 'api',
//   baseQuery: axiosBaseQuery({ baseUrl: `${import.meta.env.VITE_REACT_APP_API_URL}/api/` }),
//   tagTypes: ['GuardrailResponse'],
//   endpoints: builder => ({
//     createGuardrailResponse: builder.mutation<GuardrailResponse, NewGuardrailResponse>({
//       query: (newResponse: NewGuardrailResponse) => ({
//         url: "guardrail-responses/create/",
//         method: "POST",
//         data: newResponse
//       }),
//       invalidatesTags: ['GuardrailResponse']
//     }),

//     updateGuardrailAction: builder.mutation<GuardrailAction, GuardrailAction>({
//       query: (updatedAction: GuardrailAction) => ({
//         url: `guardrail-actions/${updatedAction.id!}/update/`,
//         method: 'PUT',
//         data: updatedAction
//       }),
//       invalidatesTags: ['GuardrailResponse']
//     }),

//     deleteGuardrailResponse: builder.mutation<null, string>({
//       query: (guardrailResponseId: string) => ({
//         url: `guardrail-responses/${guardrailResponseId}/delete/`,
//         method: "DELETE",
//       }),
//       invalidatesTags: ['GuardrailResponse']
//     }),

//     listGuardrailResponses: builder.query<GuardrailResponse[], string>({
//       query: (projectId: string) => ({
//         url: "guardrail-responses/",
//         params: { project_id: projectId }
//       }),
//       providesTags: ['GuardrailResponse']
//     })

//   })
// });

export default emptyApi;
