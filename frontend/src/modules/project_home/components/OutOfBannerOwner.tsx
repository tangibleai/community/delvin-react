import { Button } from "@/components/ui/button";
import CONSTANTS from "@/constants";
import { GoAlertFill } from "react-icons/go";

export default function OutOfBannerOwner() {

    const handleEmailClick = () => {
        window.location.href = `mailto:${CONSTANTS.CONTACT_EMAIL}?subject=contact us`;
    }

    return (
        <div className="bg-red-200 flex items-center mt-6 p-6 rounded-md">

            <div className="mr-4 flex-shrink-0 text-3xl">
                <GoAlertFill />
            </div>

            <div className="flex-1">
                <h2 className="text-lg font-semibold">
                    Your workspace is out of Credits.
                </h2>
                <p className="text-sm mt-1">
                    Your projects can now only use free generative models on limited availability bases and API usage has been disabled.
                    Contact us to upgrade your plan and get access to all the models, unlimited messages, and more!
                </p>
            </div>

            <Button onClick={handleEmailClick} variant="outline">Contact Us</Button>

        </div>
    );
};
