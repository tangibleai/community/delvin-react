import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DialogFooter,
  DialogClose,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { ScrollArea } from "@/components/ui/scroll-area";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Tag } from "./Tag";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useToast } from "@/components/ui/use-toast";
import {
  useCreateMessageTagDefinitionMutation,
  useListMessageTagDefinitionsQuery,
} from "@/slices/api/messageTagDefinition";
import { Skeleton } from "@/components/ui/skeleton";
import { BiErrorAlt } from "react-icons/bi";
import { MessageTagDefinition } from "@/typings/message_tag_definition";
import { IoReload } from "react-icons/io5";

type Props = {
  activeTagDefinition: MessageTagDefinition;
  setActiveTagDefinition: React.Dispatch<React.SetStateAction<object>>;
};

const TagDefinitionsSidebar = ({
  activeTagDefinition,
  setActiveTagDefinition,
}: Props) => {
  const { toast } = useToast();
  const [isNewtagDialogOpen, setIsNewTagDialogOpen] = useState(false);

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const {
    data: tags = [],
    isError: errorRetrievingTags,
    isFetching: retrievingTags,
  } = useListMessageTagDefinitionsQuery(projectId);

  const [
    createMessageTagDefinition,
    { isLoading: creatingMessageTagDefinition },
  ] = useCreateMessageTagDefinitionMutation();

  useEffect(() => {
    if (!errorRetrievingTags) return;

    toast({
      variant: "destructive",
      title: "Oops! Something went wrong",
      description: "Could not retrieve tags. Please, try to reload page.",
    });
  }, [errorRetrievingTags]);

  useEffect(() => {
    if (!tags.length) return;

    setActiveTagDefinition(tags[0]);
  }, [tags]);

  const newTagFormSchema = z.object({
    tag_name: z.string().min(1, "Tag name is required"),
    description: z.string().min(1, "Description is required"),
  });

  const newTagForm = useForm<z.infer<typeof newTagFormSchema>>({
    resolver: zodResolver(newTagFormSchema),
    defaultValues: {
      tag_name: "",
      description: "",
    },
  });

  async function handleSubmit(values: z.infer<typeof newTagFormSchema>) {
    const { tag_name, description } = values;
    try {
      await createMessageTagDefinition({
        project_id: projectId,
        name: tag_name,
        description,
      }).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Could not create a new tag. Please, try again.",
      });
      return;
    } finally {
      setIsNewTagDialogOpen(false); // Close the dialog
      newTagForm.reset();
    }

    toast({
      title: "Success!",
      description: "New tag was successfully created.",
    });
  }

  const toggleDialog = () => setIsNewTagDialogOpen(!isNewtagDialogOpen);

  return (
    <div className="w-1/4 h-[75vh]">
      {!tags.length ? (
        <div className="w-full h-full flex-grow flex items-center justify-center text-center text-xl rounded-2xl bg-muted">
          Create first tag
        </div>
      ) : (
        <ScrollArea className="flex flex-col justify-end flex-grow py-2.5 px-5 rounded-2xl bg-gray-100 p-4 max-h-full">
          {errorRetrievingTags ? (
            <div className="flex flex-col items-center gap-2 justify-center py-2.5 px-5 rounded-2xl bg-gray-100 p-4 w-full">
              <BiErrorAlt className="text-red-600 w-12 h-12" />
              <p className="text-xl">
                Failed to fetch tags. Try to reopen this page.
              </p>
            </div>
          ) : retrievingTags ? (
            <div className="space-y-3">
              {Array.from({ length: 5 }).map((_, index) => (
                <Skeleton
                  key={index}
                  className="h-10 max-w-md mx-auto bg-gray-300"
                />
              ))}
            </div>
          ) : (
            tags.length &&
            tags.map((t) => (
              <Tag
                key={t.name}
                tagDefinition={t}
                activeTagDefinition={activeTagDefinition}
                setActiveTagDefinition={setActiveTagDefinition}
              />
            ))
          )}
        </ScrollArea>
      )}

      {!(retrievingTags || errorRetrievingTags) && (
        <Dialog open={isNewtagDialogOpen} onOpenChange={setIsNewTagDialogOpen}>
          <DialogTrigger asChild>
            <Button className="mt-2" onClick={toggleDialog}>
              New tag
            </Button>
          </DialogTrigger>
          <DialogContent className="sm:max-w-[425px]">
            <Form {...newTagForm}>
              <form onSubmit={newTagForm.handleSubmit(handleSubmit)}>
                <DialogHeader>
                  <DialogTitle>Create new tag</DialogTitle>
                </DialogHeader>
                <div className="grid gap-4 py-4">
                  <FormField
                    control={newTagForm.control}
                    name="tag_name"
                    render={({ field }) => {
                      return (
                        <>
                          <FormItem>
                            <FormLabel>Tag name</FormLabel>
                            <FormControl>
                              <Input placeholder="Tag Name" {...field} />
                            </FormControl>
                          </FormItem>
                          {newTagForm?.formState?.errors?.tag_name?.message && (
                            <FormMessage />
                          )}
                        </>
                      );
                    }}
                  />
                  <FormField
                    control={newTagForm.control}
                    name="description"
                    render={({ field }) => {
                      return (
                        <>
                          <FormItem>
                            <FormLabel>Description</FormLabel>
                            <FormControl>
                              <Input placeholder="Description" {...field} />
                            </FormControl>
                          </FormItem>
                          {newTagForm?.formState?.errors?.description
                            ?.message && <FormMessage />}
                        </>
                      );
                    }}
                  />
                </div>
                <DialogFooter>
                  <Button type="submit" disabled={creatingMessageTagDefinition}>
                    {creatingMessageTagDefinition && (
                      <IoReload className="mr-2 h-4 w-4 animate-spin" />
                    )}
                    Create
                  </Button>
                  <DialogClose asChild>
                    <Button
                      variant="outline"
                      type="button"
                      disabled={creatingMessageTagDefinition}
                    >
                      Cancel
                    </Button>
                  </DialogClose>
                </DialogFooter>
              </form>
            </Form>
          </DialogContent>
        </Dialog>
      )}
    </div>
  );
};

export { TagDefinitionsSidebar };
