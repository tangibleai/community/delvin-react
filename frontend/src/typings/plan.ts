// Plan.ts

export interface Plan {
  id: string; // UUIDField is represented as string
  type: PlanType;
  renewal_date: Date | null;
  status: PlanStatus | null;
  properties: PlanProperties; // Updated to include free_message
}

export enum PlanType {
  FREE = "F",
  PAID = "P",
  OWN_KEY = "O",
}

export enum PlanStatus {
  ACTIVE = "A",
  EXPIRED = "E",
  PAUSED = "P",
}

export interface PlanProperties {
  free_message: string; // At least this property exists
  [key: string]: any; // For any other dynamic properties
}
