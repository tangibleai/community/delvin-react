import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { CardContent, Card } from "@/components/ui/card";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { Label } from "@/components/ui/label";
import { ChatbotConfig } from "@/typings/chatbot_config";
import { useEffect } from "react";
import { useToast } from "@/components/ui/use-toast";
import DeleteChatbotModal from "./DeleteChatbotModal";
import {
  useListChatbotsQuery,
  useUpdateChatbotMutation,
} from "@/slices/api/chatbotsApiSlice";
import { useParams } from "react-router-dom";
import { Skeleton } from "@/components/ui/skeleton";
import { IoReload } from "react-icons/io5";

const formSchema = z.object({
  chatbotName: z
    .string()
    .min(1, "Workspace name is required")
    .regex(
      /^[a-zA-Z0-9_]+$/,
      "Chatbot name can only contain letters, numbers, and underscores"
    ),
  type: z.string(),
});

type Props = {
  selectedChatbot: ChatbotConfig;
};

const ChatbotProfile = ({ selectedChatbot }: Props) => {
  const { toast } = useToast();

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const botType = selectedChatbot?.config?.bot_type;

  const { isLoading: retrievingChatbots } = useListChatbotsQuery({
    project_id: projectId,
  });
  const [updateChatbot, { isLoading: updatingChatbot }] =
    useUpdateChatbotMutation();

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      chatbotName: selectedChatbot.origin,
      type: botType
        ? botType.charAt(0).toUpperCase() + botType.slice(1).toLowerCase()
        : "",
    },
  });

  function resetForm() {
    form.setValue("chatbotName", selectedChatbot.origin);
    form.setValue(
      "type",
      botType.charAt(0).toUpperCase() + botType.slice(1).toLowerCase()
    );
  }

  useEffect(() => {
    if (!selectedChatbot || !Object.keys(selectedChatbot).length) return;

    resetForm();
  }, [selectedChatbot]);

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const { chatbotName } = values;

    const updatedChatbot = { ...selectedChatbot, origin: chatbotName };

    try {
      await updateChatbot(updatedChatbot).unwrap();
      return;
    } catch (err) {
      toast({
        variant: "destructive",
        title: "Error",
        description: "Failed to update assistant name. Please try again.",
      });
    }

    toast({
      title: "Success!",
      description: "Chatbot name updated successfully.",
    });
  };

  return (
    <Card className="p-4">
      <CardContent className="flex flex-col gap-4">
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onSubmit)}
            className="flex flex-col gap-6"
          >
            <FormField
              control={form.control}
              name="chatbotName"
              render={({ field }) => (
                <FormItem className="flex gap-4 items-center">
                  <Label htmlFor="chatbot-name" className="text-base">
                    ID
                  </Label>
                  <div className="flex gap-3">
                    {retrievingChatbots ? (
                      <Skeleton className="h-10 w-40" />
                    ) : (
                      <FormControl>
                        <Input
                          id="chatbot-name"
                          {...field}
                          className="w-full"
                        />
                      </FormControl>
                    )}
                    <Button
                      type="submit"
                      className="w-fit"
                      disabled={retrievingChatbots || updatingChatbot}
                    >
                      {updatingChatbot && (
                        <IoReload className="mr-2 h-4 w-4 animate-spin" />
                      )}
                      Save
                    </Button>
                  </div>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              name="type"
              render={({ field }) => (
                <FormItem className="flex gap-4 items-center">
                  <Label htmlFor="type" className="text-base">
                    Type
                  </Label>
                  <div className="flex gap-3">
                    {retrievingChatbots ? (
                      <Skeleton className="h-10 w-40" />
                    ) : (
                      <FormControl>
                        <Input
                          id="type"
                          disabled
                          {...field}
                          className="w-full"
                        />
                      </FormControl>
                    )}
                  </div>
                  <FormMessage />
                </FormItem>
              )}
            />
          </form>
        </Form>

        <DeleteChatbotModal selectedChatbot={selectedChatbot} />
      </CardContent>
    </Card>
  );
};

export default ChatbotProfile;
