import axiosInstance from "@/api/axios";
import { processError } from "@/lib/utils";

export const getCeleryTaskStatistics = async (taskId: string) => {
  try {
    const response = await axiosInstance.get(
      `${import.meta.env.VITE_REACT_APP_API_URL}/celery-progress/${taskId}/`
    );
    const taskStatistics = response.data;

    return taskStatistics;
  } catch (err) {
    processError(`Failed to retrieve celery task statistics. Details: ${err}`);
  }

  return {};
};
