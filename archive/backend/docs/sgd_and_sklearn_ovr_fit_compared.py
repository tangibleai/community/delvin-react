%run multilabel_intent_recognition.py
ls
import pandas as pd
df = pd.read_csv('data/multi_intent_recognizer_training_set.csv')
ls ../
%run ../../nlu-fastapi/multilabel_intent_recognition.py
df = pd.read_csv('data/multi_intent_recognizer_training_set.csv')
bert = SentenceTransformer("all-MiniLM-L6-v2")
%run api/ml/sgd.py
df = tags_from_labels(DATA_DIR / TRAINING_SET_PATH)
results = train_and_save(df)
pipe = results['pipe']
pipe.predict(['hello world'])
pipe.classes_
pipe.feature_names_in_
tagger = results['tagger']
tagger.classes_
tagger.classes
tagger.get_params()
df.columns
len(pipe.classes_)
len(df.columns)
pipe.classes_ = list(df.columns)
pipe.classes = list(df.columns)
pd.DataFrame(pipe.predict(['hello world']), columns=df.columns)
pd.DataFrame(pipe.predict_proba(['hello world']), columns=df.columns)
results_sgd = fit(x, y=df['subscribe'].values, learning_rate=.02, momentum=.5)
enc = bert.encode(df.index.values)
df.index.values
dfenc1 = pd.concat([pd.DataFrame([[1.]]*len(enc)), pd.DataFrame(enc)], axis=1)
results_sgd = fit(x=dfenc1.values, y=df['subscribe'].values, learning_rate=.02, momentum=.5)
results_sgd
results_sgd['rmse_history'][-1]
results_sgd.keys()
results_sgd = fit(x=dfenc1.values, y=df['subscribe'].values, learning_rate=.02, batch_size=32, max_iter=200, momentum=.5)
results_sgd['rmse_history'][-1]
pred1 = x.dot(results['coef'].values)
x = dfenc1.values
pred1 = x.dot(results['coef'].values)
pred1 = x.dot(results_sgd['coef'].values)
pred1 = x.dot(results_sgd['coef'])
pred1
results_sgd = fit(x=dfenc1.values, y=df['subscribe'].values, learning_rate=.02, batch_size=32, max_iter=200, momentum=.5, activation_fun='logistic')
pred1 = logistic(x.dot(results_sgd['coef']))
pred1
pd.DataFrame(pred1, index=df.index)
df.columns[0]
df[0]
dfpred = pd.DataFrame(pred1, index=df.index)
dfpred['true'] = df['subscribe']
dfpred
df
df['subscribe'].sum() / len(df)
ratio = df['subscribe'].sum() / len(df)
ratio ** -1
ratio = df['subscribe'].sum()
ratio
250-17
len(df)-17
17 * 3
17 * 15
dfpred.sum()
dfos = pd.concat([df[df['subscribe'].astype(bool)]] * 17, axis=0)
dfos
dfos = pd.concat([df[df['subscribe'].astype(bool)]] * 16, axis=0)
dfos
dfos = pd.concat([df[df['subscribe'].astype(bool)]] * 15, axis=0)
dfos
dfos = pd.concat([dfos, df], axis=0)
dfos
dfos.shuffle()
dfos.sample(len(dfos))
dfos = dfos.sample(len(dfos))
y = dfos['subscribe'])
y = dfos['subscribe']
x = dfenc1[dfos.index]
x = dfenc1[dfos.index.values]
x = dfenc1.loc[dfos.index.values]
x = dfenc1.loc[dfos.index]
dfos.index.values
messages_os = list(dfos.index.values)
len(messages_os)
dfenc1
dfenc1.index = df.index
x = dfenc1.loc[dfos.index]
x
results_sgd = fit(x=x, y=y, learning_rate=.02, batch_size=32, max_iter=200, momentum=.5, activation_fun='logistic')
results_sgd['rmse_history'][-1]
dfpred = pd.DataFrame(pred1, index=df.index)
pred1 = logistic(x.dot(results_sgd['coef']))
dfpred = pd.DataFrame(pred1, index=df.index)
dfpred = pd.DataFrame(pred1, index=dfos.index)
dfpred
dfpred['true'] = dfos['subscribe']
dfpred
dfpred['pred'] = (dfpred[0] > .5).astype(int)
dfpred
(dfpred['true'] == dfpred['pred']).sum()
(dfpred['true'] == dfpred['pred']).sum() / len(dfpred)
tagger
tagger.coef_
tagger.coef
tagger.estimators
tagger.estimator
tagger.estimator.coef_
tagger.estimator.coef
vars(tagger.estimator)
tagger
tagger.predict(dfos.index.values)
tagger.predict(x[:,1:])
x
x.values[:,1:]
tagger.predict(x.values[:,1:])
pipe.named_steps['tagger']
pipe.named_steps['tagger'].coef_
tagger = pipe.named_steps['tagger']
tagger.coef_
pipe.steps
from sklearn.linear_model import LogisticRegression
model = LogisticRegression(class_weight='balanced')
model.fit(x, y)
model.predict(x, y)
model.predict(x)
who
OneVsRestClassifier?
OneVsRestClassifier(LogisticRegression)
ovr = OneVsRestClassifier(LogisticRegression(class_weight=balanced))
ovr = OneVsRestClassifier(LogisticRegression(class_weight='balanced'))
ovr
x
y
x = dfenc1.values
ny = db.search('New Years')
y = df['subscribe']
y
y = df.values
y
x
x = x[:,1:]
x
ovr.fit(x,y)
ovr.predict(x)
((ovr.predict(x) - y) **2).sum()**.5
((ovr.predict(x) - y) **2).mean()**.5
ovr.predict(x)
ovr.predict(x).sum()
pd.DataFrame(ovr.predict(x), columns = df.columns)
ovr.estimator.coef_
ovr.estimators
pd.DataFrame(ovr.predict(x), columns = df.columns, index=df.index)
pd.DataFrame(ovr.predict(x), columns = df.columns, index=df.index).head().T
hist -o -p -f sgd_and_sklearn_ovr_fit_compared.ipy
hist -f sgd_and_sklearn_ovr_fit_compared.py
