import StepAssistantName from "../components/StepAssistantName";
import StepSetPrompt from "../components/StepSetPrompt";
import StepDocumentAdding from "../components/StepDocumentAdding";
import StepProjectCreation from "../components/StepProjectCreation";
import SubstepDiscussionBotSettings from "../components/SubstepDiscussionBotSettings";
import SubstepQuestionAnswerBotSettings from "../components/SubstepQuestionAnswerBotSettings";
import SubstepGuidedConversationsBotSettings from "../components/SubstepGuidedConversationsBotSettings";

const componentMap: {
  [key: string]: React.ForwardRefExoticComponent<React.RefAttributes<unknown>>;
} = {
  StepAssistantName,
  StepSetPrompt,
  SubstepDiscussionBotSettings,
  SubstepQuestionAnswerBotSettings,
  SubstepGuidedConversationsBotSettings,
  StepDocumentAdding,
  StepProjectCreation,
};

export default componentMap;
