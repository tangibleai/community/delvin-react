import { Node } from "reactflow";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";
import { Condition } from "../typing/condition";

export const initNewNode = (nodes: Node[], nodeType: string) => {
  const newNodeX = getClosestRightmostAvailablePosition(nodes);
  const newNodeId = getNextNodeId(nodes);
  const viewportHeight = window.innerHeight;

  const { width: newNodeWidth, height: newNodeHeight } =
    calculateNodeDimensions(nodeType, viewportHeight);
  const nodeName = generateNodeName(nodeType, newNodeId);

  const newNode: Node = {
    id: newNodeId,
    type: nodeType,
    data: {
      nodeId: newNodeId,
      nodeName,
      nodePrompt: "",
      condition: {
        properties: { tag_name: "", tag_id: "" },
      } as Partial<Condition>,
    },
    style: {
      width: newNodeWidth,
      height: newNodeHeight,
    },
    position: { x: newNodeX + 5, y: 0 },
  } as Node;

  if (nodeType === nodeNameToTypeMapping.SINGLE_CONDITION) {
    newNode.draggable = false;
  }

  return newNode;
};

const getClosestRightmostAvailablePosition = (nodes: Node[]) => {
  // uncomment everything commented below to count on 4 directions
  // ========================================================
  // const flowContainerWidth = reactFlowWrapper.current.clientWidth;
  // const flowContainerHeight = reactFlowWrapper.current.clientHeight;

  let rightmostNodeX = 0;
  // let leftmostNodeX = flowContainerWidth;

  // let uppermostNodeY = flowContainerHeight;
  // let bottommostNodeY = 0;

  for (const node of nodes) {
    const x = node.position.x;
    // const y = node.position.y;
    const width = node.width!;
    // const height = node.height!;

    // if (x < leftmostNodeX) leftmostNodeX = x;
    if (x + width > rightmostNodeX) rightmostNodeX = x + width;
    // if (y < uppermostNodeY) uppermostNodeY = y;
    // if (y + height > bottommostNodeY) bottommostNodeY = y + height;
  }
  // ===========================================================

  return rightmostNodeX;
};

const getNextNodeId = (nodes: Node[]): string => {
  const maxId =
    nodes.length === 0
      ? 0
      : Math.max(...nodes.map((node) => parseInt(node.id)));
  return (maxId + 1).toString();
};

const calculateNodeDimensions = (nodeType: string, viewportHeight: number) => {
  const nodeWidthMapping = {
    [nodeNameToTypeMapping.PROMPT_NODE]: 36,
    [nodeNameToTypeMapping.CONDITION_BLOCK]: 18,
    [nodeNameToTypeMapping.SINGLE_CONDITION]: 18,
  };

  const newNodeWidthDivisible = nodeWidthMapping[nodeType] || 36;
  const newNodeWidth = (newNodeWidthDivisible / 100) * viewportHeight;

  const nodeHeightMapping = {
    [nodeNameToTypeMapping.PROMPT_NODE]: 24,
    [nodeNameToTypeMapping.CONDITION_BLOCK]: 6,
    [nodeNameToTypeMapping.SINGLE_CONDITION]: 3,
  };

  const newNodeHeightDivisible = nodeHeightMapping[nodeType] || 24;
  const newNodeHeight = (newNodeHeightDivisible / 100) * viewportHeight;

  return { width: newNodeWidth, height: newNodeHeight };
};

const generateNodeName = (nodeType: string, newNodeId: string): string => {
  if (nodeType === nodeNameToTypeMapping.PROMPT_NODE) {
    return `message-${newNodeId}`;
  }
  if (nodeType === nodeNameToTypeMapping.CONDITION_BLOCK) {
    return `condition-${newNodeId}`;
  }
  return "";
};
