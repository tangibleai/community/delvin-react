## Cetin 

* [ ] Make https://gitlab.com/tangibleai/community/delvin-docs available on docs.delvin.to 
* [ ] User can create MessageTagDefinition when creating a response. https://gitlab.com/tangibleai/community/delvin/-/issues/58
* [ ] Define Prometheus alerts 

## Vlad 
* [ ] Finalize the linear onboarding. 
* [ ] Make the onboarding flow conditional on the bot template.
* [ ] Backend of Flows 
* [ ] Refactor the ChatContainer in ChatPreview. 


## Maria 
* [ ] Send the prompt-based onboarding step to Vlad. 
* [ ] Create a Delvin project and give Cetin access. 
* [ ] Meet with Cetin to integrate the Uptime alerts to Slack.