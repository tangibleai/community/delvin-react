import os
from pathlib import Path

import requests
from dotenv import load_dotenv

ROOT = Path(__file__).resolve().parent.parent

load_dotenv(dotenv_path=os.path.join(ROOT / "py_client" / ".env"))

endpoint = "http://localhost:8000/auth/users/activation/"

uid = os.environ.get("UID")
activation_token = os.environ.get("ACTIVATION_TOKEN")

data = {
    "uid": uid,
    "token": activation_token,
}


response = requests.post(
    endpoint,
    json=data,
)

if response.status_code == 204:
    print(response.content)
else:
    print(response.status_code)
    print(response.content)
