## Maria 
* [ ] Create a joblib file with a simple intent classification model  


## Cetin 
* [X] Add an `embedding` vector field to `Message`
* [X] For every message that is ingested, create an embedding and save it into the field.

pgvector is not working with multi sentences.

## Greg 
* Working on NLPiA Appendix E 
