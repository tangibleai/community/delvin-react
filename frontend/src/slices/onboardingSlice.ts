import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  OnboardingState,
  ONBOARDING_STATES,
  getNextOnboardingState,
  getPreviousOnboardingState,
} from "@/modules/onboarding/utils/onboardingStateManager";
import { OnboardingData } from "@/typings/onboarding_data";
import { StateName } from "@/modules/onboarding/types/state_names";

interface Onboarding {
  onboardingData: OnboardingData;
  currentOnboardingState: OnboardingState;
}

const initialState: Onboarding = {
  onboardingData: {
    stepAssistantName: { name: "", llm_provider_id: "", model_id: "" },
    stepSetPrompt: { chatbotType: "" },
    discussionBotSettings: {
      chatbotPersona: "",
      instructions: "",
      constraints: "",
    },
    questionAnswerBotSettings: {
      chatbotPersona: "",
      constraints: "",
      fallbackResponses: "",
    },
    guidedConversationsBotSettings: {
      chatbotPersona: "",
      constraints: "",
    },
    stepDocumentAdding: {
      file: {} as FileList,
      text: "",
      google: "",
    },
  },
  currentOnboardingState: ONBOARDING_STATES[StateName.StepAssistantName],
};

const onboardingSlice = createSlice({
  name: "onboarding",
  initialState,
  reducers: {
    setOnboardingData: (state, action: PayloadAction<OnboardingData>) => {
      state.onboardingData = action.payload;
      return state;
    },
    setNextOnboardingState(state) {
      const nextState = getNextOnboardingState(
        state.currentOnboardingState,
        state.onboardingData.stepSetPrompt.chatbotType
      );
      state.currentOnboardingState = nextState;
      return state;
    },
    setPreviousOnboardingState(state) {
      const previousState = getPreviousOnboardingState(
        state.currentOnboardingState
      );
      state.currentOnboardingState = previousState;
      return state;
    },
    resetOnboarding: (state) => {
      state.onboardingData = {
        stepAssistantName: { name: "", llm_provider_id: "", model_id: "" },
        stepSetPrompt: { chatbotType: "" },
        discussionBotSettings: {
          chatbotPersona: "",
          instructions: "",
          constraints: "",
        },
        questionAnswerBotSettings: {
          chatbotPersona: "",
          constraints: "",
          fallbackResponses: "",
        },
        guidedConversationsBotSettings: {
          chatbotPersona: "",
          constraints: "",
        },
        stepDocumentAdding: {
          file: {} as FileList,
          text: "",
          google: "",
        },
      };
      state.currentOnboardingState =
        ONBOARDING_STATES[StateName.StepAssistantName];
    },
  },
});

export const {
  setOnboardingData,
  setNextOnboardingState,
  setPreviousOnboardingState,
  resetOnboarding,
} = onboardingSlice.actions;
export default onboardingSlice.reducer;
