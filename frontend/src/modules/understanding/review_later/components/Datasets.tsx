import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { DatasetsListSidebar } from "./DatasetsList";
import { DatasetContent } from "./DatasetContent";
import { fetchDatasets, removeDatasetFromDb } from "../utils/datasets";
import { Dataset } from "@/typings/dataset";

const Datasets = () => {
  const [datasets, setDatasets] = useState([]);
  const [activeDataset, setActiveDataset] = useState<Partial<Dataset>>({});

  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  useEffect(() => {
    fetchAndSetDatasets();
  }, []);

  useEffect(() => {
    setActiveDataset(datasets[0]);
  }, [datasets]);

  const fetchAndSetDatasets = async () => {
    const datasets = await fetchDatasets(projectId);
    setDatasets(() => datasets);
  };

  const onDatasetDeleted = async (datasetId: string) => {
    await removeDatasetFromDb(projectId, datasetId);
    fetchAndSetDatasets();
  };

  return (
    <div className="flex flex-1">
      <DatasetsListSidebar
        datasets={datasets}
        fetchAndSetDatasets={fetchAndSetDatasets}
        onActiveDatasetChange={setActiveDataset}
      />
      <DatasetContent
        activeDataset={activeDataset as Dataset}
        onDatasetDeleted={onDatasetDeleted}
      />
    </div>
  );
};

export { Datasets };
