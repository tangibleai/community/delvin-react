from api import views as api_views
from django.urls import path

urlpatterns = [
    path("project/", api_views.project_api_view),
    path("user_account/", api_views.user_account_api_view),
    path("project/contact/", api_views.contact_api_view),
    path("project/contact/chat/", api_views.chat_api_view),
    path("project/message_tag_group/", api_views.message_tag_group_api_view),
    path("project/dataset/", api_views.dataset_api_view),
    path("project/ml_model/", api_views.ml_model_api_view),
    path("project/message_tag_definition/", api_views.message_tag_definition_api_view),
    path("project/populate_new/", api_views.copy_project_view),
    path(
        "project/dataset/copy_from_default/",
        api_views.copy_from_project_default_dataset_view,
    ),
    path(
        "project/get_datasets/",
        api_views.get_datasets_view,
    ),
    path(
        "project/dataset/delete/",
        api_views.dataset_delete_view,
    ),
    path(
        "project/get_dataset_tagged_messages/",
        api_views.get_dataset_tagged_messages_view,
    ),
    path(
        "project/understanding_remove_tagged_message/",
        api_views.understanding_remove_tagged_message_view,
    ),
]
