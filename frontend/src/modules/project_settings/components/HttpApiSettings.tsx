import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import ResetApiTokenModal from "@/modules/project_settings/components/ResetApiTokenModal";
import { ClipboardDocumentCheckIcon } from "@heroicons/react/24/outline";
import { Icon } from "@tremor/react";
import DisabledBannerApiEditor from "@/modules/project_home/components/DisabledBannerApiEditor";
import DisabledBannerApiOwner from "@/modules/project_home/components/DisabledBannerApiOwner";
import { RootState } from "@/store";
import { PlanStatus } from "@/typings/plan";
import { useParams } from "react-router-dom";
import { useGetWorkspaceQuery } from "@/slices/api/workspacesApiSlice";
import { useSelector } from "react-redux";
import { Workspace } from "@/typings/workspace";

function HttpApiSettings() {
  const user = useSelector((state: RootState) => state.auth.user);
  const routeParams = useParams();
  const workspaceId = routeParams.workspaceId!;
  const { data: workspace = {} as Workspace } =
    useGetWorkspaceQuery(workspaceId);
  const planStatus = workspace?.plan?.status;
  const isWorkspaceOwner = workspace?.owner === user?.email;

  const url = `${
    import.meta.env.VITE_REACT_APP_API_URL
  }/api/project/chatbot_response/`;

  const copyToClipboard = () => {
    navigator.clipboard.writeText(url);
    alert("URL copied to clipboard");
  };

  const DisabledApiCard = (): JSX.Element => (
    <Card className="mb-5">
      <CardHeader>
        <CardTitle>API integration</CardTitle>
      </CardHeader>
      <CardContent className="grid gap-4">
        {isWorkspaceOwner ? (
          <DisabledBannerApiOwner />
        ) : (
          <DisabledBannerApiEditor />
        )}
      </CardContent>
    </Card>
  );

  const ApiIntegrationCard = (): JSX.Element => (
    <Card className="mb-5">
      <CardHeader>
        <CardTitle>API integration</CardTitle>
        <CardDescription>
          Connect your Delvin bot to another chat platform or service using our
          API.{" "}
          <a
            className="text-primary hover:text-blue-700 underline"
            href="https://docs.delvin.to/docs/delvin-api"
            target="_blank"
          >
            Learn more
          </a>
        </CardDescription>
      </CardHeader>
      <CardContent className="grid gap-4">
        <div className="max-w-xs">
          <p>Your API integration link</p>
          <div className="flex items-center space-x-2 p-2 border border-gray-300 rounded-md">
            <input
              type="text"
              value={url}
              readOnly
              className="bg-gray-100 p-1 border-none outline-none flex-grow"
            />

            <Icon
              variant="light"
              icon={ClipboardDocumentCheckIcon}
              onClick={() => copyToClipboard()}
            />
          </div>
        </div>
      </CardContent>
      <CardFooter>
        <ResetApiTokenModal />
      </CardFooter>
    </Card>
  );

  return (
    <>
      {planStatus === PlanStatus.ACTIVE ? (
        <ApiIntegrationCard />
      ) : (
        <DisabledApiCard />
      )}
    </>
  );
}

export default HttpApiSettings;
