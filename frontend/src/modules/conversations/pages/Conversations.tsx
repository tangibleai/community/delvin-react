import { Tab, TabList, TabPanel, TabPanels, TabGroup } from "@tremor/react";

import ChatsSection from "../components/ChatsSection";

function Conversations() {
  return (
    <div className="w-full">
      <div className="head-section">
        <h1 className="header">User Conversations</h1>
      </div>
      <div className="w-full mx-auto text-base mt-7">
        <TabGroup>
          <TabList className="mt-8">
            <Tab>Training chats</Tab>
            <Tab>User chats</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <ChatsSection />
            </TabPanel>
            <TabPanel className="text-left">
              <ChatsSection internal={false} />
            </TabPanel>
          </TabPanels>
        </TabGroup>
      </div>
    </div>
  );
}

export default Conversations;
