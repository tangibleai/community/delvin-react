#!/usr/bin/env bash
python manage.py migrate

echo "Starting celery worker"
exec celery --app core.celery worker --loglevel=info --concurrency=1
