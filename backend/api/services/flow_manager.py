from api import models

from utils import text_generator
from flows import flow_graph


class FlowManager:
    def __init__(self, chat, chatbot_config, flow, user_prompt, chatbot):
        self.chat = chat
        self.chatbot_config = chatbot_config
        self.flow = flow
        self.user_prompt = user_prompt
        self.flow_graph = flow_graph.FlowGraph.instantiate(
            flow.flow_graph["source_node_id"], flow.flow_graph["nodes"]
        )
        self.flow_run = self.get_or_create_flow_run()
        self.message_history = self.flow_run.message_history
        self.llm_api_key = chatbot.provider.api_key
        self.model_string = self.chatbot.generative_model.model_string

    def get_or_create_flow_run(self):
        flow_run = (
            models.FlowRun.objects.filter(flow=self.flow, chat=self.chat)
            .order_by("-created_at")
            .first()
        )

        flow_run = self.create_new_flowrun_if_current_completed(flow_run)

        self.flow_run = flow_run

        return flow_run

    def create_new_flowrun_if_current_completed(self, flow_run):
        if not flow_run or flow_run.status == models.FlowRun.FlowRunStatus.COMPLETED:
            flow_run = models.FlowRun.objects.create(
                flow=self.flow,
                chat=self.chat,
                status=models.FlowRun.FlowRunStatus.IN_PROGRESS,
            )

        return flow_run

    def get_response(self):
        current_node = self.resolve_current_node()

        is_new_conversation = self.user_prompt == "" or not len(self.message_history)
        if is_new_conversation:
            response = self.start_new_conversation(current_node)
            return response

        user_prompt = {"role": "user", "content": self.user_prompt}
        self.message_history.append(user_prompt)

        next_node = self.flow_graph.get_next_node(
            current_node, self.flow.project, user_prompt
        )

        # next_node_id = current_node.get_next_node_id()
        # next_node = self.flow_graph.get_node(next_node_id)
        response = self.get_response_from_LLM(next_node)
        self.message_history.append({"role": "assistant", "content": response})

        self.update_flow_run_in_db(next_node)

        return response

    def resolve_current_node(self):
        current_node_id = (
            self.flow_run.current_state
            if self.flow_run.current_state != ""
            else self.flow_graph.source_node_id
        )

        current_node = self.flow_graph.get_node(current_node_id)

        return current_node

    def start_new_conversation(self, current_node):
        response = self.get_response_from_LLM(current_node)

        self.flow_run.message_history = [{"role": "assistant", "content": response}]
        self.flow_run.save()

        return response

    def get_response_from_LLM(self, node, message_history=None):
        if message_history is None:
            message_history = self.message_history

        message_history = (
            [
                {
                    "role": "system",
                    "content": self.chatbot_config.get_general_prompt_str(),
                },
            ]
            + message_history
            + [
                {"role": "system", "content": node.content},
            ]
        )

        LLM_response_generator = text_generator.LLMResponseGeneration(
            model_name=self.model_string, api_key=self.llm_api_key
        )
        response = LLM_response_generator.generate_text(message_history)

        return response

    def update_flow_run_in_db(self, next_node):
        if not len(next_node.edges):
            self.flow_run.status = models.FlowRun.FlowRunStatus.COMPLETED
        self.flow_run.current_state = next_node.id
        self.flow_run.message_history = self.message_history
        self.flow_run.save()

        return self.flow_run
