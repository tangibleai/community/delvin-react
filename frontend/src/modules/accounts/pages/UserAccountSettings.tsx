import { Card } from "@tremor/react";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import UpdatePasswordModal from "../components/UpdatePassword";
import UpdateNameForm from "../components/UpdateNameForm";
import AccountDeleteCard from "../components/AccountDeleteCard";
import { RxCross2 } from "react-icons/rx";
import { useNavigate } from "react-router-dom";

const UserAccountSettings = () => {
  const navigate = useNavigate();
  const user = useSelector((state: RootState) => state.auth.user);

  return (
    <div>
      <div className="flex justify-end p-5">
        <RxCross2 size="2rem" onClick={() => navigate(-1)} />
      </div>
      <div className="flex flex-col gap-7 p-4 w-1/2 ml-[5%]">
        <p className="text-2xl font-bold ml-[5%]">Account Settings</p>
        <Card className="border-2 border-black space-y-5 px-9">
          <UpdateNameForm />

          <div className="flex items-center gap-4">
            <Label className="text-lg">Email</Label>
            <div className="w-full">
              <Input
                disabled
                type="text"
                value={user?.email}
                className="bg-gray-300 border-2 border-black"
              />
            </div>
          </div>
          <div className="flex items-center justify-between text-lg">
            <Label className="text-lg">Password</Label>
            <UpdatePasswordModal />
          </div>
        </Card>

        <AccountDeleteCard />
      </div>
    </div>
  );
};

export default UserAccountSettings;
