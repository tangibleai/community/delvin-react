from django.conf import settings
from api import serializers, models


class ProjectCopy:
    """
    NOTE: In the future we might want to use nested json structure to represent project records instead of holding separate list for each model
    """

    SOURCE_PROJECT_NAME = settings.ENV["SOURCE_PROJECT_NAME"]
    SOURCE_OWNER_EMAIL = settings.ENV["SOURCE_OWNER_EMAIL"]

    def __init__(self, target_project_name, target_owner_email):
        self.extract_source_records()
        self.target_owner = models.UserAccount.objects.get(email=target_owner_email)
        self.target_project = models.Project.objects.get(
            name=target_project_name, owner__email=target_owner_email
        )
        self.target_contacts = []
        self.target_chats = []
        self.target_messages = []
        self.target_message_tag_groups = []
        self.target_message_tag_definitions = []
        self.target_tagged_messages = []
        self.target_datasets = []
        self.target_ml_models = []
        self.target_ml_weights = []

    def extract_source_records(self):
        self.source_owner = models.UserAccount.objects.get(
            email=self.SOURCE_OWNER_EMAIL
        )

        self.source_project = models.Project.objects.get(
            name=self.SOURCE_PROJECT_NAME, owner=self.source_owner
        )

        self.source_contacts = models.Contact.objects.filter(
            project=self.source_project
        )

        chats = []
        for rec in self.source_contacts:
            contact_chats = models.Chat.objects.filter(contact_id=rec)
            for chat_rec in contact_chats:
                chats.append(chat_rec)
        self.source_chats = chats

        messages = []
        for chat_rec in self.source_chats:
            chat_messages = models.Message.objects.filter(chat=chat_rec)
            for msg_rec in chat_messages:
                messages.append(msg_rec)
        self.source_messages = messages

        tagged_messages = []
        for msg_rec in self.source_messages:
            tagged_messages_for_msg = models.TaggedMessage.objects.get(message=msg_rec)
            tagged_messages.append(tagged_messages_for_msg)
        self.source_tagged_messages = tagged_messages

        self.source_message_tag_groups = models.MessageTagGroup.objects.filter(
            project=self.source_project
        )

        self.source_datasets = models.Dataset.objects.filter(
            project=self.source_project
        )

        self.source_ml_models = models.MLModel.objects.filter(
            project=self.source_project
        )

        self.source_message_tag_definitions = models.MessageTagDefinition.objects.none()
        for tag_group in self.source_message_tag_groups:
            tag_definitions = models.MessageTagDefinition.objects.filter(
                project=self.source_project,
                message_tag_group=tag_group,
            )
            self.source_message_tag_definitions |= tag_definitions

        ml_weights_records = models.MLWeights.objects.none()
        for rec in self.source_message_tag_definitions:
            for ml_model in self.source_ml_models:
                tag_ml_weights = models.MLWeights.objects.filter(
                    tag_definition=rec, ml_model=ml_model
                )
                ml_weights_records |= tag_ml_weights
        self.source_ml_weights = ml_weights_records

    def copy_all_records(self):
        self.copy_contacts()
        self.copy_chats()
        self.copy_messages()
        self.copy_message_tag_groups()
        self.copy_message_tag_definitions()
        self.copy_tagged_messages()
        self.copy_datasets()
        self.copy_ml_models()
        self.copy_ml_weights()

        target_records = {
            "contacts": self.target_contacts,
            "chats": self.target_chats,
            "messages": self.target_messages,
            "message_tag_groups": self.target_message_tag_groups,
            "message_tag_definitions": self.target_message_tag_definitions,
            "datasets": self.target_datasets,
            "ml_models": self.target_ml_models,
            "ml_weights": self.target_ml_weights,
        }
        serialized_records = self.serialize_target_records(target_records)

        return serialized_records

    def serialize_target_records(self, records):
        records["contacts"] = serializers.ContactSerializer(
            records["contacts"], many=True
        ).data
        records["chats"] = serializers.ChatSerializer(records["chats"], many=True).data
        records["messages"] = serializers.MessageSerializer(
            records["messages"], many=True
        ).data
        records["message_tag_groups"] = serializers.MessageTagGroupSerializer(
            records["message_tag_groups"], many=True
        ).data
        records["datasets"] = serializers.DatasetSerializer(
            records["datasets"], many=True
        ).data
        records["ml_models"] = serializers.MLModelSerializer(
            records["ml_models"], many=True
        ).data
        records["ml_weights"] = serializers.MLWeightsSerializer(
            records["ml_weights"], many=True
        ).data

        return records

    def copy_contacts(self):
        contacts_to_create = []
        for contact in self.source_contacts:
            origin = contact.origin
            target_contact = models.Contact(origin=origin, project=self.target_project)
            contacts_to_create.append(target_contact)

        self.target_contacts = models.Contact.objects.bulk_create(contacts_to_create)

        return self.target_contacts

    def copy_chats(self):
        chats_to_create = []
        for chat in self.source_chats:
            source_contact_origin = chat.contact_id.origin
            target_contact = models.Contact.objects.get(
                project=self.target_project, origin=source_contact_origin
            )
            create_params = dict(
                title=chat.title,
                is_default=chat.is_default,
                origin_id=chat.origin_id,
                contact_id=target_contact,
            )
            target_chat = models.Chat(**create_params)
            chats_to_create.append(target_chat)

        self.target_chats = models.Chat.objects.bulk_create(chats_to_create)

        return self.target_chats

    def copy_messages(self):
        messages_to_create = []
        for msg in self.source_messages:
            target_message_params = self.fill_target_message_params(msg)
            target_message = models.Message(*target_message_params)
            messages_to_create.append(target_message)

        self.target_messages = models.Message.objects.bulk_create(messages_to_create)

        return self.target_messages

    def copy_message_tag_groups(self):
        tag_groups_to_create = []
        for tag_group in self.source_message_tag_groups:
            name = tag_group.name
            target_tag_group = models.MessageTagGroup(
                name=name, project=self.target_project
            )
            tag_groups_to_create.append(target_tag_group)

        self.copied_tag_groups = models.MessageTagGroup.objects.bulk_create(
            tag_groups_to_create
        )

        return self.copied_tag_groups

    def copy_message_tag_definitions(self):
        tag_definitions_to_create = []
        for tag_definition in self.source_message_tag_definitions:
            target_tag_definition_params = self.fill_target_tag_definition_params(
                tag_definition
            )
            target_tag_definition = models.MessageTagDefinition(
                **target_tag_definition_params
            )
            tag_definitions_to_create.append(target_tag_definition)

        self.copied_tag_definitions = models.MessageTagDefinition.objects.bulk_create(
            tag_definitions_to_create
        )

        return self.copied_tag_definitions

    def copy_tagged_messages(self):
        tagged_messages_to_create = []
        for tagged_msg in self.source_tagged_messages:
            source_tag_definitions = tagged_msg.tag_definitions.all()
            source_msg_id = tagged_msg.message.id
            source_msg = models.Message.objects.get(id=source_msg_id)
            target_msg_params = self.fill_target_message_params(source_msg)
            target_msg = models.Message.get(**target_msg_params)
            target_tagged_message = models.TaggedMessage(message=target_msg)
            for tag_definition in source_tag_definitions:
                target_tag_definition_params = self.fill_copied_tag_definition(
                    tag_definition
                )
                target_tag_definition = models.MessageTagDefinition.get(
                    **target_tag_definition_params
                )
                target_tagged_message.tag_definitions.add(target_tag_definition)

            tagged_messages_to_create.append(target_tagged_message)

        self.copied_tagged_messages = models.TaggedMessage.objects.bulk_create(
            tagged_messages_to_create
        )

        return self.copied_tagged_messages

    def fill_target_message_params(self, msg):
        direction = msg.direction
        sender_type = msg.sender_type
        channel_type = msg.channel_type
        text = msg.text
        embedding = msg.embedding
        source_contact_origin = msg.contact.origin
        target_contact = models.Contact.objects.get(
            origin=source_contact_origin, project=self.target_project
        )
        source_chat_origin_id = msg.chat.origin_id
        target_chat = models.Chat.objects.get(
            contact_id=target_contact, origin_id=source_chat_origin_id
        )
        target_message_params = dict(
            direction=direction,
            sender_type=sender_type,
            channel_type=channel_type,
            text=text,
            embedding=embedding,
            project=self.target_project,
            contact=target_contact,
            chat=target_chat,
        )

        return target_message_params

    def fill_target_tag_definition_params(self, tag_definition):
        name = tag_definition.name
        description = tag_definition.description
        target_group_name = tag_definition.message_tag_group.name
        target_tag_group = models.MessageTagGroup.objects.get(
            name=target_group_name, project=self.target_project
        )

        target_tag_definition_params = dict(
            name=name,
            description=description,
            project=self.target_project,
            message_tag_group=target_tag_group,
        )

        return target_tag_definition_params

    def copy_datasets(self):
        datasets_to_create = []
        for dataset in self.source_datasets:
            name = dataset.name
            description = dataset.description
            target_dataset = models.Dataset(
                name=name, description=description, project=self.target_project
            )
            source_tagged_messages = dataset.tagged_messages.all()
            for tagged_msg in source_tagged_messages:
                msg = tagged_msg.message
                target_msg_params = self.fill_target_message_params(msg)
                target_msg = models.Message.objects.get(**target_msg_params)
                target_tagged_msg = models.TaggedMessage.objects.get(message=target_msg)
                target_dataset.tagged_messages.add(target_tagged_msg)

            datasets_to_create.append(target_dataset)

        self.target_datasets = models.Dataset.objects.bulk_create(datasets_to_create)

        return self.target_datasets

    def copy_ml_models(self):
        ml_models_to_create = []
        for ml_model in self.source_ml_models:
            name = ml_model.name
            description = ml_model.description
            dataset_name = ml_model.dataset.name
            dataset_description = ml_model.dataset.description
            target_dataset = models.Dataset.objects.get(
                name=dataset_name,
                description=dataset_description,
                project=self.target_project,
            )
            target_ml_model = models.MLModel(
                name=name,
                description=description,
                project=self.target_project,
                dataset=target_dataset,
                created_by=self.target_owner,
            )
            ml_models_to_create.append(target_ml_model)

        self.target_ml_models = models.MLModel.objects.bulk_create(ml_models_to_create)

        return self.target_ml_models

    def copy_ml_weights(self):
        ml_weights_to_create = []
        for ml_weights in self.source_ml_weights:
            coef = ml_weights.coef
            intercept = ml_weights.intercept
            tag_definition_name = ml_weights.tag_definition.name
            tag_definition_tag_group_name = (
                ml_weights.tag_definition.message_tag_group.name
            )
            target_tag_definition = models.MessageTagDefinition.objects.get(
                name=tag_definition_name,
                message_tag_group__name=tag_definition_tag_group_name,
                project=self.target_project,
            )
            ml_model_name = ml_weights.ml_model.name
            target_ml_model = models.MLModel.objects.get(
                name=ml_model_name, project=self.target_project
            )
            target_ml_weights = models.MLWeights(
                coef=coef,
                intercept=intercept,
                tag_definition=target_tag_definition,
                ml_model=target_ml_model,
            )

            ml_weights_to_create.append(target_ml_weights)

        self.target_ml_weights = models.MLWeights.objects.bulk_create(
            ml_weights_to_create
        )

        return self.target_ml_weights
