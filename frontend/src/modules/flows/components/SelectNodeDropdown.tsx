import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { HiPlusCircle } from "react-icons/hi2";
import { useState } from "react";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";

type Props = {
  onNodeAdded: (nodeType: string) => void;
};

function SelectNodeDropdown({ onNodeAdded }: Props) {
  const [isNewtagDialogOpen, setIsNewTagDialogOpen] = useState(false);

  return (
    <DropdownMenu
      open={isNewtagDialogOpen}
      onOpenChange={setIsNewTagDialogOpen}
    >
      <DropdownMenuTrigger asChild>
        {/* necessary to wrap icon with button tag to make it a trigger */}
        <button>
          <HiPlusCircle className="rounded-full text-5xl text-primary" />
        </button>
      </DropdownMenuTrigger>
      <DropdownMenuContent className="w-56">
        <DropdownMenuLabel className="text-center">
          Add New Node
        </DropdownMenuLabel>
        <DropdownMenuSeparator />
        <DropdownMenuItem className="items-center justify-center">
          <Button
            variant="ghost"
            className="w-full h-full"
            onClick={() => onNodeAdded(nodeNameToTypeMapping.PROMPT_NODE)}
          >
            Message
          </Button>
        </DropdownMenuItem>
        <DropdownMenuItem className="items-center justify-center">
          <Button
            variant="ghost"
            className="w-full h-full"
            onClick={() => onNodeAdded(nodeNameToTypeMapping.CONDITION_BLOCK)}
          >
            Condition
          </Button>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export { SelectNodeDropdown };
