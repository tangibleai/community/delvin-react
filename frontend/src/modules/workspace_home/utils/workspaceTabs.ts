export const enum WorkspaceTab {
  PROJECTS = "projects",
  SETTINGS = "settings",
  TEAM = "team",
  LLM_PROVIDERS = "llm_providers",
}
