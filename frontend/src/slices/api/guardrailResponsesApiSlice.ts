import emptyApi from "../apiSlice";
import {
  GuardrailResponse,
  NewGuardrailResponse,
} from "@/typings/guardrail_response";
import { GuardrailAction } from "@/typings/guardrail_action";

const guardrailResponsesApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    createGuardrailResponse: builder.mutation<
      GuardrailResponse,
      NewGuardrailResponse
    >({
      query: (newResponse: NewGuardrailResponse) => ({
        url: "guardrail-responses/create/",
        method: "POST",
        data: newResponse,
      }),
      invalidatesTags: [
        "GuardrailResponse",
        "MessageTagDefinition",
        "MessageTag",
      ],
    }),

    updateGuardrailAction: builder.mutation<GuardrailAction, GuardrailAction>({
      query: (updatedAction: GuardrailAction) => ({
        url: `guardrail-actions/${updatedAction.id!}/update/`,
        method: "PUT",
        data: updatedAction,
      }),
      invalidatesTags: [
        "GuardrailResponse",
        "MessageTagDefinition",
        "MessageTag",
      ],
    }),

    deleteGuardrailResponse: builder.mutation<null, string>({
      query: (guardrailResponseId: string) => ({
        url: `guardrail-responses/${guardrailResponseId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: [
        "GuardrailResponse",
        "MessageTagDefinition",
        "MessageTag",
      ],
    }),

    listGuardrailResponses: builder.query<GuardrailResponse[], string>({
      query: (projectId: string) => ({
        url: "guardrail-responses/",
        params: { project_id: projectId },
      }),
      providesTags: ["GuardrailResponse"],
    }),
  }),
});

export const {
  useCreateGuardrailResponseMutation,
  useUpdateGuardrailActionMutation,
  useDeleteGuardrailResponseMutation,
  useListGuardrailResponsesQuery,
} = guardrailResponsesApiSlice;
