import error from "@/assets/browser.png";

const UserHasNoAccessToWorkspace = () => {
  return (
    <div className="flex flex-col justify-center items-center min-h-screen bg-white">
      <img src={error} alt="error" className="w-32" />
      <div className="mt-5 text-xl text-gray-600">
        You do not have access to this page. Ask the owner of the workspace for
        an invite.
      </div>
    </div>
  );
};

export default UserHasNoAccessToWorkspace;
