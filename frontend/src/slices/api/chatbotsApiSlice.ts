import { ChatbotConfig } from "@/typings/chatbot_config";
import emptyApi from "../apiSlice";

interface CreateChatbotRequestBody {
  project_id: string;
  llm_provider_id: string;
  model_id: string;
  config: object;
  origin: string;
}

export const chatbotsApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    getChatbot: builder.query<ChatbotConfig, string>({
      query: (projectId: string) => ({
        url: `chatbot/${projectId}/details/`,
        method: "GET",
      }),
      providesTags: (result, error, arg) => [{ type: "Chatbot", id: arg }],
    }),

    listChatbots: builder.query<
      ChatbotConfig[],
      { project_id?: string; provider_id?: string }
    >({
      query: ({ project_id, provider_id }) => {
        const params: Record<string, string> = {};
        if (project_id) params.project_id = project_id;
        if (provider_id) params.provider_id = provider_id;

        return {
          url: "chatbots/",
          method: "GET",
          params,
        };
      },
      providesTags: (result = [], error, arg) => [
        "Chatbot",
        ...result.map(({ id }) => ({ type: "Chatbot", id }) as const),
      ],
    }),
    createChatbot: builder.mutation<ChatbotConfig, CreateChatbotRequestBody>({
      query: (requestBody) => ({
        url: "chatbots/create/",
        method: "POST",
        data: requestBody,
      }),
      invalidatesTags: ["Chatbot"],
    }),
    updateChatbot: builder.mutation<ChatbotConfig[], ChatbotConfig>({
      query: (chatbot) => ({
        url: `chatbots/${chatbot.id}/update/`,
        method: "PUT",
        data: chatbot,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: "Chatbot", id: arg.id },
      ],
    }),
    deleteChatbot: builder.mutation<null, string>({
      query: (chatbotId: string) => ({
        url: `chatbots/${chatbotId}/delete/`,
        method: "DELETE",
      }),
      invalidatesTags: ["Chatbot"],
    }),
  }),
});

export const {
  useGetChatbotQuery,
  useListChatbotsQuery,
  useCreateChatbotMutation,
  useUpdateChatbotMutation,
  useDeleteChatbotMutation,
} = chatbotsApiSlice;
