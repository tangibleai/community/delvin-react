import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";
import * as Sentry from "@sentry/react";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const focusInput = [
  // base
  "focus:ring-2",
  // ring color
  "focus:ring-indigo-200 focus:dark:ring-indigo-700/30",
  // border color
  "focus:border-indigo-500 focus:dark:border-indigo-700",
];

// Tremor Raw focusRing [v0.0.1]

export const focusRing = [
  // base
  "outline outline-offset-2 outline-0 focus-visible:outline-2",
  // outline color
  "outline-indigo-500 dark:outline-indigo-500",
];

export function processError(errorMessage: string) {
  const debugMode = import.meta.env.VITE_DEBUG;

  if (["False", "false", false].includes(debugMode)) {
    Sentry.captureException(errorMessage);
  } else {
    console.log(errorMessage);
  }

  return errorMessage;
}

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
