# Delvin

Delvin is an open source platform to build safe, reliable AI conversational agents.


## Installation

### Simplified local installation from source for fullstack development - frontend and Django backend only

In the `backend` folder, install the backend in a _virtual environment_ using the latest version of `pip` and the python `virtualenv` package:

```bash
pip install --upgrade pip virtualenv
python -m virtualenv .venv
source .venv/bin/activate
pip install -r backend/requirements.txt
```

Initialize the .env file in the main folder with necessary database connections.

Run migrations to initialize your database:

```bash
python manage.py migrate
```

Create a superuser:

```bash
python -m scripts.create_super_user
```

Generata OpenAPI schema

```bash
python manage.py spectacular --file schema.yml
```

Run the Django app:
```bash
python manage.py runserver
```

Check the API documentation on the URL:
`http://localhost/docs/`

In the `frontend` folder, install the dependencies with npm (this assumes you have Node.js `>=18.6.0` and `npm` installed): 
```bash
npm install
```

Run the frontend on the default port 3000:
```bash
npm run dev
```


## Contributing
Delvin welcomes contributions! Link to contributor guide will appear here. 
