from rest_framework import serializers

from api.models import Dataset


class DatasetSerializer(serializers.ModelSerializer):
    description = serializers.CharField(required=False)

    class Meta:
        model = Dataset
        fields = "__all__"
