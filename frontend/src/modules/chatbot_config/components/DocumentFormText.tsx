import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";

import * as z from "zod";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { zodResolver } from "@hookform/resolvers/zod";

import { ChatbotConfig } from "@/typings/chatbot_config";
import { Document } from "@/typings/document";
import ErrorScreen from "@/components/ErrorScreen";
import { IoReload } from "react-icons/io5";
import {
  useCreateDocumentMutation,
  useUpdateDocumentMutation,
} from "@/slices/api/documentSlice";
import { useToast } from "@/components/ui/use-toast";

type Props = {
  document: Document;
  selectedChatbot: ChatbotConfig;
  onSubmit: (newDocument: Document) => void;
  onCancel: any;
  submit_action: string;
};

function DocumentFormText({
  document,
  selectedChatbot,
  onSubmit,
  onCancel,
  submit_action,
}: Props) {
  const { toast } = useToast();

  const [error, setError] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [processingDocument, setProcessingDocument] = useState(false);

  const [createDocument] = useCreateDocumentMutation();

  const [updateDocument] = useUpdateDocumentMutation();

  // Form Text
  const modal_text =
    submit_action == "add" ? "Add document" : "Update document";

  const routeParams = useParams();
  const projectId = routeParams.projectId;

  const formSchema = z.object({
    title: z.string().min(1, {
      message: "Please enter a title",
    }),
    content: z.string().min(4, {
      message: "Please add information your bot should know",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      title: document.title,
      content: document.content,
    },
  });

  useEffect(() => {
    // Handles resetting the form after submit or x
    // Can't directly access x in Dialog
    if (!isModalOpen) {
      form.reset();
    }
  }, [isModalOpen]);

  async function handleSubmit(values: z.infer<typeof formSchema>) {
    const { title, content } = values;
    let result: any = {};

    const documentContent = { title: title, content: content };

    if (submit_action == "add") {
      let body = {
        project: projectId,
        chatbot: selectedChatbot.id,
        title: documentContent.title,
        content: documentContent.content,
        document_type: "text",
      };

      try {
        result = await createDocument(body).unwrap();
      } catch {
        return <ErrorScreen />;
      }
    } else {
      let body = {
        project: projectId,
        chatbot: selectedChatbot.id,
        title: documentContent.title,
        content: documentContent.content,
        document_type: "text",
        document: document.id,
      };
      try {
        result = await updateDocument(body).unwrap();
      } catch {
        return <ErrorScreen />;
      }
    }

    if ("error" in result) {
      setError(result.error);
      setProcessingDocument(false);
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: `Couldn't ${submit_action} document`,
      });
      return;
    }
    onSubmit(result);
    setError(null);
    setIsModalOpen(false);
    setProcessingDocument(false);

    toast({
      title: "Success!",
      description: `Successfully ${
        submit_action === "add" ? "added" : "updated"
      } document`,
    });
  }

  return (
    <>
      {error && (
        <p className="bg-red-100 border border-red-400 text-red-800 px-4 py-2 rounded">
          Error: {error}
        </p>
      )}
      <Form {...form}>
        <form onSubmit={form.handleSubmit(handleSubmit)} className="space-y-8">
          <FormField
            control={form.control}
            name="title"
            defaultValue={document.title}
            render={({ field }) => (
              <FormItem>
                <FormLabel>Document name</FormLabel>
                <FormControl>
                  <Input placeholder="A title for your document" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="content"
            defaultValue={document.content}
            render={({ field }) => (
              <FormItem>
                <FormLabel>Content</FormLabel>
                <FormControl>
                  <Textarea
                    placeholder="The information your bot should know"
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          {processingDocument ? (
            <Button disabled className="order-2 sm:order-1">
              <IoReload className="mr-2 h-4 w-4 animate-spin" />
              {modal_text}
            </Button>
          ) : (
            <>
              <Button type="submit" className="order-2 sm:order-1">
                {modal_text}
              </Button>
              <Button
                type="button"
                variant="outline"
                className="order-1 sm:order-2"
                onClick={onCancel}
              >
                Cancel
              </Button>
            </>
          )}
        </form>
      </Form>
    </>
  );
}

export default DocumentFormText;
