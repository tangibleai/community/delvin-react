from __future__ import absolute_import, unicode_literals

import logging
import math

import requests
from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail
from django.utils import timezone

from api.models import (
    Chatbot,
    Document,
    GenerativeModel,
    Message,
    MessageTag,
    MessageTagRule,
    Plan,
    Project,
    Provider,
    ProviderChoices,
    ResponseLog,
)
from api.services.rag.chat_knowledge_manager import (
    KnowledgeDocumentIndexManager,
    KnowledgeDocumentProcessor,
)
from core.constant import CONSTANTS

logger = logging.getLogger(__name__)


# logger.setLevel(logging.DEBUG)


@shared_task
def send_email_task(subject, message, recipient_list):
    send_mail(
        subject,
        message,
        settings.DEFAULT_FROM_EMAIL,
        recipient_list,
        fail_silently=False,
    )


@shared_task
def sample_task():
    return "Task has been finished!"


@shared_task
def sync_task():
    """Call the sync function here and define periodic task from the admin panel."""
    return "Sync task has been finished!"


@shared_task()
def generate_message_embeddings_task(message_id, message_text):
    """Create an embedding for the given message and save it."""
    url = settings.ENV["SENTENCE_EMBEDDINGS_URL"]
    data = {"data": {"ndarray": [message_text]}}
    r = requests.post(url=url, json=data)
    embeddings = r.json()["data"]["ndarray"]
    message = Message.objects.get(id=message_id)
    message.embedding = embeddings[0]
    message.save()
    return "Message embedding has been created!"


@shared_task()
def apply_message_rules_task(project_id, message_id):
    """After receiving a message check for the message tag rules and apply"""
    message = Message.objects.get(id=message_id)
    project = Project.objects.get(id=project_id)
    inbound_contains_rules = MessageTagRule.objects.filter(
        project=project_id,
        direction__in=[MessageTagRule.Direction.INBOUND, MessageTagRule.Direction.ANY],
        rule_type=MessageTagRule.RuleType.CONTAINS,
    )
    outbound_contains_rules = MessageTagRule.objects.filter(
        project=project_id,
        direction__in=[MessageTagRule.Direction.OUTBOUND, MessageTagRule.Direction.ANY],
        rule_type=MessageTagRule.RuleType.CONTAINS,
    )
    return_value = False
    for message_tag_rule in inbound_contains_rules:
        if (
            message.sender_type == Message.SenderType.BOT
            and message.direction == Message.Direction.INBOUND
            and message_tag_rule.rule_content.get("contains").lower()
            in message.text.lower()
        ):
            MessageTag.objects.create(
                message=message, tag=message_tag_rule.message_tag_definition
            )
            return_value = "inbound"
    for message_tag_rule in outbound_contains_rules:
        if (
            message.sender_type == Message.SenderType.BOT
            and message.direction == Message.Direction.OUTBOUND
            and message_tag_rule.rule_content.get("contains").lower()
            in message.text.lower()
        ):
            MessageTag.objects.create(
                message=message, tag=message_tag_rule.message_tag_definition
            )
            return_value = True
    return return_value


@shared_task(bind=True)
def update_document_metadata(self, project, chatbot_id, document_data, db_action):
    """Handles interactions necessary to keep the Document and metadata/embeddings in data_chatbot_knowledge in sync"""
    task = self

    document = Document.objects.filter(id=document_data["id"]).first()

    chatbot_knowledge_manager = KnowledgeDocumentIndexManager(
        project, chatbot_id, document_data
    )
    process_knowledge_document = KnowledgeDocumentProcessor(
        project, chatbot_id, document_data
    )

    task.update_state(
        state="PROGRESS",
        meta={
            "current": 1,
            "total": 2,
        },
    )
    try:
        if db_action == "create":
            doc_nodes = (
                process_knowledge_document.convert_single_form_document_to_llamaindex_document()
            )
            chatbot_knowledge_manager.create_or_update_vector_store(doc_nodes)
            chatbot_knowledge_manager.set_docstore(doc_nodes)
            document.status = Document.DocumentStatus.PROCESSED
            document.save()
        elif db_action == "update":
            doc_nodes = (
                process_knowledge_document.convert_single_form_document_to_llamaindex_document()
            )
            chatbot_knowledge_manager.create_or_update_vector_store(
                doc_nodes, db_action="update"
            )
            chatbot_knowledge_manager.set_docstore(doc_nodes)
            document.status = Document.DocumentStatus.PROCESSED
            document.save()
        elif db_action == "delete":
            chatbot_knowledge_manager.delete_docstore_record()
            chatbot_knowledge_manager.delete_chat_knowledge_document()
        else:
            document.status = Document.DocumentStatus.ERROR
    except Exception as e:
        document.status = Document.DocumentStatus.ERROR

    task.update_state(
        state="SUCCESS",
        meta={
            "current": 2,
            "total": 2,
        },
    )


@shared_task()
def delete_project_document_metadata(project_id):
    """Deletes all a project's metadata from the data_chatbot_knowledge table"""
    project = Project.objects.filter(id=project_id)
    chatbot = Chatbot.objects.filter(project=project).first()
    chatbot_knowledge_manager = KnowledgeDocumentIndexManager(project_id, chatbot.id)
    chatbot_knowledge_manager.delete_all_project_document_metadata()


@shared_task(bind=True)
def extract_document_file_metadata(self, project, chatbot_id, file_name, document_data):
    """Extracts metadata from a downloaded PDF document"""
    chatbot_knowledge_manager = KnowledgeDocumentIndexManager(
        project, chatbot_id, document_data
    )
    process_knowledge_document = KnowledgeDocumentProcessor(
        project, chatbot_id, document_data
    )
    task = self

    document = Document.objects.filter(id=document_data["id"]).first()
    task.update_state(
        state="PROGRESS",
        meta={
            "current": 1,
            "total": 3,
        },
    )
    doc_nodes = (
        process_knowledge_document.convert_single_file_document_to_llamaindex_document(
            project, document_data
        )
    )
    chatbot_knowledge_manager.create_or_update_vector_store(doc_nodes)
    task.update_state(
        state="PROGRESS",
        meta={
            "current": 2,
            "total": 3,
        },
    )
    chatbot_knowledge_manager.set_docstore(doc_nodes)
    document.status = Document.DocumentStatus.PROCESSED
    document.save()
    task.update_state(
        state="SUCCESS",
        meta={
            "current": 3,
            "total": 3,
        },
    )


@shared_task()
def delete_document_file_and_metadata(project_id, chatbot_id, document_data):
    chatbot_knowledge_manager = KnowledgeDocumentIndexManager(
        project_id, chatbot_id, document_data
    )
    chatbot_knowledge_manager.delete_docstore_record()
    chatbot_knowledge_manager.delete_chat_knowledge_document()


@shared_task()
def extract_google_doc_metadata(project, chatbot_id, document_data, doc_url):
    document = Document.objects.filter(id=document_data["id"]).first()
    process_knowledge_document = KnowledgeDocumentProcessor(
        project, chatbot_id, document_data
    )
    doc_nodes = process_knowledge_document.convert_single_google_document_to_llamaindex_document(
        doc_url
    )

    chatbot_knowledge_manager = KnowledgeDocumentIndexManager(
        project, chatbot_id, document_data
    )
    chatbot_knowledge_manager.create_or_update_vector_store(doc_nodes)
    chatbot_knowledge_manager.set_docstore(doc_nodes)

    document.status = Document.DocumentStatus.PROCESSED
    document.save()


@shared_task()
def update_google_doc_metadata(project, chatbot_id, document_data, doc_url):
    document = Document.objects.filter(id=document_data["id"]).first()
    chatbot_knowledge_manager = KnowledgeDocumentIndexManager(
        project, chatbot_id, document_data
    )
    chatbot_knowledge_manager.delete_chat_knowledge_document()

    process_knowledge_document = KnowledgeDocumentProcessor(
        project, chatbot_id, document_data
    )
    doc_nodes = process_knowledge_document.convert_single_google_document_to_llamaindex_document(
        doc_url
    )

    chatbot_knowledge_manager.create_or_update_vector_store(doc_nodes, "update")
    chatbot_knowledge_manager.set_docstore(doc_nodes)

    document.status = Document.DocumentStatus.PROCESSED
    document.save()


@shared_task()
def transition_old_bot_type_to_new_bot_type(project_id, config):
    try:
        Chatbot.objects.filter(project__id=project_id).update(config=config)
        logger.info(f"Updated chatbot configurations for project {project_id}")
    except Exception as e:
        logger.error(
            f"Failed to update chatbot configurations for project {project_id}: {e}"
        )


@shared_task
def evaluate_plan_type_and_properties(project_id, chatbot_uses_default_llm_provider):
    """
    Decrements the free_message count for the project's plan and updates
    the plan status if necessary.

    Args:
        project_id (String): The project id.
    """

    try:
        # Fetch project and plan in a single query
        project = Project.objects.select_related("workspace__plan").get(pk=project_id)
        plan = project.workspace.plan
        owner_email = project.workspace.owner.email

        # Update plan status and create a celery task to update model

        if plan.type == Plan.Type.FREE and chatbot_uses_default_llm_provider:

            if plan.properties["free_message"] < CONSTANTS.MAX_FREE_MESSAGES:
                # Update free_messages directly on the plan object
                plan.properties["free_message"] += 1
                plan.save(update_fields=["properties"])

            if (
                plan.properties["free_message"] >= CONSTANTS.MAX_FREE_MESSAGES
                and plan.status != Plan.Status.EXPIRED
            ):
                changed = change_workspace_models_to_llama.delay(project_id)
                if not changed:
                    send_email_task.delay(
                        subject="OpenRouter provider not found workspace.",
                        message=f"We couldn't switch your chatbots models in {project.workspace.name} workspace since ypu didn't add this provider type.\n",
                        recipient_list=[owner_email],
                    )
                plan.status = Plan.Status.EXPIRED
                plan.save(update_fields=["status"])

            if plan.properties["free_message"] == math.floor(
                CONSTANTS.MAX_FREE_MESSAGES * CONSTANTS.WARNING_RATIO
            ):
                send_email_task.delay(
                    subject="Your workspace is running out of credits.",
                    message="Your workspace's Free plan has fewer than 20% of the free message quota left.\n"
                    "Contact us to upgrade your plan and get access to all the models, unlimited messages, and more.",
                    recipient_list=[owner_email],
                )

            if plan.properties["free_message"] == CONSTANTS.MAX_FREE_MESSAGES:
                send_email_task.delay(
                    subject="Your workspace is out of Credits",
                    message="Your projects can now only use free generative models on limited availability bases and API usage has been disabled.\n"
                    "Contact us to upgrade your plan and get access to all the models, unlimited messages, and more!",
                    recipient_list=[owner_email],
                )

        # Update plan status for paid plans
        if plan.type == Plan.Type.PAID:
            if not plan.renewal_date or plan.renewal_date < timezone.now():
                plan.status = Plan.Status.EXPIRED
                plan.save(update_fields=["status"])

        return True

    except Project.DoesNotExist:
        logger.warning(f"Project with ID {project_id} not found")
        return False


@shared_task
def create_response_logs(
    user_message_id, response_id, source, ai_usage, metadata, context
):
    """
    Creates logs for incoming and outgoing messages
    """

    # TODO ai usage and metadata should be finished later.
    response_log = ResponseLog(
        user_message=Message.objects.get(pk=user_message_id),
        response=Message.objects.get(pk=response_id),
        source=source,
        ai_usage=ai_usage,
        metadata=metadata,
        context=context,
    )
    response_log.save()
    return True


@shared_task
def change_workspace_models_to_llama(project_id):
    """
    Updates all chatbot models in the workspace
    """
    try:
        # Find the workspace associated with the project
        project = Project.objects.select_related("workspace").get(id=project_id)
        workspace = project.workspace
    except Project.DoesNotExist:
        logger.warning(f"Project with ID {project_id} does not exist.")
        # Handle the missing project case here (e.g., return, raise an error, etc.)
        return False

    # Find all projects within the same workspace
    projects_in_workspace = Project.objects.filter(workspace=workspace)

    workspace_chatbots_with_openrouter_provider = Chatbot.objects.filter(
        project__id__in=[p.id for p in projects_in_workspace]
    )
    openrouter_provider = Provider.objects.filter(
        workspace=workspace, type=ProviderChoices.OPENROUTER.value
    ).first()
    lama_generative_model = GenerativeModel.objects.get(
        model_string="openrouter/meta-llama/llama-3-70b-instruct",
        provider_type=ProviderChoices.OPENROUTER.value,
    )
    workspace_chatbots_with_openrouter_provider.update(
        provider=openrouter_provider, generative_model=lama_generative_model
    )

    return bool(openrouter_provider)
