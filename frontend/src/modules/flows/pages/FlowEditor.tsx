import { useEffect, useRef, useState } from "react";
import { RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import ReactFlow, {
  Node,
  Edge,
  addEdge,
  OnConnect,
  OnConnectStart,
  OnConnectEnd,
  OnNodesDelete,
  Panel,
  Controls,
  useNodesState,
  useEdgesState,
} from "reactflow";
import "reactflow/dist/style.css";
import {
  setActiveNode,
  setConnectionInProgress,
  setConnectionSourceNodeId,
  setErroneousNodes,
  setNodeSidebar,
  closeNodeSidebar,
} from "@/slices/flowSlice";
import { DeleteNodeDialog } from "../components/DeleteNodeDialog";
import { ReactFlowProvider } from "reactflow";
import { SelectNodeDropdown } from "../components/SelectNodeDropdown";
import { checkFlowErrors } from "../utils/flow_validation";
import PromptNodeSidebar from "../components/PromptNodeSidebar";
import { useToast } from "@/components/ui/use-toast";
import { HiOutlineEye } from "react-icons/hi";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import { ExclamationTriangleIcon } from "@heroicons/react/20/solid";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";
import { nodeTypes } from "../typing/node_types";
import { useParams } from "react-router-dom";
import ConditionBlockSidebar from "../components/ConditionBlockSidebar";
import { initNewNode } from "../utils/init_new_node";
import { Button } from "@/components/ui/button";
import NodeSidebar from "../components/NodeSidebar";
import {
  useGetFlowQuery,
  useUpdateFlowMutation,
} from "@/slices/api/flowApiSlice";
import { Flow } from "@/typings/flow";
import { IoReload } from "react-icons/io5";

const FlowEditor = () => {
  const dispatch = useDispatch();
  const { toast } = useToast();

  //holds a previous edge between two nodes while user reconnects from source node
  const [edge, setEdge] = useState<Edge>();

  const [previewError, setPreviewError] = useState("");

  const routeParams = useParams();
  const flowId = routeParams.flowId!;

  const flowName: string = useSelector(
    (state: RootState) => state.flow.flowName ?? ""
  );

  const activeNode = useSelector((state: RootState) => state.flow.activeNode);
  const isNodeSidebarOpen = useSelector(
    (state: RootState) => state.flow.nodeSidebar.opened
  );

  const [nodes, setNodes, onNodesChange] = useNodesState([]);
  const [edges, setEdges, onEdgesChange] = useEdgesState([]);

  const reactFlowWrapper = useRef(null);
  // Used as a context between `onConnect` and `onConnectEnd`. Can't be a component variable since react doesn't update it in time before `onConnectEnd` gets called right after `onConnect`
  const edgeConnectedRef = useRef(false);

  const { data: flow = {} as Flow } = useGetFlowQuery(flowId, {
    skip: !flowId,
  });
  const [updateFlow, { isLoading: updatingFlow }] = useUpdateFlowMutation();

  useEffect(() => {
    dispatch(setErroneousNodes([]));
    fetchAndSetFlowDetails();
    dispatch(setNodeSidebar({ opened: false, nodeType: null }));
  }, []);

  const fetchAndSetFlowDetails = async () => {
    const nodes = flow?.ui?.nodes ?? [
      {
        id: "0",
        type: "startNode",
        data: {
          nodeId: "0",
          nodeName: "start",
          nodePrompt: "start node",
        },
        style: { width: 100, height: 100 },
        position: { x: 0, y: 0 },
      } as Node,
    ];
    const edges = flow?.ui?.edges ?? [];
    setNodes(nodes);
    setEdges(edges);
  };

  // handlers for flow events
  // =================================================================
  const onConnect: OnConnect = (connection) => {
    edgeConnectedRef.current = true;

    return setEdges((eds) => {
      const edgeStartingFromTargetNodeExists = eds.some(
        (edg) => edg.source == connection.source
      );
      if (edgeStartingFromTargetNodeExists) {
        toast({
          variant: "destructive",
          title: "Oops! Something went wrong",
          description: "Node can't point on more than 1 other nodes.",
        });
        return eds;
      }
      return addEdge(connection, eds);
    });
  };

  const onConnectStart: OnConnectStart = (event, params) => {
    dispatch(setConnectionInProgress(true));
    dispatch(setConnectionSourceNodeId(params.nodeId!));

    const sourceEdge = edges.find((edg) => edg.source == params.nodeId);
    if (!sourceEdge) return;

    setEdge(sourceEdge);
    setEdges((eds) => {
      return eds.filter(
        (ed) =>
          !(
            ed.source === sourceEdge!.source && ed.target === sourceEdge!.target
          )
      );
    });
  };

  const onConnectEnd: OnConnectEnd = (event) => {
    if (!edgeConnectedRef.current)
      setEdges((eds) => (edge ? eds.concat(edge) : eds));
    edgeConnectedRef.current = false;
    dispatch(setConnectionInProgress(false));
  };

  const onNodesDelete: OnNodesDelete = (deletedNodes) => {
    // handler for delete node via Backspace event

    const deletedStartNode = deletedNodes.find(
      (node) => node.data.nodeName === "start"
    );

    if (deletedStartNode) {
      // needs to be done with setTimeout to wait until React reacts to this change
      setTimeout(() => setNodes((nds) => [...nds, deletedStartNode]), 0);

      toast({
        variant: "destructive",
        title: "Start state can't be deleted!",
      });

      return;
    }

    const deletedNodesIds = deletedNodes.map((node) => node.id);

    const deletedSingleConditionBlocksWithoutParent = deletedNodes.filter(
      (node) => node?.parentId && !deletedNodesIds.includes(node?.parentId)
    );

    if (deletedSingleConditionBlocksWithoutParent.length) {
      // needs to be done with setTimeout to wait until React reacts to this change
      setTimeout(
        () =>
          setNodes((nds) =>
            nds.concat(deletedSingleConditionBlocksWithoutParent)
          ),
        0
      );

      toast({
        variant: "destructive",
        title:
          "Single condition blocks have to be deleted via right-hand sidebar",
      });

      return;
    }

    if (
      deletedNodes.some(
        (node) => node.type === nodeNameToTypeMapping.CONDITION_BLOCK
      )
    )
      dispatch(
        setNodeSidebar({
          opened: false,
          nodeType: nodeNameToTypeMapping.CONDITION_BLOCK,
        })
      );
  };

  const onNodeAdded = (nodeType: string) => {
    const newNode = initNewNode(nodes, nodeType);

    const updatedNodes = [
      ...nodes.map((node) => ({
        ...node,
        style: node.style
          ? { width: node.style.width, height: node.style.height }
          : {},
      })),
      newNode,
    ];
    setNodes(() => updatedNodes);

    dispatch(setActiveNode(newNode));
    if (newNode.type === nodeNameToTypeMapping.PROMPT_NODE)
      dispatch(
        setNodeSidebar({
          opened: true,
          nodeType: nodeNameToTypeMapping.PROMPT_NODE,
        })
      );
    else if (newNode.type === nodeNameToTypeMapping.CONDITION_BLOCK)
      dispatch(
        setNodeSidebar({
          opened: true,
          nodeType: nodeNameToTypeMapping.CONDITION_BLOCK,
        })
      );
  };

  const onSaveClick = async () => {
    const flowIsCorrect = validateFlow().erroneousNodes.length === 0;
    if (!flowIsCorrect) return;

    try {
      await updateFlow({ flowId, nodes, edges }).unwrap();
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Couldn't save flow. Please, try again.",
      });
      return;
    }

    toast({
      title: "Flow is saved!",
    });
  };

  const validateFlow = () => {
    const flowErrorData = checkFlowErrors(nodes, edges);
    const errorMessage = flowErrorData.errorMessage;
    const erroneousNodes = flowErrorData.erroneousNodes;
    setPreviewError(errorMessage);
    dispatch(setErroneousNodes(erroneousNodes));

    return flowErrorData;
  };

  const onPreviewButtonClicked = () => {
    const flowIsCorrect = validateFlow().erroneousNodes.length === 0;
    if (!flowIsCorrect) return;

    toast({
      title: "Everything is correct! You can save flow now.",
    });
  };

  const onNodeSidebarClose = () => {
    dispatch(closeNodeSidebar());
  };

  return (
    <div className="flex flex-col w-full h-full pt-5">
      <div className="flex items-center justify-between border-b pb-5 -mx-10 px-10">
        <div className="flex items-center gap-5">
          <p className="text-2xl">{flowName}</p>
          <HiOutlineEye
            className={`text-2xl ${previewError ? "text-destructive" : ""}`}
            onClick={onPreviewButtonClicked}
          />
        </div>

        <Button onClick={onSaveClick} disabled={updatingFlow} className="gap-1">
          {updatingFlow && (
            <IoReload className="h-[1.2rem] w-[1.2rem] animate-spin" />
          )}
          Save
        </Button>
      </div>

      {previewError && (
        <Alert variant="destructive">
          <ExclamationTriangleIcon className="h-4 w-4" />
          <AlertTitle>Error</AlertTitle>
          <AlertDescription>{previewError}</AlertDescription>
        </Alert>
      )}

      <ReactFlowProvider>
        <DeleteNodeDialog />
        <div className="flex h-screen border-collapse overflow-hidden -mx-10">
          <div
            ref={reactFlowWrapper}
            style={{ height: "100%", width: "100%" }}
            className="flex-1 overflow-y-auto overflow-x-hidden bg-secondary/10 pb-1"
          >
            <ReactFlow
              nodes={nodes}
              edges={edges}
              onNodesChange={onNodesChange}
              onEdgesChange={onEdgesChange}
              onConnect={onConnect}
              onConnectStart={onConnectStart}
              onConnectEnd={onConnectEnd}
              onNodesDelete={onNodesDelete}
              nodeTypes={nodeTypes}
              fitView
            >
              <Panel position={"top-left"}>
                <SelectNodeDropdown onNodeAdded={onNodeAdded} />
              </Panel>
              <Controls />
            </ReactFlow>
          </div>
          <NodeSidebar onClose={onNodeSidebarClose} isOpen={isNodeSidebarOpen}>
            {activeNode.type === nodeNameToTypeMapping.PROMPT_NODE && (
              <PromptNodeSidebar />
            )}
            {activeNode.type === nodeNameToTypeMapping.CONDITION_BLOCK && (
              <ConditionBlockSidebar />
            )}
          </NodeSidebar>
        </div>
      </ReactFlowProvider>
    </div>
  );
};

export { FlowEditor };
