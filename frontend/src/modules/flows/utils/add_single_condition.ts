import { Node } from "reactflow";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";
import { Condition } from "../typing/condition";
import { initNewNode } from "./init_new_node";
import { swapElseNodePosition } from "./shared";
import { MessageTagDefinition } from "@/typings/message_tag_definition";

export const addSingleCondition = (
  nodes: Node[],
  conditions: Condition[],
  activeNode: Node,
  type: string,
  tag?: MessageTagDefinition
) => {
  // create a node object of single condition type with calculated size, dimensions, name and initial position
  let newNode = initNewNode(nodes, nodeNameToTypeMapping.SINGLE_CONDITION);

  const siblings = nodes.filter((node) => node.parentId === activeNode.id);
  const recentlyAddedSibling = getRecentlyAddedSibling(siblings);

  const viewportHeight = window.innerHeight;
  const parentNodeBorderWidth = activeNode.data.borderWidth;
  const gap = (viewportHeight * 0.5) / 100;

  const { newNodeX, newNodeY, newNodeWidth, newNodeHeight } =
    calculateNewNodePositionAndSize(
      recentlyAddedSibling,
      activeNode,
      gap,
      viewportHeight,
      parentNodeBorderWidth
    );

  // increase height of parent node if total height taken by single condition nodes exceeds it
  nodes = updateParentNodeHeightIfNeeded(
    nodes,
    activeNode,
    newNodeY,
    newNodeHeight,
    gap,
    parentNodeBorderWidth
  );

  // create a Node object by provided parameters
  newNode = configureNewNode(
    newNode,
    activeNode.id,
    type,
    newNodeX,
    newNodeY,
    newNodeWidth,
    newNodeHeight
  );

  const newCondition = createNewCondition(type, newNode.id, tag);

  newNode = {
    ...newNode,
    data: { ...newNode.data, condition: newCondition },
  };

  nodes = [...nodes, newNode];
  // when new single condition nodes are added the order can become messed, so make sure else node comes last
  nodes = swapElseNodePosition(activeNode, nodes);

  return {
    nodes: nodes,
    conditions: [...conditions, newCondition],
  };
};

const getRecentlyAddedSibling = (siblings: Node[]): Node | null => {
  return siblings.reduce(
    (maxNode, currentNode) =>
      currentNode.position.y > maxNode!.position.y ? currentNode : maxNode,
    siblings.length ? siblings[0] : null
  );
};

const calculateNewNodePositionAndSize = (
  recentlyAddedSibling: Node | null,
  activeNode: Node,
  gap: number,
  viewportHeight: number,
  parentNodeBorderWidth: number
) => {
  let newNodeX: number,
    newNodeY: number,
    newNodeWidth: number,
    newNodeHeight: number;

  if (!recentlyAddedSibling) {
    const parentNodeWidth = Number(String(activeNode.style!.width));
    newNodeWidth = parentNodeWidth * 0.9;
    newNodeX = (parentNodeWidth * 0.1) / 2;
    newNodeHeight = (viewportHeight * 3) / 100;
    const parentNodeHeaderHeight: number = activeNode.data.headerHeight;
    newNodeY = parentNodeHeaderHeight + parentNodeBorderWidth + gap;
  } else {
    newNodeWidth = Number(String(recentlyAddedSibling.style!.width));
    newNodeX = recentlyAddedSibling.position.x;
    newNodeHeight = Number(String(recentlyAddedSibling.style!.height));
    newNodeY = recentlyAddedSibling.position.y + newNodeHeight + gap;
  }

  return { newNodeX, newNodeY, newNodeWidth, newNodeHeight };
};

const updateParentNodeHeightIfNeeded = (
  nodes: Node[],
  activeNode: Node,
  newNodeY: number,
  newNodeHeight: number,
  gap: number,
  parentNodeBorderWidth: number
) => {
  const maxCurrentBlockReachY =
    newNodeY + newNodeHeight + gap + parentNodeBorderWidth;
  if (maxCurrentBlockReachY > Number(String(activeNode.style!.height))) {
    return nodes.map((node) =>
      node.id === activeNode!.id
        ? {
            ...activeNode,
            style: {
              ...activeNode.style,
              height: maxCurrentBlockReachY,
            },
          }
        : node
    );
  }
  return nodes;
};

const configureNewNode = (
  newNode: Node,
  activeNodeId: string,
  type: string,
  newNodeX: number,
  newNodeY: number,
  newNodeWidth: number,
  newNodeHeight: number
) => {
  return {
    ...newNode,
    data: {
      ...newNode.data,
      parentId: activeNodeId,
      condition: {
        type,
        properties: { tag_name: "", tag_id: "" },
      } as Partial<Condition>,
    },
    style: {
      width: newNodeWidth,
      height: newNodeHeight,
    },
    position: { x: newNodeX, y: newNodeY },
    parentId: activeNodeId,
    extent: "parent",
  } as Node;
};

const createNewCondition = (
  type: string,
  attachedNodeId: string,
  tag?: MessageTagDefinition
): Condition => {
  return {
    properties: {
      tag_name: tag ? tag.name : "",
      tag_id: tag ? tag.id : "",
    },
    type,
    attachedNodeId,
  } as Condition;
};
