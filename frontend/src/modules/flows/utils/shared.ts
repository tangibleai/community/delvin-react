// shared utils for other utils

import { ConditionTypes } from "../typing/condition";
import { Node } from "reactflow";

export const swapElseNodePosition = (
  parentNode: Node,
  nodes: Node[]
): Node[] => {
  let elseNode: Node = {} as Node;
  let maxNode: Node = {} as Node;

  const parentNodeChildren = nodes.filter(
    (node) => node?.parentId === parentNode.id
  );

  parentNodeChildren.forEach((node) => {
    if (node.data.condition.type === ConditionTypes.ELSE) {
      elseNode = node;
    }
    if (
      !Object.keys(maxNode).length ||
      node.position.y > maxNode?.position?.y
    ) {
      maxNode = node;
    }
  });

  if (Object.keys(elseNode).length && Object.keys(maxNode).length) {
    const tempY = elseNode.position.y;
    elseNode.position.y = maxNode.position.y;
    maxNode.position.y = tempY;
  }

  return nodes;
};
