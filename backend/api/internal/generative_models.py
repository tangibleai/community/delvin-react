from rest_framework import generics, serializers, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.models import GenerativeModel


class GenerativeModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = GenerativeModel
        fields = "__all__"


class GenerativeModelListView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = GenerativeModelSerializer

    def list(self, request, *args, **kwargs):
        queryset = GenerativeModel.objects.all()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
