import Activate from "@/modules/accounts/pages/Activate";
import Login from "@/modules/accounts/pages/Login";
import ResetPassword from "@/modules/accounts/pages/ResetPassword";
import ResetPasswordConfirm from "@/modules/accounts/pages/ResetPasswordConfirm";
import Signup from "@/modules/accounts/pages/Signup";
import Verify from "@/modules/accounts/pages/Verify";
import ResendConfirmationEmail from "@/modules/accounts/pages/ResendConfirmationEmail";
import NotFound from "@/components/NotFound";
import withAuth from "@/lib/with-auth";

import { AUTH_PAGE_URLS } from "./pageUrls";
import Home from "@/modules/Home";
import UserAccountSettings from "@/modules/accounts/pages/UserAccountSettings";

const HomeWithAuth = withAuth(Home);

export const ROUTES = [
  { path: AUTH_PAGE_URLS.home, element: <HomeWithAuth /> },
  { path: AUTH_PAGE_URLS.signup, element: <Signup /> },
  { path: AUTH_PAGE_URLS.login, element: <Login /> },
  { path: AUTH_PAGE_URLS.reset_password, element: <ResetPassword /> },
  {
    path: AUTH_PAGE_URLS.reset_password_confirm,
    element: <ResetPasswordConfirm />,
  },
  { path: AUTH_PAGE_URLS.verify_team_member, element: <Verify /> },
  { path: AUTH_PAGE_URLS.activate, element: <Activate /> },
  { path: AUTH_PAGE_URLS.resend_email, element: <ResendConfirmationEmail /> },
  { path: AUTH_PAGE_URLS.not_found, element: <NotFound /> },
  {
    path: AUTH_PAGE_URLS.account_settings,
    element: <UserAccountSettings />,
  },
];
