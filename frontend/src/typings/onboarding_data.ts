export interface OnboardingData {
  stepAssistantName: {
    name: string;
    llm_provider_id: string;
    model_id: string;
  };
  stepSetPrompt: {
    chatbotType:
      | "everything"
      | "discussion"
      | "question-answer"
      | "prompt-based"
      | "knowledge-based"
      | "hybrid"
      | "guided-conversation"
      | "";
  };
  discussionBotSettings: {
    chatbotPersona: string;
    instructions: string;
    constraints: string;
  };
  questionAnswerBotSettings: {
    chatbotPersona: string;
    constraints: string;
    fallbackResponses: string;
  };
  guidedConversationsBotSettings: {
    chatbotPersona: string;
    constraints: string;
  };
  stepDocumentAdding: {
    file: FileList;
    text: string;
    google: string;
  };
}
