import os
from uuid import uuid4

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from api.models import *
from api.tests import utils as test_utils  # since "utils" is reserved name in python
from api.tests import mock


class MessageTagsAPITests(TestCase):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    @classmethod
    def setUpTestData(cls):
        set_up_data = test_utils.user_and_project_set_up(cls.client)
        cls.headers = set_up_data["headers"]
        cls.project = set_up_data["project"]

        tag_definition_1 = MessageTagDefinition.objects.create(
            project=cls.project,
            name=mock.mock["user1"]["message_tag_definition_name"],
            description="A test tag",
        )
        tag_definition_2 = MessageTagDefinition.objects.create(
            project=cls.project,
            name=mock.mock["user1"]["message_tag_definition_name"],
            description="A test tag",
        )

        cls.message_tag_definitions = [tag_definition_1, tag_definition_2]

    def test_create_message_tag_valid(self):
        url = reverse("message_tags_view")
        data = {
            "project_id": str(self.project.id),
            "message_text": "Sample message text",
            "tag_ids": [tag.id for tag in self.message_tag_definitions],
        }
        response = self.client.post(url, data, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data), 2
        )  # Expecting two `MessageTag` records created

    def test_create_message_tag_missing_fields(self):
        url = reverse("message_tags_view")
        data = {
            "project_id": str(self.project.id),
            "message_text": "Sample message text",
            # 'tag_ids' missing
        }
        response = self.client.post(url, data, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_message_tag_unauthorized(self):
        url = reverse("message_tags_view")
        data = {
            "project_id": str(self.project.id),
            "message_text": "Sample message text",
            "tag_ids": [tag.id for tag in self.message_tag_definitions],
        }
        response = self.client.post(url, data)  # No authentication headers
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def tearDown(self):
        Message.objects.filter(project=self.project).delete()
        # `MessageTag` records are deleted with the corresponding records deletion of one of the models above

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()


class MessageTagAPITests(TestCase):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    @classmethod
    def setUpTestData(cls):
        set_up_data = test_utils.user_and_project_set_up(cls.client)
        cls.headers = set_up_data["headers"]
        cls.project = set_up_data["project"]

    def setUp(self):
        self.tag_definition = MessageTagDefinition.objects.create(
            project=self.project,
            name=mock.mock["user1"]["message_tag_definition_name"],
            description="A test tag",
        )
        contact = Contact.objects.get(project=self.project)
        self.message = Message.objects.create(
            text="Sample message", project=self.project, contact=contact
        )
        self.message_tag = MessageTag.objects.create(
            message=self.message, tag=self.tag_definition
        )

    def test_delete_message_tag_valid(self):
        url = reverse("alter_message_tag", kwargs={"pk": self.message_tag.id})
        response = self.client.delete(url, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(MessageTag.objects.filter(id=self.message_tag.id).exists())

    def test_delete_message_tag_invalid_id(self):
        url = reverse(
            "alter_message_tag", kwargs={"pk": 999999}
        )  # Assuming this ID does not exist
        response = self.client.delete(url, headers=self.headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_message_tag_unauthorized(self):
        self.client.logout()
        url = reverse("alter_message_tag", kwargs={"pk": self.message_tag.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def tearDown(self):
        Message.objects.filter(project=self.project).delete()
        MessageTagDefinition.objects.filter(project=self.project).delete()
        # `MessageTag` records are deleted with the corresponding records deletion of one of the models above

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()


class MessageTagsWithTagAPITests(TestCase):
    lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
    client = APIClient()
    fixtures = ["default_account_data.json"]

    @classmethod
    def setUpTestData(cls):
        set_up_data = test_utils.user_and_project_set_up(cls.client)
        cls.headers = set_up_data["headers"]
        cls.project = set_up_data["project"]

    def setUp(self):
        tag_definition_1 = MessageTagDefinition.objects.create(
            project=self.project,
            name=mock.mock["user1"]["message_tag_definition_name"],
            description="A test tag",
        )
        tag_definition_2 = MessageTagDefinition.objects.create(
            project=self.project,
            name=mock.mock["user1"]["message_tag_definition_name"],
            description="A test tag",
        )
        self.message_tag_definitions = [tag_definition_1, tag_definition_2]
        self.tag_ids = [str(tag.id) for tag in self.message_tag_definitions]
        self.tag_names = [tag.name for tag in self.message_tag_definitions]

        contact = Contact.objects.get(project=self.project)
        self.message = Message.objects.create(
            text="Sample message", project=self.project, contact=contact
        )
        self.message_tags = []
        for tag in self.message_tag_definitions:
            message_tag = MessageTag.objects.create(message=self.message, tag=tag)
            self.message_tags.append(message_tag)

    def test_get_message_tags_with_valid_tag(self):
        url = reverse("messages_with_specified_tag")
        response = self.client.get(
            url,
            {"message_tag_definition_id": self.message_tag_definitions[0].id},
            headers=self.headers,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(
            len(response.data), 0, "Should return at least one tagged message"
        )
        for item in response.json():
            self.assertTrue({"message", "tag", "message_tags"}.issubset(set(item)))
            message_tags = item.get("message_tags")
            self.assertEqual(len(message_tags), 2)
            for tag in message_tags:
                self.assertTrue(tag["id"] in self.tag_ids)
                self.assertTrue(tag["name"] in self.tag_names)

    def test_get_message_tags_with_invalid_tag_id(self):
        url = reverse("messages_with_specified_tag")
        invalid_id = uuid4()
        response = self.client.get(
            url, {"message_tag_definition_id": invalid_id}, headers=self.headers
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 0)

    def test_get_message_tags_unauthorized(self):
        url = reverse("messages_with_specified_tag")
        response = self.client.get(
            url, {"message_tag_definition_id": self.message_tag_definitions[0].id}
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def tearDown(self):
        Message.objects.filter(project=self.project).delete()
        MessageTagDefinition.objects.filter(project=self.project).delete()
        # `MessageTag` records are deleted with the corresponding records deletion of one of the models above

    @classmethod
    def tearDownClass(cls):
        UserAccount.objects.all().delete()
        super().tearDownClass()
