"use client";

import { Button } from "@/components/ui/button";
import { cn, focusRing } from "@/lib/utils";
import { RiMore2Fill } from "react-icons/ri";

import { DropdownUserProfile } from "./DropdownUserProfile";
import { Avatar, AvatarFallback } from "@/components/ui/avatar";

type Props = {
  isOpen: boolean;
  userName: string;
  userEmail: string;
};

export const UserProfileDesktop = ({ isOpen, userName, userEmail }: Props) => {
  return (
    <DropdownUserProfile userEmail={userEmail}>
      <Button
        aria-label="User settings"
        variant="ghost"
        className={cn(
          focusRing,
          "group flex w-full items-center justify-between rounded-md p-2 text-sm font-medium text-gray-900 data-[state=open]:bg-gray-100 data-[state=open]:bg-gray-400/10",
        )}
      >
        {isOpen ? (
          <>
            <span className="flex items-center gap-3">
              <Avatar>
                <AvatarFallback>{userName.charAt(0)}</AvatarFallback>
              </Avatar>
              <span className="font-sans">{userName}</span>
            </span>
            <RiMore2Fill
              className="size-4 shrink-0 text-gray-500 group-hover:text-gray-700 group-hover:dark:text-gray-400"
              aria-hidden="true"
            />
          </>
        ) : (
          <span className="flex items-center ">
            <Avatar>
              <AvatarFallback>{userName.charAt(0)}</AvatarFallback>
            </Avatar>
          </span>
        )}
      </Button>
    </DropdownUserProfile>
  );
};
