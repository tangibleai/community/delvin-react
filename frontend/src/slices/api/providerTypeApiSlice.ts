import emptyApi from "../apiSlice";

interface ProviderType {
  name: string;
}

const providerTypesApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    fetchProviderTypes: builder.query<ProviderType[], void>({
      query: () => ({
        url: "provider-types/",
        method: "GET",
      }),
    }),
  }),
});

export const { useFetchProviderTypesQuery } = providerTypesApiSlice;

export default providerTypesApiSlice;
