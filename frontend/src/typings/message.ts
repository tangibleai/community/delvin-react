export const MessageDirections = {
  INBOUND: "inbound",
  OUTBOUND: "outbound",
} as const;

export type MessageDirection =
  (typeof MessageDirections)[keyof typeof MessageDirections];

export const SenderTypes = {
  BOT: "bot",
  USER: "user",
  OPERATOR: "operator",
} as const;

export type SenderType = (typeof SenderTypes)[keyof typeof SenderTypes];

export interface Message {
  id: string;
  text: string;
  direction: MessageDirection;
  timestamp: string;
  sender_type: SenderType;
}
