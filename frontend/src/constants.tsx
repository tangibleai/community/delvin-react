const CONSTANTS = Object.freeze({
    CONTACT_EMAIL: "mailto:delvin@tangibleai.com" as const,
    MAX_FREE_MESSAGES: 2000 as const,
    WARNING_RATIO: 0.8 as const,
});

export default CONSTANTS;
