import { useEffect, useState } from "react";

import logo from "@/assets/delvin_logo.png";
import { Button } from "@/components/ui/button";
import { reset_password } from "@/actions/auth";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { useToast } from "@/components/ui/use-toast";
import { retrieveUserByEmail } from "../actions/action";

const ResetPassword = () => {
  const { toast } = useToast();

  const [userEmail, setUserEmail] = useState<string>();
  const [resetPasswordEmailSent, setResetPasswordEmailSent] = useState(false);
  const [timeLeftToResendEmail, setTimeLeftToResendEmail] = useState(0);

  useEffect(() => {
    let timer: string | number | NodeJS.Timeout | undefined;
    if (resetPasswordEmailSent && timeLeftToResendEmail > 0) {
      timer = setInterval(() => {
        setTimeLeftToResendEmail((prevTime) => {
          if (prevTime <= 1) {
            clearInterval(timer);
            setResetPasswordEmailSent(false);
            return 0;
          }
          return prevTime - 1;
        });
      }, 1000);
    }
    return () => clearInterval(timer);
  }, [resetPasswordEmailSent, timeLeftToResendEmail]);

  const formSchema = z.object({
    email: z.string().email("Please provide a valid email"),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
    },
  });

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const { email } = values;

    const response = await retrieveUserByEmail(email);

    if ("error" in response) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "Rate limit exceeded. Please try again in a minute.",
      });
      return response.error;
    }

    const usersWithEmail = response.users!;

    if (usersWithEmail.length === 0) {
      toast({
        variant: "destructive",
        title: "Oops! Something went wrong",
        description: "User with this email does not exist.",
      });
      return;
    }

    setUserEmail(email);
    setResetPasswordEmailSent(true);
    setTimeLeftToResendEmail(60);

    const statusCode = await reset_password(email);

    if (statusCode >= 200 && statusCode <= 299) {
      toast({
        title: "Success!",
        description: `Reset password email was successfully sent to ${email}.`,
      });
      return statusCode;
    }

    toast({
      variant: "destructive",
      title: "Oops! Something went wrong",
      description: "Couldn't send reset password email.",
    });
  };

  return resetPasswordEmailSent ? (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0 gap-3">
      <p className="text-4xl">
        Please, follow the link we have sent you on email!
      </p>
      <p className="text-2xl">We sent a verification email on {userEmail}</p>
      <p className="text-2xl">
        If you didn't receive an email, please check your "Spam" folder.
      </p>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="mt-4 space-y-4 lg:mt-5 md:space-y-5"
        >
          <FormField
            control={form.control}
            name="email"
            render={({ field }) => (
              <>
                <FormItem>
                  <FormLabel className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Your email
                  </FormLabel>
                  <FormControl>
                    <Input disabled placeholder="Your email" {...field} />
                  </FormControl>
                </FormItem>
                {form?.formState?.errors?.email?.message && <FormMessage />}
              </>
            )}
          />
          <Button disabled type="submit" className="text-base">
            {resetPasswordEmailSent && timeLeftToResendEmail} Resend Password
            Reset Email
          </Button>
        </form>
      </Form>
    </div>
  ) : (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
      <img className="w-80 mr-2" src={logo} alt="logo" />
      <div className="w-full p-6 bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md dark:bg-gray-800 dark:border-gray-700 sm:p-8">
        <h2 className="mb-1 text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
          Request Password Reset
        </h2>
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onSubmit)}
            className="mt-4 space-y-4 lg:mt-5 md:space-y-5"
          >
            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <>
                  <FormItem>
                    <FormLabel className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                      Your email
                    </FormLabel>
                    <FormControl>
                      <Input placeholder="Your email" {...field} />
                    </FormControl>
                  </FormItem>
                  {form?.formState?.errors?.email?.message && <FormMessage />}
                </>
              )}
            />
            <Button type="submit" className="text-base">
              Reset password
            </Button>
          </form>
        </Form>
      </div>
    </div>
  );
};

export default ResetPassword;
