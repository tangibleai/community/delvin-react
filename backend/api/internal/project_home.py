from datetime import timedelta, datetime

from django.db.models import Count
from django.db.models.functions import TruncDay
from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiTypes
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.models import Message, Contact


@extend_schema(
    tags=["api"],
    description="Project home views",
    parameters=[
        OpenApiParameter(
            name="project_id",
            required=True,
            location=OpenApiParameter.PATH,
            type=OpenApiTypes.STR,
            description="Unique identifier of the project",
        ),
    ],
    responses={
        200: {
            "type": "object",
            "properties": {
                "total": {
                    "type": "object",
                    "properties": {
                        "new_users": {"type": "integer", "description": "New users"},
                        "active_users": {
                            "type": "integer",
                            "description": "Active users",
                        },
                        "messages": {"type": "integer", "description": "Messages"},
                    },
                },
                "chart": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "formatted_results": {
                                "type": "integer",
                                "description": "Formatted results",
                            },
                        },
                    },
                },
            },
        },
    },
)
@api_view(["GET"])
def get_home_charts(request):
    first_date = datetime.now().date() - timedelta(days=30)

    project_id = request.query_params.get("project_id")
    if not project_id:
        return Response("Project id missing", status=status.HTTP_400_BAD_REQUEST)

    base_message_query = Message.objects.filter(
        timestamp__gte=first_date,
        contact__internal__in=(False, None),
        project__id=project_id,
    ).annotate(day=TruncDay("timestamp"))

    total_active_users = base_message_query.values("contact").distinct().count()

    # initialize the date array
    date_counts = {
        (first_date + timedelta(days=i)): {
            "active_users": 0,
            "total_users": 0,
            "inbound": 0,
            "outbound": 0,
        }
        for i in range((datetime.now().date() - first_date).days + 2)
    }

    # Calculate active_users
    active_users_query = (
        base_message_query.values("day")
        .annotate(active_users=Count("contact", distinct=True))
        .order_by("day")
    )

    for entry in active_users_query:
        date_key = entry["day"].date()
        date_counts[date_key]["active_users"] = entry["active_users"]

    # Calculate incoming/outgoing messages
    messages_query = (
        base_message_query.values("day", "direction")
        .annotate(count=Count("id"))
        .order_by("day", "direction")
    )

    total_messages = 0
    for entry in messages_query:
        date_key = entry["day"].date()
        direction = (
            "inbound" if entry["direction"] == Message.Direction.INBOUND else "outbound"
        )
        date_counts[date_key][direction] = entry["count"]
        total_messages += entry["count"]

    # Calculate new users
    cumulative_users_count = Contact.objects.filter(
        inserted_at__lt=first_date, internal__in=(False, None)
    ).count()

    new_users_per_day = list(
        Contact.objects.filter(
            project__id=project_id,
            internal__in=(False, None),
            inserted_at__gte=first_date,
        )
        .annotate(date=TruncDay("inserted_at"))
        .values("date")
        .annotate(new_users=Count("id"))
        .order_by("date")
    )

    total_new_users = 0
    if new_users_per_day:
        current_entry = new_users_per_day.pop(0)
        for key in date_counts.keys():
            if current_entry["date"].date() == key:
                cumulative_users_count += current_entry["new_users"]
                total_new_users += current_entry["new_users"]
                if new_users_per_day:
                    current_entry = new_users_per_day.pop(0)
            date_counts[key]["total_users"] = cumulative_users_count
    else:
        for key in date_counts.keys():
            date_counts[key]["total_users"] = cumulative_users_count

    # Reshape results into flat array for Tremor
    formatted_results = [
        {
            "date": date.strftime("%b %d"),
            "active_users": value["active_users"],
            "total_users": value["total_users"],
            "inbound": value["inbound"],
            "outbound": value["outbound"],
        }
        for date, value in date_counts.items()
    ]

    response_data = {
        "total": {
            "new_users": total_new_users,
            "active_users": total_active_users,
            "messages": total_messages,
        },
        "chart": formatted_results,
    }

    return Response(response_data, status=status.HTTP_200_OK)
