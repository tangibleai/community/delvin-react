"""Reference for enumeration
https://docs.djangoproject.com/en/4.2/ref/models/fields/
"""

import copy
import os
import uuid

from django.db import models
from django_cryptography.fields import encrypt
from pgvector.django import VectorField

from accounts.models import UserAccount
from api import crypto
from core.constant import CONSTANTS
from scripts.ml import embedding
from api.utils import moderation

from django.core.cache import cache
from django.http import JsonResponse

DEFAULT_CACHE_TIMEOUT = CONSTANTS.DEFAULT_CACHE_TIMEOUT


class ProjectTokenManager(models.Manager):

    def create(self, name, workspace):
        token = CONSTANTS.TOKEN_PREFIX + crypto.create_token_string()
        digest = crypto.hash_token(token)
        instance = super().create(
            name=name,
            token_key=token[: CONSTANTS.TOKEN_KEY_LENGTH],
            digest=digest,
            workspace=workspace,
        )
        return instance, token

    def get_or_create(self, name, workspace_owner):
        """Returns a tuple of 3 elements - project, token key and boolean - True if project is created else False"""

        try:
            instance = super().get(name=name, workspace__owner=workspace_owner)
            token_key = instance.token_key
            return instance, token_key, False
        except Project.DoesNotExist:
            instance, token = self.create(
                name, Workspace.objects.filter(owner=workspace_owner).first()
            )
            token_key = instance.token_key
            return instance, token_key, True

    def update_token(self, instance):
        token = CONSTANTS.TOKEN_PREFIX + crypto.create_token_string()
        digest = crypto.hash_token(token)
        instance.digest = digest
        instance.token_key = token[: CONSTANTS.TOKEN_KEY_LENGTH]
        instance.save()
        return instance, token

    def update_project(self, instance, name=None):
        instance.name = name if name else instance.name
        instance.save()
        return instance

    def get_cached(self, project_id):
        """Retrieve a project from cache or database, then cache it."""
        cache_key = f"project_{project_id}"
        project = cache.get(cache_key)
        if not project:
            project = self.filter(id=project_id).first()
            if project:
                cache.set(cache_key, project, timeout=DEFAULT_CACHE_TIMEOUT)
        return project


class PlanManager(models.Manager):
    def create(self):
        """Creates a default FREE active plan."""
        instance = super().create(
            type=Plan.Type.FREE,
            status=Plan.Status.ACTIVE,
            properties={"free_message": 0},
        )
        return instance

    def get_or_create(self, pk=None):
        """Returns or creates a default FREE active plan."""
        if pk is not None:
            try:
                instance = super().get(pk=pk)
                created = False
                return instance, created
            except Plan.DoesNotExist:
                pass
        instance = self.create()
        created = True
        return instance, created


class Plan(models.Model):
    objects = PlanManager()

    class Type(models.TextChoices):
        """The types of a plan"""

        FREE = "F", "Free"
        PAID = "P", "Paid"
        OWN_KEY = "O", "Your-Own-Key"

    class Status(models.TextChoices):
        """Status of the plan"""

        ACTIVE = "A", "Active"
        EXPIRED = "E", "Expired"
        PAUSED = "P", "Paused"

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    type = models.CharField(
        max_length=1,
        choices=Type.choices,
        default=Type.FREE,
        help_text="The types of a plan",
    )
    renewal_date = models.DateTimeField(
        null=True, help_text="Renewal date of the subscription"
    )
    status = models.CharField(
        max_length=1,
        choices=Status.choices,
        null=True,
        help_text="Status of the subscription",
    )
    properties = models.JSONField(default=dict, help_text="Properties of the plan")


class WorkspaceManager(models.Manager):
    def create(self, **kwargs):
        """Creates a workspace with a default plan."""
        default_plan = Plan.objects.create()
        kwargs["plan"] = default_plan

        instance = super().create(**kwargs)
        return instance

    def get_or_create(self, pk=None, **kwargs):
        """Returns workspace or creates a workspace with a default plan."""
        if pk is not None:
            try:
                instance = super().get(pk=pk)
                created = False
                return instance, created
            except Workspace.DoesNotExist:
                pass
        instance = self.create(**kwargs)
        created = True
        return instance, created


class Workspace(models.Model):
    objects = WorkspaceManager()

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(
        UserAccount, null=False, blank=False, on_delete=models.CASCADE
    )
    properties = models.JSONField(default=dict)
    plan = models.OneToOneField(
        Plan,
        on_delete=models.SET_NULL,
        null=True,
    )
    feature_flags = models.JSONField(
        default=dict,
        null=False,
        blank=False,
        help_text="Enable/disable experimental features for workspace",
    )

    def has_default_provider(self):
        return self.plan and self.plan.type in [Plan.Type.FREE, Plan.Type.PAID]


DEFAULT_PROJECT_CONFIG = {
    "guardrails": {
        "builtin": {
            "enabled": False,
            "categories": moderation.BUILTIN_DEFAULT_CATEGORIES,
        }
    },
}


def create_project_config():
    return copy.deepcopy(DEFAULT_PROJECT_CONFIG)


class Project(models.Model):
    objects = ProjectTokenManager()

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    name = models.CharField(max_length=50, null=False)
    digest = models.CharField(max_length=128, null=False, unique=True)
    token_key = models.CharField(
        max_length=CONSTANTS.MAXIMUM_TOKEN_PREFIX_LENGTH + CONSTANTS.TOKEN_KEY_LENGTH
    )
    activated = models.BooleanField(default=False)
    workspace = models.ForeignKey(
        Workspace, null=True, blank=True, on_delete=models.CASCADE
    )
    config = models.JSONField(
        default=create_project_config,
        null=False,
        blank=False,
        help_text="Guardrail settings",
    )

    def update_moderation_enabled_field(
        self,
        enabled: bool,
    ) -> None:
        """
        Update `Project.config["guardrails"]["builtin"]["enabled"]` value.

        Args:
            enabled (bool):Boolean value to enable/disable `Project.config["guardrails"]["builtin"]["enabled"]` value.
        """

        config = self.config

        if "guardrails" not in config:
            config["guardrails"] = {}
        if "builtin" not in config["guardrails"]:
            config["guardrails"]["builtin"] = {}
        if "enabled" not in config["guardrails"]["builtin"]:
            config["guardrails"]["builtin"]["enabled"] = False

        config["guardrails"]["builtin"]["enabled"] = enabled

        self.config = config
        self.save()

    def update_category(
        self, code: str, name: str, enabled: bool, response: str
    ) -> None:
        """
        Update an existing category or add a new one in the config's categories dictionary.

        Args:
            category_key (str): The key for the category to update or add.
            partial_data (dict): The partial data for the category.
        """

        config = self.config

        if "guardrails" not in config:
            config["guardrails"] = {}
        if "builtin" not in config["guardrails"]:
            config["guardrails"]["builtin"] = {}
        if "categories" not in config["guardrails"]["builtin"]:
            config["guardrails"]["builtin"]["categories"] = copy.deepcopy(
                moderation.BUILTIN_DEFAULT_CATEGORIES
            )

        category_details = config["guardrails"]["builtin"]["categories"].get(code, {})
        category_details.update(
            {"code": code, "name": name, "enabled": enabled, "response": response}
        )

        config["guardrails"]["builtin"]["categories"][code] = category_details

        self.config = config
        self.save()

    def __str__(self):
        return f"{self.digest} : {self.workspace.owner}"


class ProjectCollaborator(models.Model):
    """Offers an interface between the UserAccount and Project models

    Project.workspace.owner is the initial creator of the project.
    Other team members with owner status are added through the ProjectCollaborator model.
    """

    class ProjectCollaboratorRoles(models.TextChoices):
        VIEWER = "V", "Viewer"
        EDITOR = "E", "Editor"
        OWNER = "O", "Owner"

    class ProjectCollaboratorStatus(models.TextChoices):
        VERIFIED = "V", "Verified"
        UNVERIFIED = "U", "Unverified"
        EXPIRED = "E", "Expired"

    delvin_user = models.ForeignKey(
        UserAccount,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        help_text="A Delvin user account",
    )
    project = models.ForeignKey(
        Project,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        help_text="A Delvin project",
    )
    verification_status = models.CharField(
        max_length=1,
        choices=ProjectCollaboratorStatus.choices,
        default=ProjectCollaboratorStatus.UNVERIFIED,
        help_text="The status of a potential team member's user account activation",
    )
    role = models.CharField(
        max_length=1,
        choices=ProjectCollaboratorRoles.choices,
        default=ProjectCollaboratorRoles.VIEWER,
        help_text="The permission level for a team member in a project",
    )
    properties = models.JSONField(default=dict)

    @classmethod
    def get_role_abbreviation(cls, full_role):
        for choice in cls.ProjectCollaboratorRoles.choices:
            if choice[1] == full_role:
                return choice[0]
        return None


class TeamMember(models.Model):
    """Offers an interface between the UserAccount and Workspace models

    Workspace.useraccount_set.first() is the initial creator of workspace.
    Other team members with owner status are added through the TeamMember model.
    """

    class TeamMemberRoles(models.TextChoices):
        VIEWER = "V", "Viewer"
        EDITOR = "E", "Editor"
        OWNER = "O", "Owner"

    class TeamMemberStatus(models.TextChoices):
        VERIFIED = "V", "Verified"
        UNVERIFIED = "U", "Unverified"
        EXPIRED = "E", "Expired"

    delvin_user = models.ForeignKey(
        UserAccount,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        help_text="A Delvin user account",
    )
    workspace = models.ForeignKey(
        Workspace,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        help_text="A Delvin workspace",
    )
    verification_status = models.CharField(
        max_length=1,
        choices=TeamMemberStatus.choices,
        default=TeamMemberStatus.UNVERIFIED,
        help_text="The status of a potential team member's user account activation",
    )
    role = models.CharField(
        max_length=1,
        choices=TeamMemberRoles.choices,
        default=TeamMemberRoles.VIEWER,
        help_text="The permission level for a team member in a workspace",
    )
    properties = models.JSONField(default=dict)

    @classmethod
    def get_role_abbreviation(cls, full_role):
        for choice in cls.TeamMemberRoles.choices:
            if choice[1] == full_role:
                return choice[0]
        return None


class ContactManager(models.Manager):
    def get_cached(self, contact_origin, project, internal):
        """Retrieve the contact from cache or database, then cache it."""

        contact_cache_key = f"contact_{contact_origin}"
        contact = cache.get(contact_cache_key)

        if not contact:
            contact = self.filter(
                origin=contact_origin, project=project, internal=internal
            ).first()

            if not contact:
                contact = Contact.objects.create(origin=contact_origin, project=project, internal=internal)

            cache.set(contact_cache_key, contact, timeout=DEFAULT_CACHE_TIMEOUT)
        return contact


class Contact(models.Model):
    """User created within project to own a list of conversations with bot

    Contact.origin - whatever combination of chars user wants to represent a contact.
    Only unique within one project, so when querying add project field to filter to get a unique record
    """

    objects = ContactManager()

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    origin = models.CharField(max_length=99)
    inserted_at = models.DateTimeField(auto_now=True)
    project = models.ForeignKey(
        Project, null=False, blank=False, on_delete=models.CASCADE
    )
    internal = models.BooleanField(null=True, blank=True, default=True)


class Message(models.Model):
    """Represents chat message between bot and user

    Model.text - message text
    """

    class Direction(models.TextChoices):
        INBOUND = "inbound"
        OUTBOUND = "outbound"

    class SenderType(models.TextChoices):
        BOT = "bot"
        USER = "user"
        OPERATOR = "operator"

    class ChannelType(models.TextChoices):
        WEB = "web"
        FACEBOOK = "facebook"

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    direction = models.CharField(max_length=99, choices=Direction.choices)
    sender_type = models.CharField(max_length=99, choices=SenderType.choices)
    channel_type = models.CharField(max_length=99, choices=ChannelType.choices)
    timestamp = models.DateTimeField(auto_now=True)
    text = models.TextField()
    project = models.ForeignKey(
        Project, null=False, blank=False, on_delete=models.CASCADE
    )
    embedding = VectorField(dimensions=384, null=True, blank=True, default=None)
    contact = models.ForeignKey(
        Contact, null=False, blank=False, on_delete=models.CASCADE
    )
    chat = models.ForeignKey(
        "Chat", related_name="message", null=True, blank=True, on_delete=models.CASCADE
    )
    tags = models.ManyToManyField("MessageTagDefinition", through="MessageTag")

    def save(self, *args, **kwargs):
        if self.embedding is None:
            embedding_vector = embedding.embed_text(self.text)
            self.embedding = embedding_vector

        super().save(*args, **kwargs)


class MessageTagGroup(models.Model):
    """Used to recognize different sets of intents for different stages of a conversation"""

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    name = models.CharField(max_length=50, null=False)
    project = models.ForeignKey(
        Project, null=False, blank=False, on_delete=models.CASCADE
    )


class MessageTagDefinition(models.Model):
    """Tag (hashtag) for user intent within a message"""

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    name = models.CharField(max_length=50, null=False)
    description = models.CharField(max_length=255)
    project = models.ForeignKey(
        Project, null=False, blank=False, on_delete=models.CASCADE
    )
    message_tag_group = models.ForeignKey(
        MessageTagGroup, null=True, blank=False, on_delete=models.CASCADE
    )


class MessageTagRule(models.Model):
    """Similar to Trigger on Convohub"""

    class Direction(models.TextChoices):
        INBOUND = "inbound"
        OUTBOUND = "outbound"
        ANY = "any"

    class RuleType(models.TextChoices):
        CONTAINS = "contains"
        NOT_CONTAINS = "not_contains"

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    direction = models.CharField(max_length=99, choices=Direction.choices)
    rule_type = models.CharField(max_length=99, choices=RuleType.choices)
    rule_content = models.JSONField(null=False, blank=False)
    project = models.ForeignKey(
        Project, null=False, blank=False, on_delete=models.CASCADE
    )
    message_tag_definition = models.ForeignKey(
        MessageTagDefinition, null=False, blank=False, on_delete=models.CASCADE
    )


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ChatManager(models.Manager):
    def get_cached(self, chat_origin_id, contact):
        """Retrieve a chat from cache or database, then cache it."""

        chat_cache_key = f"chat_{chat_origin_id}"
        chat = cache.get(chat_cache_key)
        if not chat:
            if chat_origin_id:
                chat, _ = self.get_or_create(origin_id=chat_origin_id, contact_id=contact)
            else:
                chat, _ = self.get_or_create(
                    contact_id=contact,
                    origin_id="default",
                    title="default",
                    is_default=True,
                )
            cache.set(chat_cache_key, chat, DEFAULT_CACHE_TIMEOUT)
        return chat


class Chat(TimeStampMixin):
    """Representation of chat holding a list of messages

    Chat.origin_id - whatever combination of chars user wants to represent a chat. Non-unique, so when querying add `Chat.contact_id` and `Chat.contact_id.project` field to filter to get a unique record
    """

    objects = ChatManager()

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    title = models.CharField(max_length=99)
    contact_id = models.ForeignKey(
        Contact, null=False, blank=False, on_delete=models.CASCADE
    )
    is_default = models.BooleanField(null=False, blank=False, default=False)
    origin_id = models.CharField(max_length=99)


# NOTE: This value is intended to help Projects/Chatbots
# created before Chatbot.config was added to transition.
# The structure will likely change in the future
# and may need a more flexible approach
DEFAULT_CHATBOT_CONFIG = {
    "bot_type": "discussion",
    "general_prompt": {
        "constraints": "",
        "persona": "",
        "instructions": "",
    },
    "knowledge": {
        "enabled": False,
        "activation": {"type": "never", "threshold": 0.0},
        "fallback": "",
    },
    "flow": "",
}


def create_chatbot_config():
    return copy.deepcopy(DEFAULT_CHATBOT_CONFIG)


class ChatbotManager(models.Manager):
    def get_cached(self, project, chatbot_origin):
        """Retrieve a chatbot from cache or database, then cache it."""
        chatbot_cache_key = f"chatbot_{project.id}_{chatbot_origin}"
        chatbot = cache.get(chatbot_cache_key)
        if not chatbot:
            chatbot = self.filter(origin=chatbot_origin, project=project).first()
            if not chatbot:
                return JsonResponse({"error": "Assistant with this ID not found."}, status=404)

            cache.set(chatbot_cache_key, chatbot, timeout=DEFAULT_CACHE_TIMEOUT)
        return chatbot


class Chatbot(models.Model):
    """Chat rerpesentation with input feature to chat with bot

    Chatbot.generation_parameters - json object with generation parameters sent to LLM according to which it's got more precise and relevant responses
    """

    objects = ChatbotManager()

    class GenerativeModel(models.TextChoices):
        ANTHROPIC_CLAUDE_35_SONNET = "anthropic/claude-3.5-sonnet"
        GOOGLE_GEMINI_FLASH_15 = "google/gemini-flash-1.5"
        META_LLAMA_3_70B = "meta-llama/llama-3-70b"
        META_LLAMA_3_8B_INSTRUCT_FREE = "meta-llama/llama-3.1-8b-instruct:free"
        MISTRAL_7b_INSTRUCT = "mistralai/mistral-7b-instruct"
        OPENAI_GPT_TURBO = "openai/gpt-3.5-turbo"
        OPENAI_GPT_4O = "openai/gpt-4o"  # O as in letter o
        QWEN_2_72B_INSTRUCT = "qwen/qwen-2-72b-instruct"

    project = models.ForeignKey(
        Project, null=False, blank=False, on_delete=models.CASCADE
    )
    config = models.JSONField(
        default=create_chatbot_config,
        null=False,
        blank=False,
        help_text="Various prompt and knowledge settings",
    )
    origin = models.CharField(max_length=99, default="default_assistant")
    generative_model = models.ForeignKey(
        "GenerativeModel", null=True, on_delete=models.SET_NULL, related_name="chatbots"
    )
    provider = models.ForeignKey(
        "Provider",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text="If null, uses default provider",
    )


class MLModel(TimeStampMixin):
    """Hold generic machine learning models (list of floats)"""

    class Status(models.TextChoices):
        UP_TO_DATE = "up-to-date"
        NEEDS_RETRAINING = "needs-retraining"
        TRAINING = "training"

    sklearn_pipeline = models.BinaryField(
        help_text="Sklearn logistic regression or SVM etc",
        blank=True,
        null=True,
        default=None,
    )
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=False, null=False, default="")
    description = models.TextField(default="", help_text="Description for model")
    dataset = models.ForeignKey(
        "Dataset", blank=True, null=True, default=None, on_delete=models.CASCADE
    )
    status = models.CharField(
        max_length=16,
        choices=Status.choices,
        default=Status.UP_TO_DATE,
        help_text="The state of processing and working with a MLModel instance",
    )
    created_by = models.ForeignKey(UserAccount, on_delete=models.CASCADE, default=None)

    def set_ml_model_status_to_needs_retraining(self):
        self.status = self.Status.NEEDS_RETRAINING
        self.save()


class MessageTag(TimeStampMixin):
    """Represents a connection between `Message` and `MessageTagDefinition`"""

    message = models.ForeignKey(
        Message, null=False, blank=False, on_delete=models.CASCADE
    )
    tag = models.ForeignKey(
        MessageTagDefinition,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        help_text="Predicted intent tag for a text message, created by either a human or an MLModel.",
    )
    ml_model = models.ForeignKey(
        MLModel, null=True, blank=True, default=None, on_delete=models.CASCADE
    )
    labeler = models.ForeignKey(
        UserAccount,
        null=True,
        blank=True,
        default=None,
        help_text="Labeler of tagged message, if set to null machine is labeler",
        on_delete=models.CASCADE,
    )


class MLWeights(models.Model):
    """Coef and intercept for one tag definition. TagDefintion <- MlWeights -> MlModel"""

    tag_definition = models.ForeignKey(MessageTagDefinition, on_delete=models.CASCADE)
    coef = VectorField(dimensions=384, default=[0.0] * 384)
    intercept = models.FloatField(default=0.0)
    ml_model = models.ForeignKey("MLModel", on_delete=models.CASCADE)


class Dataset(models.Model):
    """Django data model, Dataset.project_id, Dataset.message_labels - many to many relation"""

    name = models.CharField(max_length=50, default="")
    description = models.TextField(
        default="",
        help_text="User describes the conversation domain",
    )
    project = models.ForeignKey(
        Project, null=False, blank=False, on_delete=models.CASCADE
    )


class Document(models.Model):
    """Content to ground the chatbot in particular information

    Note: Both Delvin and LlamaIndex have Document classes, so LlamaIndex is prefixed as LlamaDocument
    """

    class DocumentStatus(models.TextChoices):
        """The stages a Document instance may go through through its lifecycle"""

        CREATED = "C", "Created"  # Document instance created
        UPDATED = "U", "Updated"  # Document instance was changed and needs reprocessing
        PROCESSED = "P", "Processed"  # Metabase + embeddings created
        ERROR = "E", "Error"  # Received error during Processing
        DELETED = "D", "Deleted"  # Use deleted Document instance

    title = models.TextField(
        blank=False,
        null=False,
        default="",
        help_text="A unique name for the information",
    )
    content = models.TextField(
        blank=False,
        null=False,
        help_text="The information that the chatbot should use to answer questions",
    )
    project = models.ForeignKey(
        Project,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        help_text="The project the Document was uploaded to",
    )
    chatbot = models.ForeignKey(
        Chatbot,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        help_text="The chatbot the Document was uploaded to",
    )
    status = models.CharField(
        max_length=1,
        choices=DocumentStatus.choices,
        default=DocumentStatus.CREATED,
        help_text="The state of processing and working with a Document instance",
    )
    properties = models.JSONField(
        default=dict, help_text="Miscellaneous information about the document"
    )


class GuardrailAction(models.Model):
    class ActionType(models.TextChoices):
        SCRIPTED = "scripted"
        GENERATIVE = "generative"

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    action_type = models.CharField(
        max_length=100, choices=ActionType.choices, help_text="GuardrailAction"
    )
    content = models.TextField(null=False, blank=False)


class GuardrailResponse(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    guardrail_action = models.ForeignKey(
        GuardrailAction,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        help_text="Will be used as action type and content text",
    )
    project = models.ForeignKey(
        Project,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    message_tag_definition = models.ForeignKey(
        MessageTagDefinition,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        help_text="Will be used as response trigger",
    )


class ResponseLog(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )

    class Source(models.TextChoices):
        GUARDRAILS = (
            "G",
            "Guardrails",
        )
        MODERATION = (
            "M",
            "Moderation",
        )
        KNOWLEDGE = (
            "K",
            "Knowledge",
        )
        PROMPT = (
            "P",
            "Prompt",
        )
        FLOW = "F", "Flow"

    user_message = models.ForeignKey(
        Message,
        null=False,
        blank=False,
        related_name="user_message",
        on_delete=models.CASCADE,
        help_text="FK to inbound message object that we're responding to",
    )

    # We can not create two different field for Message
    response = models.ForeignKey(
        Message,
        null=False,
        blank=False,
        related_name="response",
        on_delete=models.CASCADE,
        help_text="FK to outbound message object",
    )

    source = models.CharField(
        max_length=1,
        choices=Source.choices,
        null=True,
        help_text="The source of the message",
    )

    ai_usage = models.JSONField(
        default=dict, help_text="{usage_records:list[], cost:float}"
    )

    metadata = models.JSONField(
        default=dict, help_text="Each service adds its own metadata with service key"
    )

    context = models.JSONField(
        default=dict,
        help_text="Project and user context",
        null=True,
        blank=False,
    )


class Flow(TimeStampMixin):
    """
    Model representing a workflow or process flow within a project.
    """

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    project = models.ForeignKey(
        "Project", on_delete=models.CASCADE, related_name="flows"
    )
    name = models.CharField(max_length=255)
    description = models.TextField()
    assistant_role = models.TextField()
    metrics = models.JSONField(default=dict)
    ui = models.JSONField(default=dict)
    flow_graph = models.JSONField(default=dict)
    published = models.BooleanField(default=False)


class FlowRun(TimeStampMixin):
    class FlowRunStatus(models.TextChoices):
        """The stages a FlowRun instance may go through its lifecycle"""

        IN_PROGRESS = "P", "In-Progress"
        INTERRUPTED = (
            "I",
            "Interrupted",
        )
        COMPLETED = "C", "Completed"

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    chat = models.ForeignKey(
        Chat, on_delete=models.CASCADE, default=""
    )  # default addee since model alrleady exists and db requires it to populate existing records
    flow = models.ForeignKey(Flow, on_delete=models.CASCADE)
    status = models.CharField(
        max_length=20, choices=FlowRunStatus.choices, default=FlowRunStatus.IN_PROGRESS
    )
    current_state = models.CharField(max_length=255)
    message_history = models.JSONField(default=list)


class ProviderChoices(models.TextChoices):
    OPENAI = "OpenAI", "OpenAI"
    ANTHROPIC = "Anthropic", "Anthropic"
    OPENROUTER = "OpenRouter", "OpenRouter"


class Provider(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    label = models.CharField(max_length=100)
    type = models.CharField(max_length=50, choices=ProviderChoices.choices)
    workspace = models.ForeignKey(
        "Workspace", null=True, blank=True, on_delete=models.CASCADE
    )
    api_key = encrypt(models.CharField(max_length=255))

    @classmethod
    def get_default_provider(cls):
        default_provider, created = cls.objects.get_or_create(
            type=ProviderChoices.OPENROUTER,
            id=uuid.UUID(int=1),
            workspace=None,
            label="Default Provider",
            defaults={"api_key": os.environ["OPENROUTER_API_KEY"]},
        )

        return default_provider

    def __str__(self):
        return f"{self.label} ({self.type})"


class GenerativeModel(models.Model):
    class PriorityChoices(models.IntegerChoices):
        DEFAULT = 1, "Default"
        STANDARD = 2, "Standard"
        CUSTOM = 3, "Custom"

    label = models.CharField(
        max_length=100, help_text="User-friendly name of the model"
    )
    model_string = models.CharField(
        max_length=255, help_text="Model identifier for API calls"
    )
    priority = models.IntegerField(
        choices=PriorityChoices.choices, default=PriorityChoices.DEFAULT
    )
    provider_type = models.CharField(max_length=50, choices=ProviderChoices.choices)

    def __str__(self):
        return f"{self.label} ({self.get_priority_display()})"
