import { GenerativeModel } from "@/typings/generative_model";
import emptyApi from "../apiSlice";

const GenerativeModelsApiSlice = emptyApi.injectEndpoints({
  endpoints: (builder) => ({
    fetchGenerativeModels: builder.query<GenerativeModel[], void>({
      query: () => ({
        url: "generative-models/",
        method: "GET",
      }),
      providesTags: ["GenerativeModel"],
    }),
  }),
});

export const { useFetchGenerativeModelsQuery } = GenerativeModelsApiSlice;
export default GenerativeModelsApiSlice;
