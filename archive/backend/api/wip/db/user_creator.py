import uuid

from api.models import (
    UserAccount,
    Project,
    Contact,
)
from core.constant import CONSTANTS


user_kwargs = dict(
    email="vlad@tangibleai.com",
    name="Vlad",
    is_active=True,
    is_admin=False,
    project_name="Vlad's Test Project",
    contact_origin_name="Test Contact For Vlad",
)


def get_or_create_user_account(
    email="test@example.com", name="", is_active=True, is_admin=False, **kwargs
):
    user_account, created = UserAccount.objects.get_or_create(
        email=email, name=name, is_active=is_active, is_admin=is_admin
    )
    assert user_account is not None, "Failed to create user account"
    return user_account


def get_or_create_project(project_name="Test Project", **kwargs):
    user_account = get_or_create_user_account(**kwargs)
    project, token_key, created = Project.objects.get_or_create(
        name=project_name, owner=user_account
    )
    assert (
        project is not None and len(token_key) == CONSTANTS.TOKEN_KEY_LENGTH
    ), "Failed to create project"
    return project


def get_or_create_contact(contact_origin_name="Test Contact", **kwargs):
    project = get_or_create_project(**kwargs)
    contact_uuid = uuid.uuid4()
    while Contact.objects.filter(id=contact_uuid):
        contact_uuid = uuid.uuid4()
    contact, created = Contact.objects.get_or_create(
        id=contact_uuid, origin=contact_origin_name, project=project
    )
    assert contact is not None, "Failed to create contact"
    return contact


get_or_create_user_account(**user_kwargs)
get_or_create_project(**user_kwargs)
get_or_create_contact(**user_kwargs)
