from django.core.cache import cache
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from api.models import Chatbot, Contact, GuardrailResponse, Project, Chat


@receiver(post_delete, sender=GuardrailResponse)
def delete_associated_guardrail_action(sender, instance, **kwargs):
    """Deletes guardrail action after the guardrail response is deleted"""
    instance.guardrail_action.delete()


# Cache clearing for Project
@receiver(post_save, sender=Project)
def clear_project_cache_on_update(sender, instance, **kwargs):
    cache_key = f"project_{instance.id}"
    cache.delete(cache_key)


@receiver(post_delete, sender=Project)
def clear_project_cache_on_delete(sender, instance, **kwargs):
    cache_key = f"project_{instance.id}"
    cache.delete(cache_key)


# Cache clearing for Chatbot
@receiver(post_save, sender=Chatbot)
def clear_chatbot_cache_on_update(sender, instance, **kwargs):
    cache_key = f"chatbot_{instance.project.id}_{instance.origin}"
    cache.delete(cache_key)


@receiver(post_delete, sender=Chatbot)
def clear_chatbot_cache_on_delete(sender, instance, **kwargs):
    cache_key = f"chatbot_{instance.project.id}_{instance.origin}"
    cache.delete(cache_key)


# Cache clearing for Contact
@receiver(post_save, sender=Contact)
def clear_contact_cache_on_update(sender, instance, **kwargs):
    """Clear cache when a contact is created or updated."""
    cache_key = f"contact_{instance.origin}"
    cache.delete(cache_key)


@receiver(post_delete, sender=Contact)
def clear_contact_cache_on_delete(sender, instance, **kwargs):
    """Clear cache when a contact is deleted."""
    cache_key = f"contact_{instance.origin}"
    cache.delete(cache_key)


# Cache clearing for Chat
@receiver(post_save, sender=Chat)
def clear_chat_cache_on_update(sender, instance, **kwargs):
    """Clear cache when a chat is created or updated."""
    cache_key = f"chat_{instance.origin_id}"
    cache.delete(cache_key)


@receiver(post_delete, sender=Chat)
def clear_chat_cache_on_delete(sender, instance, **kwargs):
    """Clear cache when a chat is deleted."""
    cache_key = f"chat_{instance.origin_id}"
    cache.delete(cache_key)
