import math
from datetime import datetime
from unittest.mock import patch

from django.core import mail
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIClient

from accounts.models import UserAccount
from api.models import (
    Chat,
    Chatbot,
    Contact,
    GenerativeModel,
    Message,
    Plan,
    Project,
    Provider,
    ProviderChoices,
    ResponseLog,
    Workspace,
)
from api.tests import mock
from api.utils.prompt_configuration import create_chatbot_config_object
from core.constant import CONSTANTS


@override_settings(
    EMAIL_BACKEND="django.core.mail.backends.locmem.EmailBackend",
    CELERY_TASK_ALWAYS_EAGER=True,
    CELERY_TASK_EAGER_PROPAGATES=True,
)
class SubscriptionTests(TestCase):
    fixtures = ["default_account_data.json"]

    def setUp(self):
        super().setUp()
        self.client = APIClient()

        self.user = UserAccount.objects.create_user(
            email="test@example.com", name="Test", password="password"
        )

        self.workspace = Workspace.objects.create(
            name="Test Workspace", owner=self.user
        )

        self.project, self.token = Project.objects.create(
            name="Test Project", workspace=self.workspace
        )

        self.headers = {
            "HTTP_AUTHORIZATION": "Bearer " + self.token,
            "Content-Type": "application/json",
        }

        self.contact = Contact.objects.create(
            project=self.project, **mock.mock["contact1"]
        )
        self.contact_id = self.contact.pk

        self.provider = Provider.objects.create(
            label="Test",
            type=ProviderChoices.OPENROUTER.value,
            workspace=self.workspace,
            api_key="abcd",
        )
        self.generative_model, _ = GenerativeModel.objects.get_or_create(
            label="Test Generative model",
            model_string="openrouter/meta-llama/llama-3-70b-instruct",
            priority=1,
            provider_type=ProviderChoices.OPENROUTER.value,
        )
        self.chatbot = Chatbot.objects.create(
            project=self.project,
            generative_model=self.generative_model,
            config=create_chatbot_config_object(),
            origin="default_assistant",
        )

        self.chat = Chat.objects.create(contact_id=self.contact, **mock.mock["chat1"])

    def test_initial_values(self):
        # Assert chatbot model
        chatbot = Chatbot.objects.get(project=self.project)
        self.assertEqual(chatbot.generative_model, self.generative_model)

        # Assert plan properties free message count is 0
        plan = self.workspace.plan
        self.assertEqual(plan.properties["free_message"], 0)

        # Assert response log count is 0
        logs = ResponseLog.objects.all()
        self.assertEqual(len(logs), 0)

    @patch("api.external.chatbot_responses.engine_response")
    def test_external_subscription_system_message_count(self, mock_engine_response):
        mock_engine_response.return_value = {
            "response_text": "Python is a popular programming language.",
            "source": ResponseLog.Source.GUARDRAILS,
            "context": None,
        }

        url = reverse("chatbot_response_external_view")

        data = {
            "chat": self.chat.origin_id,
            "user_message": "What is python in CS.",
            "assistant": self.chatbot.origin,
            "contact": self.contact.origin,
        }

        response = self.client.post(url, data=data, **self.headers)

        # Assert status code and message
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["chatbot_response"],
            "Python is a popular programming language.",
        )

        # Assert plan properties free message count is 1
        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        self.assertEqual(plan.properties["free_message"], 1)

        # Assert response log count is 1
        logs = ResponseLog.objects.all()
        self.assertEqual(len(logs), 1)

    @patch("api.external.chatbot_responses.engine_response")
    def test_external_subscription_system_running_out_of_credits(
        self, mock_engine_response
    ):
        mock_engine_response.return_value = {
            "response_text": "Python is a popular programming language.",
            "source": ResponseLog.Source.GUARDRAILS,
            "context": None,
        }

        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        plan.properties["free_message"] = (
            math.floor(CONSTANTS.MAX_FREE_MESSAGES * CONSTANTS.WARNING_RATIO) - 1
        )
        plan.save()

        url = reverse("chatbot_response_external_view")

        data = {
            "chat": self.chat.origin_id,
            "user_message": "What is python in CS.",
            "assistant": self.chatbot.origin,
            "contact": self.contact.origin,
        }

        response = self.client.post(url, data=data, **self.headers)

        # Assert status code and message
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["chatbot_response"],
            "Python is a popular programming language.",
        )

        # Assert plan properties free message count is increased by 1
        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        self.assertEqual(
            plan.properties["free_message"],
            math.floor(CONSTANTS.MAX_FREE_MESSAGES * CONSTANTS.WARNING_RATIO),
        )

        # Assert response log count is 1
        logs = ResponseLog.objects.all()
        self.assertEqual(len(logs), 1)

        # Assert the outbox
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(
            "Your workspace is running out of credits.", mail.outbox[0].subject
        )
        self.assertIn(self.user.email, mail.outbox[0].to)

    @patch("api.external.chatbot_responses.engine_response")
    def test_external_subscription_system_is_out_of_credits(self, mock_engine_response):
        mock_engine_response.return_value = {
            "response_text": "Python is a popular programming language.",
            "source": ResponseLog.Source.GUARDRAILS,
            "context": None,
        }

        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        plan.properties["free_message"] = CONSTANTS.MAX_FREE_MESSAGES - 1
        plan.save()

        url = reverse("chatbot_response_external_view")

        data = {
            "chat": self.chat.origin_id,
            "user_message": "What is python in CS.",
            "assistant": self.chatbot.origin,
            "contact": self.contact.origin,
        }

        response = self.client.post(url, data=data, **self.headers)

        # Assert status code and message
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["chatbot_response"],
            "Python is a popular programming language.",
        )

        # Assert plan properties free message count is increased by 1
        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        self.assertEqual(plan.properties["free_message"], CONSTANTS.MAX_FREE_MESSAGES)

        # Assert response log count is 1
        logs = ResponseLog.objects.all()
        self.assertEqual(len(logs), 1)

        # Assert generative model
        chatbot = Chatbot.objects.get(project=self.project)
        self.assertEqual(
            chatbot.generative_model,
            self.generative_model,
        )

        # Assert the outbox
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn("Your workspace is out of Credits", mail.outbox[0].subject)
        self.assertIn(self.user.email, mail.outbox[0].to)

    @patch("api.external.chatbot_responses.engine_response")
    def test_external_subscription_system_paid_account_null_renewal_date(
        self, mock_engine_response
    ):
        mock_engine_response.return_value = {
            "response_text": "Python is a popular programming language.",
            "source": ResponseLog.Source.GUARDRAILS,
            "context": None,
        }

        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        plan.type = Plan.Type.PAID
        plan.save()

        url = reverse("chatbot_response_external_view")

        data = {
            "chat": self.chat.origin_id,
            "user_message": "What is python in CS.",
            "assistant": self.chatbot.origin,
            "contact": self.contact.origin,
        }

        response = self.client.post(url, data=data, **self.headers)

        # Assert status code and message
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["chatbot_response"],
            "Python is a popular programming language.",
        )

        # Assert plan properties free message count is didn't change
        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        self.assertEqual(plan.properties["free_message"], 0)

        # Assert plan status is `expired`
        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        self.assertEqual(plan.status, Plan.Status.EXPIRED)

        # Assert notification for expired plan
        response = self.client.post(url, data=data, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["chatbot_response"],
            "Thank you for using our service. To send more messages, please consider upgrading your plan.",
        )

    @patch("api.external.chatbot_responses.engine_response")
    def test_external_subscription_system_paid_account_expired_renewal_date(
        self, mock_engine_response
    ):
        mock_engine_response.return_value = {
            "response_text": "Python is a popular programming language.",
            "source": ResponseLog.Source.GUARDRAILS,
            "context": None,
        }

        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        plan.type = Plan.Type.PAID

        # Create a timezone-aware for `January 1, 1900` in UTC timezone
        naive_datetime_1900 = datetime(1900, 1, 1)
        plan.renewal_date = timezone.make_aware(
            naive_datetime_1900, timezone=timezone.utc
        )
        plan.save()

        url = reverse("chatbot_response_external_view")

        data = {
            "chat": self.chat.origin_id,
            "user_message": "What is python in CS.",
            "assistant": self.chatbot.origin,
            "contact": self.contact.origin,
        }

        response = self.client.post(url, data=data, **self.headers)

        # Assert status code and message
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["chatbot_response"],
            "Python is a popular programming language.",
        )

        # Assert plan properties free message count is didn't change
        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        self.assertEqual(plan.properties["free_message"], 0)

        # Assert plan status is `expired`
        workspace = Workspace.objects.get(pk=self.workspace.pk)
        plan = workspace.plan
        self.assertEqual(plan.status, Plan.Status.EXPIRED)

        # Assert notification for expired plan
        response = self.client.post(url, data=data, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["chatbot_response"],
            "Thank you for using our service. To send more messages, please consider upgrading your plan.",
        )

    def tearDown(self):
        # Delete in reverse order of dependencies
        ResponseLog.objects.all().delete()
        Message.objects.all().delete()
        Chat.objects.all().delete()
        Contact.objects.all().delete()
        Chatbot.objects.all().delete()
        Project.objects.all().delete()
        Workspace.objects.all().delete()
        UserAccount.objects.all().delete()
