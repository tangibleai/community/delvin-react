from typing import Callable, List, Optional, Union

from llama_index.core.base.embeddings.base import (
    DEFAULT_EMBED_BATCH_SIZE,
    BaseEmbedding,
    Embedding,
)
from llama_index.core.bridge.pydantic import Field
from llama_index.core.callbacks import CallbackManager
import llama_index.core.instrumentation as instrument

# NOTE: The type for Embedding may change in the future because of the TODO below copied from the llama_index repo
# TODO: change to numpy array
Embedding = List[float]

DEFAULT_EMBEDDING_URL = "https://ml-staging.delvin.to/api/v1.0/predictions"


class CustomTextEmbeddingsInference(BaseEmbedding):
    """Facilitates the metadata and embedding generation and retrieval

    Uses Delvin's embedding service

    Replaces llama_index TextEmbeddingInference class
    """

    base_url: str = Field(
        default=DEFAULT_EMBEDDING_URL,
        description="Base URL for the text embeddings service.",
    )
    query_instruction: Optional[str] = Field(
        default="Represent the question for retrieving supporting documents: ",
        description="Instruction to prepend to query text.",
    )
    text_instruction: Optional[str] = Field(
        default="Represent the document for retrieval: ",
        description="Instruction to prepend to text.",
    )
    timeout: float = Field(
        default=60.0,
        description="Timeout in seconds for the request.",
    )
    truncate_text: bool = Field(
        default=True,
        description="Whether to truncate text or not when generating embeddings.",
    )
    auth_token: Optional[Union[str, Callable[[str], str]]] = Field(
        default=None,
        description="Authentication token or authentication token generating function for authenticated requests",
    )

    def __init__(
        self,
        model_name: str,
        base_url: str = DEFAULT_EMBEDDING_URL,
        # Add *_instruction.default to set default str value for instructions
        text_instruction: Optional[str] = None,
        query_instruction: Optional[str] = None,
        embed_batch_size: int = DEFAULT_EMBED_BATCH_SIZE,
        timeout: float = 60.0,
        truncate_text: bool = True,
        callback_manager: Optional[CallbackManager] = None,
        auth_token: Optional[Union[str, Callable[[str], str]]] = None,
    ):
        super().__init__(
            base_url=base_url,
            model_name=model_name,
            text_instruction=text_instruction,
            query_instruction=query_instruction,
            embed_batch_size=embed_batch_size,
            timeout=timeout,
            truncate_text=truncate_text,
            callback_manager=callback_manager,
            auth_token=auth_token,
        )

    @classmethod
    def class_name(cls) -> str:
        return "CustomTextEmbeddingsInference"

    def _call_api(self, texts: List[str]) -> List[List[float]]:
        import httpx

        headers = {"Content-Type": "application/json"}
        if self.auth_token is not None:
            if callable(self.auth_token):
                headers["Authorization"] = self.auth_token(self.base_url)
            else:
                headers["Authorization"] = self.auth_token
        json_data = {"data": {"ndarray": texts}}

        with httpx.Client() as client:
            response = client.post(
                f"{self.base_url}",
                headers=headers,
                json=json_data,
                timeout=self.timeout,
            )
        return response.json()["data"]["ndarray"]

    async def _acall_api(self, texts: List[str]) -> List[List[float]]:
        import httpx

        headers = {"Content-Type": "application/json"}
        if self.auth_token is not None:
            if callable(self.auth_token):
                headers["Authorization"] = self.auth_token(self.base_url)
            else:
                headers["Authorization"] = self.auth_token
        json_data = {"data": {"ndarray": texts}}

        async with httpx.AsyncClient() as client:
            response = client.post(
                f"{self.base_url}",
                headers=headers,
                json=json_data,
                timeout=self.timeout,
            )

        return response.json()["data"]["ndarray"]

    def append_instruction_to_input_text(
        self, text: str, model_name: Optional[str], instruction: Optional[str] = None
    ) -> str:
        """Appends an embedding instruction to the text chunk
            Currently just appends an empty string because instructions aren't set in the class

            Overwritten from:
            from llama_index.embeddings.huggingface.utils import format_query, format_text
            Originally appended English or Mandarin instruction to prompt, or provided an empty string

            TODO: Will adding instruction improve embeddings?
            DEFAULT_EMBED_INSTRUCTION = "Represent the document for retrieval: "
            DEFAULT_QUERY_INSTRUCTION = (
            "Represent the question for retrieving supporting documents: "
        )
        """
        if not instruction:
            instruction = ""

        # NOTE: strip() enables backdoor for defeating instruction prepend by
        # passing empty string

        return f"{instruction} {text}".strip()

    def _get_query_embedding(self, query: str) -> List[float]:
        """Get query embedding."""
        query = self.append_instruction_to_input_text(
            query, self.model_name, self.query_instruction
        )
        return self._call_api([query])[0]

    def _get_text_embedding(self, text: str) -> List[float]:
        """Get text embedding."""
        text = self.append_instruction_to_input_text(
            text, self.model_name, self.text_instruction
        )
        return self._call_api([text])[0]

    def _get_text_embeddings(self, texts: List[str]) -> List[List[float]]:
        """Get text embeddings."""
        texts = [
            self.append_instruction_to_input_text(
                text, self.model_name, self.text_instruction
            )
            for text in texts
        ]
        return self._call_api(texts)

    async def _aget_query_embedding(self, query: str) -> List[float]:
        """Get query embedding async."""
        query = self.append_instruction_to_input_text(
            query, self.model_name, self.query_instruction
        )
        return (await self._acall_api([query]))[0]

    async def _aget_text_embedding(self, text: str) -> List[float]:
        """Get text embedding async."""
        text = self.append_instruction_to_input_text(
            text, self.model_name, self.text_instruction
        )
        return (await self._acall_api([text]))[0]

    async def _aget_text_embeddings(self, texts: List[str]) -> List[Embedding]:
        texts = [
            self.append_instruction_to_input_text(
                text, self.model_name, self.text_instruction
            )
            for text in texts
        ]
        return await self._acall_api(texts)
