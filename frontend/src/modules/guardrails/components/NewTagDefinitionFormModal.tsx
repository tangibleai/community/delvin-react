import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Progress } from "@/components/ui/progress";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Textarea } from "@/components/ui/textarea";
import { useListMessageTagDefinitionsQuery } from "@/slices/api/messageTagDefinition";
import { setResponse } from "@/slices/responseSlice";
import { GuardrailResponse } from "@/typings/guardrail_response";
import { zodResolver } from "@hookform/resolvers/zod";
import { Tab, TabGroup, TabList, TabPanel, TabPanels } from "@tremor/react";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { z } from "zod";

interface Props {
  guardrailResponses: GuardrailResponse[]; // guardrail responses
  tagDefinitionOpen: boolean; // state to open tag definition modal
  guardrailActionOpen: boolean; // state to open guardrail action modal
  setTagDefinitionOpen: (isOpen: boolean) => void; // Function to set tagDefinitionOpen state
  setGuardrailActionOpen: (isOpen: boolean) => void; // Function to set guardrailActionOpen state
}

export default function NewTagDefinition(props: Props) {
  const routeParams = useParams();
  const projectId: any = routeParams.projectId;
  const dispatch = useDispatch();

  const { data: tags = [] } = useListMessageTagDefinitionsQuery(projectId);

  const preprocessExamples = (input: any) => {
    if (typeof input == "string") {
      return input
        .split("\n")
        .map((str) => str.trim())
        .filter((str) => str.length > 0);
    }
    return input;
  };

  const preprocessTagName = (input: any) => {
    const messageTagDefinitionNames = props.guardrailResponses.map((item) =>
      item.message_tag_definition.name.toLocaleLowerCase()
    );
    if (messageTagDefinitionNames.includes(input.toLowerCase())) {
      return "";
    }
    return input;
  };

  const FormSchema1 = z.object({
    tagDescription: z.string().min(2, {
      message: "Please write a user message.",
    }),
    examples: z.preprocess(
      preprocessExamples,
      z
        .array(
          z.string().min(1, {
            message: "Each example must be at least 1 character long.",
          })
        )
        .min(5, { message: "Please, provide at least 5 examples." })
    ),
    tagName: z.preprocess(
      preprocessTagName,
      z.string().min(2, {
        message: "Please write a message tag that is unique.",
      })
    ),
  });

  const FormSchema2 = z.object({
    tagID: z.string().min(2, {
      message: "Please select a message tag.",
    }),
  });

  const form1 = useForm<z.infer<typeof FormSchema1>>({
    resolver: zodResolver(FormSchema1),
    defaultValues: {
      tagDescription: "",
      examples: [],
      tagName: "",
    },
  });

  const form2 = useForm<z.infer<typeof FormSchema2>>({
    resolver: zodResolver(FormSchema2),
    defaultValues: {
      tagID: "",
    },
  });

  function onSubmit1(values: z.infer<typeof FormSchema1>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    const response = {
      tagDescription: values.tagDescription,
      examples: values.examples,
      tagName: values.tagName,
      tagID: "",
    };
    dispatch(setResponse(response));
    props.setTagDefinitionOpen(false);
    props.setGuardrailActionOpen(true);
  }

  function onSubmit2(values: z.infer<typeof FormSchema2>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    const response = {
      tagDescription: "",
      examples: [""],
      tagName: "",
      tagID: values.tagID,
    };
    dispatch(setResponse(response));
    props.setTagDefinitionOpen(false);
    props.setGuardrailActionOpen(true);
  }

  function handleOpenChange(e: any) {
    // Handles the modal visibility if user click cross or out of modal
    props.setTagDefinitionOpen(false);
  }

  function resetForms() {
    form1.setValue("tagDescription", "");
    form1.setValue("examples", [""]);
    form1.setValue("tagName", "");
    form2.setValue("tagID", "");
  }

  function handleCancel(e: any) {
    props.setTagDefinitionOpen(false);
  }

  useEffect(() => {
    // Fetches message tag definitions depending on modal is visible or not
    resetForms();
  }, [props.tagDefinitionOpen]);

  const guardrailResponsesIds = props.guardrailResponses.map(
    (item) => item.message_tag_definition.id
  );

  const filteredTagDefinitions = tags.filter(
    (item) => !guardrailResponsesIds.includes(item.id)
  );

  return (
    <Dialog
      open={props.tagDefinitionOpen}
      onOpenChange={props.setTagDefinitionOpen}
    >
      <DialogContent className="sm:max-w-[900px]">
        <DialogHeader>
          <DialogTitle>Create new response</DialogTitle>
          <div className="flex flex-col items-center">
            <Progress value={50} className="w-[60%] my-4" />
            <h3>Step 1: Define a trigger</h3>
          </div>
        </DialogHeader>
        <TabGroup className="max-w-[800px] min-h-[540px]">
          <TabList>
            <Tab>New tag</Tab>
            <Tab>Existing tag</Tab>
          </TabList>
          <TabPanels>
            <TabPanel className="text-left ">
              <Form {...form1}>
                <form
                  onSubmit={form1.handleSubmit(onSubmit1)}
                  className="space-y-6"
                >
                  <div className="flex flex-col">
                    <FormField
                      control={form1.control}
                      name="tagDescription"
                      render={({ field }) => (
                        <FormItem>
                          <div className="flex flex-row p-2">
                            <div className="flex flex-col w-1/4 p-2">
                              <FormLabel className="">When ...</FormLabel>
                              <FormDescription>
                                Describe the user's message.
                              </FormDescription>
                            </div>
                            <div className="flex flex-col w-3/4 p-2">
                              <FormControl>
                                <Input
                                  placeholder="User greets the bot"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </div>
                          </div>
                        </FormItem>
                      )}
                    />
                    <FormField
                      control={form1.control}
                      name="examples"
                      render={({ field }) => (
                        <FormItem>
                          <div className="flex flex-row p-2">
                            <div className="flex flex-col w-1/4 p-2">
                              <FormLabel>Examples</FormLabel>
                              <FormDescription>
                                Add 5-7 examples of user inputs, seperated by a
                                new line.
                              </FormDescription>
                            </div>
                            <div className="flex flex-col w-3/4 p-2">
                              <FormControl>
                                <Textarea
                                  placeholder="hi&#10;hey&#10;hello&#10;howdy&#10;sup"
                                  className="resize-none"
                                  {...field}
                                />
                              </FormControl>
                              <FormMessage />
                            </div>
                          </div>
                        </FormItem>
                      )}
                    />
                    <FormField
                      control={form1.control}
                      name="tagName"
                      render={({ field }) => (
                        <FormItem>
                          <div className="flex flex-row p-2">
                            <div className="flex flex-col w-1/4 p-2">
                              <FormLabel>Apply tag</FormLabel>
                              <FormDescription>
                                Give short name to refer to user's intent
                              </FormDescription>
                            </div>
                            <div className="flex flex-col w-3/4 p-2">
                              <FormControl>
                                <Input placeholder="greeting" {...field} />
                              </FormControl>
                              <FormMessage />
                            </div>
                          </div>
                        </FormItem>
                      )}
                    />
                    <div className="flex flex-row justify-evenly">
                      <Button
                        type="button"
                        variant="outline"
                        onClick={handleCancel}
                        className="w-32"
                      >
                        Cancel
                      </Button>
                      <Button type="submit" className="w-32">
                        Next
                      </Button>
                    </div>
                  </div>
                </form>
              </Form>
            </TabPanel>
            <TabPanel>
              <Form {...form2}>
                <form
                  onSubmit={form2.handleSubmit(onSubmit2)}
                  className="space-y-6"
                >
                  <div className="flex flex-col">
                    <FormField
                      control={form2.control}
                      name="tagID"
                      render={({ field }) => (
                        <FormItem>
                          <div className="flex flex-row p-2">
                            <div className="flex flex-col w-1/4 p-2">
                              <FormLabel className="">Apply tag</FormLabel>
                              <FormDescription>
                                Choose from the list of existing tags
                              </FormDescription>
                            </div>
                            <div className="flex flex-col w-3/4 p-2">
                              <Select
                                onValueChange={field.onChange}
                                defaultValue={field.value}
                              >
                                <FormControl>
                                  <SelectTrigger>
                                    <SelectValue placeholder="Select action type" />
                                  </SelectTrigger>
                                </FormControl>
                                <SelectContent>
                                  {filteredTagDefinitions.map((tag) => (
                                    <SelectItem key={tag.id} value={tag.id}>
                                      {tag.name}
                                    </SelectItem>
                                  ))}
                                </SelectContent>
                              </Select>
                              <FormMessage />
                            </div>
                          </div>
                        </FormItem>
                      )}
                    />
                    <div className="flex flex-row justify-evenly">
                      <Button
                        type="button"
                        variant="outline"
                        onClick={handleCancel}
                        className="w-32"
                      >
                        Cancel
                      </Button>
                      <Button type="submit" className="w-32">
                        Next
                      </Button>
                    </div>
                  </div>
                </form>
              </Form>
            </TabPanel>
          </TabPanels>
        </TabGroup>
      </DialogContent>
    </Dialog>
  );
}
