import { useEffect, useState, useRef } from "react";
import { ScrollArea } from "@/components/ui/scroll-area";
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import React from "react";
import { ChatInput } from "./ChatInput";

import {
  getViewportHeight,
  getScrollArea,
} from "../../../modules/chatbot_config/utils/chat_container";
import { getResponsefromLLM } from "../../../modules/chatbot_config/actions/chat_container";
import moment from "moment";
import { Message, TypingIndicator } from "@chatscope/chat-ui-kit-react";
import { Message as MessageType, SenderTypes } from "@/typings/message";
import { ChatbotConfig } from "@/typings/chatbot_config";
import { Skeleton } from "@/components/ui/skeleton";
import { useGetMessagesQuery } from "@/slices/api/messageApiSlice";

type Props = {
  projectId: string;
  selectedChatbot: ChatbotConfig;
  chatOrigin: string | undefined;
  contactOrigin: string | undefined;
};

function ChatContainer({
  projectId,
  selectedChatbot,
  chatOrigin,
  contactOrigin,
}: Props) {
  const [isAwaitingLLMResponse, setIsAwaitingLLMResponse] = useState(false);
  const [userPrompt, setUserPrompt] = useState("");
  const [messages, setMessages] = useState<MessageType[]>([]);
  const [fillerHeight, setFillerHeight] = useState(0); // height for filler element
  const scrollContainerRef = useRef<HTMLDivElement>(null);
  const scrollToBottom = useRef(false); // do the scroll to bottom?

  const { data: loadedMessages = [], isFetching } = useGetMessagesQuery(
    {
      chatOrigin,
      projectId,
    },
    { skip: !chatOrigin || !contactOrigin, refetchOnMountOrArgChange: true }
  );

  useEffect(() => {
    if (!loadedMessages.length) return;

    setMessages(loadedMessages);
  }, [loadedMessages]);

  // initially loads messages and scrolls to the latest
  useEffect(() => {
    if (!chatOrigin || !contactOrigin) return;

    scrollToBottom.current = true; // trigger to scroll to the bottom when messages are loaded
  }, [chatOrigin, contactOrigin]);

  // recalculates filler height and scrolls to the bottom if needed when messages array is updated
  useEffect(() => {
    recalculateFillerElementHeight();

    if (scrollToBottom.current && messages.length > 0) {
      scrollToTheBottom();
      scrollToBottom.current = false;
    }
  }, [messages]);

  const recalculateFillerElementHeight = () => {
    const viewportHeight = getViewportHeight(scrollContainerRef) || 0;

    const scrollArea = getScrollArea(scrollContainerRef);
    if (scrollArea) {
      const scrollAreaVisibleHeight = scrollArea.clientHeight;

      let newFillerHeight = 0;
      if (viewportHeight == 0) {
        newFillerHeight = scrollAreaVisibleHeight;
      } else if (viewportHeight - fillerHeight < scrollAreaVisibleHeight) {
        newFillerHeight =
          scrollAreaVisibleHeight - (viewportHeight - fillerHeight);
      }
      setFillerHeight(newFillerHeight);
    }
  };

  const scrollToTheBottom = () => {
    // height occupied by chat container content
    const viewportHeight = getViewportHeight(scrollContainerRef) || 0;

    const scrollArea = getScrollArea(scrollContainerRef);
    if (scrollArea) {
      // scrolling down to the latest message
      scrollArea.scrollTop = viewportHeight;
    }
  };

  const handleSubmittedPrompt = async (
    e:
      | React.KeyboardEvent<HTMLInputElement>
      | React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    if (e && "key" in e && e.key !== "Enter") return;

    const scrollArea = getScrollArea(scrollContainerRef);
    if (!scrollArea) return;

    let messageTimestamp = new Date().toString();
    const message: MessageType = {
      id: messageTimestamp,
      text: userPrompt,
      direction: "inbound",
      timestamp: messageTimestamp,
      sender_type: SenderTypes.USER,
    };

    setMessages((prevMessages) => {
      const newMessages = [...prevMessages, message];
      const distanceFromBottom =
        scrollArea.scrollHeight -
        scrollArea.scrollTop -
        scrollArea.clientHeight;
      if (distanceFromBottom === 0) scrollToBottom.current = true;
      return newMessages;
    });
    setIsAwaitingLLMResponse(true);

    const LLMResponse = await getResponsefromLLM(
      userPrompt,
      chatOrigin!,
      projectId,
      selectedChatbot.origin,
      contactOrigin!
    );
    messageTimestamp = new Date().toString();
    const LLMmessage: MessageType = {
      id: messageTimestamp,
      text: LLMResponse,
      direction: "outbound",
      timestamp: messageTimestamp,
      sender_type: SenderTypes.BOT,
    };

    setMessages((prevMessages) => {
      const newMessages = [...prevMessages, LLMmessage];
      const distanceFromBottom =
        scrollArea.scrollHeight -
        scrollArea.scrollTop -
        scrollArea.clientHeight;
      if (distanceFromBottom === 0) scrollToBottom.current = true;
      return newMessages;
    });
    setIsAwaitingLLMResponse(false);
    setUserPrompt("");
  };

  return (
    <div className="flex flex-col h-full items-end w-full flex-grow overflow-y-auto">
      <ScrollArea
        ref={scrollContainerRef}
        key={"scroll area"}
        className="flex flex-col justify-end h-full flex-grow py-2.5 px-5 w-full rounded-r-2xl"
      >
        {/* Filler div */}
        <div style={{ height: `${fillerHeight}px` }} key={"filler"}></div>{" "}
        {isFetching
          ? Array.from({ length: 5 }).map((_, index) => (
              <>
                <div
                  className={`w-full flex ${
                    (index % 2) - 1 ? "justify-end" : ""
                  }`}
                >
                  <Skeleton key={index} className="h-10 w-32 bg-gray-300" />
                </div>
              </>
            ))
          : Array.isArray(messages) &&
            messages.map((message) => {
              return (
                <div key={`${message.id}${message.timestamp}`}>
                  <Message
                    model={{
                      message: message.text,
                      direction:
                        message.direction == "outbound"
                          ? "incoming"
                          : "outgoing",
                      position: "normal",
                    }}
                    key={message.id}
                  >
                    <Message.Footer
                      sentTime={moment(message.timestamp).format("HH:mm")}
                    />
                  </Message>
                </div>
              );
            })}
        {isAwaitingLLMResponse && <TypingIndicator key={"typing indicator"} />}
      </ScrollArea>
      <ChatInput
        className="px-8 pb-3 gap-1"
        userPrompt={userPrompt}
        setUserPrompt={setUserPrompt}
        handleSubmittedPrompt={handleSubmittedPrompt}
        isDisabled={isAwaitingLLMResponse || isFetching}
      />
    </div>
  );
}

export default ChatContainer;
