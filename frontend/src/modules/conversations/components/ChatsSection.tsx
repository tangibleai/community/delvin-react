import { Link, useParams } from "react-router-dom";

import moment from "moment";
import {
  Table,
  TableHead,
  TableRow,
  TableHeaderCell,
  TableBody,
  TableCell,
} from "@tremor/react";

import { ChatDetails } from "@/typings/chat_details";
import { useGetChatsQuery } from "@/slices/api/chatApiSlice";
import { Skeleton } from "@/components/ui/skeleton";

type Props = {
  internal?: boolean;
};

function ChatsSection({ internal = true }: Props) {
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  const { data: chats = [] as ChatDetails[], isFetching: retrievingChats } =
    useGetChatsQuery({
      project_id: projectId,
      internal,
    });

  return (
    <div className="w-full mx-auto">
      <Table>
        <TableHead>
          <TableRow className="text-lg">
            <TableHeaderCell>Chat</TableHeaderCell>
            <TableHeaderCell>Last message</TableHeaderCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {retrievingChats
            ? Array.from({ length: 5 }).map((_, index) => (
                <TableRow className="hover:bg-slate-100 text-lg" key={index}>
                  <TableCell>
                    <Skeleton key={index} className="h-10 w-1/2" />
                  </TableCell>
                  <TableCell>
                    <Skeleton key={index} className="h-10 w-1/2" />
                  </TableCell>
                </TableRow>
              ))
            : chats.map((chat) => {
                const lastMessage = chat.latest_message;

                const messageSender = lastMessage?.sender_type;
                let capitalizedMessageSender = "";
                if (messageSender)
                  capitalizedMessageSender =
                    messageSender!.charAt(0).toUpperCase() +
                    messageSender!.slice(1);

                const charsLimit = 35;
                let messageText = lastMessage?.text;
                const messageIsCut =
                  messageText && messageText.length > charsLimit;
                if (messageIsCut) {
                  messageText = messageText!.slice(0, charsLimit);
                }

                return (
                  <TableRow
                    className="hover:bg-slate-100 text-lg"
                    key={chat.id}
                  >
                    <TableCell>
                      <Link to={`/project/${projectId}/chat/${chat.id}`}>
                        <div className="w-full">
                          <p>
                            {chat.contact_id.origin} / {chat.origin_id}
                          </p>
                          {lastMessage ? (
                            <>
                              <span className="pl-5 text-sm">
                                {capitalizedMessageSender}: {messageText}
                              </span>
                              <span className="text-2xl">
                                {messageIsCut ? "..." : ""}
                              </span>
                            </>
                          ) : (
                            <></>
                          )}
                        </div>
                      </Link>
                    </TableCell>

                    <TableCell>
                      {chat.latest_message?.timestamp
                        ? moment(chat.latest_message.timestamp).fromNow()
                        : "No messages"}
                    </TableCell>
                  </TableRow>
                );
              })}
        </TableBody>
      </Table>
    </div>
  );
}

export default ChatsSection;
