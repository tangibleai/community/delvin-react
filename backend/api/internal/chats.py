from datetime import datetime

import pytz
from django.db.models import Max, Subquery, OuterRef, Prefetch
from rest_framework import serializers, generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.pagination import PageNumberPagination

from api.internal.messages import MessageSerializer
from api.internal.contacts import ContactDetailSerializer
from api.models import Chat, Contact, Message, Project
from api.services.rag.chat_knowledge_manager import KnowledgeDocumentQueryResponder


class ChatListRequestSerializer(serializers.Serializer):
    project_id = serializers.UUIDField(required=False)
    internal = serializers.BooleanField(required=False)
    contact_id = serializers.UUIDField(required=False)


class ChatDetailsResponseSerializer(serializers.ModelSerializer):
    latest_message = serializers.SerializerMethodField()
    contact_id = ContactDetailSerializer(read_only=True)

    class Meta:
        model = Chat
        fields = [
            "id",
            "contact_id",
            "title",
            "is_default",
            "origin_id",
            "latest_message",
        ]

    def get_latest_message(self, obj):
        time_ordered_messages = getattr(
            obj, "time_ordered_messages", Message.objects.none()
        )
        latest_message = time_ordered_messages[0] if time_ordered_messages else None
        if latest_message:
            return MessageSerializer(latest_message).data
        return None


class ChatsListView(generics.ListAPIView):
    serializer_class = ChatDetailsResponseSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        serializer = ChatListRequestSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        contact_id = validated_data.get("contact_id")
        queryset = Chat.objects.filter(contact_id__id=contact_id).order_by(
            "-created_at"
        )

        if queryset.exists():
            return queryset

        project_id = validated_data.get("project_id")
        internal = validated_data.get("internal", False)

        # Read the 'ascending' query parameter with a default value of 'false'
        ascending = (
            self.request.query_params.get("ascending", "false").lower() == "true"
        )
        # Set the order direction based on the 'ascending' parameter
        order = "" if ascending else "-"

        time_ordered_messages = Prefetch(
            "message",
            queryset=Message.objects.order_by("-timestamp")[:1],
            to_attr="time_ordered_messages",
        )

        queryset = (
            Chat.objects.annotate(latest_message_date=Max("message__timestamp"))
            .select_related("contact_id")
            .filter(contact_id__project=project_id, contact_id__internal=internal)
            .order_by(f"{order}latest_message_date")
            .prefetch_related(time_ordered_messages)
        )

        # Return the ordered queryset
        return queryset


chats_list_view = ChatsListView.as_view()


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = "__all__"


class ContactChatsListView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ChatSerializer

    def get_queryset(self):
        contact_id = self.request.query_params.get("contact_id")
        return Chat.objects.filter(contact_id=contact_id)


contact_chats_view = ContactChatsListView.as_view()


class ChatMessagesListView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = MessageSerializer

    def get_queryset(self):
        chat_id = self.request.query_params.get("chat_id")
        try:
            return Message.objects.filter(chat_id=chat_id)
        except ValidationError:
            return Message.objects.none()


chat_messages_view = ChatMessagesListView.as_view()


class AnswerUserQueryAPIView(APIView):
    """Searches the ChatbotKnowledge documents for information similar to the user's query"""

    def post(self, request, *args, **kwargs):
        data = request.data.copy()

        project_id = data.get("project_id", "")
        query = data.get("query", "")

        chatbot_knowledge_manager = KnowledgeDocumentQueryResponder(project_id)
        answer, confidence_score = chatbot_knowledge_manager.answer_user_query(query)

        if str(answer) == "Empty Response" or not answer:
            return Response({"answer", ""}, status=status.HTTP_204_NO_CONTENT)
        return Response(
            {"answer": str(answer), "confidence_score": confidence_score},
            status=status.HTTP_200_OK,
        )


answer_user_query_view = AnswerUserQueryAPIView.as_view()


class HistoricalMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ["timestamp", "text", "direction"]


class ChatMessageListView(generics.ListAPIView):
    serializer_class = HistoricalMessageSerializer

    def get_queryset(self):
        chat_id = self.kwargs["chat_id"]
        try:
            chat = Chat.objects.get(pk=chat_id)
        except Chat.DoesNotExist:
            return (
                Message.objects.none()
            )  # Return an empty queryset if chat does not exist

        return Message.objects.filter(chat=chat).order_by("-timestamp")

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        paginate = request.query_params.get("paginate", "true").lower() == "true"

        if paginate:
            offset = int(request.query_params.get("offset", 0))
            limit = int(request.query_params.get("limit", 20))
            end = min(offset + limit, len(queryset))
            paginated_queryset = queryset[offset:end]
            serializer = self.get_serializer(paginated_queryset, many=True)

            # Constructing paginated response
            pagination_data = {"results": serializer.data}
            return Response(pagination_data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


chat_message_list_view = ChatMessageListView.as_view()


class CreateChatRequestSerializer(serializers.Serializer):
    contact_id = serializers.UUIDField(required=True)
    title = serializers.CharField(required=False)
    origin = serializers.CharField(required=False)
    is_default = serializers.BooleanField(required=False)


class CreateChatView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CreateChatRequestSerializer

    def post(self, request, *args, **kwargs):
        data = request.data

        serializer = self.serializer_class(data=data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        validated_data = serializer.validated_data
        contact_id = validated_data.get("contact_id")
        title = validated_data.get("title")
        origin = validated_data.get("origin")
        is_default = validated_data.get("is_default", False)

        contact = Contact.objects.get(id=contact_id)

        init_params = {
            "contact_id": contact,
            "is_default": is_default,
            "title": title,
            "origin_id": origin,
        }

        if not all((title, origin)):
            contact = Contact.objects.get(id=contact_id)
            max_chat_seq_num = str(Chat.objects.filter(contact_id=contact).count() + 1)
            init_params["title"] = max_chat_seq_num
            init_params["origin_id"] = max_chat_seq_num

        new_chat = Chat.objects.create(**init_params)

        serializer = ChatDetailsResponseSerializer(new_chat)

        return Response(serializer.data)


create_chat_view = CreateChatView.as_view()


class ChatDetailsView(generics.RetrieveAPIView):
    """Returns the detail of the Chat with a given ID"""

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ChatDetailsResponseSerializer
    queryset = Chat.objects.all()
    lookup_field = "id"

    def get(self, request, *args, **kwargs):
        data = request.query_params

        serializer = self.serializer_class(data=data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        contact_id = serializer.validated_data
        chat = Chat.objects.filter(contact_id__id=contact_id).first()

        response_serializer = ChatDetailsResponseSerializer(chat)

        return Response(response_serializer.data, status=status.HTTP_200_OK)


get_chat_details_view = ChatDetailsView.as_view()
