from django.urls import path

from api.external import chatbot_responses

urlpatterns = [
    path(
        "project/chatbot_response/",
        chatbot_responses.get_chatbot_response,
        name="chatbot_response_external_view"),
    path("v2/chatbot_response/", chatbot_responses.get_chatbot_response),
]
