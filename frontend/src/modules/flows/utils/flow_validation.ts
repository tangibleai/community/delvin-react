import { Node, Edge } from "reactflow";
import { nodeNameToTypeMapping } from "@/lib/utils/flow";
import { ConditionTypes } from "../typing/condition";

const checkFlowErrors = (nodes: Node[], edges: Edge[]) => {
  const error =
    checkStartNodeConnected(nodes, edges) ||
    checkStartNodeConnectedToAllowedNode(nodes, edges) ||
    checkEmptyPromptNodes(nodes) ||
    checkSameNameNodes(nodes) ||
    checkConditionBlockErrors(nodes, edges);

  return error || { errorMessage: "", erroneousNodes: [] };
};

const generateError = (message: string, nodes: Node[]) => ({
  errorMessage: message,
  erroneousNodes: nodes,
});

const checkStartNodeConnected = (nodes: Node[], edges: Edge[]) => {
  const startNode = figureStartNode(nodes);
  const startNodeId = startNode?.id;
  const isConnected = edges.some((edge) => edge.source === startNodeId);

  if (!isConnected) {
    return generateError("Please, connect start node. Fix and try again.", [
      startNode!,
    ]);
  }
  return null;
};

const checkStartNodeConnectedToAllowedNode = (nodes: Node[], edges: Edge[]) => {
  const startNode = figureStartNode(nodes);
  const startNodeId = startNode?.id;
  const edgeFromStartNode = edges.find((edge) => edge.source === startNodeId)!;

  const targetNode = nodes.find(
    (node) => node.id === edgeFromStartNode.target
  )!;

  if (targetNode?.type !== nodeNameToTypeMapping.PROMPT_NODE) {
    return generateError("Start node must only be connected to message node.", [
      targetNode!,
    ]);
  }
  return null;
};

// helper
const figureStartNode = (nodes: Node[]) => {
  const startNode = nodes.find((node) => node.data.nodeName === "start");

  return startNode;
};

const checkEmptyPromptNodes = (nodes: Node[]) => {
  const emptyPromptNodes = nodes.filter(
    (node) =>
      node.type === nodeNameToTypeMapping.PROMPT_NODE &&
      !node.data?.nodePrompt?.trim()
  );

  if (emptyPromptNodes.length) {
    return generateError(
      "Node contains empty prompt. Fix and try again.",
      emptyPromptNodes
    );
  }
  return null;
};

const checkSameNameNodes = (nodes: Node[]) => {
  const nodesWithoutSingleConditions = nodes.filter(
    (node) => node.type !== nodeNameToTypeMapping.SINGLE_CONDITION
  );
  // Group nodes by `data.nodeName`
  const nodesByName: Record<string, Node[]> =
    nodesWithoutSingleConditions.reduce(
      (acc, node) => {
        const name = node.data?.nodeName || "";
        if (!acc[name]) {
          acc[name] = [];
        }
        acc[name].push(node);
        return acc;
      },
      {} as Record<string, Node[]>
    );

  // Find nodes with duplicate names
  const duplicateNameNodes = Object.values(nodesByName).filter(
    (group) => group.length > 1
  );

  if (duplicateNameNodes.length) {
    return generateError(
      "Nodes with same names detected. Each node should have unique name. Fix and try again.",
      duplicateNameNodes[0]
    );
  }
  return null;
};

const checkConditionBlockErrors = (nodes: Node[], edges: Edge[]) => {
  const conditionBlockNodes = nodes.filter(
    (node) => node.type === nodeNameToTypeMapping.CONDITION_BLOCK
  );

  for (const conditionBlockNode of conditionBlockNodes) {
    const singleConditionBlocks = nodes.filter(
      (node) => node.parentId === conditionBlockNode.id
    );

    if (singleConditionBlocks.length < 2) {
      return generateError(
        "Condition block node must have at least 2 single conditions.",
        [conditionBlockNode]
      );
    }

    for (const singleCond of singleConditionBlocks) {
      const isConnected = edges.some((edge) => edge.source === singleCond.id);
      if (!isConnected) {
        return generateError("All single conditions must be connected.", [
          conditionBlockNode,
        ]);
      }

      const conditionType = singleCond.data.condition.type;
      if (conditionType == ConditionTypes.ELSE) continue;

      const conditionTag = singleCond.data.condition.properties.tag_name;
      if (!conditionTag) {
        return generateError("All single conditions must be set.", [
          conditionBlockNode,
        ]);
      }
    }
  }
  return null;
};

export { checkFlowErrors };
