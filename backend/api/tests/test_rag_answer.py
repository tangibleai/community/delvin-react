# import json
# import os
# from unittest.mock import patch

# from django.conf import settings
# from django.test import TestCase
# from django.test.testcases import SerializeMixin
# from rest_framework.test import APIClient

# import api.tasks
# from api.models import *
# from api.etl.runner import create_records

# mock = {
#     "uuid": "d3f6e231-4f0c-4e77-995e-d82c897347ae",
#     "origin": "f0b619c4-467f-47fd-9f8e-9baa429e7e27",
#     "chat": "04603472-28e8-45b7-b29d-7e62190635fb",
#     "user1": {
#         "email": "user1@example.com",
#         "name": "user1",
#         "password": "Pwd5544**",
#         "project_name": "Project1",
#         "message_tag_group_name": "MessageTagGroup1",
#         "message_tag_definition_name": "MessageTagDefinition1",
#     },
#     "document1": {
#         "title": "My First Document",
#         "content": "This is my first document.",
#     },
# }


# def filter_by_fields(input_dict, filter_fields):
#     filtered_data = {
#         key: value for key, value in input_dict.items() if key in filter_fields
#     }
#     return filtered_data


# class TestDocumentViews(TestCase, SerializeMixin):
#     """Test the API views.
#     To test in order using the SerializeMixin
#     """

#     lockfile = os.path.join(settings.PROJECT_ROOT, "lockfile.txt")
#     client = APIClient()

#     def setUp(self):
#         # Create dummy company account records
#         create_records()

#         # Set up user account for authentication
#         user = filter_by_fields(mock["user1"], ["email", "name", "password"])
#         UserAccount.objects.create_user(**user)
#         response = self.client.post("/auth/jwt/create/", user)
#         self.user1_access_token = response.json()["access"]
#         self.headers = {"Authorization": "JWT " + self.user1_access_token}

#         # Create User1's Project
#         response = self.send_put_or_post_request(
#             "/api/project/create/",
#             {"name": mock["user1"]["project_name"]},
#         )
#         self.project1_id = response["id"]
#         self.project = Project.objects.get(id=self.project1_id)
#         self.project1_token = response["token"]
#         self.project1_owner = response["owner"]

#         # Create an initial document within the Project
#         self.create_document_response = self.send_put_or_post_request(
#             "/api/project/document/create/",
#             {
#                 "project": self.project1_id,
#                 "title": mock["document1"]["title"],
#                 "content": mock["document1"]["content"],
#             },
#         )

#     def send_put_or_post_request(self, endpoint, payload, method="post"):
#         request_data = {
#             "path": endpoint,
#             "data": json.dumps(payload),
#             "headers": self.headers,
#             "content_type": "application/json",
#         }

#         if method == "put":
#             response = self.client.put(**request_data).json()
#         else:
#             response = self.client.post(**request_data).json()
#         return response

#     def test_edit_document(self):
#         response = self.send_put_or_post_request(
#             "/api/project/knowledge_base/query",
#             {"project_id": self.project1_id, "query": "What document is this?"},
#         )

#         assert response["answer"] is not None
#         assert response["confidence_score"] is not None

#     def tearDown(self):
#         Document.objects.all().delete()
#         UserAccount.objects.all().delete()
